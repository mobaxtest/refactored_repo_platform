/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.iprogames.platform.admin.common.ReconcileRequest;
import com.iprogames.platform.admin.common.TerminateSessionRequest;
import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.Credential;
import com.iprogames.platform.authentication.common.Gps;
import com.iprogames.platform.authentication.common.LobbyData;
import com.iprogames.platform.authentication.common.enums.OperatorEnum;
import com.iprogames.platform.authentication.common.enums.SkinEnum;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.authentication.dao.UserSessionDaoHandler;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.chippurchase.dao.ChipPurchaseDaoHandler;
import com.iprogames.platform.error.ErrorResponse;
import com.iprogames.test.util.TestUtil;
import com.iprogames.util.uuid.UUIDUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
(locations={"file:src/main/webapp/WEB-INF/rest-servlet.xml"}
)

@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthenticationControllerTest {

	Logger logger = LogManager.getLogger(AuthenticationControllerTest.class);
	
	private static String accessToken = UUIDUtil.generateUUIDString();
	private static String latitude = /*"13.0900";*/"40.47";
	private static String longitude = /*"80.2700";*/"73.58";
	private static String latitude0 ="0.000000";// "40.47";
	private static String longitude0 = "0.000000";//"73.58";
	private static String clientIdentity = "MOB-";
	private static String providerauthtoken = null;
	private String pid = "MOCK", sid = "MOCK";
	private static String ipaddress = "127.0.0.1";	
	
	private static String guestProvider = OperatorEnum.GUST.name();
	private static String guestSkin = SkinEnum.GUST.name();
	private static String udid = UUIDUtil.generateUUID().toString();
	
	private static String iproUserName = UUID.randomUUID().toString().substring(0, 5);
	private static String iproPassword = "12345678";
		
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@SuppressWarnings("rawtypes")
	@Autowired
	UserSessionDaoHandler userSessionDao;
	
	@Autowired
	AuthenticationService service;

	@SuppressWarnings("rawtypes")
	@Autowired
	ChipPurchaseDaoHandler chippurchasedao;

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		try {
			ipaddress = Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	*/
	@Test
	public void TestCase_00_VerifyLocation() throws Exception {
		logger.info("Verify location");
		AuthRequest request = new AuthRequest();		
		request.setClientIdentity(clientIdentity);			
		MvcResult result = mockMvc.perform(post("/auth/verifylocation").with(new RequestPostProcessor() { 
		    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
		        request.setRemoteAddr("127.0.0.1"); 
		        return request;
		     }}).contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getUserAccessMode());
		logger.info("Response content : " + result.getResponse().getContentAsString());
		logger.debug("Verify Location - PASS");
	}
	
	@Test
	public void TestCase_0000_VerifyLocation() throws Exception {
		logger.debug("Verify location");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verifylocation")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		logger.debug("Verify Location - PASS");
	}
	
	@Test
	public void TestCase_0001_VerifyLocationForSocial() throws Exception {
		logger.debug("Verify location for social");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verifylocation")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		logger.debug("Verify Location - PASS");
	}
	
	@Test
	public void TestCase_0002_VerifyLocationForReal() throws Exception {
		logger.debug("Verify location for real");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifylocation")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		logger.debug("Verify Location - PASS");
	}
	
	@Test
	public void TestCase_0003_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with legal jurisdiction");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		//assertEquals("RMG", response.getMessage().getRef_id());
		assertEquals("SLG", response.getUserAccessMode().getMode());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_0004_ValidateProviderTokenFromClient() throws Exception {
		logger.debug("Verify valid provider token from client");
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		request.setiProToken(providerauthtoken);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifyiprotoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Verify valid provider token from client - PASS");		
	}
	
	@Test
	public void TestCase_0005_VerifyLocation() throws Exception {
		logger.debug("Verify location");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		mockMvc.perform(post("/auth/verifylocation")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk());
		
		logger.debug("Verify Location - PASS");
	}
	
	@Test
	public void TestCase_0006_ValidateSocialToken2() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}
	
	
	
	@Test
	public void TestCase_0009_Logout() throws Exception {
		logger.debug("Logout");
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		mockMvc.perform(post("/auth/logout")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk());		
		
		logger.debug("Logout - PASS");
	}
	
	@Test
	public void TestCase_0010_LoginAsGuest3() throws Exception {
		logger.debug("Login as guest 3");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0011_ValidateSocialToken() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		request.setiProToken(providerauthtoken);
		request.setUdid(udid);
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}
	
	@Test
	public void TestCase_0012_ValidateSocialToken2() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}

	@Test
	public void TestCase_0013_GetLobbyData() throws Exception {
		logger.debug("Get the lobby data");
		
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		
		MvcResult result = mockMvc.perform(post("/auth/getlobbydata")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		LobbyData response = TestUtil.convertStringToLobbyDataObject(result.getResponse().getContentAsString());
		assertNotNull(response.getMode());
		
		logger.debug("Get the lobby data -- PASS");
	}
	
	@Test
	public void TestCase_0100_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account ");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		iproUserName = response.getUsername();
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0101_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with existing user id");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		request.setUdid(udid);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		logger.debug("Create IPRO Account with existing user id - PASS");
	}
	
	@Test
	public void TestCase_0102_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with existing user id and different password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("12345");
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		
		logger.debug("Create IPRO Account with existing user id and different password - PASS");
	}
	
	@Test
	public void TestCase_0103_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		request.setUdid(udid);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	@Test
	public void TestCase_0104_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with incorrect user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername("test@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	@Test
	public void TestCase_0105_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with corrent user id and incorrect password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		logger.debug("Verify IPRO Account with corrent user id and incorrect password - PASS");
	}
	
	@Test
	public void TestCase_0106_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with incorrect user id and correct password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		logger.debug("Verify IPRO Account with incorrect user id and correct password - PASS");
	}
	
	@Test
	public void TestCase_0107_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user email and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	@Test
	public void TestCase_0108_LoginAsGuest1() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		providerauthtoken = response.getiProToken();
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0109_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account ");
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUdid(udid);
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		iproUserName = response.getUsername();
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0010_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with correct user id and correct password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		System.out.println("IPRO USER NAME : " + iproUserName);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		
		logger.debug("Verify IPRO Account with correct user id and correct password - PASS");
	}
	
	@Test
	public void TestCase_0111_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with correct user id and correct password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		System.out.println("IPRO USER NAME : " + iproUserName);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		
		logger.debug("Verify IPRO Account with correct user id and correct password - PASS");
	}
	
	@Test
	public void TestCase_0300_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account ");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		//iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0301_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with same user Name");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@1iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0302_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with same user email");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName + 1);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0303_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with same user email and same user name");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account - PASS");
	}
	
	@Test
	public void TestCase_0304_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	// Admin reconcile checks
	@Test
	public void TestCase_0800_TerminateSession() throws Exception {
		logger.debug("Verify location");
		TerminateSessionRequest request = new TerminateSessionRequest();
		
		mockMvc.perform(post("/user/terminate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk());
		logger.debug("Verify Location - PASS");
	}
		
	@Test
	public void TestCase_0800_ReconcileTxn() throws Exception {
		logger.debug("Verify location");
		ReconcileRequest request = new ReconcileRequest();
		request.getProvider();
		
		mockMvc.perform(post("/user/reconcile/txn")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk());
		logger.debug("Verify Location - PASS");
	}
	
	// Validation checks
	@Test
	public void TestCase_0900_VerifyLocation() throws Exception {
		logger.debug("Verify location");
		AuthRequest request = new AuthRequest();
		
		mockMvc.perform(post("/auth/verifylocation")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk());
		
		logger.debug("Verify Location - PASS");	
	}
	
	
	
	@Test
	public void TestCase_1000_VerifyLocation() throws Exception {
		logger.info("Verify location");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);
		MvcResult result = mockMvc.perform(post("/auth/verifylocation").with(new RequestPostProcessor() { 
		    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
		        request.setRemoteAddr(ipaddress); 
		        return request;
		     }}).contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		logger.debug("Verify Location with social mode - PASS");
	}
	
	@Test
	public void TestCase_1001_VerifyLocation() throws Exception {
		logger.info("Verify location");
		AuthRequest request = new AuthRequest();
		request.setClientIdentity(clientIdentity);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
			
		MvcResult result = mockMvc.perform(post("/auth/verifylocation").with(new RequestPostProcessor() { 
		    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
		        request.setRemoteAddr(ipaddress); 
		        return request;
		     }}).contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		logger.debug("Response content : " + result.getResponse().getContentAsString());		
		logger.debug("Verify Location with real mode - PASS");
	}
	
	@Test
	public void TestCase_1002_Validategps() throws Exception {
		logger.debug("Validate valid provider token");
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		request.setClientIdentity(clientIdentity);

		MvcResult result = mockMvc.perform(post("/auth/validategps")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getMessage());
		logger.info("Validate valid provider token - PASS");
	}
	
	@Test
	public void TestCase_1003_Validategps() throws Exception {
		logger.debug("Validate valid provider token");
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		request.setClientIdentity(clientIdentity);

		MvcResult result = mockMvc.perform(post("/auth/validategps")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());			
		logger.info("Validate valid provider token - PASS");
	}
	
	
	@Test
	public void TestCase_1004_Validategps() throws Exception {
		logger.debug("Validate valid provider token");
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		request.setClientIdentity(clientIdentity);
		Gps gps = new Gps();
		gps.setLatitude(latitude0);
		gps.setLongitude(longitude0);
		request.setGps(gps);				
		MvcResult result = mockMvc.perform(post("/auth/validategps")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getMessage());		
		logger.info("Validate valid provider token - PASS");
	}
	
	@Test
	public void TestCase_1010_LoginAsGuest1() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_1011_LoginAsGuest2() throws Exception {
		logger.debug("Login as guest 2");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		//assertEquals(providerauthtoken, response.getiProToken());
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_1012_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with legal jurisdiction");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		//assertEquals("RMG", response.getMessage().getRef_id());
		assertEquals("SLG", response.getUserAccessMode().getMode());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1013_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with same email id and password");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1014_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with same email id and diff password");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("12334545");
		request.getCredential().add(Cndtl);
		
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1015_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with diff email id, same user name and diff password");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("12334545");
		request.getCredential().add(Cndtl);
		
		request.setUsername(iproUserName + iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1016_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with diff email id and diff user name and password");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		
		request.setUsername(iproUserName + iproUserName);
		request.setUseremail(iproUserName + iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		assertEquals("SLG", response.getUserAccessMode().getMode());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1017_ValidateProviderTokenFromClient() throws Exception {
		logger.debug("Verify valid provider token from client");
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		request.setiProToken(providerauthtoken);
		
		MvcResult result = mockMvc.perform(post("/auth/verifyiprotoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());		
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Verify valid provider token from client - PASS");		
	}
	
	@Test
	public void TestCase_1100_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	@Test
	public void TestCase_1101_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with incorrect user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername("test@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	@Test
	public void TestCase_1102_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with corrent user id and incorrect password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());
		
		logger.debug("Verify IPRO Account with corrent user id and incorrect password - PASS");
	}
	
	@Test
	public void TestCase_1103_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with incorrect user id and correct password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue("2323q132");
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getType());		
		logger.debug("Verify IPRO Account with incorrect user id and correct password - PASS");
	}
	
	@Test
	public void TestCase_1104_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user email and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUseremail(iproUserName + "@iprogames.com");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	} 
	
	@Test
	public void TestCase_1200_ValidateSocialToken() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}
	
	@Test
	public void TestCase_1201_ValidateSocialToken() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}
	
	@Test
	public void TestCase_1300_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with legal jurisdiction");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		udid = UUIDUtil.generateUUID().toString();
		request.setUdid(udid);
		
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		//assertEquals("RMG", response.getMessage().getRef_id());
		assertEquals("SLG", response.getUserAccessMode().getMode());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1301_LoginAsGuest() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		udid = UUIDUtil.generateUUID().toString();
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_1302_CreateiProAccount() throws Exception {
		logger.debug("Create IPRO Account with legal jurisdiction");
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUdid(udid);
		
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		
		MvcResult result = mockMvc.perform(post("/auth/register/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		//assertEquals("RMG", response.getMessage().getRef_id());
		assertEquals("SLG", response.getUserAccessMode().getMode());
		iproUserName = response.getUsername();
		providerauthtoken = response.getiProToken();
		logger.debug("Create IPRO Account with legal jurisdiction- PASS");
	}
	
	@Test
	public void TestCase_1303_VerifyiProAccount() throws Exception {
		logger.debug("Verify IPRO Account with existing user id and password");		
		
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity(clientIdentity);
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);
		request.setUsername(iproUserName);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);		
		request.setUdid(udid);
		
		MvcResult result = mockMvc.perform(post("/auth/verify/iproaccount")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		logger.debug("Verify IPRO Account with existing user id and password - PASS");
	}
	
	/*@Test
	public void TestCase_1005_KeepAlive() throws Exception {
		logger.debug("Validate valid provider token");
		AuthRequest request = new AuthRequest();
		request.setiProToken(providerauthtoken);
		
		
		MvcResult result = mockMvc.perform(post("/auth/keepsessionalive")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getAcctype());			
		logger.info("Validate valid provider token - PASS");
	}*/
}