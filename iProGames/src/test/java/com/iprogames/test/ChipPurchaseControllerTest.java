/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.Credential;
import com.iprogames.platform.authentication.common.Gps;
import com.iprogames.platform.authentication.common.enums.OperatorEnum;
import com.iprogames.platform.authentication.common.enums.SkinEnum;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.ChipPurchaseResponse;
import com.iprogames.platform.chippurchase.common.Transaction;
import com.iprogames.platform.chippurchase.dao.ChipPurchaseDaoHandler;
import com.iprogames.platform.error.ErrorResponse;
import com.iprogames.test.util.TestUtil;
import com.iprogames.util.uuid.UUIDUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
(locations={"file:src/main/webapp/WEB-INF/rest-servlet.xml"}
)

@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChipPurchaseControllerTest {

	Logger logger = LogManager.getLogger(ChipPurchaseControllerTest.class);
	
	private static String accessToken = UUIDUtil.generateUUIDString();
	private static String latitude = /*"13.0900";*/"40.47";
	private static String longitude = /*"80.2700";*/"73.58";
	private static String providerauthtoken = null;
	private static String purchaseReceipt = "MIITyAYJKoZIhvcNAQcCoIITuTCCE7UCAQExCzAJBgUrDgMCGgUAMIIDeQYJKoZIhvcNAQcBoIIDagSCA2YxggNiMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFlMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBAwIBAQQFDAMxLjAwDQIBDQIBAQQFAgMBEdYwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjMxMBgCAQQCAQIEEN+z1GC2sax8IbcLkMWjn/YwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAcAgEFAgEBBBQdtNpK5R2pX+wrr23GxymG3BCrLjAeAgECAgEBBBYMFGNvbS5pcHJvZ2FtZXMucGlja2l0MB4CAQwCAQEEFhYUMjAxNC0wOC0wN1QxNzoxNjozOFowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBDAgEHAgEBBDtnDM2vzxZVDdyeqxJNgX24XS5fslSo0cEv1hU0mG1215JbH7gbFH3I6T710LNUftFauJ0LaWJM6YPEYjBPAgEGAgEBBEd2ptrKc00IZYynF/cs7W8+jSdkokw9XnbEp9lSxYs4df9+x4AirwQfN4/2S63o6lBI5Gheyij0kPYa6OQ4wDTqe1yWBYhFZTCCAWICARECAQEEggFYMYIBVDALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBATAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwMTE5NTI1MTI0MBsCAgapAgEBBBIMEDEwMDAwMDAxMTk1MjUxMjQwHwICBqgCAQEEFhYUMjAxNC0wOC0wN1QxNzoxNjozOFowHwICBqoCAQEEFhYUMjAxNC0wOC0wN1QxNzoxNjozOFowKAICBqYCAQEEHwwdY29tLmlwcm9nYW1lcy5waWNraXQuYmVnaW5uZXKggg5VMIIFazCCBFOgAwIBAgIIGFlDIXJ0nPwwDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTAxMTExMjE1ODAxWhcNMTUxMTExMjE1ODAxWjB4MSYwJAYDVQQDDB1NYWMgQXBwIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtpPCtw8kXu3SNEjohQXjM5RmW+gnN797Q0nr+ckXlzNzMklKyG9oKRS4lKb0ZUs7R9fRLGZLuJjZvPUSUcvmL6n0s58c6Cj8UsCBostWYoBaopGuTkDDfSgu19PtTdmtivvyZ0js63m9Am0EWRj/jDefijfxYv+7ogNQhwrVkuCGEV4jRvXhJWMromqMshC3kSNNmj+DQPJkCVr3ja5WXNT1tG4DGwRdLBuvAJkX16X7SZHO4qERMV4ZAcDazlCDXsjrSTtJGirq4J+/0kZJnNiroYNhbA/B/LOtmXUq/COb7yII63tZFBGfczQt5rk5pjv35j7syqb7q68m34+IgQIDAQABo4IB2DCCAdQwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzBNBgNVHR8ERjBEMEKgQKA+hjxodHRwOi8vZGV2ZWxvcGVyLmFwcGxlLmNvbS9jZXJ0aWZpY2F0aW9uYXV0aG9yaXR5L3d3ZHJjYS5jcmwwDgYDVR0PAQH/BAQDAgeAMB0GA1UdDgQWBBR1diSia2IMlzSh+k5eCAwiv3PvvjCCAREGA1UdIASCAQgwggEEMIIBAAYKKoZIhvdjZAUGATCB8TCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjApBggrBgEFBQcCARYdaHR0cDovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAKA78Ye8abS3g3wZ9J/EAmTfAsmOMXPLHD7cJgeL/Z7z7b5D1o1hLeTw3BZzAdY0o2kZdxS/uVjHUsmGAH9sbICXqZmF6HjzmhKnfjg4ZPMEy1/y9kH7ByXLAiFx80Q/0OJ7YfdC46u/d2zdLFCcgITFpW9YWXpGMUFouxM1RUKkjPoR1UsW8jI13h+80pldyOYCMlmQ6I3LOd8h2sN2+3o2GhYamEyFG+YrRS0vWRotxprWZpKj0jZSUIAgTTPIsprWU2KxYFLw9fd9EFDkEr+9cb60gMdtxG9bOTXR57fegSAnjjhcgoc6c2DE1vEcoKlmRH7ODCibI3+s7OagO90wggQjMIIDC6ADAgECAgEZMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wODAyMTQxODU2MzVaFw0xNjAyMTQxODU2MzVaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GuMIGrMA4GA1UdDwEB/wQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjA2BgNVHR8ELzAtMCugKaAnhiVodHRwOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhL3Jvb3QuY3JsMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQDaMgCWxVSU0zuCN2Z9LmjVw8a4yyaMSJDPEyRqRo5j1PDQEwbd2MTBNxXyMxM5Ji3OLlVA4wsDr/oSwucNIbjVgM+sKC/OLbNOr4YZBMbpUN1MKUcQI/xsuxuYa0iJ4Vud3kbbNYU17z7Q4lhLOPTtdVofXHAdVjkS5eENEeSJJQa91bQVjl7QWZeQ6UuB4t8Yr0R0HhmgOkfMkR066yNa/qUtl/d7u9aHRkKF61I9JrJjqLSxyo/0zOKzyEfgv5pZg/ramFMqgvV8ZS6V2TNd9e1lzDE3xVoE6Gvh54gDSnWemyjLSkCIZUN13cs6JSPFnlf4Ls7SqZJecy4vJXUVMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCBhZQyFydJz8MAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAo0Ms+SdjvrpsflE5iy8QJw6w32Q4xMvJIm5IPuzMvOKafUEH57QZ46cpZll2BCI40xv/4eercku2trwDx4elcVtNUpHtG421mWV1I0WCIlpkhntnro7xWGuDHxkVDJt2cCy6dA5HmNWe5oT7ZuM06gloj3idnVvsYLyGeAoHBqlbySI04oWiSzm3IpNT/eGHl5D9foxASPmtIpwx35scB9qPkTyCxQxz/px0KZ58J1oNMFNtQTDfEX0L1WhoiB0/WJFeKdYdGRDu2aU6tPBDsdqXQ4OYarn2DihOyUX8pExXWoe+JvXvv1Bj2ye6tZEAwbQPDCPRaOQv6zvX5DbBBg==";
	private static String productId = "com.iprogames.pickit.4kchips";
	private static String provider = "APPL";
	private static String purchaseRefId = null;
	private String pid = "MOCK", sid = "MOCK";
	
	private static String guestProvider = OperatorEnum.GUST.name();
	private static String guestSkin = SkinEnum.GUST.name();
	private static String udid = UUIDUtil.generateUUID().toString();
	
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	ChipPurchaseDaoHandler chippurchasedao;

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	public void TestCase_0000_LoginAsGuest3() throws Exception {
		logger.debug("Login as guest 3");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0001_ValidateSocialToken() throws Exception {
		logger.debug("Validate valid social token");
		
		AuthRequest request = new AuthRequest();
		request.setSkin(sid);
		request.setProvider(pid);
		request.setClientIdentity("MOB-");
		Credential Cndtl = new Credential();
		Cndtl.setType("token");
		Cndtl.setValue(accessToken);
		request.getCredential().add(Cndtl);
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		request.setiProToken(providerauthtoken);
		request.setUdid(udid);
		
		MvcResult result = mockMvc.perform(post("/auth/verifysocialtoken")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		logger.debug("Content on converting the guest user to registered user : " + TestUtil.convertObjectToString(response));
		logger.debug("iProToken for the unit test cases : " + providerauthtoken);
		logger.debug("Validate valid social token -- PASS");
	}
	
	@Test
	public void TestCase_0003_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		//assertEquals(3, response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0004_CancelInitiateBuyChips() throws Exception {
		logger.debug("CancelInitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		request.setTxn(txn);

		mockMvc.perform(post("/purchase/cancel")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted());
		
		logger.debug("CancelInitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0005_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0005_CompletePurchase() throws Exception {
		logger.debug("CompletePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnreceipt(purchaseReceipt);		
		txn.setTxnproductid(productId);
		txn.setTxnid("1000000119330213");
		request.setTxn(txn);
		
		MvcResult result = mockMvc.perform(post("/purchase/complete")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();		
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("CompletePurchase - PASS");
	}
	
	@Test
	public void TestCase_0007_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		//assertEquals(3, response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0012_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0013_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0014_CancelInitiateBuyChips() throws Exception {
		logger.debug("CancelInitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		request.setTxn(txn);

		mockMvc.perform(post("/purchase/cancel")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted());
		
		logger.debug("CancelInitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0015_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0016_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0017_CancelPurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		request.setTxn(txn);

		mockMvc.perform(post("/purchase/cancel")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted());

		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0018_CompletePurchase() throws Exception {
		logger.debug("CompletePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnreceipt(purchaseReceipt);
		txn.setTxnproductid(productId);
		txn.setTxnid("1000000119330213");
		request.setTxn(txn);
		
		MvcResult result = mockMvc.perform(post("/purchase/complete")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("CompletePurchase - PASS");
	}
	
	@Test
	public void TestCase_0019_ClientAcknowledgement() throws Exception {
		logger.debug("ClientAcknowledgement");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/ack")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0020_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0021_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0022_CancelPurchase() throws Exception {
		logger.debug("CancelInitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setIsretry(true);
		request.setTxn(txn);

		MvcResult result =mockMvc.perform(post("/purchase/cancel")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("CancelInitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0023_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid("sdfwerwe34234243");
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0024_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0025_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0026_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0027_CancelPurchase() throws Exception {
		logger.debug("CancelInitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/cancel")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())				
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		
		logger.debug("CancelInitiateBuyChips - PASS");
	}	
	
	@Test
	public void TestCase_0050_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		//assertEquals(3, response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0051_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase - Invalid reference id");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid("1231232132");
		txn.setTxnproductid(productId);
		txn.setTxnamount(300.00);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0052_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase - Invalid reference id");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300.00);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0100_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		//assertEquals(3, response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0101_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300.00);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0102_CompletePurchase() throws Exception {
		logger.debug("CompletePurchase");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnreceipt(purchaseReceipt);		
		txn.setTxnproductid(productId);
		txn.setTxnid("1000000119330213");
		request.setTxn(txn);
		
		MvcResult result = mockMvc.perform(post("/purchase/complete")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		
		logger.debug("CompletePurchase - PASS");
	}
	
	@Test
	public void TestCase_0103_ClientAcknowledgement() throws Exception {
		logger.debug("ClientAcknowledgement");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		request.setTxn(txn);

		mockMvc.perform(post("/purchase/ack")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted());
		
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0150_InitiateBuyChips() throws Exception {
		logger.debug("InitiateBuyChips");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		request.setProvider(provider);
		Transaction txn = new Transaction();
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate/buychips")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getTxn().getTxnrefid());
		purchaseRefId = response.getTxn().getTxnrefid();
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		assertNotNull(response.getPackages());
		logger.info("No of packages : " + response.getPackages().size());
		//assertEquals(3, response.getPackages().size());
		logger.debug("InitiateBuyChips - PASS");
	}
	
	@Test
	public void TestCase_0151_InitiatePurchase() throws Exception {
		logger.debug("InitiatePurchase - Invalid reference id");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid(purchaseRefId);
		txn.setTxnproductid(productId);
		txn.setTxnamount(300.00);
		txn.setTxncurrencycode("INR");
		request.setTxn(txn);

		MvcResult result = mockMvc.perform(post("/purchase/initiate")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isAccepted())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ChipPurchaseResponse response = TestUtil.convertStringToChipPurchaseResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getTxn().getTxnrefid());
		logger.debug("InitiatePurchase - PASS");
	}
	
	@Test
	public void TestCase_0152_CompletePurchase() throws Exception {
		logger.debug("CompletePurchase - invalid reference id");
		ChipPurchase request = new ChipPurchase();
		request.setiProToken(providerauthtoken);
		Transaction txn = new Transaction();
		txn.setTxnrefid("sdfsdfdsfdf");
		txn.setTxnreceipt(purchaseReceipt);		
		txn.setTxnproductid(productId);
		txn.setTxnid("1000000119330213");
		request.setTxn(txn);
		
		MvcResult result = mockMvc.perform(post("/purchase/complete")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
		logger.debug("Purchase Reference Id : " + purchaseRefId);
		
		logger.debug("CompletePurchase - PASS");
	}
}
