/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.test.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.LobbyData;
import com.iprogames.platform.chippurchase.common.ChipPurchaseResponse;
import com.iprogames.platform.error.ErrorResponse;
import com.iprogames.platform.gameserver.common.GameSelectResponse;
import com.iprogames.platform.gameserver.common.GetActiveGameResponse;
import com.iprogames.platform.gameserver.common.UserGameCreateResponse;
import com.iprogames.platform.gameserver.common.UserGameJoinResponse;
import com.iprogames.platform.gameserver.common.UserGameSearchResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;

public class TestUtil {
	static Logger logger = LogManager.getLogger(TestUtil.class);

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static byte[] convertObjectToJsonBytes(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.writeValueAsBytes(object);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static String convertObjectToString(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.writeValueAsString(object);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
       
    public static AuthResponse convertStringToAuthResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), AuthResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static GameSelectResponse convertStringToGameSelectResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), GameSelectResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static ChipPurchaseResponse convertStringToChipPurchaseResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), ChipPurchaseResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static LobbyData convertStringToLobbyDataObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), LobbyData.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static ErrorResponse convertStringToErrorResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), ErrorResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static UserGameSearchResponse convertStringToUserGameSearchResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), UserGameSearchResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static UserGameCreateResponse convertStringToUserGameCreateResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), UserGameCreateResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static UserGameJoinResponse convertStringToUserGameJoinResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), UserGameJoinResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static GetActiveGameResponse convertStringToGetActiveGameResponseObject(String jsonData) {
    	logger.debug("Json Data to be converted to object : " + jsonData);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.readValue(jsonData.getBytes(), GetActiveGameResponse.class);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
}
