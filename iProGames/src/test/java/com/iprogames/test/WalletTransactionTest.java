package com.iprogames.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.Credential;
import com.iprogames.platform.authentication.common.Gps;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.platform.wallettransaction.service.WalletTransactionService;
import com.iprogames.test.util.TestUtil;
import com.iprogames.platform.exception.InsufficientChipsException;
import static com.iprogames.util.uuid.UUIDUtil.generateUUID;;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
(locations={"file:src/main/webapp/WEB-INF/rest-servlet.xml"}
		)

@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WalletTransactionTest {
	Logger logger = LogManager.getLogger(WalletTransactionTest.class);
	private static String latitude = "40.47";
	private static String longitude = "73.58";
	private MockMvc mockMvc;
	private static String iproUserName = UUID.randomUUID().toString().substring(0, 5);
	private static String iproPassword = "12345678";
	private static String ipaddress = "127.0.0.1";	
	private static String providerauthtoken = null;
	private static String fundType = FundTypeEnum.VCHP.name();
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	WalletTransactionService service;
	
	
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		try {
			ipaddress = Inet4Address.getLocalHost().getHostAddress();		
		
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void TestCase_0000_createAccount()
	{
		AuthRequest request = new AuthRequest();
		request.setSkin("IPRO");
		request.setProvider("IPRO");
		request.setClientIdentity("MOB-");
		Credential Cndtl = new Credential();
		Cndtl.setType("password");
		Cndtl.setValue(iproPassword);
		request.getCredential().add(Cndtl);	
		iproUserName = UUID.randomUUID().toString().substring(0, 5);
		request.setUsername(iproUserName);
		request.setUseremail(iproUserName + "@iprogames.com");		
		//request.setIsLocationEnabled("TRUE");
		Gps gps = new Gps();
		gps.setLatitude(latitude);
		gps.setLongitude(longitude);
		request.setGps(gps);
		logger.debug("UserName  " + iproUserName);
		MvcResult result;
		try {
			result = mockMvc.perform(post("/auth/register/iproaccount").with(new RequestPostProcessor() { 
					    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
					        request.setRemoteAddr(ipaddress); 
					        return request;
					     }})
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(request)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
					.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());		
		assertNotNull(response.getiProToken());
		assertEquals(UserAccountTypeEnum.REG.name(), response.getAcctype());
		providerauthtoken = response.getiProToken();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Create IPRO Account with legal jurisdiction with REAL GAME mode- PASS");
	}
	
	@Test(expected=InsufficientChipsException.class)
	public void TestCase_0001_DebitChipsFromUserWallet() throws Exception {
		logger.info("Debit Chips from User Wallet");		
		CreditDebitChipsRequest request = new CreditDebitChipsRequest();
		BigDecimal chips = new BigDecimal(1000000);
		request.setTxnAmt(chips);
		request.setFundType(FundTypeEnum.valueOf(fundType));
		request.setTxnType(TxnTypeEnum.WAGR);
		request.setRefTxnId(generateUUID().toString());
		service.debitChipsFromUserAccount(request , providerauthtoken);
	}
	
	@Test
	public void TestCase_0002_CreditChipsToUserWallet() throws Exception {
		logger.debug ("credit Chips To User Wallet");
		UserWallet wallet = service.getBalanceAmount(fundType, providerauthtoken);
		if(wallet != null)
			logger.info("wallet  " + wallet.getBalanceAmount());
		CreditDebitChipsRequest request = new CreditDebitChipsRequest();
		BigDecimal chips = new BigDecimal(50);
		request.setTxnAmt(chips);
		request.setFundType(FundTypeEnum.valueOf(fundType));
		request.setTxnType(TxnTypeEnum.ADJT);
		request.setRefTxnId(generateUUID().toString());
		service.creditChipsToUserAccount(request , providerauthtoken);	
		assertEquals(wallet.getBalanceAmount().add(chips).intValue(),service.getBalanceAmount(fundType, providerauthtoken).getBalanceAmount().intValue());
	}
	
	@Test
	public void TestCase_0003_DebitChipsToUserWallet() throws Exception {
		logger.debug("Debit Chips from User Wallet");
		UserWallet wallet = service.getBalanceAmount(fundType, providerauthtoken);		
		CreditDebitChipsRequest request = new CreditDebitChipsRequest();
		BigDecimal chips = new BigDecimal(50);
		request.setTxnAmt(chips);
		request.setFundType(FundTypeEnum.valueOf(fundType));
		request.setTxnType(TxnTypeEnum.ADJT);
		request.setRefTxnId(generateUUID().toString());
		service.debitChipsFromUserAccount(request , providerauthtoken);
		int balance = (wallet.getBalanceAmount().subtract(chips)).intValue();		
		assertEquals(balance,service.getBalanceAmount(fundType, providerauthtoken).getBalanceAmount().intValue());
	}
	
	@Test
	public void TestCase_0004_GetChipsFromUserWallet() throws Exception {
		logger.debug("Debit Chips To User Wallet");				
		UserWallet wallet = service.getBalanceAmount(fundType, providerauthtoken);
		assertNotNull(wallet.getBalanceAmount());
	}
}
