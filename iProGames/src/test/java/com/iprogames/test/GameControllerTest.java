package com.iprogames.test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.enums.OperatorEnum;
import com.iprogames.platform.authentication.common.enums.SkinEnum;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.chippurchase.dao.ChipPurchaseDaoHandler;
import com.iprogames.platform.error.ErrorResponse;
import com.iprogames.platform.gameserver.common.GameEvent;
import com.iprogames.platform.gameserver.common.GameSelectRequest;
import com.iprogames.platform.gameserver.common.GameSelectResponse;
import com.iprogames.platform.gameserver.common.GetActiveGameRequest;
import com.iprogames.platform.gameserver.common.GetActiveGameResponse;
import com.iprogames.platform.gameserver.common.UserGameCreateRequest;
import com.iprogames.platform.gameserver.common.UserGameCreateResponse;
import com.iprogames.platform.gameserver.common.UserGameJoinRequest;
import com.iprogames.platform.gameserver.common.UserGameJoinResponse;
import com.iprogames.platform.gameserver.common.UserGameSearchRequest;
import com.iprogames.platform.gameserver.common.UserGameSearchResponse;
import com.iprogames.platform.gameserver.common.enums.GameMode;
import com.iprogames.platform.gameserver.common.enums.GameType;
import com.iprogames.test.util.TestUtil;
import com.iprogames.util.uuid.UUIDUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
(locations={"file:src/main/webapp/WEB-INF/rest-servlet.xml"}
)

@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameControllerTest {

	Logger logger = LogManager.getLogger(GameControllerTest.class);
	
	private static String providerauthtoken = null;
	
	private static String guestProvider = OperatorEnum.GUST.name();
	private static String guestSkin = SkinEnum.GUST.name();
	private static String udid = UUIDUtil.generateUUID().toString();
	private static int eventId = 0, nextEventId = 0;	
	private static int userGameId = 0;
	
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	ChipPurchaseDaoHandler chippurchasedao;

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	public void TestCase_0500_LoginAsGuest1() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(udid);
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0501_GetGameEventList() throws Exception {
		logger.debug("GetGameEventList");		
		GameSelectRequest request = new GameSelectRequest();
		request.setiProToken(providerauthtoken);
		request.setSearch(true);
				
		MvcResult result = mockMvc.perform(post("/game/list")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		GameSelectResponse response = TestUtil.convertStringToGameSelectResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getGameEvents());
		for(GameEvent event : response.getGameEvents()) {
			logger.debug(event.getEventId());
			if (eventId == 0) {
				eventId = event.getEventId();
			} else if (nextEventId == 0) {
				nextEventId = event.getEventId();
			}
		}
		logger.debug("EventId : " + eventId);
		logger.debug("nextEventId : " + nextEventId);
		logger.debug("GetGameEventList - PASS");
	}
	
	@Test
	public void TestCase_0502_SearchUserGames() throws Exception {
		logger.debug("SearchUserGames-No user games added");		
		
		UserGameSearchRequest request = new UserGameSearchRequest();
		request.setiProToken(providerauthtoken);
		request.setEventId(eventId);
		//request.setChips(12000);
		//request.setOpponents(1);
		//request.setPicksPerGame(10);
		//request.setRebuys(-1);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/search")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getType());
		} else {
			UserGameSearchResponse response = TestUtil.convertStringToUserGameSearchResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getGameEvent());
		}
		
		logger.debug("SearchUserGames - PASS");
	}
	
	@Test
	public void TestCase_0503_CreateUserGames() throws Exception {
		logger.debug("CreateUserGames");	
		UserGameCreateRequest request = new UserGameCreateRequest();
		request.setiProToken(providerauthtoken);
		request.setGameMode(GameMode.PUBLIC.value());
		request.setGameType(GameType.REG.name());
		request.setEventId(eventId);
		//request.setChips(12000);
		request.setNoOfSeat(1);
		request.setPicksCount(10);
		request.setRebuyCount(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/create")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getType());
		} else {
			UserGameCreateResponse response = TestUtil.convertStringToUserGameCreateResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getUserGame());
			userGameId = response.getUserGame().getUserGameId();
		}
		logger.debug("CreateUserGames - PASS");
	}
	
	@Test
	public void TestCase_0504_SearchUserGames() throws Exception {
		logger.debug("SearchUserGames-user games added");		
		
		UserGameSearchRequest request = new UserGameSearchRequest();
		request.setiProToken(providerauthtoken);
		request.setEventId(eventId);
		request.setChips(12000);
		request.setOpponents(3);
		request.setPicksPerGame(20);
		request.setRebuys(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/search")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getType());
	}
	
	@Test
	public void TestCase_0505_JoinUserGame() throws Exception {
		logger.debug("JoinUserGame");		
		
		UserGameJoinRequest request = new UserGameJoinRequest();
		request.setiProToken(providerauthtoken);
		request.setUserGameId(userGameId);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/join")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		UserGameJoinResponse response = TestUtil.convertStringToUserGameJoinResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getPlayerGameInfo());
	}
	
	@Test
	public void TestCase_0506_SearchUserGames() throws Exception {
		logger.debug("SearchUserGames-No user games added");		
		
		UserGameSearchRequest request = new UserGameSearchRequest();
		request.setiProToken(providerauthtoken);
		request.setEventId(eventId);
		request.setChips(12000);
		request.setOpponents(1);
		request.setPicksPerGame(10);
		request.setRebuys(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/search")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getType());
		} else {
			UserGameSearchResponse response = TestUtil.convertStringToUserGameSearchResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getGameEvent());
		}
		
		logger.debug("SearchUserGames - PASS");
	}
	
	@Test
	public void TestCase_0507_JoinUserGame() throws Exception {
		logger.debug("JoinUserGame");		
		
		UserGameJoinRequest request = new UserGameJoinRequest();
		request.setiProToken(providerauthtoken);
		request.setUserGameId(userGameId);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/join")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		UserGameJoinResponse response = TestUtil.convertStringToUserGameJoinResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getPlayerGameInfo());
	}
	
	@Test
	public void TestCase_0508_LoginAsGuest2() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(UUIDUtil.generateUUID().toString());
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0509_JoinUserGame() throws Exception {
		logger.debug("JoinUserGame");		
		
		UserGameJoinRequest request = new UserGameJoinRequest();
		request.setiProToken(providerauthtoken);
		request.setUserGameId(userGameId);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/join")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		UserGameJoinResponse response = TestUtil.convertStringToUserGameJoinResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getPlayerGameInfo());
	}
	
	@Test
	public void TestCase_0510_SearchUserGames() throws Exception {
		logger.debug("SearchUserGames-user games added");		
		
		UserGameSearchRequest request = new UserGameSearchRequest();
		request.setiProToken(providerauthtoken);
		request.setEventId(eventId);
		request.setChips(12000);
		request.setOpponents(1);
		request.setPicksPerGame(10);
		request.setRebuys(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/search")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response);
		} else {
			UserGameSearchResponse response = TestUtil.convertStringToUserGameSearchResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response);
		}
	}
	
	@Test
	public void TestCase_0511_LoginAsGuest3() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(UUIDUtil.generateUUID().toString());
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}	
	
	@Test
	public void TestCase_0512_JoinUserGame() throws Exception {
		logger.debug("JoinUserGame");		
		
		UserGameJoinRequest request = new UserGameJoinRequest();
		request.setiProToken(providerauthtoken);
		request.setUserGameId(userGameId);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/join")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response);
	}
	
	@Test
	public void TestCase_0513_CreateUserGames() throws Exception {
		if (nextEventId != 0) {
			logger.debug("CreateUserGames");	
			UserGameCreateRequest request = new UserGameCreateRequest();
			request.setiProToken(providerauthtoken);
			request.setGameMode(GameMode.PUBLIC.value());
			request.setGameType(GameType.REG.name());
			request.setEventId(nextEventId);
			request.setChips(12000);
			request.setNoOfSeat(1);
			request.setPicksCount(10);
			request.setRebuyCount(3);
					
			MvcResult result = mockMvc.perform(post("/game/usergame/create")
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(request)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
					.andReturn();
			
			logger.debug("Response content : " + result.getResponse().getContentAsString());
			if (result.getResponse().getContentAsString().contains("type")) {
				ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
				assertNotNull(response.getType());
			} else {
				UserGameCreateResponse response = TestUtil.convertStringToUserGameCreateResponseObject(result.getResponse().getContentAsString());
				assertNotNull(response.getUserGame());
				userGameId = response.getUserGame().getUserGameId();
			}
			logger.debug("CreateUserGames - PASS");
		}
	}	
	
	@Test
	public void TestCase_0514_JoinUserGame() throws Exception {
		if (nextEventId != 0) {
			logger.debug("JoinUserGame");		
			
			UserGameJoinRequest request = new UserGameJoinRequest();
			request.setiProToken(providerauthtoken);
			request.setUserGameId(userGameId);
					
			MvcResult result = mockMvc.perform(post("/game/usergame/join")
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(request)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
					.andReturn();
			
			logger.debug("Response content : " + result.getResponse().getContentAsString());
			
			UserGameJoinResponse response = TestUtil.convertStringToUserGameJoinResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getPlayerGameInfo());
		}
	}
	
	@Test
	public void TestCase_0515_GetActiveGames() throws Exception {
		logger.debug("GetActiveGames");		
		
		GetActiveGameRequest request = new GetActiveGameRequest();
		request.setiProToken(providerauthtoken);
		if (nextEventId == 0) {
			mockMvc.perform(post("/game/player/activegames")
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(request)))
					.andExpect(status().isOk());
		} else {
			MvcResult result = mockMvc.perform(post("/game/player/activegames")
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(request)))
					.andExpect(status().isOk())
					.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
					.andReturn();
			
			logger.debug("Response content : " + result.getResponse().getContentAsString());
			GetActiveGameResponse response = TestUtil.convertStringToGetActiveGameResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getPlayerActiveGames());
		}
	}
	
	@Test
	public void TestCase_0516_CreateUserGames() throws Exception {
		logger.debug("CreateUserGames");	
		UserGameCreateRequest request = new UserGameCreateRequest();
		request.setiProToken(providerauthtoken);
		request.setGameMode(GameMode.PUBLIC.value());
		request.setGameType(GameType.REG.name());
		request.setEventId(eventId);
		request.setChips(12000);
		request.setNoOfSeat(1);
		request.setPicksCount(10);
		request.setRebuyCount(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/create")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getType());
		} else {
			UserGameCreateResponse response = TestUtil.convertStringToUserGameCreateResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response.getUserGame());
			userGameId = response.getUserGame().getUserGameId();
		}
		logger.debug("CreateUserGames - PASS");
	}
	
	@Test
	public void TestCase_0517_LoginAsGuest3() throws Exception {
		logger.debug("Login as guest 1");
		AuthRequest request = new AuthRequest();
		request.setProvider(guestProvider);
		request.setSkin(guestSkin);
		request.setUdid(UUIDUtil.generateUUID().toString());
		logger.info("***********************Guest login Content : " + TestUtil.convertObjectToString(request));
		logger.info("*********************************************");
		MvcResult result = mockMvc.perform(post("/auth/guestlogin")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		AuthResponse response = TestUtil.convertStringToAuthResponseObject(result.getResponse().getContentAsString());
		
		assertNotNull(response.getiProToken());
		providerauthtoken = response.getiProToken();
		assertEquals(UserAccountTypeEnum.GUST.name(), response.getAcctype());
		logger.debug("Login as guest - PASS");
	}
	
	@Test
	public void TestCase_0518_JoinUserGame() throws Exception {
		logger.debug("JoinUserGame");		
		
		UserGameJoinRequest request = new UserGameJoinRequest();
		request.setiProToken(providerauthtoken);
		request.setUserGameId(userGameId);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/join")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		UserGameJoinResponse response = TestUtil.convertStringToUserGameJoinResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getPlayerGameInfo());
	}
	
	@Test
	public void TestCase_0519_GetActiveGames() throws Exception {
		logger.debug("GetActiveGames");		
		
		GetActiveGameRequest request = new GetActiveGameRequest();
		request.setiProToken(providerauthtoken);
		
		MvcResult result = mockMvc.perform(post("/game/player/activegames")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		GetActiveGameResponse response = TestUtil.convertStringToGetActiveGameResponseObject(result.getResponse().getContentAsString());
		assertNotNull(response.getPlayerActiveGames());		
	}
	
	@Test
	public void TestCase_0520_SearchUserGames() throws Exception {
		logger.debug("SearchUserGames-user games added");		
		
		UserGameSearchRequest request = new UserGameSearchRequest();
		request.setiProToken(providerauthtoken);
		request.setEventId(eventId);
		request.setChips(12000);
		request.setOpponents(1);
		request.setPicksPerGame(10);
		request.setRebuys(3);
				
		MvcResult result = mockMvc.perform(post("/game/usergame/search")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
				.andReturn();
		
		logger.debug("Response content : " + result.getResponse().getContentAsString());
		
		if (result.getResponse().getContentAsString().contains("type")) {
			ErrorResponse response = TestUtil.convertStringToErrorResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response);
		} else {
			UserGameSearchResponse response = TestUtil.convertStringToUserGameSearchResponseObject(result.getResponse().getContentAsString());
			assertNotNull(response);
		}
	}
}
