package com.iprogames.platform.util.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum TxnTypeEnum {

	ACHP,
	ADJT,
	DPT,
	POUT,
	PUR,
	WAGR,
	WTD,
	FREE,
	WCHP,
	EFEE
}
