/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.platform.util;

import java.util.Properties;

public enum Config {
	
	INSTANCE;
	private Properties generalconfigFile = new Properties();
	
	private Config() {		
		loadConfigFile();
	}
	
	public String getGeneralValue(String key) {		
		return generalconfigFile.getProperty(key);
	}
	
	private void loadConfigFile() {
		try {
			generalconfigFile.load(this.getClass().getClassLoader().getResourceAsStream("iproserver.properties"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
