package com.iprogames.platform.util.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum FundTypeEnum {
	 BONS, CASH, CHP, CRE , FBET, LPT ,VCHP, TCHP, LCHP 
}
