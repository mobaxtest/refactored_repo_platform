package com.iprogames.platform.util;

import java.util.List;

import com.iprogames.platform.authentication.common.Credential;
import com.iprogames.platform.authentication.common.enums.ClientIdentityEnum;
import com.iprogames.platform.error.Actions;
import com.iprogames.platform.error.ButtonType;
import com.iprogames.platform.error.CodeType;
import com.iprogames.platform.error.CommandType;
import com.iprogames.platform.error.ErrorMessage;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * Utility functions
 */
public class Utility {

	public static String getTokens(List<Credential> credentials, String searchString) {
		String result = null;
		for(Credential cdntl : credentials) {
			if(cdntl.getType().equalsIgnoreCase(searchString)) {
				result = cdntl.getValue();
				break;
			}
		}
		return result;
	}
	
	public static ErrorMessage getAlertMessage(String optionalMode, String optionalMessage) {
		ErrorMessage message = new ErrorMessage(CodeType.LOCATION_ALERT, optionalMessage, optionalMode);
		Actions action = new Actions(ButtonType.CONTINUE, CommandType.LOGIN);
		message.getActions().add(action);
		action = new Actions(ButtonType.CANCEL, CommandType.NONE);
		message.getActions().add(action);
		
		return message;
	}
	
	public static ClientIdentityEnum checkClient(String clientIdentity) {
		
		if (clientIdentity.startsWith("MOB-")) {
			return ClientIdentityEnum.MOB;
		} else if (clientIdentity.startsWith("DESK-")) {
			return ClientIdentityEnum.DESK;
		}
		return null;
	}
}
