/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.platform.util;

public class Constants {

	public static final String LOBBY_SERVER = Config.INSTANCE.getGeneralValue("LOBBY_SERVER");
	public static final String LOBBY_PORT = Config.INSTANCE.getGeneralValue("LOBBY_PORT");
	public static final String LOBBY_NAME = Config.INSTANCE.getGeneralValue("LOBBY_NAME");
	
	public static final String TWITTER_CONSUMER_KEY = Config.INSTANCE.getGeneralValue("TWITTER_CONSUMER_KEY");
	public static final String TWITTER_CONSUMER_SECRET = Config.INSTANCE.getGeneralValue("TWITTER_CONSUMER_SECRET");
	
	public static final String IPROGAMES_SESSION_EXPIRATION = Config.INSTANCE.getGeneralValue("SESSION_EXPIRATION");
	
	/* User Credentials type */
	public static final String CREDENTIAL_TYPE_PASSWORD = "password";
	public static final String CREDENTIAL_TYPE_TOKEN = "token";
	public static final String CREDENTIAL_TYPE_SECRET = "secret";
	
	public static final String ITUNES_SANDBOX_URL = Config.INSTANCE.getGeneralValue("ITUNES_SANDBOX_URL");
	
	public static final String GAME_SELECT = Config.INSTANCE.getGeneralValue("GAME_SELECT");
	public static final String SIDE_BETS_MARKET = Config.INSTANCE.getGeneralValue("SIDE_BETS_MARKET");
	public static final String ACTIVE_GAMES = Config.INSTANCE.getGeneralValue("ACTIVE_GAMES");
	public static final String LEAGUE_PLAY = Config.INSTANCE.getGeneralValue("LEAGUE_PLAY");
	public static final String BUY_CHIPS = Config.INSTANCE.getGeneralValue("BUY_CHIPS");
	public static final String ACTIVE_PICKS = Config.INSTANCE.getGeneralValue("ACTIVE_PICKS");
	public static final String PROFILE = Config.INSTANCE.getGeneralValue("PROFILE");
	public static final String LEADERS = Config.INSTANCE.getGeneralValue("LEADERS");
	public static final String FRIENDS = Config.INSTANCE.getGeneralValue("FRIENDS");
	public static final String HOW_TO_PLAY = Config.INSTANCE.getGeneralValue("HOW_TO_PLAY");
	public static final String SETTINGS = Config.INSTANCE.getGeneralValue("SETTINGS");
	
	public static final String PLATFORM_MODULE_NAME = "PLT";
	
}
