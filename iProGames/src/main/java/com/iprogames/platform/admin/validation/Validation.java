package com.iprogames.platform.admin.validation;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.admin.common.ReconcileRequest;
import com.iprogames.platform.admin.common.TerminateSessionRequest;
import com.iprogames.util.constraints.Condition;
import com.iprogames.util.constraints.exception.InvalidArgumentException;
import com.iprogames.util.constraints.exception.NullArgumentException;

@Service("adminValidation")
public class Validation {

	@Resource
	MessageSource messageSource;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notNullCondition;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notValidIntegerCondition;
	
	@SuppressWarnings("unchecked")
	public void checkTerminateSessionRequest(TerminateSessionRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
			notValidIntegerCondition.check(request.getUserId());
		} catch (InvalidArgumentException e) {
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkReconcileTxnRequest(ReconcileRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getUuid());
			notNullCondition.check(request.getUsername());
			notNullCondition.check(request.getProvider());
			notNullCondition.check(request.getSkin());			
		} catch (InvalidArgumentException e) {
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
}