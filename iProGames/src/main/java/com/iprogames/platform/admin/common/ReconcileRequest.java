package com.iprogames.platform.admin.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.iprogames.platform.chippurchase.common.Transaction;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ReconcileRequest {

	private String uuid			= null;
	private String username		= null;
	private String skin			= null;
	private String provider		= null;
	private Transaction txn		= null;
	
	public ReconcileRequest() {
		
	}
	
	public ReconcileRequest (String uuid, String username, String skin, String provider) {
		this.uuid = uuid;
		this.username = username;
		this.skin = skin;
		this.provider = provider;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Transaction getTxn() {
		return txn;
	}

	public void setTxn(Transaction txn) {
		this.txn = txn;
	}
}
