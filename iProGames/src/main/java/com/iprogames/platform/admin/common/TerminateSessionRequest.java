package com.iprogames.platform.admin.common;

public class TerminateSessionRequest {

	private String iProToken	= null;
	private int userId			= 0;
	
	public TerminateSessionRequest() {
		
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
