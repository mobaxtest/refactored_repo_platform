package com.iprogames.platform.admin.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iprogames.platform.admin.common.ReconcileRequest;
import com.iprogames.platform.admin.common.TerminateSessionRequest;
import com.iprogames.platform.admin.validation.Validation;
import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.authentication.controller.AuthenticationController;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.service.ChipPurchaseService;
import com.iprogames.platform.profiler.Monitorable;

@Controller
@RequestMapping("/user")
public class AdminController {
	
	static Logger logger = LogManager.getLogger(AuthenticationController.class);
	
	private AuthenticationService service;
	
	private ChipPurchaseService chipPurchaseService;
	
	@Autowired
	Validation adminValidation;
	
	@Autowired
    public AdminController(AuthenticationService service, ChipPurchaseService chipPurchaseService) {
        this.service = service;
        this.chipPurchaseService = chipPurchaseService;
    }
	
	/** API to terminate the session forcefully by Admin 
	 * 
	 * @params - request - TerminateSessionRequest
	 * 			request.getiProToken() - IproToken to validate the user and terminate the session
	 * 			request.getUserId() - userid of the admin who is terminating the session
	 * @return - void
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidSessionException - Not a valid iprotoken or the session is not active
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/terminate", method = RequestMethod.POST)
	@ResponseBody
	public void terminateSession(@RequestBody TerminateSessionRequest request) {
		adminValidation.checkTerminateSessionRequest(request);
		AuthRequest authRequest = new AuthRequest(request.getiProToken());
		service.logout(authRequest, UserSessionStatusEnum.FOR.name());
	}
	
	/** API to reconcile transcation by Admin 
	 * 
	 * @params - request - Reconcilerequest
	 * 			request.getUuid() - uuid of the user
	 * 			request.getUsername() - User name of the user
	 * 			request.getProvider() - Provider name - FB/TWT/G+/IPRO
	 * 			request.getSkin() - Skin used for the login	based on the provider
	 * @return - void
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidProductException - Not a valid product
	 * 				InvalidPurchaseIdException - Not a valid transaction id for the purchase
	 * 				FundNotEnabledException - Fund not enabled
	 * 				TransactionTypeNotEnabledException - Given txn type is not enabled
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				IProGamesGeneralException - SQL error
	 *  
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/reconcile/txn", method = RequestMethod.POST)
	@ResponseBody
	public void reconcileTxn(@RequestBody ReconcileRequest request) {
		adminValidation.checkReconcileTxnRequest(request);
		SecureToken token = new SecureToken(request);
		ChipPurchase purchaseRequest = new ChipPurchase(request.getTxn());
		chipPurchaseService.reconcileTransaction(purchaseRequest, token);
	}
	
}
