package com.iprogames.platform.wallettransaction.dao;

import static com.iprogames.util.timestamp.TimeStampUtil.getCurrentSQLTimeStamp;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDString;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDFromString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.dao.SessionFactoryHandler;
import com.iprogames.platform.exception.FundNotEnabledException;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.InsufficientChipsException;
import com.iprogames.platform.exception.InvalidiProTokenException;
import com.iprogames.platform.exception.TransactionTypeNotEnabledException;
import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.util.model.IproFundType;
import com.iprogames.util.model.IproTxnType;
import com.iprogames.util.model.IproUser;
import com.iprogames.util.model.IproUserSession;
import com.iprogames.util.model.IproUserTransaction;
import com.iprogames.util.model.IproWallet;
import com.iprogames.util.timestamp.TimeStampUtil;
/**
 * @author rubavathi 
 */
/**
 * Transaction manager to retrieve chips,credit chips and debit chips from user wallet. 
*/

@Repository("walletDao")
public class WalletTransactionDaoHandler <T extends Serializable> extends SessionFactoryHandler {
	Logger logger = LogManager.getLogger(WalletTransactionDaoHandler.class);
	
	@Resource
	MessageSource messageSource;
	private IMap<String, IproFundType> iproFundTypes;
	private IMap<String, IproTxnType> iproTxnTypes;
	public WalletTransactionDaoHandler() {
		super();
	}
	public WalletTransactionDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	/**
	 * Debits the chips from the user wallet. User is identified by the given iprotoken.
	 * @param request - 
	 * @param iProToken
	 */
	public void debitChips(CreditDebitChipsRequest request,
			String iProToken) {
		logger.debug("Debit chips from user wallet");
		Session session = getCurrentSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();			
			queryString.append("select  t from IproUserSession t, IproUser u where t.providerAuthToken=:token ");
			queryString.append("and hex(t.iproUser.id) = hex(u.userUuid) ");
			queryString.append("and t.iproUserSessionStatus.id=:status");
			Query query = session.createQuery(queryString.toString());
			logger.debug(queryString.toString());
	        query.setParameter("token", iProToken);
	        query.setParameter("status", UserSessionStatusEnum.ACT.name());
	        IproUserSession userSession = (IproUserSession) query.uniqueResult();	        
	        if (userSession != null ) {	       
	        	IproUser user  = userSession.getIproUser();
	        	UUID userUuid = user.getUserUuid();
	        	/* Verify the fund type */
		        /*query = session.createQuery("select f from IproFundType f where f.fundType=:fundType and f.enabledInd='Y'");
		        query.setParameter("fundType", request.getFundType().name());
		        IproFundType fundtype = (IproFundType)query.uniqueResult();
		        if (fundtype == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }*/
		        iproFundTypes = getFundTypes();
	        	Set<IproFundType> iprofundtype = (Set<IproFundType>) iproFundTypes.values(new SqlPredicate("fundType="+request.getFundType().name()+" AND enabledInd=Y"));
	        	IproFundType fundtype;
		        if (iprofundtype == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }		        
		        else
		        {
		        	fundtype = iprofundtype.iterator().next();
		        	logger.info("FUND TYPE  " + fundtype.getFundType());
		        }
		        	
		        /*query = session.createQuery("select f from IproTxnType f where f.txnTypeCode=:txnTypeCode");
		        query.setParameter("txnTypeCode", request.getTxnType().name());
		        IproTxnType txnType = (IproTxnType)query.uniqueResult();
		        if (txnType == null) {
		        	throw new TransactionTypeNotEnabledException(messageSource.getMessage("TXN_NOT_ENABLED",null,null));
		        }*/
		        
		        iproTxnTypes = getTxnTypes();
	        	Set<IproTxnType> iprotxntype = (Set<IproTxnType>) iproTxnTypes.values(new SqlPredicate("txnTypeCode="+request.getTxnType().name()));
	        	IproTxnType txnType;
		        if (iprotxntype == null) {
		        	throw new TransactionTypeNotEnabledException(messageSource.getMessage("TXN_NOT_ENABLED",null,null));
		        }		        
		        else
		        {
		        	txnType = iprotxntype.iterator().next();
		        	logger.info("FUND TYPE  " + txnType.getTxnTypeCode());
		        }
		        
		        queryString.delete(0,  queryString.length());
		        queryString.append("select w from IproWallet w where ");
		        queryString.append("hex(w.iproUser.id)=:userUuid and ");
		        queryString.append("w.iproFundType.id=:fundType ");
		        query = session.createQuery(queryString.toString());
		        query.setParameter("userUuid", getUUIDString(userUuid));
		        query.setParameter("fundType", request.getFundType().name());
		        IproWallet wallet = (IproWallet)query.uniqueResult();
		        if (wallet == null) {
		        	throw new InsufficientChipsException(messageSource.getMessage("INSUFFICIENT_CHIPS", null, null));
		        } else {
		        	if(wallet.getBalanceAmt().intValue() >= request.getTxnAmt().intValue())
		        	{
		        		wallet.setBalanceAmt(wallet.getBalanceAmt().subtract(request.getTxnAmt()));
		        		wallet.setUpdatedDate(getCurrentSQLTimeStamp());		        		
		        	}
		        	else
		        	{
		        		throw new InsufficientChipsException(messageSource.getMessage("INSUFFICIENT_CHIPS", null, null));
		        	}
		        }		        
		        request.setTxnAmt(request.getTxnAmt().negate());
		        IproUserTransaction userTxn = constructUserTransaction(userSession,request.getRefTxnId(),user,request.getTxnAmt(), fundtype,txnType,wallet.getBalanceAmt());
		        session.saveOrUpdate(userTxn);	
		        session.saveOrUpdate(wallet);		        
	        } else {
	        	throw new InvalidiProTokenException(messageSource.getMessage("INVALID_TOKEN", null, null));
	        }
	        tx.commit();
		} catch (InvalidiProTokenException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new InvalidiProTokenException(ex.getMessage());
		} catch (FundNotEnabledException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new FundNotEnabledException(ex.getMessage());
		} catch (InsufficientChipsException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new InsufficientChipsException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	/**
	 * Credit chips from into user wallet. User is identified by the given iprotoken.
	 * @param request - 
	 * @param iProToken
	 */
	public void creditChips(CreditDebitChipsRequest request,
			String iProToken) {
		logger.debug("Credit chips into user wallet");
		Session session = getCurrentSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.delete(0, queryString.length());
			queryString.append("select  t from IproUserSession t, IproUser u where t.providerAuthToken=:token ");
			queryString.append("and hex(t.iproUser.id) = hex(u.userUuid) ");
			queryString.append("and t.iproUserSessionStatus.id=:status");
			Query query = session.createQuery(queryString.toString());
			logger.debug(queryString.toString());
	        query.setParameter("token", iProToken);
	        query.setParameter("status", UserSessionStatusEnum.ACT.name());
	        IproUserSession userSession = (IproUserSession) query.uniqueResult();	        
	        if (userSession != null ) {	       
	        	IproUser user  = userSession.getIproUser();	        	
	        	UUID userUuid = user.getUserUuid();
	        	/* Verify the fund type */
		       /* query = session.createQuery("select f from IproFundType f where f.fundType=:fundType and f.enabledInd='Y'");
		        query.setParameter("fundType", request.getFundType().name());
		        IproFundType fundtype = (IproFundType)query.uniqueResult();
		        if (fundtype == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }*/
		        iproFundTypes = getFundTypes();
	        	Set<IproFundType> iprofundType = (Set<IproFundType>) iproFundTypes.values(new SqlPredicate("fundType="+request.getFundType().name()+" AND enabledInd=Y"));	        	       	
	        	IproFundType fundtype;
	        	if (iprofundType == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }
		        else
		        {
		        	fundtype = iprofundType.iterator().next();
		        	logger.info("FUND TYPE  " + fundtype.getFundType());
		        }		        
		        /*query = session.createQuery("select f from IproTxnType f where f.txnTypeCode=:txnTypeCode");
		        query.setParameter("txnTypeCode", request.getTxnType().name());
		        IproTxnType txnType = (IproTxnType)query.uniqueResult();
		        if (txnType == null) {
		        	throw new TransactionTypeNotEnabledException(messageSource.getMessage("TXN_NOT_ENABLED",null,null));
		        }*/
		        iproTxnTypes = getTxnTypes();
	        	Set<IproTxnType> iprotxntype = (Set<IproTxnType>) iproTxnTypes.values(new SqlPredicate("txnTypeCode="+request.getTxnType().name()));
	        	IproTxnType txnType;
		        if (iprotxntype == null) {
		        	throw new TransactionTypeNotEnabledException(messageSource.getMessage("TXN_NOT_ENABLED",null,null));
		        }		        
		        else
		        {
		        	txnType = iprotxntype.iterator().next();
		        	logger.info("FUND TYPE  " + txnType.getTxnTypeCode());
		        }
		        queryString.delete(0,  queryString.length());
		        queryString.append("select w from IproWallet w where ");
		        queryString.append("hex(w.iproUser.id)=:userUuid and ");
		        queryString.append("w.iproFundType.id=:fundType ");
		        query = session.createQuery(queryString.toString());
		        query.setParameter("userUuid", getUUIDString(userUuid));
		        query.setParameter("fundType", request.getFundType().name());
		        IproWallet wallet = (IproWallet)query.uniqueResult();
		        logger.info("Credit into user wallet table");
		        if (wallet == null) {
		        	wallet = constructWallet(userUuid,request.getTxnAmt(),fundtype);		        	
		        } else {
		        	wallet.setBalanceAmt(wallet.getBalanceAmt().add(request.getTxnAmt()));
			        wallet.setUpdatedDate(getCurrentSQLTimeStamp());
		        }		
		        request.setTxnAmt(request.getTxnAmt().plus());
		        IproUserTransaction userTxn = constructUserTransaction(userSession,request.getRefTxnId(),user,request.getTxnAmt(), fundtype,txnType,wallet.getBalanceAmt());
		        session.saveOrUpdate(userTxn);		        
		        session.saveOrUpdate(wallet);
		        logger.info("Wallet updated");
	        } else {
	        	throw new InvalidiProTokenException(messageSource.getMessage("INVALID_TOKEN", null, null));
	        }
	        tx.commit();
		} catch (InvalidiProTokenException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new InvalidiProTokenException(ex.getMessage());
		} catch (TransactionTypeNotEnabledException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new TransactionTypeNotEnabledException(ex.getMessage());
		} catch (FundNotEnabledException ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new FundNotEnabledException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}		
	}
	
	/**
	 * Retrieves chips from user wallet.
	 * @param fundType
	 * @param iProToken
	 * @return UserWallet
	 */
	public UserWallet getChips(String fundType,String iProToken) {
		logger.debug("get chips from user wallet");
		Session session = getCurrentSession();		
		UserWallet userWallet = null;
		try{			
			StringBuilder queryString = new StringBuilder();
			queryString.delete(0, queryString.length());
			queryString.append("select  u from IproUserSession t, IproUser u where t.providerAuthToken=:token ");
			queryString.append("and hex(t.iproUser.id) = hex(u.userUuid) ");
			queryString.append("and t.iproUserSessionStatus.id=:status");
			Query query = session.createQuery(queryString.toString());
			logger.debug(queryString.toString());
	        query.setParameter("token", iProToken);
	        query.setParameter("status", UserSessionStatusEnum.ACT.name());
	        @SuppressWarnings("unchecked")			
	        List<IproUser> userList = query.list();
	        if (userList != null && userList.size() > 0) {
	        	IproUser user = userList.get(0);
	        	UUID userUuid = user.getUserUuid();
	        	/* Verify the fund type */
		        /*query = session.createQuery("select f from IproFundType f where f.fundType=:fundType and f.enabledInd='Y'");
		        query.setParameter("fundType", fundType);
		        IproFundType fundtype = (IproFundType)query.uniqueResult();
		        if (fundtype == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }*/
		        iproFundTypes = getFundTypes();
	        	Set<IproFundType> fundtype = (Set<IproFundType>) iproFundTypes.values(new SqlPredicate("fundType="+fundType+" AND enabledInd=Y"));
	        	
		        if (fundtype == null) {
		        	throw new FundNotEnabledException(messageSource.getMessage("FUND_NOT_ENABLED",null,null));
		        }
		        else
		        {
		        	IproFundType f = fundtype.iterator().next();
		        	logger.info("FUND TYPE  " + f.getFundType());
		        }
	        	queryString.delete(0,  queryString.length());
		        queryString.append("select w from IproWallet w where ");
		        queryString.append("hex(w.iproUser.id)=:userUuid and ");
		        queryString.append("w.iproFundType.id=:fundType ");
		        query = session.createQuery(queryString.toString());
		        query.setParameter("userUuid", getUUIDString(userUuid));
		        query.setParameter("fundType", fundType);
		        IproWallet wallet = (IproWallet)query.uniqueResult();		        
		        if (wallet != null) {
		        	logger.info("Wallet is not null");
		        	userWallet = new UserWallet(wallet.getIproFundType().getFundType(),wallet.getBalanceAmt(),wallet.getUpdatedDate());		        			
		        } 
	        } else {
	        	throw new InvalidiProTokenException(messageSource.getMessage("INVALID_TOKEN", null, null));
	        }	        
		} catch (FundNotEnabledException ex) {
			ex.printStackTrace();			
			throw new FundNotEnabledException(ex.getMessage());
		} catch (InvalidiProTokenException ex) {
			ex.printStackTrace();			
			throw new InvalidiProTokenException(ex.getMessage());
		} catch (Exception ex) {		
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return userWallet;
	}
	
	/**
	 * Method retrieves the fundtype from database table and store it in the hazelcast map
	 * @return IMap<String, IproFundType>
	 */
	private IMap<String, IproFundType> getFundTypes()
	{
		 logger.info("getFundTypes  " );
		 return getHazelcastInstance().getMap("iproFundTypeMap");		 
	}
	
	/**
	 * Method retrieves the txnType from database table and store it in the hazelcast map
	 * @return IMap<String, IproTxnType>
	 */
	private IMap<String, IproTxnType> getTxnTypes()
	{
		 logger.info("getTxnTypes  " );
		 return getHazelcastInstance().getMap("iproTxnTypeMap");		 
	}
	
	/**
	 * Constructs user wallet object.
	 * @param userUuid
	 * @param chips
	 * @param fundtype
	 * @return IproWallet
	 */
	private IproWallet constructWallet(UUID userUuid, BigDecimal chips, IproFundType fundtype) {
    	IproWallet wallet = new IproWallet();
    	
    	IproUser user = new IproUser();
    	user.setUserUuid(userUuid);
    	wallet.setIproUser(user);
    	
    	wallet.setBalanceAmt(chips);
    	wallet.setIproFundType(fundtype);
    	wallet.setUpdatedDate(getCurrentSQLTimeStamp());
    	
    	return wallet;
    }
	
	/**
	 * Constructs user transaction table.
	 * @param userSession
	 * @param refTxnId
	 * @param iproUser
	 * @param txnAmt
	 * @param fundtype
	 * @param iproTxnType
	 * @param endBalAmount
	 * @return IproUserTransaction
	 */
	private IproUserTransaction constructUserTransaction(IproUserSession userSession,String refTxnId,IproUser iproUser, BigDecimal txnAmt, IproFundType fundtype,IproTxnType iproTxnType,BigDecimal endBalAmount) {
		IproUserTransaction userTransaction = new IproUserTransaction();
		userTransaction.setRefTxnId(getUUIDFromString(refTxnId));
		userTransaction.setIproUser(iproUser);
		userTransaction.setIproFundType(fundtype);
		userTransaction.setIproTxnType(iproTxnType);
		userTransaction.setTxnAmt(txnAmt);
		userTransaction.setIproUserSession(userSession);
		userTransaction.setEndBalAmount(endBalAmount);
		userTransaction.setCreatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
		return userTransaction;
	}
}
