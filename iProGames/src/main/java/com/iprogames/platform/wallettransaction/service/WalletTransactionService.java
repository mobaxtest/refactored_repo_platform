package com.iprogames.platform.wallettransaction.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.platform.wallettransaction.dao.WalletTransactionDaoHandler;

@Service
public class WalletTransactionService {
	Logger logger = LogManager.getLogger(WalletTransactionService.class);
	@SuppressWarnings("rawtypes")
	@Autowired
	WalletTransactionDaoHandler walletDao;
	
	public void debitChipsFromUserAccount(CreditDebitChipsRequest request,String iProToken) {		
		walletDao.debitChips(request, iProToken);
	}
	
	public void creditChipsToUserAccount(CreditDebitChipsRequest request, String iProToken) {
		walletDao.creditChips(request, iProToken);
	}
	
	public UserWallet getBalanceAmount(String fundType,String iProToken) {
		return walletDao.getChips(fundType, iProToken);
	}
}
