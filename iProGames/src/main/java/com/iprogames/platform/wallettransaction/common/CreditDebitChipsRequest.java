package com.iprogames.platform.wallettransaction.common;

import java.math.BigDecimal;

import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;

/**
 * @author rubavathi
 * Represents a request object to store user wallet transaction details.
 */
public class CreditDebitChipsRequest {
	private FundTypeEnum fundType;
	private BigDecimal txnAmt;
	private String refTxnId;
	private TxnTypeEnum txnType;

	public CreditDebitChipsRequest() {
	}
	
	public CreditDebitChipsRequest(FundTypeEnum fundType, BigDecimal txnAmt,String refTxnId, TxnTypeEnum txnType ) {
		this.setFundType(fundType);
		this.setTxnAmt(txnAmt);
		this.setTxnType(txnType);
		this.setRefTxnId(refTxnId);
	}
	
	public String getRefTxnId() {
		return refTxnId;
	}
	
	public void setRefTxnId(String refTxnId) {
		this.refTxnId = refTxnId;
	}
	
	public TxnTypeEnum getTxnType() {
		return txnType;
	}
	
	public void setTxnType(TxnTypeEnum txnType) {
		this.txnType = txnType;
	}
	
	public FundTypeEnum getFundType() {
		return fundType;
	}
	
	public void setFundType(FundTypeEnum fundType) {
		this.fundType = fundType;
	}
	
	public BigDecimal getTxnAmt() {
		return txnAmt;
	}
	
	public void setTxnAmt(BigDecimal txnAmt) {
		this.txnAmt = txnAmt;
	}
}