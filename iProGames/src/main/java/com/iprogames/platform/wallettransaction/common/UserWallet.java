package com.iprogames.platform.wallettransaction.common;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author rubavathi
 * Represents a user wallet object which contains details about the fundtype, balance amount and the last date when the wallet is updated.
 */
public class UserWallet {
	private String fundType;
	private BigDecimal balanceAmount;
	private Date lasUpdatedDate;
	
	public UserWallet() {
	}
	
	public UserWallet(String fundType, BigDecimal balanceAmount,Date lasUpdatedDate) {
		this.fundType = fundType;
		this.balanceAmount = balanceAmount;		
		this.lasUpdatedDate = lasUpdatedDate;
	}
	
	public String getFundType() {
		return fundType;
	}
	public void setFundType(String fundType) {
		this.fundType = fundType;
	}
	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Date getLasUpdatedDate() {
		return lasUpdatedDate;
	}
	public void setLasUpdatedDate(Date lasUpdatedDate) {
		this.lasUpdatedDate = lasUpdatedDate;
	}
	
}
