/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.hibernate.instance.HazelcastAccessor;
/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public abstract class SessionFactoryHandler {	

    @Autowired
	SessionFactory sessionFactory;
    HazelcastInstance hazelcastInstance;
    public SessionFactoryHandler() {
    	
    }
    
	public SessionFactoryHandler(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getCurrentSession() {
	    Session session = null;
	    try { 
	        session = sessionFactory.getCurrentSession();
	    } catch ( HibernateException he ) {
	        session = sessionFactory.openSession();
	    }
	    return session;
	}
	
	public HazelcastInstance getHazelcastInstance()
	{
		if(hazelcastInstance == null)
		{
			try{
				hazelcastInstance = HazelcastAccessor.getHazelcastInstance(sessionFactory);				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return hazelcastInstance;
	}
	
}
