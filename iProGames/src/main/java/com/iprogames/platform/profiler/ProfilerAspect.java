package com.iprogames.platform.profiler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Aspect
@Component
public class ProfilerAspect {
	static Logger logger = LogManager.getLogger(ProfilerAspect.class);
	
	@Pointcut("execution(@com.iprogames.platform.profiler.Monitorable * *.*(..))")
    public void monitor() {
    }
	
    @Around("monitor()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        //logger.info("JVM memory in use = "+ (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
        Object output = null;	
        try {
		output = pjp.proceed();
        } finally {
        	long elapsedTime = System.currentTimeMillis() - start;
            logger.info(pjp.getTarget()+"."+pjp.getSignature()+": Execution time: " + elapsedTime + " ms. ("+ elapsedTime/60000 + " minutes)");
        }

        return output;
    }

    /*@After("monitor()")
    public void profileMemory() {
        logger.info("JVM memory in use = "+ (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
    }  */  
}