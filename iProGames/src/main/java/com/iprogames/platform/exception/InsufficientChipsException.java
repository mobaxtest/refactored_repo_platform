package com.iprogames.platform.exception;

public class InsufficientChipsException extends RuntimeException {

	private static final long serialVersionUID = 3625544134872681745L;
	
	private String message;
	 
	public InsufficientChipsException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
