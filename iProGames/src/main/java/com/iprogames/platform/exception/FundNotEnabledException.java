package com.iprogames.platform.exception;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public final class FundNotEnabledException extends RuntimeException {

	private static final long serialVersionUID = 3625544134872681745L;
	
	private String message;
	 
	public FundNotEnabledException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
