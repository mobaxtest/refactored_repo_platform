package com.iprogames.platform.exception;

public class GPSNotEnabledONLegalException extends RuntimeException {

	private static final long serialVersionUID = 3625544555487281745L;
	
	private String message;
	 
	public GPSNotEnabledONLegalException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
