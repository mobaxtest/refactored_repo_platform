package com.iprogames.platform.exception;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public final class UserAlreadyRegisteredException extends RuntimeException {

	private static final long serialVersionUID = -8780876780002185769L;
	
	private String message;
	 
	public UserAlreadyRegisteredException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
