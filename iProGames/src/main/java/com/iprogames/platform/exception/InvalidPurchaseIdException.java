package com.iprogames.platform.exception;

public class InvalidPurchaseIdException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6237151519256698103L;
	
	private String message;
	 
	public InvalidPurchaseIdException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
