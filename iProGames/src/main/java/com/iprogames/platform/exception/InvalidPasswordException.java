package com.iprogames.platform.exception;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public final class InvalidPasswordException extends RuntimeException {

	private static final long serialVersionUID = 6237151519256698103L;
	
	private String message;
	 
	public InvalidPasswordException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
