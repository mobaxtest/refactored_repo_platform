package com.iprogames.platform.exception;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public final class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5650271053190269649L;
	
	private String message;
	 
	public UserNotFoundException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
