/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.operator.impl;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.operator.VerifyWithOperator;
import com.iprogames.platform.profiler.Monitorable;

@Service("googleplus")
public class VerifyGooglePlusToken implements VerifyWithOperator {

	@Resource
	MessageSource messageSource;
	
	Logger logger = LogManager.getLogger(VerifyGooglePlusToken.class);
	
	@Override
	public UserSocialProfile verifyToken(String authToken, String authTokenSecret) {
		return null;
	}
	
	@Monitorable
	@Override
	public UserSocialProfile verifyToken(String authToken) {
		try {
			Google google = new GoogleTemplate(authToken);
			/*logger.info("google.plusOperations().getGoogleProfile().getAboutMe()" + google.plusOperations().getGoogleProfile().getAboutMe());
			logger.info("google.plusOperations().getGoogleProfile().getAccountEmail()" + google.plusOperations().getGoogleProfile().getAccountEmail());
			logger.info("google.plusOperations().getGoogleProfile().getDisplayName()" + google.plusOperations().getGoogleProfile().getDisplayName());
			logger.info("google.plusOperations().getGoogleProfile().getEtag()" + google.plusOperations().getGoogleProfile().getEtag());
			logger.info("google.plusOperations().getGoogleProfile().getFamilyName()" + google.plusOperations().getGoogleProfile().getFamilyName());
			logger.info("google.plusOperations().getGoogleProfile().getGender()" + google.plusOperations().getGoogleProfile().getGender());
			logger.info("google.plusOperations().getGoogleProfile().getGivenName()" + google.plusOperations().getGoogleProfile().getGivenName());
			logger.info("google.plusOperations().getGoogleProfile().getImageUrl()" + google.plusOperations().getGoogleProfile().getImageUrl());
			logger.info("google.plusOperations().getGoogleProfile().getOccupation()" + google.plusOperations().getGoogleProfile().getOccupation());
			logger.info("google.plusOperations().getGoogleProfile().getRelationshipStatus()" + google.plusOperations().getGoogleProfile().getRelationshipStatus());
			logger.info("google.plusOperations().getGoogleProfile().getUrl()" + google.plusOperations().getGoogleProfile().getUrl());
			if (google.plusOperations().getGoogleProfile().getBirthday() != null)
				logger.info("google.plusOperations().getGoogleProfile().getBirthday()" + google.plusOperations().getGoogleProfile().getBirthday().toString());
			
			if (google.plusOperations().getGoogleProfile().getEmailAddresses() != null && google.plusOperations().getGoogleProfile().getEmailAddresses().size() > 0) {
				logger.info("google.plusOperations().getGoogleProfile().getEmailAddresses()" + google.plusOperations().getGoogleProfile().getEmailAddresses().toString());
				logger.info("google.plusOperations().getGoogleProfile().getEmailAddresses().size()" + google.plusOperations().getGoogleProfile().getEmailAddresses().size());
			}
			
			if (google.plusOperations().getGoogleProfile().getUrls() != null && google.plusOperations().getGoogleProfile().getUrls().size() > 0) {
				logger.info("google.plusOperations().getGoogleProfile().getUrls()" + google.plusOperations().getGoogleProfile().getUrls().toString());
				logger.info("google.plusOperations().getGoogleProfile().getUrls().size()" + google.plusOperations().getGoogleProfile().getUrls().size());
			}
			
			if (google.plusOperations().getGoogleProfile().getOrganizations() != null && google.plusOperations().getGoogleProfile().getOrganizations().size() > 0) {
				logger.info("google.plusOperations().getGoogleProfile().getOrganizations()" + google.plusOperations().getGoogleProfile().getOrganizations().toString());
				logger.info("google.plusOperations().getGoogleProfile().getOrganizations().size()" + google.plusOperations().getGoogleProfile().getOrganizations().size());
			}
			
			if (google.plusOperations().getGoogleProfile().getEmails() != null) {
				logger.info("google.plusOperations().getGoogleProfile().getEmails()" + google.plusOperations().getGoogleProfile().getEmails().toString());
			}
			
			if (google.plusOperations().getGoogleProfile().getPlacesLived() != null && google.plusOperations().getGoogleProfile().getPlacesLived().size() > 0) {
				logger.info("google.plusOperations().getGoogleProfile().getPlacesLived()" + google.plusOperations().getGoogleProfile().getPlacesLived().toString());
				logger.info("google.plusOperations().getGoogleProfile().getPlacesLived().size()" + google.plusOperations().getGoogleProfile().getPlacesLived().size());
			}*/
			return new UserSocialProfile(google.plusOperations().getGoogleProfile().getAccountEmail(),
					google.plusOperations().getGoogleProfile().getDisplayName());
		} catch (Exception ex) {
			throw new InvalidAuthorizationException("Google Plus", messageSource.getMessage("GOOGLEPLUS_EXCEPTION", null,null));
		}
	}
}
