/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.operator.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.operator.VerifyWithOperator;

@Service("mock")
public class VerifyMockToken implements VerifyWithOperator {

	public UserSocialProfile verifyToken(String authToken) {
		UserSocialProfile profile = new UserSocialProfile();
		profile.setDisplayName("Test Account");
		profile.setUserId(UUID.randomUUID().toString().substring(0, 5) + "@iprogames.com");
		return profile;
	}

	@Override
	public UserSocialProfile verifyToken(String authToken, String authTokenSecret) {
		UserSocialProfile profile = new UserSocialProfile();
		profile.setDisplayName("Test Account");
		profile.setUserId(UUID.randomUUID().toString().substring(0, 5) + "@iprogames.com");
		return profile;
	}
	
}
