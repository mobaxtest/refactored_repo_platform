/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.operator.impl;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.operator.VerifyWithOperator;
import com.iprogames.platform.profiler.Monitorable;

@Service("facebook")
public class VerifyFacebookToken implements VerifyWithOperator {

	Logger logger = LogManager.getLogger(VerifyFacebookToken.class);
	
	@Resource
	MessageSource messageSource;
	
	@Override
	public UserSocialProfile verifyToken(String authToken, String authTokenSecret) {
		return null;
	}
	
	@Monitorable
	@Override
	public UserSocialProfile verifyToken(String authToken) {
		try {
			Facebook facebook = new FacebookTemplate(authToken);
			/*logger.info("facebook.userOperations().getUserProfile().getGender()" + facebook.userOperations().getUserProfile().getGender());
			logger.info("facebook.userOperations().getUserProfile().getFirstName()" + facebook.userOperations().getUserProfile().getFirstName());
			logger.info("facebook.userOperations().getUserProfile().getLastName()" + facebook.userOperations().getUserProfile().getLastName());
			logger.info("facebook.userOperations().getUserProfile().getMiddleName()" + facebook.userOperations().getUserProfile().getMiddleName());
			logger.info("facebook.userOperations().getUserProfile().getBirthday()" + facebook.userOperations().getUserProfile().getBirthday());
			logger.info("facebook.userOperations().getUserProfile().getReligion()" + facebook.userOperations().getUserProfile().getReligion());
			logger.info("facebook.userOperations().getUserProfile().getAbout()" + facebook.userOperations().getUserProfile().getAbout());
			logger.info("facebook.userOperations().getUserProfile().getBio()" + facebook.userOperations().getUserProfile().getBio());
			logger.info("facebook.userOperations().getUserProfile().getLink()" + facebook.userOperations().getUserProfile().getLink());
			logger.info("facebook.userOperations().getUserProfile().getPolitical()" + facebook.userOperations().getUserProfile().getPolitical());
			logger.info("facebook.userOperations().getUserProfile().getQuotes()" + facebook.userOperations().getUserProfile().getQuotes());
			logger.info("facebook.userOperations().getUserProfile().getRelationshipStatus()" + facebook.userOperations().getUserProfile().getRelationshipStatus());
			logger.info("facebook.userOperations().getUserProfile().getReligion()" + facebook.userOperations().getUserProfile().getReligion());
			logger.info("facebook.userOperations().getUserProfile().getUsername()" + facebook.userOperations().getUserProfile().getUsername());
			logger.info("facebook.userOperations().getUserProfile().getWebsite()" + facebook.userOperations().getUserProfile().getWebsite());
			logger.info("facebook.userOperations().getUserProfile().getAgeRange()" + facebook.userOperations().getUserProfile().getAgeRange());
			logger.info("facebook.userOperations().getUserProfile().getUpdatedTime()" + facebook.userOperations().getUserProfile().getUpdatedTime());
			if (facebook.userOperations().getUserProfile().getEducation() != null && facebook.userOperations().getUserProfile().getEducation().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getEducation()" + facebook.userOperations().getUserProfile().getEducation().toString());
				logger.info("facebook.userOperations().getUserProfile().getEducation().size()" + facebook.userOperations().getUserProfile().getEducation().size());
			}
			
			if (facebook.userOperations().getUserProfile().getFavoriteAtheletes() != null && facebook.userOperations().getUserProfile().getFavoriteAtheletes().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getFavoriteAtheletes()" + facebook.userOperations().getUserProfile().getFavoriteAtheletes().toString());
				logger.info("facebook.userOperations().getUserProfile()..getFavoriteAtheletes().size()" + facebook.userOperations().getUserProfile().getFavoriteAtheletes().size());
			}
			
			if (facebook.userOperations().getUserProfile().getFavoriteTeams() != null && facebook.userOperations().getUserProfile().getFavoriteTeams().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getFavoriteTeams()" + facebook.userOperations().getUserProfile().getFavoriteTeams().toString());
				logger.info("facebook.userOperations().getUserProfile()..getFavoriteTeams().size()" + facebook.userOperations().getUserProfile().getFavoriteTeams().size());
			}
			
			if (facebook.userOperations().getUserProfile().getHometown() != null) {
				logger.info("facebook.userOperations().getUserProfile().getHometown()" + facebook.userOperations().getUserProfile().getHometown().toString());
			}
			
			if (facebook.userOperations().getUserProfile().getInspirationalPeople() != null && facebook.userOperations().getUserProfile().getInspirationalPeople().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getInspirationalPeople()" + facebook.userOperations().getUserProfile().getInspirationalPeople().toString());
				logger.info("facebook.userOperations().getUserProfile().getInspirationalPeople().size()" + facebook.userOperations().getUserProfile().getInspirationalPeople().size());
			}
			
			if (facebook.userOperations().getUserProfile().getSports() != null && facebook.userOperations().getUserProfile().getSports().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getSports()" + facebook.userOperations().getUserProfile().getSports().toString());
				logger.info("facebook.userOperations().getUserProfile().getSports().size()" + facebook.userOperations().getUserProfile().getSports().size());
			}
			
			if (facebook.userOperations().getUserProfile().getInterestedIn() != null && facebook.userOperations().getUserProfile().getInterestedIn().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getInterestedIn()" + facebook.userOperations().getUserProfile().getInterestedIn().toString());
				logger.info("facebook.userOperations().getUserProfile().getInterestedIn().size()" + facebook.userOperations().getUserProfile().getInterestedIn().size());
			}
			
			if (facebook.userOperations().getUserProfile().getLanguages() != null && facebook.userOperations().getUserProfile().getLanguages().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getLanguages()" + facebook.userOperations().getUserProfile().getLanguages().toString());
				logger.info("facebook.userOperations().getUserProfile().getLanguages().size()" + facebook.userOperations().getUserProfile().getLanguages().size());
			}
			
			if (facebook.userOperations().getUserProfile().getWork() != null && facebook.userOperations().getUserProfile().getWork().size() > 0) {
				logger.info("facebook.userOperations().getUserProfile().getWork()" + facebook.userOperations().getUserProfile().getWork().toString());
				logger.info("facebook.userOperations().getUserProfile().getWork().size()" + facebook.userOperations().getUserProfile().getWork().size());
			}			*/
			return new UserSocialProfile(facebook.userOperations().getUserProfile().getEmail(), 
					facebook.userOperations().getUserProfile().getName());
		} catch (Exception ex) {
			throw new InvalidAuthorizationException("Facebook", messageSource.getMessage("FACEBOOK_EXCEPTION", null,null));
		}
	}
}
