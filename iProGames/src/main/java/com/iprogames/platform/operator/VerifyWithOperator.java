/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.operator;

import com.iprogames.platform.authentication.common.UserSocialProfile;

public interface VerifyWithOperator {
	public UserSocialProfile verifyToken(String authToken);
	public UserSocialProfile verifyToken(String authToken, String authTokenSecret);
}
