/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

package com.iprogames.platform.operator.impl;

import static com.iprogames.platform.util.Constants.TWITTER_CONSUMER_KEY;
import static com.iprogames.platform.util.Constants.TWITTER_CONSUMER_SECRET;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.operator.VerifyWithOperator;
import com.iprogames.platform.profiler.Monitorable;

@Service("twitter")
public class VerifyTwitterToken implements VerifyWithOperator {

	Logger logger = LogManager.getLogger(VerifyTwitterToken.class);
	
	@Resource
	MessageSource messageSource;
	
	@Override
	public UserSocialProfile verifyToken(String authToken) {
		return null;
	}
	
	@Monitorable
	@Override
	public UserSocialProfile verifyToken(String authToken, String authTokenSecret) {
		try {
			Twitter twitter = new TwitterTemplate(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, authToken, authTokenSecret);
			/*
			//logger.info("twitter.userOperations().getUserProfile().getBackgroundColor()" + twitter.userOperations().getUserProfile().getBackgroundColor());
			//logger.info("twitter.userOperations().getUserProfile().getBackgroundImageUrl()" + twitter.userOperations().getUserProfile().getBackgroundImageUrl());
			logger.info("twitter.userOperations().getUserProfile().getDescription()" + twitter.userOperations().getUserProfile().getDescription());
			logger.info("twitter.userOperations().getUserProfile().getFavoritesCount()" + twitter.userOperations().getUserProfile().getFavoritesCount());
			logger.info("twitter.userOperations().getUserProfile().getFollowersCount()" + twitter.userOperations().getUserProfile().getFollowersCount());
			logger.info("twitter.userOperations().getUserProfile().getFriendsCount()" + twitter.userOperations().getUserProfile().getFriendsCount());
			logger.info("twitter.userOperations().getUserProfile().getId()" + twitter.userOperations().getUserProfile().getId());
			logger.info("twitter.userOperations().getUserProfile().getLanguage()" + twitter.userOperations().getUserProfile().getLanguage());
			//logger.info("twitter.userOperations().getUserProfile().getLinkColor()" + twitter.userOperations().getUserProfile().getLinkColor());
			logger.info("twitter.userOperations().getUserProfile().getListedCount()" + twitter.userOperations().getUserProfile().getListedCount());
			logger.info("twitter.userOperations().getUserProfile().getLocation()" + twitter.userOperations().getUserProfile().getLocation());
			logger.info("twitter.userOperations().getUserProfile().getName()" + twitter.userOperations().getUserProfile().getName());
			//logger.info("twitter.userOperations().getUserProfile().getProfileImageUrl()" + twitter.userOperations().getUserProfile().getProfileImageUrl());
			//logger.info("twitter.userOperations().getUserProfile().getProfileUrl()" + twitter.userOperations().getUserProfile().getProfileUrl());
			logger.info("twitter.userOperations().getUserProfile().getScreenName()" + twitter.userOperations().getUserProfile().getScreenName());
			//logger.info("twitter.userOperations().getUserProfile().getSidebarBorderColor()" + twitter.userOperations().getUserProfile().getSidebarBorderColor());
			//logger.info("twitter.userOperations().getUserProfile().getSidebarFillColor()" + twitter.userOperations().getUserProfile().getSidebarFillColor());
			logger.info("twitter.userOperations().getUserProfile().getStatusesCount()" + twitter.userOperations().getUserProfile().getStatusesCount());
			//logger.info("twitter.userOperations().getUserProfile().getTextColor()" + twitter.userOperations().getUserProfile().getTextColor());
			logger.info("twitter.userOperations().getUserProfile().getTimeZone()" + twitter.userOperations().getUserProfile().getTimeZone());
			logger.info("twitter.userOperations().getUserProfile().getUrl()" + twitter.userOperations().getUserProfile().getUrl());
			//logger.info("twitter.userOperations().getUserProfile().isBackgroundImageTiled()" + twitter.userOperations().getUserProfile().isBackgroundImageTiled());
			//logger.info("twitter.userOperations().getUserProfile().isContributorsEnabled()" + twitter.userOperations().getUserProfile().isContributorsEnabled());
			//logger.info("twitter.userOperations().getUserProfile().isFollowing()" + twitter.userOperations().getUserProfile().isFollowing());
			//logger.info("twitter.userOperations().getUserProfile().isFollowRequestSent()" + twitter.userOperations().getUserProfile().isFollowRequestSent());
			//logger.info("twitter.userOperations().getUserProfile().isGeoEnabled()" + twitter.userOperations().getUserProfile().isGeoEnabled());
			//logger.info("twitter.userOperations().getUserProfile().isNotificationsEnabled()" + twitter.userOperations().getUserProfile().isNotificationsEnabled());
*/			
			return new UserSocialProfile(Long.toString(twitter.userOperations().getUserProfile().getId()), 
					twitter.userOperations().getUserProfile().getScreenName());
		} catch (Exception ex) {
			throw new InvalidAuthorizationException("Twitter", messageSource.getMessage("TWITTER_EXCEPTION", null,null));
		}
	}
}
