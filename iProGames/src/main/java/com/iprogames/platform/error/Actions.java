package com.iprogames.platform.error;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public class Actions {

	private String button;
	private String command;
	
	public Actions() {
		
	}
	
	public Actions(ButtonType button) {
		this.button = button.name();
	}
	
	public Actions(ButtonType button, CommandType command) {
		this.button = button.name();
		this.command = command.value();
	}
	
	public Actions(CommandType button, CommandType command) {
		this.button = button.name();
		this.command = command.value();
	}
	
	public String getButton() {
		return button;
	}
	
	public void setButton(String button) {
		this.button = button;
	}
	
	public String getCommand() {
		return command;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
}
