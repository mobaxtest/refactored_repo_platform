package com.iprogames.platform.error;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public class ErrorMessage {

	private String code;
	private String value;
	private String ref_id;
	private List<Actions> actions;
	
	
	public ErrorMessage() {
		
	}
	
	public ErrorMessage(CodeType code, String value, String ref_id) {
		this.code = code.value();
		this.value = value;
		this.ref_id = ref_id;
	}
	
	public ErrorMessage(CodeType code, String value, String ref_id, List<Actions> actions) {
		this.code = code.value();
		this.value = value;
		this.ref_id = ref_id;
		this.actions = actions;
	}
		
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getRef_id() {
		return ref_id;
	}
	
	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	public List<Actions> getActions() {
		if (actions == null)
			actions = new ArrayList<Actions>();
		
		return actions;
	}

	public void setActions(List<Actions> actions) {
		this.actions = actions;
	}
}
