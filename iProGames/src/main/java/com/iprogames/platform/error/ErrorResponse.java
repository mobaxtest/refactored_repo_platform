package com.iprogames.platform.error;

import java.util.List;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public class ErrorResponse {

	private ErrorType type;
	private ErrorMessage message;
	
	public ErrorResponse() {
		
	}
	
	public ErrorResponse(ErrorType type, ErrorMessage message) {
		this.type = type;
		this.message = message;
	}
	
	public ErrorResponse(ErrorType type, CodeType code, String value, String ref_id, List<Actions> actions) {
		this.type = type;
		this.message = new ErrorMessage(code, value, ref_id, actions);
	}

	public ErrorType getType() {
		return type;
	}

	public void setType(ErrorType type) {
		this.type = type;
	}

	public ErrorMessage getMessage() {
		return message;
	}

	public void setMessage(ErrorMessage message) {
		this.message = message;
	}
	
}
