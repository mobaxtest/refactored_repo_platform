package com.iprogames.platform.error;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum CommandType {

	NONE (""),
	BACK ("BACK"),
	CLOSE ("CLOSE"),
	ACK ("ACK"),
	LOGIN ("LOGIN"),
	BUYCHIPS ("BUYCHIPS"),
	LOBBY ("LOBBY"),
	RETRY ("RETRY"),
	CLEAR ("CLEAR"),
	ALLOWGPS ("ALLOWGPS"),
	DENYGPS ("DENYGPS");
	
	private final String value;
	
	CommandType(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
}
