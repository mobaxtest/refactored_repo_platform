/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.platform.error;

import static com.iprogames.platform.util.Constants.PLATFORM_MODULE_NAME;
import static com.iprogames.util.uuid.UUIDUtil.getReferenceId;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.iprogames.platform.exception.FundNotEnabledException;
import com.iprogames.platform.exception.GameNotActiveException;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.GPSNotEnabledONLegalException;
import com.iprogames.platform.exception.InvalidPasswordException;
import com.iprogames.platform.exception.InvalidProductException;
import com.iprogames.platform.exception.InvalidProviderException;
import com.iprogames.platform.exception.InvalidPurchaseIdException;
import com.iprogames.platform.exception.InvalidSearchOptionsException;
import com.iprogames.platform.exception.InvalidSessionException;
import com.iprogames.platform.exception.InvalidSkinException;
import com.iprogames.platform.exception.InvalidSocialTokenException;
import com.iprogames.platform.exception.InsufficientChipsException;
import com.iprogames.platform.exception.InvalidiProTokenException;
import com.iprogames.platform.exception.NoEventsFoundException;
import com.iprogames.platform.exception.PlayersExceedingException;
import com.iprogames.platform.exception.UserAlreadyRegisteredException;
import com.iprogames.platform.exception.UserNotFoundException;
import com.iprogames.util.constraints.exception.InvalidArgumentException;
import com.iprogames.util.constraints.exception.NullArgumentException;

@ControllerAdvice
public class iProGamesExceptionHandler extends ResponseEntityExceptionHandler {

	public iProGamesExceptionHandler() {
        super();
    }
	
	@ExceptionHandler({ InvalidiProTokenException.class, InvalidSessionException.class, 
		InvalidSocialTokenException.class, InvalidProviderException.class})
    public ResponseEntity<Object> handleAuthenticationErrorResponse(final Exception ex, final WebRequest request) {	
		Actions action = new Actions(ButtonType.OK, CommandType.LOGIN);
		ErrorMessage msg = new ErrorMessage(CodeType.AUTHENTICATION_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({InvalidAuthorizationException.class})
    public ResponseEntity<Object> handleAuthorizationnErrorResponse(final Exception ex, final WebRequest request) {	
		Actions action = new Actions(ButtonType.RETRY, CommandType.RETRY);
		ErrorMessage msg = new ErrorMessage(CodeType.AUTHENTICATION_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		action = new Actions(ButtonType.CANCEL, CommandType.CLEAR);
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({ InvalidPurchaseIdException.class, FundNotEnabledException.class, 
		InvalidProductException.class })
    public ResponseEntity<Object> handlePurchaseErrorResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.BUYCHIPS);
		ErrorMessage msg = new ErrorMessage(CodeType.PURCHASE_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({ UserAlreadyRegisteredException.class, 
		InvalidPasswordException.class, UserNotFoundException.class})
    public ResponseEntity<Object> handleInvalidResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.NONE);
		ErrorMessage msg = new ErrorMessage(CodeType.AUTHENTICATION_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({InvalidSkinException.class})
    public ResponseEntity<Object> handleNotFoundServerResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.CLOSE);
		ErrorMessage msg = new ErrorMessage(CodeType.AUTHENTICATION_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({GPSNotEnabledONLegalException.class})
    public ResponseEntity<Object> handleGPSNotFoundResponse(final Exception ex, final WebRequest request) {		
		ErrorMessage msg = new ErrorMessage(CodeType.LOCATION_ALERT, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(new Actions(ButtonType.ALLOW, CommandType.ALLOWGPS));
		msg.getActions().add(new Actions(ButtonType.DENY, CommandType.DENYGPS));
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({ IProGamesGeneralException.class})
    public ResponseEntity<Object> handleInternalServerResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.EXIT, CommandType.CLOSE);
		ErrorMessage msg = new ErrorMessage(CodeType.SYSTEM_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({NoEventsFoundException.class})
    public ResponseEntity<Object> handleGameServerAlertResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK);
		ErrorMessage msg = new ErrorMessage(CodeType.GAMESERVER_ALERT, ex.getMessage(),null);
		msg.getActions().add(action);
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.message, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
		
	@ExceptionHandler({GameNotActiveException.class, PlayersExceedingException.class})
    public ResponseEntity<Object> handleGameServerErrorResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.BACK);
		ErrorMessage msg = new ErrorMessage(CodeType.GAMESERVER_FAILURE, ex.getMessage(),getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({InvalidArgumentException.class, NullArgumentException.class})
    public ResponseEntity<Object> handleValidationErrorResponse(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.BACK);
		ErrorMessage msg = new ErrorMessage(CodeType.VALIDATION_FAILURE, ex.getMessage(),null);
		msg.getActions().add(action);
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({ InsufficientChipsException.class})
    public ResponseEntity<Object> handleInsufficientChipsException(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.NONE);
		ErrorMessage msg = new ErrorMessage(CodeType.GAMESERVER_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);		
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
	@ExceptionHandler({ InvalidSearchOptionsException.class})
    public ResponseEntity<Object> handleInvalidSearchOptionsException(final Exception ex, final WebRequest request) {
		Actions action = new Actions(ButtonType.OK, CommandType.NONE);
		ErrorMessage msg = new ErrorMessage(CodeType.GAMESERVER_FAILURE, ex.getMessage(), 
				getReferenceId(PLATFORM_MODULE_NAME));
		msg.getActions().add(action);		
		logger.error(msg.getRef_id() + " : " + msg.getValue());
		ex.printStackTrace();
        return handleExceptionInternal(ex, new ErrorResponse(ErrorType.error, msg), new HttpHeaders(), HttpStatus.OK, request);
    }
	
}
