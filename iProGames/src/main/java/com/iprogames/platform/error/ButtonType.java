package com.iprogames.platform.error;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum ButtonType {

	OK, //Ok button
	CONTINUE, //Continue Button
	CANCEL, // Cancel button
	RETRY, //Retry button
	ALLOW, //ALLOWGPS
	DENY, //DENYGPS
	EXIT; //Exit the app button
}
