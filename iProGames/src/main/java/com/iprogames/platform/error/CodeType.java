package com.iprogames.platform.error;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum CodeType {

	SYSTEM_FAILURE ("1001"),
	AUTHENTICATION_FAILURE ("1002"),
	PURCHASE_FAILURE ("1003"),
	GAMESERVER_FAILURE ("1004"),
	VALIDATION_FAILURE ("1005"),
	LOCATION_ALERT ("1020"),
	BALANCE_ALERT ("1021"),	
	GAMESERVER_ALERT ("1022");
	
	
	private final String value;
	
	CodeType(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
}
