package com.iprogames.platform.error;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum ErrorType {

	message, // alert message to the client 
	error, // error message to the client
	notification //
}
