package com.iprogames.platform.authentication.service;

import static com.iprogames.platform.util.Constants.CREDENTIAL_TYPE_SECRET;
import static com.iprogames.platform.util.Constants.CREDENTIAL_TYPE_TOKEN;
import static com.iprogames.platform.util.Constants.CREDENTIAL_TYPE_PASSWORD;
import static com.iprogames.platform.util.Utility.getTokens;
import static com.iprogames.platform.util.Utility.checkClient;
import static com.iprogames.util.uuid.UUIDUtil.generateUUIDString;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.ChangePlayModeRequest;
import com.iprogames.platform.authentication.common.ChangePlayModeResponse;
import com.iprogames.platform.authentication.common.Credential;
import com.iprogames.platform.authentication.common.ModeData;
import com.iprogames.platform.authentication.common.Gps;
import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.common.UserAccessMode;
import com.iprogames.platform.authentication.common.UserPlayMode;
import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.authentication.common.enums.AccessModeEnum;
import com.iprogames.platform.authentication.common.enums.ClientIdentityEnum;
import com.iprogames.platform.authentication.common.enums.OperatorEnum;
import com.iprogames.platform.authentication.common.enums.PlayModeEnum;
import com.iprogames.platform.authentication.common.enums.SkinEnum;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.authentication.dao.UserSessionDaoHandler;
import com.iprogames.platform.authentication.iprotoken.handler.IProTokenHandler;
import com.iprogames.platform.chippurchase.common.enums.ProductProviderEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.platform.exception.InvalidSessionException;
import com.iprogames.platform.exception.InvalidiProTokenException;
import com.iprogames.platform.exception.UserNotFoundException;
import com.iprogames.platform.operator.VerifyWithOperator;
import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.platform.wallettransaction.service.WalletTransactionService;
import com.iprogames.provider.geolocation.api.GeolocationHandler;
import com.iprogames.provider.geolocation.api.Location;
import com.iprogames.util.ipaddress.IPAddressUtil;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuthenticationService {

	static Logger logger = LogManager.getLogger(AuthenticationService.class);
	@SuppressWarnings("rawtypes")
	@Autowired
	UserSessionDaoHandler userSessionDao;

	@Autowired
	WalletTransactionService wallet;

	@Autowired
	VerifyWithOperator twitter;

	@Autowired
	VerifyWithOperator facebook;

	@Autowired
	VerifyWithOperator googleplus;

	@Autowired
	VerifyWithOperator mock;

	@Autowired
	GeolocationHandler quova;

	@Autowired
	GeolocationHandler google;

	@Autowired
	GeolocationHandler mockLocation;

	@Autowired
	IProTokenHandler authapi;

	@Resource
	MessageSource messageSource;

	/**
	 * Login the user as guest - Set the user to Soclal Mode in default. Update
	 * the users wallet with free chips and update the transaction table
	 */
	public AuthResponse guestLogin(AuthRequest authRequest) {
		String accType = UserAccountTypeEnum.GUST.name();
		boolean isUserExists = userSessionDao.checkIfUserExists(
				authRequest.getUdid(), authRequest.getSkin());
		UserSocialProfile profile = new UserSocialProfile(authRequest.getUdid(), "GUEST");
		String userUuid = null;
		if (isUserExists) {
			logger.info(" User Exists : ");
			userUuid = userSessionDao.overwriteExistingSession(authRequest.getUdid(), SkinEnum.GUST.name(), UserSessionStatusEnum.OVR);
		} else {
			logger.info(" User does not Exists : ");
			userUuid = userSessionDao.createAccount(authRequest, profile, accType);
		}

		AuthResponse response = userSessionDao.createSessionAndSetAccessMode(userUuid, profile, accType, 
				null, authRequest.getProvider(), authRequest.getSkin(), null);
		response.setAcctype(accType);
		response.setUsername(null);
		if (!isUserExists) {
			BigDecimal chips = userSessionDao
					.getChipsForProduct(ProductProviderEnum.IPROGST.name());
			CreditDebitChipsRequest request = new CreditDebitChipsRequest(
					FundTypeEnum.VCHP, chips, generateUUIDString(),
					TxnTypeEnum.FREE);
			wallet.creditChipsToUserAccount(request, response.getiProToken());
		}		
		UserWallet userWallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), response.getiProToken());
		logger.info("Balance Amount : " + userWallet.getBalanceAmount());
		userSessionDao.updateSession(response.getiProToken(), userWallet.getBalanceAmount());

		return response;
	}

	/** Verify the users location for legal */
	public AuthResponse verifyLocation(AuthRequest authRequest) {
		Location location = getLocation(authRequest.getGps(),
				authRequest.getClientIdentity());
		ModeData modeData = userSessionDao.getAccessModeFromLocation(location, null);
		/*
		 * TODO we have to set the accessmode and play mode in the auth response
		 * even when the app is launched. We are defaulting to specific play
		 * mode based on the configuration in the system.
		 */
		String accessMessage = messageSource.getMessage("MESSAGE_"
				+ modeData.getUserAccessMode().getMode(), null, null);
		String playMessage = messageSource.getMessage("MESSAGE_"
				+ modeData.getUserPlayMode().getMode(), null, null);
		return new AuthResponse(modeData.getUserAccessMode().getMode(),
				modeData.getUserPlayMode().getMode(), accessMessage,
				playMessage);
	}

	/*
	 * Verify the iProToken sent from the client Set the play mode based on the
	 * location
	 */
	public AuthResponse verifyiProToken(AuthRequest authRequest,
			boolean isLobbyData) {
		logger.debug("Auth token to verify : " + authRequest.getiProToken());
		SecureToken secureToken = authapi.getDecryptediProToken(authRequest
				.getiProToken());

		AuthResponse response = userSessionDao.validateiProToken(secureToken,
				authRequest.getiProToken(), isLobbyData);
		return response;
	}

	public AuthResponse keepSessionAlive(AuthRequest authRequest,
			AuthResponse response) {
		if (response != null) {
			logger.debug("Auth token to verify : " + authRequest.getiProToken());
			SecureToken secureTokenFromSession = authapi
					.getDecryptediProToken(response.getiProToken());
			SecureToken secureToken = authapi.getDecryptediProToken(authRequest
					.getiProToken());
			if (secureToken.getUid().equals(secureTokenFromSession.getUid())) {
				logger.info("token matches");
				return response;
			} else {
				logger.info("token not matching");
			}
			return userSessionDao.validateiProToken(secureToken,
					authRequest.getiProToken(), false);			
		} else {
			throw new InvalidSessionException(messageSource.getMessage(
					"SESSION_" + UserSessionStatusEnum.TMT, null, null));
		}
	}

	/*
	 * Verify the iProToken sent from the client Set the play mode based on the
	 * location
	 */
	public AuthResponse validateGPS(AuthRequest authRequest,
			boolean isLobbyData, AuthResponse response) {
		if (response != null) {
			logger.debug("Auth token to verify : " + authRequest.getiProToken());
			SecureToken secureTokenFromSession = authapi
					.getDecryptediProToken(response.getiProToken());
			SecureToken secureToken = authapi.getDecryptediProToken(authRequest
					.getiProToken());
			if (secureToken.getUid().equals(secureTokenFromSession.getUid())) {
				if (authRequest.getClientIdentity() != null) {
					Location location = getLocation(
							authRequest.getGps(),
							authRequest.getClientIdentity());
					ModeData modeData = null;
					if (location != null) {
						modeData = userSessionDao.getAccessModeFromLocation(location, authRequest.getProvider());
					} else {
						modeData = new ModeData(new UserAccessMode(AccessModeEnum.SLG.name(), messageSource.getMessage("MESSAGE_" + AccessModeEnum.SLG.name(), null, null)), 
								new UserPlayMode(PlayModeEnum.SOC.name(), messageSource.getMessage("MESSAGE_" + PlayModeEnum.SOC.name(), null, null)));
					}
					logger.debug("IPADDRESS FROM THE USER : "
							+ location.getIpaddress());
					userSessionDao.updateSessionAndSetAccessMode(secureToken, location, modeData);
					/*
					 * if (userAccessMode != null)
					 * if(userAccessMode.getOptionalMode() != null &&
					 * userAccessMode.getOptionalMode().length() > 0) {
					 * response.setType(ErrorType.notification);
					 * response.setMessage
					 * (getAlertMessage(userAccessMode.getOptionalMode(),
					 * userAccessMode.getOptionalMessage()));
					 * userAccessMode.setOptionalMode(null);
					 * userAccessMode.setOptionalMessage(null); }
					 * response.setUserAccessMode(userAccessMode);
					 */
					response.setUserAccessMode(modeData.getUserAccessMode());
					response.setUserPlayMode(modeData.getUserPlayMode());
				}
				return response;
			} else {
				throw new InvalidiProTokenException(messageSource.getMessage(
						"INVALID_TOKEN", null, null));
			}
		} else {
			throw new InvalidSessionException(messageSource.getMessage(
					"SESSION_" + UserSessionStatusEnum.TMT, null, null));
		}
	}

	/*
	 * Verifying the social token with the provider Create the session and set
	 * the play mode based on the location Update the wallet and transaction
	 * table for the free chips if applicable
	 */
	public AuthResponse verifySocialToken(AuthRequest authRequest) {
		UserSocialProfile userProfile = null;
		List<Credential> credentialList = authRequest.getCredential();

		switch (OperatorEnum.fromValue(authRequest.getProvider())) {
		case FB:
			userProfile = facebook.verifyToken(getTokens(credentialList,
					CREDENTIAL_TYPE_TOKEN));
			break;
		case GPLUS:
			userProfile = googleplus.verifyToken(getTokens(credentialList,
					CREDENTIAL_TYPE_TOKEN));
			break;
		case TWT:
			userProfile = twitter.verifyToken(
					getTokens(credentialList, CREDENTIAL_TYPE_TOKEN),
					getTokens(credentialList, CREDENTIAL_TYPE_SECRET));
			break;
		case MOCK:
			userProfile = mock.verifyToken(
					getTokens(credentialList, CREDENTIAL_TYPE_TOKEN),
					getTokens(credentialList, CREDENTIAL_TYPE_SECRET));
			break;
		default:
			break;
		}
		boolean isUserExists = userSessionDao.checkIfUserExists (userProfile.getUserId(), authRequest.getSkin());

		boolean isGuestUser = false;

		String accType = UserAccountTypeEnum.REG.name();
		Location location = null;
		ModeData modeData = null;
		if (authRequest.getClientIdentity().equalsIgnoreCase(ClientIdentityEnum.MOB.name()) &&
				authRequest.getGps() != null) {
			location = getLocation(authRequest.getGps(), authRequest.getClientIdentity());
			if (location != null) {
				modeData = userSessionDao.getAccessModeFromLocation(location, authRequest.getProvider());
			}
		}
		String userUuid = null;
		if (isUserExists) {
			logger.debug("User exists");
			userUuid = userSessionDao.overwriteExistingSession(userProfile.getUserId(), authRequest.getSkin(), UserSessionStatusEnum.OVR);
		} else {
			logger.debug("User not exist");
			if (authRequest.getUdid() != null)
				isGuestUser = verifyGuestUser(authRequest.getUdid());
			if (isGuestUser) {
				userUuid = userSessionDao.updateAccount(authRequest, userProfile, accType);
			} else {
				userUuid = userSessionDao.createAccount(authRequest, userProfile, accType);
			}
		}

		AuthResponse response = userSessionDao.createSessionAndSetAccessMode(userUuid, userProfile, accType, 
				location, authRequest.getProvider(), authRequest.getSkin(), modeData);

		if (response != null && response.getiProToken() != null)
			updateChipsAndTransaction(authRequest, response.getiProToken(),
					isUserExists, response.getUserAccessMode().getMode());

		UserWallet userWallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), response.getiProToken());
		logger.info("Balance Amount : " + userWallet.getBalanceAmount());
		userSessionDao.updateSession(response.getiProToken(), userWallet.getBalanceAmount());

		return response;
	}

	private boolean verifyGuestUser(String udid) {
		boolean isUserExists = userSessionDao.checkIfUserExists (udid, SkinEnum.GUST.name());
		if (isUserExists) {
			userSessionDao.overwriteExistingSession(udid, SkinEnum.GUST.name(), UserSessionStatusEnum.TMT);
		}

		return isUserExists;
	}

	/* Validating the iProToken */
	public AuthResponse validateiProTokenWithoutSession(String iProToken,
			boolean isLobbyData) {
		logger.debug("Auth token to verify : " + iProToken);
		SecureToken secureToken = authapi.getDecryptediProToken(iProToken);
		return userSessionDao.validateiProToken(secureToken, iProToken, false);
	}

	/* Validating the iProToken for internal purpose */
	public SecureToken validateiProTokenInternal(String iProToken) {
		SecureToken secureToken = authapi.getDecryptediProToken(iProToken);
		userSessionDao.validateiProToken(secureToken, iProToken, false);
		return secureToken;
	}

	/* Get the play mode */
	public String getAccessModeForUser(String iProToken) {
		logger.info("getaccessmode for user called");
		return userSessionDao.getAccessModeForUser(iProToken);
	}

	/* Get the play mode */
	public String getPlayModeForUser(String iProToken) {
		return PlayModeEnum.SOC.toString();
	}

	/* Verify the IPRO user name and password and create the session */
	public AuthResponse verifyiProAccount(AuthRequest authRequest) {
		List<Credential> credentialList = authRequest.getCredential();
		boolean isUserExists = userSessionDao.checkIfUserExists(
				authRequest.getUsername(), authRequest.getUseremail(),
				getTokens(credentialList, CREDENTIAL_TYPE_PASSWORD),
				authRequest.getSkin());
		if (!isUserExists) {
			throw new UserNotFoundException(messageSource.getMessage(
					"USER_NOT_FOUND_EXCEPTION", null, null));
		}

		if (authRequest.getUsername() == null) {
			String userName = userSessionDao.getUserNameFromEmailId(authRequest
					.getUseremail());
			authRequest.setUsername(userName);
		}

		String accType = UserAccountTypeEnum.REG.name();

		/*
		 * Guest users cannot be modified to a registered user if the ipro
		 * account already exists
		 */
		/* Terminate the guest user session abruptly */	
		if (authRequest.getUdid() != null) {
			userSessionDao.overwriteExistingSession(authRequest.getUdid(), SkinEnum.GUST.name(), UserSessionStatusEnum.TMT);
		}
		String userUuid = userSessionDao.overwriteExistingSession(authRequest.getUsername(), authRequest.getSkin(), UserSessionStatusEnum.OVR);
		logger.info("User UUID to be validated : " + userUuid);
		UserSocialProfile userProfile = new UserSocialProfile(authRequest.getUsername());
		ModeData modeData = null;
		Location location = null;
		if (authRequest.getClientIdentity().equalsIgnoreCase(ClientIdentityEnum.MOB.name()) &&
				authRequest.getGps() != null) {
			location = getLocation(authRequest.getGps(), authRequest.getClientIdentity());
			if (location != null) {
				modeData = userSessionDao.getAccessModeFromLocation(location, authRequest.getProvider());
			}
		}
		logger.debug("Got the location");
		AuthResponse response = userSessionDao.createSessionAndSetAccessMode(userUuid, userProfile, accType, 
				location != null ? location : null, authRequest.getProvider(), authRequest.getSkin(), modeData);
		logger.debug("iProToken : " + response.getiProToken());
		UserWallet userWallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), response.getiProToken());
		logger.info("Balance Amount : " + userWallet.getBalanceAmount());
		userSessionDao.updateSession(response.getiProToken(), userWallet.getBalanceAmount());

		return response;
	}

	/*
	 * Verify the IPRO user name and password; If not exists, create the
	 * account; If exists, throw exception
	 */
	public AuthResponse registeriProAccount(AuthRequest authRequest) {

		boolean isUserExists = userSessionDao.checkIfUserExists(
				authRequest.getUsername(), authRequest.getUseremail(),
				authRequest.getSkin());

		String accType = UserAccountTypeEnum.REG.name();
		UserSocialProfile userProfile = new UserSocialProfile(
				authRequest.getUsername());

		Location location = null;
		ModeData modeData = null;
		if (authRequest.getClientIdentity().equalsIgnoreCase(ClientIdentityEnum.MOB.name()) &&
				authRequest.getGps() != null) {
			location = getLocation(authRequest.getGps(), authRequest.getClientIdentity());
			if (location != null) {
				modeData = userSessionDao.getAccessModeFromLocation(location, authRequest.getProvider());
			}
		}
		String userUuid = null;

		logger.debug("User doesnot exist");
		boolean isGuestUser = false;
		if (authRequest.getUdid() != null) {
			logger.debug("Verify if the user id guest user");
			isGuestUser = verifyGuestUser(authRequest.getUdid());
		}
		if (isGuestUser) {
			logger.debug("Guest user ");
			userUuid = userSessionDao.updateAccount(authRequest, userProfile, accType);
		} else {
			logger.debug("Not a guest user ");
			/* If the user does not exist, create an account for the user */
			userUuid = userSessionDao.createAccount(authRequest, userProfile, accType);
			authRequest.setUdid(null);
		}

		/* Create the session and set the access mode for the user */
		AuthResponse response = userSessionDao.createSessionAndSetAccessMode(userUuid, userProfile, accType, location,
				authRequest.getProvider(), authRequest.getSkin(), modeData);

		if (response != null && response.getiProToken() != null)
			updateChipsAndTransaction(authRequest, response.getiProToken(),
					isUserExists, response.getUserAccessMode().getMode());

		UserWallet userWallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), response.getiProToken());
		logger.info("Balance Amount : " + userWallet.getBalanceAmount());
		userSessionDao.updateSession(response.getiProToken(), userWallet.getBalanceAmount());

		return response;

	}

	public void logout(AuthRequest authRequest, String statusStr) {
		userSessionDao.sessionTimeout(authRequest.getiProToken(), statusStr);
	}
	
	public ChangePlayModeResponse changePlayMode (ChangePlayModeRequest playModeRequest) {		
		SecureToken secureToken = validateiProTokenInternal(playModeRequest.getiProToken());
		
		Location location = null;
		ModeData modeData = null;
		if (playModeRequest.getClientIdentity().equalsIgnoreCase(ClientIdentityEnum.MOB.name()) &&
				playModeRequest.getGps() != null) {
			location = getLocation(playModeRequest.getGps(), playModeRequest.getClientIdentity());
			if (location != null) {
				modeData = userSessionDao.getAccessModeFromLocation(location, secureToken.getPid());
			}
		}
		
		if (modeData == null) {
			modeData = new ModeData(new UserAccessMode(AccessModeEnum.SLG.name(), messageSource.getMessage("MESSAGE_" + AccessModeEnum.SLG.name(), null, null)), 
					new UserPlayMode(PlayModeEnum.SOC.name(), messageSource.getMessage("MESSAGE_" + PlayModeEnum.SOC.name(), null, null)));
		}
		
		boolean isPlayModeApplicable = PlayModeMachine.isAllowedMode(Arrays.asList(modeData.getUserAccessMode().getMode(), 
				modeData.getUserPlayMode().getMode()), playModeRequest.getUserPlayMode().getMode());
		
		if (isPlayModeApplicable) {
			modeData.setUserPlayMode(playModeRequest.getUserPlayMode());
		}
		return userSessionDao.updateSessionAndSetAccessMode(secureToken, location, modeData);
	}

	/* Check for the location with the providers */
	/*private Location getLocation(Gps gps, String clientIdentity) {
		String remoteAddress = IPAddressUtil
				.getAddressFromRequest(((ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes()).getRequest());
		logger.info("***************************************Remote address for the user : "
				+ remoteAddress);
		Location location = null;
		// TODO Code has to check for the client Type and determine the location
		// data. If it is mobile then use the gps data or if desktop use the ipaddress if not either of them then the locaiton
		// will be null.
		ClientIdentityEnum clientType = checkClient(clientIdentity);
		if (clientType == ClientIdentityEnum.MOB) {
			if (gps != null) {
				location = google.getLocationJson(gps.getLatitude(), gps.getLongitude());
				if (location != null)
					location.setIpaddress(remoteAddress);
			} 
		} else if(clientType.equals("DESK")){
			location = quova.getLocationXml(remoteAddress);
		}
		return location;
	}	*/

	/* Check for the location with the providers */
	private Location getLocation(Gps gps, String clientIdentity) {
		String remoteAddress = IPAddressUtil
				.getAddressFromRequest(((ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes()).getRequest());

		/*
		 * TODO we have to check the client identify for mobile or desktop
		 * device so we can do the proper check for the location. Client
		 * identify should be a encrypted data from the client and the server
		 * and client uses the public private model to encrypt and decrypt the
		 * data. This client identity key can be attached to the mobile app as
		 * part of the app bundle proces We need to prevent the key from leaking
		 * to anybody. if(Clientidenty is Desktop) follow the ip check process
		 * else if(Client identity is Mobile) follow the mobile gps check
		 * process
		 */
		Location location = null;
		ClientIdentityEnum clientType = checkClient(clientIdentity);
		if (clientType == ClientIdentityEnum.MOB) {
			if (gps != null) {
				location = google.getLocationJson(gps.getLatitude(), gps.getLongitude());
				if (location != null) {
					location.setIpaddress(remoteAddress);
				}
			}
		}else if(clientType == ClientIdentityEnum.DESK) {
			logger.info("*************************************** Remote address for the user : "
					+ remoteAddress);
			location = quova.getLocationXml(remoteAddress);

		}
		return location;
	}

	/*
	 * Set the play mode and update the free chips for the user if any and the
	 * update the transaction table
	 */
	public void updateChipsAndTransaction(AuthRequest authRequest,
			String iProToken, boolean isUserExists, String accessMode) {
		if (accessMode.equalsIgnoreCase(AccessModeEnum.SLG.name())) {
			String provider = null;
			if (!isUserExists && authRequest.getUdid() != null) {
				provider = ProductProviderEnum.IPROGREG.name();
			} else if (!isUserExists) {
				provider = ProductProviderEnum.IPROREG.name();
			}

			if (provider != null) {
				BigDecimal chips = userSessionDao.getChipsForProduct(provider);
				CreditDebitChipsRequest request = new CreditDebitChipsRequest(
						FundTypeEnum.VCHP, chips, generateUUIDString(),
						TxnTypeEnum.FREE);
				wallet.creditChipsToUserAccount(request, iProToken);
			}
		}
	}
}