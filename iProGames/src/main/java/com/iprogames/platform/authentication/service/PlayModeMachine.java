package com.iprogames.platform.authentication.service;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.iprogames.platform.authentication.common.enums.AccessModeEnum;
import com.iprogames.platform.authentication.common.enums.PlayModeEnum;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 */
public class PlayModeMachine {

	static Logger logger = LogManager.getLogger(PlayModeMachine.class);
	
	private static Map<List<String>, EnumSet<?>[]> nextmodeMap;
	
	static {
		
		nextmodeMap = new HashMap<List<String>, EnumSet<?>[]>();
		nextmodeMap.put(Arrays.asList(AccessModeEnum.RMG.name(), PlayModeEnum.REAL.name()), new EnumSet[] { EnumSet.of(PlayModeEnum.REAL), EnumSet.of(PlayModeEnum.FAN), EnumSet.of(PlayModeEnum.SOC)});
		nextmodeMap.put(Arrays.asList(AccessModeEnum.SLG.name(), PlayModeEnum.FAN.name()), new EnumSet[] { EnumSet.of(PlayModeEnum.FAN), EnumSet.of(PlayModeEnum.SOC)});
		nextmodeMap.put(Arrays.asList(AccessModeEnum.SLG.name(), PlayModeEnum.SOC.name()), new EnumSet[] { EnumSet.of(PlayModeEnum.SOC)});
		
	}

	/**
	 * check of the next mode is a valid mode
	 * @param mode
	 * @param desiredMode
	 * @return
	 */
	public static boolean isAllowedMode(List<String> mode, String desiredMode) {
		boolean isReachable = false;
		if (nextmodeMap.get(mode) != null) {
			for (EnumSet<?> s : nextmodeMap.get(mode)) {
				if (s.iterator().hasNext()) {
					logger.debug("Status code : " + s.iterator().next().name());
					if (s.iterator().next().name().equalsIgnoreCase(desiredMode)) {
						isReachable = true;
						break;
					}
				}
			}
		}
		
		return isReachable;
	}
}