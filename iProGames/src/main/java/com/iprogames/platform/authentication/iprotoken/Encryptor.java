/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.platform.authentication.iprotoken;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.codec.Base64;

public class Encryptor {
	private static final String provider = "AES";
	
	public static byte[] keybytes = new byte[16];
	private static SecretKeySpec spec = null;
	static {
		try {
			SecretKey key = null;
			KeyGenerator kgen = KeyGenerator.getInstance(provider);
			kgen.init(128);
			key = kgen.generateKey();
			keybytes = key.getEncoded();			
			spec = new SecretKeySpec(keybytes, provider);
		} catch (Exception e) {

		}
	}
	
	public static String getEncryptedString(String info) {
		try {			
			Cipher cipher = Cipher.getInstance(provider);
			cipher.init(Cipher.ENCRYPT_MODE, spec);
			
            byte[] encodedText = Base64.encode(cipher.doFinal(info.getBytes()));
            return new String(encodedText, "US-ASCII");
            
		} catch (Exception e) {
			// Nothing to do
		}
		return null;
	}
	
	public static String decrypt(String encodedtext) {
		try {			
			Cipher cipher = Cipher.getInstance(provider);
			cipher.init(Cipher.DECRYPT_MODE, spec);			
			return new String(cipher.doFinal(Base64.decode(encodedtext.getBytes()))); 
		} catch (Exception e) {
			//Nothing to do
		}
		return null;
	}
}
