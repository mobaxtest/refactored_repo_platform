package com.iprogames.platform.authentication.common.enums;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
public enum AccessModeEnum {

	RMG ("RMG"), // Real Money Jurisdiction
	SLG ("SLG"); // Social Jurisdiction
	
	private final String value;
	
	AccessModeEnum(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
	public static AccessModeEnum fromValue(String v) {
        for (AccessModeEnum c: AccessModeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
