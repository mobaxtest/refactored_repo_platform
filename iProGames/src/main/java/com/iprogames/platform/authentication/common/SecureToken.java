package com.iprogames.platform.authentication.common;

import com.iprogames.platform.admin.common.ReconcileRequest;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 */
public class SecureToken {

	private String uuid	= null;
	private String uid	= null;
	private String pid	= null;
	private String sid	= null;
	private String exp	= null;
	private String accessmode = null;
	private String playmode = null;
	private int sesid = 0;
	
	public SecureToken() {
		
	}
	
	public SecureToken(ReconcileRequest request) {
		this.uuid = request.getUuid();
		this.uid = request.getUsername();
		this.pid = request.getProvider();
		this.sid = request.getSkin();	
	}
	
	public SecureToken(String uuid,String uid, String pid, String sid, String exp, String accessmode, String playmode, int sesid) {
		this.uuid = uuid;
		this.uid = uid;
		this.pid = pid;
		this.sid = sid;
		this.exp = exp;
		this.accessmode = accessmode;
		this.playmode = playmode;
		this.sesid = sesid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getAccessmode() {
		return accessmode;
	}

	public void setAccessmode(String accessmode) {
		this.accessmode = accessmode;
	}
	public String getPlaymode() {
		return playmode;
	}

	public void setPlaymode(String mode) {
		this.playmode = mode;
	}

	/**
	 * @return the sesid
	 */
	public int getSesid() {
		return sesid;
	}

	/**
	 * @param sesid the sesid to set
	 */
	public void setSesid(int sesid) {
		this.sesid = sesid;
	}

	@Override
	public String toString() {
		return "SecureToken [uuid=" + uuid + ", uid=" + uid + ", pid=" + pid
				+ ", sid=" + sid + ", exp=" + exp + ", accessmode="
				+ accessmode + ", playmode=" + playmode + "]";
	}
}
