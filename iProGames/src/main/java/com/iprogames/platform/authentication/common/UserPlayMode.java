package com.iprogames.platform.authentication.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class UserPlayMode {

	private String mode				= null;
	private String message			= null;

	public UserPlayMode() {
	}

	public UserPlayMode(String mode) {
		this.mode = mode;
	}
	public UserPlayMode(String mode, String message) {
		this.mode = mode;
		this.message = message;
	}
		
	public String getMode() {
		return mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "UserPlayMode [mode=" + mode + ", message=" + message + "]";
	}
}
