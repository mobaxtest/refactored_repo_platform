package com.iprogames.platform.authentication.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum OperatorEnum {

	IPRO ("IPRO"),
	FB ("FB"), 
	GPLUS ("G+"), 
	TWT ("TWT"),
	GUST ("GUST"), 
	MOCK ("MOCK");
	
	private final String value;
	
	OperatorEnum(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
	public static OperatorEnum fromValue(String v) {
        for (OperatorEnum c: OperatorEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
