package com.iprogames.platform.authentication.common;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ModeData {

    private UserAccessMode accessMode;
    private UserPlayMode playMode;

    
    public ModeData() {
     }

    public ModeData(UserAccessMode accessMode, UserPlayMode playMode){
    	this.accessMode = accessMode;
    	this.playMode = playMode;
    }

	public UserAccessMode getUserAccessMode() {
		return accessMode;
	}

	public void setUserAccessMode(UserAccessMode accessMode) {
		this.accessMode = accessMode;
	}

	public UserPlayMode getUserPlayMode() {
		return playMode;
	}

	public void setUserPlayMode(UserPlayMode playMode) {
		this.playMode = playMode;
	}

	@Override
	public String toString() {
		return "ModeData [accessMode=" + accessMode.toString() + ", playMode=" + playMode.toString()
				+ "]";
	}

}
