package com.iprogames.platform.authentication.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class ChangePlayModeResponse {

	private String iProToken				= null;
	private UserAccessMode userAccessMode	= null;
	private UserPlayMode userPlayMode		= null;
	
	public ChangePlayModeResponse() {
	}
	
	public ChangePlayModeResponse(String iProToken, UserAccessMode userAccessMode, UserPlayMode userPlayMode) {
		this.iProToken = iProToken;
		this.userAccessMode = userAccessMode;
		this.userPlayMode = userPlayMode;
	}

	/**
	 * @return the iProToken
	 */
	public String getiProToken() {
		return iProToken;
	}

	/**
	 * @param iProToken the iProToken to set
	 */
	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}	
	
	/**
	 * @return the userAccessMode
	 */
	public UserAccessMode getUserAccessMode() {
		return userAccessMode;
	}

	/**
	 * @param userAccessMode the userAccessMode to set
	 */
	public void setUserAccessMode(UserAccessMode userAccessMode) {
		this.userAccessMode = userAccessMode;
	}

	/**
	 * @return the userPlayMode
	 */
	public UserPlayMode getUserPlayMode() {
		return userPlayMode;
	}

	/**
	 * @param userPlayMode the userPlayMode to set
	 */
	public void setUserPlayMode(UserPlayMode userPlayMode) {
		this.userPlayMode = userPlayMode;
	}

	/**
	 * @return ChangePlayMode as String
	 */
	@Override
	public String toString() {
		return "ChangePlayMode [iProToken=" + iProToken + "]";
	}

}
