package com.iprogames.platform.authentication.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.ChangePlayModeRequest;
import com.iprogames.platform.authentication.common.ChangePlayModeResponse;
import com.iprogames.platform.authentication.common.LobbyData;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.authentication.validation.Validation;
import com.iprogames.platform.chippurchase.service.ReconcileTxn;
import com.iprogames.platform.profiler.Monitorable;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Controller
@RequestMapping("/auth")
public class AuthenticationController {
	
	static Logger logger = LogManager.getLogger(AuthenticationController.class);
	
	private AuthenticationService service;
	
	@Autowired
    public AuthenticationController(AuthenticationService service) {
        this.service = service;
    }
	
	@Autowired
	Validation authValidation;

	@Autowired
	ReconcileTxn reconcileTxn;
	
	/**
	 * Verify the location before login
	 * @param authRequest
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/verifylocation", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse verifyLocation(@RequestBody AuthRequest authRequest) {
		logger.info("verifyLocation called");
		authValidation.checkValidateLocation(authRequest);
		return service.verifyLocation(authRequest);
	}
	
	/**
	 * Create the guest user and in default set the mode as SOCIAL
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/guestlogin", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse guestLogin(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		AuthResponse response = service.guestLogin(authRequest);		
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		return response;
	}	
	
	/**
	 * Verify the iprotoken for validity - set the play mode verifying the location
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/verifyiprotoken", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse verifyiProToken(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		logger.info("verifyiProToken called");
		AuthResponse response = service.verifyiProToken(authRequest, true);		
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		/* Reconcile the transaction on login */
		reconcileTxn.setTxn(response.getiProToken(), authRequest.getTxn());
		reconcileTxn.startProcess();
		return response;
	}

	/**
	 * Verify the social access tokens - facebook, twitter, google+ 
	 * for validity and create the session for the user
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/verifysocialtoken", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse verifySocialToken(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		logger.info("Verifying the social Token");
		logger.debug("----------------------------UDID form the client ------------------- : " + authRequest.getUdid());
		AuthResponse response = service.verifySocialToken(authRequest);
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		/* Reconcile the transaction on login */
		reconcileTxn.setTxn(response.getiProToken(), authRequest.getTxn());
		reconcileTxn.startProcess();
		return response;
	}

	/**
	 * Verify IPRO account - check username and password  
	 * for validity; If exists, create the session for the user 
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/verify/iproaccount", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse verifyiProAccount(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		logger.info("Verifying the iPro Account");
		AuthResponse response = service.verifyiProAccount(authRequest);
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		/* Reconcile the transaction on login */
		reconcileTxn.setTxn(response.getiProToken(), authRequest.getTxn());
		reconcileTxn.startProcess();
		return response;
	}

	/**
	 * Verify IPRO account - check username and password  
	 * for validity; If exists, create the session for the user 
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/register/iproaccount", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse registeriProAccount(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		logger.info("Verifying the iPro Account");
		AuthResponse response = service.registeriProAccount(authRequest);		
		logger.debug("User name : " + response.getUsername());
		logger.debug("IPRO TOKEN : " + response.getiProToken());
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		return response;
	}
	
	/**
	 * Verify the iprotoken for validity and check for the 
	 * location and update the play mode based on the location
	 * if network or gps data is sent as a part of the reques
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/validategps", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse validateGPS(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		logger.info("validateGPS called");
		AuthResponse response = service.validateGPS(authRequest, false, (AuthResponse)httpSession.getAttribute("authResponse"));
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		return response;
	}
	
	/**
	 * Keep the session alive for the user
	 * @param authRequest
	 * @param httpSession
	 * @return AuthResponse
	 */
	@Monitorable
	@RequestMapping(value = "/keepsessionalive", method = RequestMethod.POST)
	@ResponseBody
	public AuthResponse keepAlive(@RequestBody AuthRequest authRequest,HttpSession httpSession) {
		logger.info("keepAlive called");
		AuthResponse response = service.keepSessionAlive(authRequest, (AuthResponse)httpSession.getAttribute("authResponse"));
		httpSession.setAttribute("authResponse", new AuthResponse(response.getUsername(), response.getiProToken(), false));
		return response;
	}
	
	/**
	 * Get the dummy lobby data
	 * @param authRequest
	 * @return LobbyData
	 */
	@Monitorable
	@RequestMapping(value = "/getlobbydata", method = RequestMethod.POST)
	@ResponseBody
	public LobbyData getLobbyData(@RequestBody AuthRequest authRequest) {
		logger.info("GET Lobby data request ************************");
		service.validateiProTokenWithoutSession(authRequest.getiProToken(), false);
		logger.info("validatewihout session completed");
		String mode = service.getAccessModeForUser(authRequest.getiProToken());
		if (mode != null) {
			logger.debug("Mode ************************ : " + mode);
			return new LobbyData(mode);
		}
		return new LobbyData();
	}
	
	/**
	 * Logout the user
	 * @param authRequest
	 * @param httpSession
	 */
	@Monitorable
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public void logout(@RequestBody AuthRequest authRequest, HttpSession httpSession) {
		service.logout(authRequest, UserSessionStatusEnum.LOG.name());
		httpSession.invalidate();
	}
	
	/**
	 * 
	 * @param playModeRequest
	 * @param httpSession
	 * @return
	 */
	@Monitorable
	@RequestMapping(value = "/change/playmode", method = RequestMethod.POST)
	public ChangePlayModeResponse changePlayMode(@RequestBody ChangePlayModeRequest playModeRequest, HttpSession httpSession) {
		return service.changePlayMode(playModeRequest);		
	}
}
