package com.iprogames.platform.authentication.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class ChangePlayModeRequest {

	private String iProToken			= null;
	private UserPlayMode userPlayMode	= null;
	private Gps gps						= null;
	private String clientIdentity 		= null;
	
	public ChangePlayModeRequest() {
	}

	/**
	 * @return the iProToken
	 */
	public String getiProToken() {
		return iProToken;
	}

	/**
	 * @param iProToken the iProToken to set
	 */
	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	
	
	/**
	 * @return the userPlayMode
	 */
	public UserPlayMode getUserPlayMode() {
		return userPlayMode;
	}

	/**
	 * @param userPlayMode the userPlayMode to set
	 */
	public void setUserPlayMode(UserPlayMode userPlayMode) {
		this.userPlayMode = userPlayMode;
	}

	/**
	 * @return the gps
	 */
	public Gps getGps() {
		return gps;
	}

	/**
	 * @param gps the gps to set
	 */
	public void setGps(Gps gps) {
		this.gps = gps;
	}

	/**
	 * @return the clientIdentity
	 */
	public String getClientIdentity() {
		return clientIdentity;
	}

	/**
	 * @param clientIdentity the clientIdentity to set
	 */
	public void setClientIdentity(String clientIdentity) {
		this.clientIdentity = clientIdentity;
	}

	/**
	 * @return ChangePlayMode as String
	 */
	@Override
	public String toString() {
		return "ChangePlayMode [iProToken=" + iProToken + ", clientIdentity=" + clientIdentity + "]";
	}

}
