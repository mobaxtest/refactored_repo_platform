package com.iprogames.platform.authentication.validation;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.util.constraints.Condition;
import com.iprogames.util.constraints.exception.InvalidArgumentException;
import com.iprogames.util.constraints.exception.NullArgumentException;

@Service("authValidation")
public class Validation {

	@Resource
	MessageSource messageSource;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notNullCondition;
	
	@SuppressWarnings("unchecked")
	public void checkValidateLocation(AuthRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getClientIdentity());			
		} catch (InvalidArgumentException e) {
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkValidateAuthRequest(AuthRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getClientIdentity());
		} catch (InvalidArgumentException e) {
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
}