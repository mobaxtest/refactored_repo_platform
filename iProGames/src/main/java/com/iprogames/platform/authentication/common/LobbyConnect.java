package com.iprogames.platform.authentication.common;

import static com.iprogames.platform.util.Constants.LOBBY_NAME;
import static com.iprogames.platform.util.Constants.LOBBY_PORT;
import static com.iprogames.platform.util.Constants.LOBBY_SERVER;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class LobbyConnect {

    private String lobbyserver;
    private String lobbyport;
    private String name;
    
    public LobbyConnect() {
    	this.lobbyserver = LOBBY_SERVER;
    	this.lobbyport = LOBBY_PORT;
    	this.name = LOBBY_NAME;
    }

    public String getLobbyserver() {
        return lobbyserver;
    }

    public void setLobbyserver(String lobbyserver) {
        this.lobbyserver = lobbyserver;
    }

    public String getLobbyport() {
        return lobbyport;
    }

    public void setLobbyport(String lobbyport) {
        this.lobbyport = lobbyport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	@Override
	public String toString() {
		return "LobbyConnect [lobbyserver=" + lobbyserver + ", lobbyport="
				+ lobbyport + ", name=" + name + "]";
	}
}
