package com.iprogames.platform.authentication.session;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.authentication.dao.UserSessionDaoHandler;
import com.iprogames.util.session.context.ApplicationContextUtils;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@WebListener
@Component
public class IProServerSessionListener implements HttpSessionListener {
	
	static final Logger logger = LogManager.getLogger(IProServerSessionListener.class);           
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (applicationContext instanceof WebApplicationContext) {
            ((WebApplicationContext) applicationContext).getServletContext().addListener(this);
        } else {
            //Either throw an exception or fail gracefully, up to you
            throw new RuntimeException("Must be inside a web application context");
        }
    }
	/**
	 * Catch sessionDestroyed from the container and cleanup the user
	 * session in the database.
	 */    
    
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

		logger.debug("The sesson destroyed is called by the container");
		ApplicationContext appContext = ApplicationContextUtils.getApplicationContext();

		SessionFactory sessionFactory = (SessionFactory) appContext
				.getBean("sessionFactory");
		
		HttpSession session = httpSessionEvent.getSession();

		if (session.getAttribute("authResponse") != null) {
			AuthResponse response = (AuthResponse) session
					.getAttribute("authResponse");
			if (response != null && response.getUsername() != null && response.getUsername().length() > 0 && 
					response.getiProToken() != null && response.getiProToken().length() > 0) {
				logger.debug("The userid for the session to be distroyed is: "
						+ response.getUsername());			
	
				@SuppressWarnings("rawtypes")
				UserSessionDaoHandler<?> userSessionDao = new UserSessionDaoHandler(
						sessionFactory);
				
				userSessionDao.sessionTimeout(response.getiProToken(), 
						UserSessionStatusEnum.TMT.name());
				logger.debug("The session: "+session.getId() +" is destroyed for userId : " + response.getUsername());
			}
		}
	}

	public void sessionCreated(HttpSessionEvent httpSessionEvent) {

		logger.debug("HttpSession is created "
				+ httpSessionEvent.getSession().getId());
	}
}
