/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
package com.iprogames.platform.authentication.iprotoken.handler;

import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.iprotoken.Encryptor;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.InvalidiProTokenException;

@Service
public class IProTokenHandler {

	@Resource
	MessageSource messageSource;
	
	public String getEncryptediProToken(SecureToken token){
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
			String ObjectString = mapper.writeValueAsString(token);
			return Encryptor.getEncryptedString(ObjectString);
		} catch (Exception e) {
			// Nothing to do
			e.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION", null, null));
		}
	}

	public SecureToken getDecryptediProToken(String iProToken) {
		try {
			String decryptedText = Encryptor.decrypt(iProToken);
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
			return mapper.readValue(decryptedText, SecureToken.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidiProTokenException(messageSource.getMessage("INVALID_TOKEN", null, null));
		}
	}
}
