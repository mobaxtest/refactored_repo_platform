package com.iprogames.platform.authentication.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.iprogames.platform.chippurchase.common.Transaction;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class AuthRequest {

	private String udid 				= null;
	private String useremail			= null;
    private String username				= null;
    private String iProToken			= null;
    private List<Credential> credential = new ArrayList<Credential>();
    private String provider				= null;
    private String skin					= null;
    private Gps gps						= null;
    private Transaction txn				= null;
    private String clientIdentity 		= null;

    public AuthRequest() {
    	
    }
    
    public AuthRequest(String iProToken) {
        this.iProToken = iProToken;
    }
    
    public String getUdid() {
		return udid;
	}
	public void setUdid(String udid) {
		this.udid = udid;
	}
	public String getUseremail() {
		return useremail;
	}
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}
	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public List<Credential> getCredential() {
        return credential;
    }

    public void setCredential(List<Credential> credential) {
        this.credential = credential;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public Gps getGps() {
        return gps;
    }

    public void setGps(Gps gps) {
        this.gps = gps;
    }

	public Transaction getTxn() {
		return txn;
	}

	public void setTxn(Transaction txn) {
		this.txn = txn;
	}

	public String getClientIdentity() {
		return clientIdentity;
	}

	public void setClientIdentity(String clientIdentity) {
		this.clientIdentity = clientIdentity;
	}

	@Override
	public String toString() {
		return "AuthRequest [udid=" + udid + ", useremail=" + useremail
				+ ", username=" + username + ", iProToken=" + iProToken
				+ ", credential=" + credential.toString() + ", provider=" + provider
				+ ", skin=" + skin + ", gps=" + gps.toString()
				+ ", txn=" + txn.toString() + ", clientIdentity=" + clientIdentity + "]";
	}
	
}
