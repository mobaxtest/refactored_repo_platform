package com.iprogames.platform.authentication.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum PlayModeEnum {
	REAL, SOC, FAN
}
