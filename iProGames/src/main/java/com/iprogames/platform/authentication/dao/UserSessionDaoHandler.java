package com.iprogames.platform.authentication.dao;

import static com.iprogames.platform.util.Constants.IPROGAMES_SESSION_EXPIRATION;
import static com.iprogames.platform.util.Constants.CREDENTIAL_TYPE_PASSWORD;
import static com.iprogames.platform.util.Utility.getTokens;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.hazelcast.core.IMap;
import com.iprogames.platform.authentication.common.AuthRequest;
import com.iprogames.platform.authentication.common.AuthResponse;
import com.iprogames.platform.authentication.common.ChangePlayModeResponse;
import com.iprogames.platform.authentication.common.ModeData;
import com.iprogames.platform.authentication.common.UserAccessMode;
import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.common.UserPlayMode;
import com.iprogames.platform.authentication.common.UserSocialProfile;
import com.iprogames.platform.authentication.common.enums.AccessModeEnum;
import com.iprogames.platform.authentication.common.enums.OperatorEnum;
import com.iprogames.platform.authentication.common.enums.PlayModeEnum;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.authentication.common.enums.UserStatusEnum;
import com.iprogames.platform.authentication.iprotoken.handler.IProTokenHandler;
import com.iprogames.platform.dao.SessionFactoryHandler;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.InvalidPasswordException;
import com.iprogames.platform.exception.InvalidProviderException;
import com.iprogames.platform.exception.InvalidSessionException;
import com.iprogames.platform.exception.InvalidSkinException;
import com.iprogames.platform.exception.InvalidiProTokenException;
import com.iprogames.platform.exception.UserAlreadyRegisteredException;
import com.iprogames.platform.hazelcast.common.HazelUserInfo;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.provider.geolocation.api.Location;
import com.iprogames.util.ipaddress.IPAddressUtil;
import com.iprogames.util.model.IproAccessMode;
import com.iprogames.util.model.IproCountry;
import com.iprogames.util.model.IproFundType;
import com.iprogames.util.model.IproLegalCountryState;
import com.iprogames.util.model.IproSkin;
import com.iprogames.util.model.IproUser;
import com.iprogames.util.model.IproUserAccountType;
import com.iprogames.util.model.IproUserDetail;
import com.iprogames.util.model.IproUserSession;
import com.iprogames.util.model.IproUserSessionStatus;
import com.iprogames.util.model.IproUserStatus;
import com.iprogames.util.model.IproUserVerification;
import com.iprogames.util.timestamp.TimeStampUtil;
import com.iprogames.util.uuid.UUIDUtil;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */

@Repository("userSessionDao")
public class UserSessionDaoHandler<T extends Serializable> extends SessionFactoryHandler {
	
	Logger logger = LogManager.getLogger(UserSessionDaoHandler.class);
	
	@Resource
	MessageSource messageSource;
	
	@Autowired
	IProTokenHandler authapi;
	private IMap<String, HazelUserInfo> mapUserDetails;
	public UserSessionDaoHandler() {
		super();
	}
	public UserSessionDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	/* Validate the iProToken for 'ACTIVE' state and if not throw the exception */
	public AuthResponse validateiProToken(SecureToken secureToken, String iProToken, boolean isLobbyData) {
		Session session = getCurrentSession();
		Transaction tx = null;
		AuthResponse response = null;
		HazelUserInfo hzUserInfo = new HazelUserInfo();
		try {
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.append("select t from IproUserSession t, IproUser u, IproSkin s, IproOperator o where ");
			queryString.append("t.providerAuthToken=:iProToken ");
			queryString.append("and hex(t.iproUser.id)=hex(u.userUuid) and s.skinCode = u.iproSkin.id ");
			queryString.append("and s.iproOperator.id = o.operatorCode ");
			queryString.append("and o.operatorCode=:operatorCode ");
			queryString.append("and s.skinCode=:skincode ");
			queryString.append("and hex(u.userUuid)=:useruuid ");
			queryString.append("and u.userName=:userid order by t.sessionId desc");
			Query query = session.createQuery(queryString.toString());
			query.setMaxResults(1);
	        query.setParameter("operatorCode", secureToken.getPid());
	        query.setParameter("skincode", secureToken.getSid());
	        query.setParameter("useruuid", secureToken.getUuid());
	        query.setParameter("userid", secureToken.getUid());
	        query.setParameter("iProToken", iProToken);
	        
	        IproUserSession userSession = (IproUserSession)query.uniqueResult();
	        if (userSession != null) {
	        	if(userSession.getIproUserSessionStatus().getSessionStatusCode().equalsIgnoreCase(UserSessionStatusEnum.ACT.name())) {	        		
	        		userSession.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
	        		session.saveOrUpdate(userSession);
	        		response = new AuthResponse(userSession.getIproUser().getUserName(), iProToken, 
	        				userSession.getIproUser().getIproUserAccountType().getAccountTypeCode(),isLobbyData);
	        		hzUserInfo.setUserAccssMode(userSession.getIproAccessMode() != null ? userSession.getIproAccessMode().getAccessMode() : null);
	        		logger.debug("Token :" + iProToken);
	        		logger.debug("User Name for this token :" + userSession.getIproUser().getUserName());
	        	} else {
	        		throw new InvalidSessionException(messageSource.getMessage("SESSION_"+ userSession.getIproUserSessionStatus().getSessionStatusCode(), 
	        				null, null));
	        	}
	        } else {
	        	throw new InvalidiProTokenException(messageSource.getMessage("INVALID_TOKEN", null, null));
	        }
	        tx.commit();
		} catch (InvalidiProTokenException ex) {
			if (tx != null)
				tx.rollback();
			throw new InvalidiProTokenException(ex.getMessage());
		} catch (InvalidSessionException ex) {
			if (tx != null)
				tx.rollback();
			throw new InvalidSessionException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		logger.info("**************************************************** initialize map  validateiProToken");
		mapUserDetails =  getHazelcastInstance().getMap("iprouserdetails");		
		mapUserDetails.put(response.getiProToken(),hzUserInfo);
        return response;
	}	
	
	public String createAccount(AuthRequest authRequest, UserSocialProfile userProfile, String accType) {
		Session session = getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			verifySkin(authRequest.getSkin(), session);
			UUID user_uuid = UUIDUtil.generateUUID();
			IproUser iprouser = constructUser(userProfile, user_uuid, authRequest.getSkin(), UserStatusEnum.ACTV.name(), accType);
			session.saveOrUpdate(iprouser);
			if (authRequest.getProvider().equals(OperatorEnum.IPRO.name())) {
				IproUserDetail userDetails = constructUserDetail(iprouser, authRequest.getUseremail(), getTokens(authRequest.getCredential(), CREDENTIAL_TYPE_PASSWORD));
				session.saveOrUpdate(userDetails);
			}
			tx.commit();
			return getUUIDString(user_uuid);
		} catch (InvalidSkinException ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new InvalidSkinException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public String updateAccount (AuthRequest authRequest, UserSocialProfile userProfile, String accType) {
		Session session = getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("select u from IproUser u where u.userName=:userName")
								.setParameter("userName", authRequest.getUdid())
						        .setMaxResults(1);
				
			IproUser iProUser = (IproUser)query.uniqueResult();

			iProUser.setUserName(userProfile.getUserId());
			iProUser.setDisplayName(userProfile.getDisplayName());
			IproUserAccountType acctype = new IproUserAccountType();
			acctype.setAccountTypeCode(accType);
			logger.debug("**************************************IPRO_USER_ACCOUNT_TYPE : " + accType);
			iProUser.setIproUserAccountType(acctype);
			IproSkin iproSkin = new IproSkin();
			logger.debug("**************************************IPRO_SKIN : " + authRequest.getSkin());
			iproSkin.setSkinCode(authRequest.getSkin());
			iProUser.setIproSkin(iproSkin);
			iProUser.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());					
			logger.debug("Operator for the account : " + authRequest.getProvider());
			logger.debug("Operator to be checked against : " + OperatorEnum.IPRO.name());
			if (authRequest.getProvider().equals(OperatorEnum.IPRO.name())) {
				IproUserDetail userDetails = constructUserDetail(iProUser, authRequest.getUseremail(), getTokens(authRequest.getCredential(), CREDENTIAL_TYPE_PASSWORD));
				session.saveOrUpdate(userDetails);
			}
			
			tx.commit();
			return getUUIDString(iProUser.getUserUuid());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public String overwriteExistingSession(String userId, String skin, UserSessionStatusEnum userStatus) {
		Session session = getCurrentSession();
		Transaction tx = null;
		String user_UUID = null;
		try {
			tx = session.beginTransaction();
			verifySkin(skin, session);
			StringBuilder queryString = new StringBuilder();
							
			queryString.append("select t from IproUserSession t, IproUser u, IproSkin s where ")
						.append("hex(t.iproUser.id)=hex(u.userUuid) and u.iproSkin.id=:skincode ")
						.append("and u.userName=:userName order by t.sessionId desc");
			
			Query query = session.createQuery(queryString.toString())
								.setParameter("userName", userId)
								.setParameter("skincode", skin)
						        .setMaxResults(1);
			
	        IproUserSession uSession = (IproUserSession)query.uniqueResult();
	        if (uSession != null) {
        		/* Session exists; Overwrite the session to create new session */
	        	if(uSession.getIproUserSessionStatus().getSessionStatusCode().equalsIgnoreCase(UserSessionStatusEnum.ACT.name())) {
	        		uSession.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
	        		uSession.setEndDateTime(TimeStampUtil.getCurrentSQLTimeStamp());
	        		IproUserSessionStatus status = new IproUserSessionStatus();
	        		status.setSessionStatusCode(userStatus.name());
	        		uSession.setIproUserSessionStatus(status);
	        		
	        		queryString.delete(0,  queryString.length())
				        	.append("select SUM(w.balanceAmt) from IproWallet w where ")
				        	.append("hex(w.iproUser.id) =:uuid");
	        		
			        query = session.createQuery(queryString.toString())
			        				.setParameter("uuid", UUIDUtil.getUUIDString(uSession.getIproUser().getUserUuid()));
			        
			        BigDecimal balanceChips = (BigDecimal)query.uniqueResult();
			        
	        		if (balanceChips != null) {
	        			uSession.setEndBalance(balanceChips);
	        		}
	        		session.saveOrUpdate(uSession);
	        		logger.info("Session overwritten");
	        	}
        		user_UUID = getUUIDString(uSession.getIproUser().getUserUuid());
        		logger.debug("***************************************************User UUId for the user : " + user_UUID);
	        } else {
	        	logger.debug("User session does not exist ************************************");
	        }
	        tx.commit();
			return user_UUID;
			
		} catch (InvalidSkinException ex) {
			if (tx != null) tx.rollback();
			throw new InvalidSkinException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public ChangePlayModeResponse updateSessionAndSetAccessMode (SecureToken token, Location location, ModeData modeData) {
		Session session = getCurrentSession();
		Transaction tx = null;
		ChangePlayModeResponse response = null;
		String fundType = FundTypeEnum.VCHP.name();
		try {
			tx = session.beginTransaction();
			
			Query query = session.createQuery("select u from IproUser u where hex(u.id) = :userUuid")			
					.setParameter("userUuid", token.getUuid());

			IproUser iProUser = (IproUser)query.uniqueResult();

			if (location != null) {
				IproCountry country = new IproCountry();
            	country.setCountryCode(location.getCountrycode());            	
            	iProUser.setIproCountry(country);
				session.saveOrUpdate(iProUser);
			}
			
			
			IproUserSession userSession = new IproUserSession();
			userSession.setStartDateTime(TimeStampUtil.getCurrentSQLTimeStamp());
			userSession.setIproUser(iProUser);
			
			userSession.setTotalGameTxn(0);			
			IproUserSessionStatus status = new IproUserSessionStatus();
			status.setSessionStatusCode(UserSessionStatusEnum.ACT.name());/* Set User session status to Active */
			userSession.setIproUserSessionStatus(status);
			userSession.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
			
			StringBuilder queryString = new StringBuilder();
	        queryString.append("select SUM(w.balanceAmt) from IproWallet w where ");
	        queryString.append("hex(w.iproUser.id) =:uuid");
	        query = session.createQuery(queryString.toString());
	        query.setParameter("uuid", token.getUuid());
	        BigDecimal totalChips = (BigDecimal)query.uniqueResult();
	        
			if (totalChips != null) {
				userSession.setStartingBalance(totalChips);
			}
			
			if (fundType != null) {
				IproFundType iproFundType = new IproFundType();
				iproFundType.setFundType(fundType);
				userSession.setIproFundType(iproFundType);
			}
			
			IproAccessMode proAccessMode = new IproAccessMode();
            proAccessMode.setAccessMode(modeData.getUserAccessMode().getMode());
            userSession.setIproAccessMode(proAccessMode);
            
            if (location != null) {					        	
            	IproUserVerification userVerification = new IproUserVerification();
            	if (userSession.getIproUserVerification() != null)
            		userVerification = userSession.getIproUserVerification();
            	userVerification.setGpsLatitude(location.getLatitude());
            	userVerification.setGpsLongitude(location.getLongitude());
            	userVerification.setIproSkin(userSession.getIproUser().getIproSkin());
            	userVerification.setIproUser(userSession.getIproUser());
            	userVerification.setIpAddress(location.getIpaddress());            	
	            userVerification.setProCountryCode(location.getCountrycode());
	            userVerification.setProStateName(location.getStatecode());	            
	            session.saveOrUpdate(userVerification);
	            userSession.setIpAddress(location.getIpaddress());
	            userSession.setIproUserVerification(userVerification);
			} else  {
				String remoteAddress = IPAddressUtil.getAddressFromRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes())
					       .getRequest());
				userSession.setIpAddress(remoteAddress);
				
			}
            
			session.saveOrUpdate(userSession);
			
			String uuidStr = UUIDUtil.getUUIDString(iProUser.getUserUuid());
			SecureToken secureToken = new SecureToken(uuidStr, token.getUid(), token.getPid(),
					token.getSid(), IPROGAMES_SESSION_EXPIRATION, modeData.getUserAccessMode().getMode(), 
					modeData.getUserPlayMode().getMode(), userSession.getSessionId());
			String iProToken = authapi.getEncryptediProToken(secureToken);
			
			userSession.setProviderAuthToken(iProToken);
			session.saveOrUpdate(userSession);
			tx.commit();
			response = new ChangePlayModeResponse(iProToken, modeData.getUserAccessMode(), modeData.getUserPlayMode());
			
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return response;		
	}
	
	/* Create the session for the user and if the user is not in the ipro server,
	 * add the user to ipro server and create the new session for the user 
	 */
	public AuthResponse createSessionAndSetAccessMode(String userUuid, UserSocialProfile userProfile, 
			String accType, Location location, String provider, String skin, ModeData modeData) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		AuthResponse response = null;
		try {
			tx = session.beginTransaction();
			
			Query query = session.createQuery("select u from IproUser u where hex(u.id) = :userUuid")			
								.setParameter("userUuid", userUuid);

			IproUser iProUser = (IproUser)query.uniqueResult();
			UserAccessMode userAccessMode = null;
			UserPlayMode userPlayMode = null;
			if (modeData != null) {
				userAccessMode = modeData.getUserAccessMode();	
				userPlayMode = modeData.getUserPlayMode();
			} else {
				userAccessMode = new UserAccessMode(AccessModeEnum.SLG.name(), messageSource.getMessage("MESSAGE_" + AccessModeEnum.SLG.name(), null, null));
				userPlayMode = new UserPlayMode(PlayModeEnum.SOC.name(), messageSource.getMessage("MESSAGE_" + PlayModeEnum.SOC.name(), null, null));
			}
        	String fundType = FundTypeEnum.VCHP.name();
        	StringBuilder queryString = new StringBuilder();
			if (location != null) {	        	
				IproCountry country = new IproCountry();
            	country.setCountryCode(location.getCountrycode());            	
            	iProUser.setIproCountry(country);
				session.saveOrUpdate(iProUser);
			}
			
			IproUserSession userSession = new IproUserSession();
			userSession.setStartDateTime(TimeStampUtil.getCurrentSQLTimeStamp());
			userSession.setIproUser(iProUser);
			
			userSession.setTotalGameTxn(0);			
			IproUserSessionStatus status = new IproUserSessionStatus();
			status.setSessionStatusCode(UserSessionStatusEnum.ACT.name());/* Set User session status to Active */
			userSession.setIproUserSessionStatus(status);
			userSession.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
			
			queryString.delete(0,  queryString.length());
	        queryString.append("select SUM(w.balanceAmt) from IproWallet w where ");
	        queryString.append("hex(w.iproUser.id) =:uuid");
	        query = session.createQuery(queryString.toString());
	        query.setParameter("uuid", userUuid);
	        BigDecimal totalChips = (BigDecimal)query.uniqueResult();
	        
			if (totalChips != null) {
				userSession.setStartingBalance(totalChips);
			}
			
			if (fundType != null) {
				IproFundType iproFundType = new IproFundType();
				iproFundType.setFundType(fundType);
				userSession.setIproFundType(iproFundType);
			}
			
			IproAccessMode proAccessMode = new IproAccessMode();
            proAccessMode.setAccessMode(userAccessMode.getMode());
            userSession.setIproAccessMode(proAccessMode);
            
            if (location != null) {					        	
            	IproUserVerification userVerification = new IproUserVerification();
            	if (userSession.getIproUserVerification() != null)
            		userVerification = userSession.getIproUserVerification();
            	userVerification.setGpsLatitude(location.getLatitude());
            	userVerification.setGpsLongitude(location.getLongitude());
            	userVerification.setIproSkin(userSession.getIproUser().getIproSkin());
            	userVerification.setIproUser(userSession.getIproUser());
            	userVerification.setIpAddress(location.getIpaddress());            	
	            userVerification.setProCountryCode(location.getCountrycode());
	            userVerification.setProStateName(location.getStatecode());	            
	            session.saveOrUpdate(userVerification);
	            userSession.setIpAddress(location.getIpaddress());
	            userSession.setIproUserVerification(userVerification);
			} else  {
				String remoteAddress = IPAddressUtil.getAddressFromRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes())
					       .getRequest());
				userSession.setIpAddress(remoteAddress);
				
			}
            
			session.saveOrUpdate(userSession);
			
			String uuidStr = UUIDUtil.getUUIDString(iProUser.getUserUuid());
			SecureToken secureToken = new SecureToken(uuidStr, userProfile.getUserId(), provider,
					skin, IPROGAMES_SESSION_EXPIRATION, userAccessMode.getMode(), userPlayMode.getMode(), userSession.getSessionId());
			String iProToken = authapi.getEncryptediProToken(secureToken);
			
			userSession.setProviderAuthToken(iProToken);
			session.saveOrUpdate(userSession);
			logger.info("Session newly created");
			tx.commit();
			response = new AuthResponse(userProfile.getDisplayName(), iProToken, UserAccountTypeEnum.REG.name(), true);
			//Setting up the accessmode and play mode to the response so we can keep it in the session. 
			response.setUserAccessMode(userAccessMode);
			response.setUserPlayMode(userPlayMode);
		} catch (InvalidSkinException ex) {
			if (tx != null)	tx.rollback();
			ex.printStackTrace();
			throw new InvalidSkinException(ex.getMessage());
		} catch (ConstraintViolationException ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new UserAlreadyRegisteredException(messageSource.getMessage("USER_ALREADY_REGISTERED",null,null));
		} catch (Exception ex){
			if (tx != null)	tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally{
			session.close();
		}
		
		return response;
	}
	
	public void updateSession(String iProToken, BigDecimal balanceAmt) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();			
			queryString.append("select t from IproUserSession t where t.providerAuthToken=:token ")
						.append("and t.iproUserSessionStatus.sessionStatusCode = :status");
			
			Query query = session.createQuery(queryString.toString())
							.setParameter("token", iProToken)
							.setParameter("status", UserSessionStatusEnum.ACT.name());
			
	        IproUserSession userSession = (IproUserSession)query.uniqueResult();
			
	        if (userSession != null) {
	        	userSession.setStartingBalance(balanceAmt);
	        	userSession.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
				session.saveOrUpdate(userSession);
	        } else {
	        	throw new InvalidSessionException(messageSource.getMessage("INVALID_SESSION", null, null));
	        }
			tx.commit();
		} catch (InvalidSessionException ex) {
			if (tx != null)	tx.rollback();
			throw new InvalidSessionException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null)	tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	/* Set the access mode as SOCIAL for the user */
	public void setSocialAccessMode(String iProToken) {
		String remoteAddress = IPAddressUtil.getAddressFromRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes())
			       .getRequest());
		Session session = getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.append("select t from IproUserSession t, IproUserSessionStatus st where t.providerAuthToken=:token ")
				.append("and st.sessionStatusCode = t.iproUserSessionStatus.id ")
				.append("and st.sessionStatusCode = :status");
			
			Query query = session.createQuery(queryString.toString())
		        .setParameter("token", iProToken)
		        .setParameter("status", UserSessionStatusEnum.ACT.name());
	        IproUserSession userSession = (IproUserSession)query.uniqueResult();
			
	        if (userSession != null) {
	        	IproAccessMode accessmode = new IproAccessMode();
				accessmode.setAccessMode(AccessModeEnum.SLG.name()); // Set default mode as social
				userSession.setIpAddress(remoteAddress);
				userSession.setIproAccessMode(accessmode);
				userSession.setStartDateTime(TimeStampUtil.getCurrentSQLTimeStamp());
				session.saveOrUpdate(userSession);
	        }
			tx.commit();
		}catch(Exception e){
			if (tx != null) tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}finally{
			session.close();
		}
	}

	/* Get the play mode of the user from the token */
	public String getAccessModeForUser(String iProToken) {
		logger.info("Inside getaccessmode for user");
		try{
			
			if(mapUserDetails != null)
			{
				logger.info("mapuserDetails is notnull");
			}
			else
			{
				logger.info("Getting map");
				mapUserDetails =  getHazelcastInstance().getMap("iprouserdetails");
			}
			HazelUserInfo response = (HazelUserInfo)mapUserDetails.get(iProToken);
			String accessMode = null;
			if(response != null)
			{
				logger.info("UserInfo is not null");
				accessMode = response.getUserAccssMode();		
				if(accessMode != null)
				{
					logger.info("accessMode is notnull");
				}
				else
				{
					logger.info("accessMode is null");
				}
			}
			else
			{
				logger.info("UserInfo is null");
			}
			return accessMode;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
		/*Session session = getCurrentSession();
		try {
			StringBuilder queryString = new StringBuilder();
			
			queryString.append("select t.iproAccessMode.id from IproUserSession t where ");
			queryString.append("t.providerAuthToken=:authToken and t.iproUserSessionStatus.id = :status order by t.sessionId desc");		
			
			Query query = session.createQuery(queryString.toString());
			query.setMaxResults(1);
	        query.setParameter("authToken", iProToken);
	        query.setParameter("status", UserSessionStatusEnum.ACT.name());
	        String accessMode = (String)query.uniqueResult();
	        if (accessMode != null) {	        	
	        	return accessMode;
	        }
		} catch(Exception e) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return null;*/
	}

	/* Get the play mode verifying the location for legal */
	public ModeData getAccessModeFromLocation(Location location, String provider) {
		logger.info("getAccessModeFromLocation");
		Session session = getCurrentSession();
		ModeData modeData = null;
		try {
			if (location != null) {
				StringBuilder queryString = new StringBuilder();
		    	queryString.append("select lcs from IproCountry c, IproCountryState cs, IproLegalCountry lc,")
		    				.append(" IproLegalCountryState lcs")
					    	.append(" where c.countryCode =:countryCode and c.countryCode = lc.iproCountry.countryCode")
					    	.append(" and cs.shortName = :stateCode and cs.countryStateId = lcs.iproCountryState.id")
					    	.append(" and lcs.enabledInd = 'Y' and lc.enabledInd = 'Y' and lc.rmg='Y' and lcs.rmg='Y'");
		    	Query query = session.createQuery(queryString.toString())
							        .setParameter("countryCode", location.getCountrycode())
							        .setParameter("stateCode", location.getStatecode());
		        logger.info("============ The country code is "+location.getCountrycode());
		        logger.info("============ The state code is "+location.getStatecode());
		        IproLegalCountryState countrystate = (IproLegalCountryState)query.uniqueResult();
		        if (countrystate != null) {
		        	logger.info("====================The access mode is set to "+ AccessModeEnum.RMG.toString());
		        	if (provider != null) {
		        		String accessMode = null;
		        		String playMode = null;
		        		switch(OperatorEnum.fromValue(provider)) {
		            	case FB:
		            	case TWT:
		            	case GPLUS:
		            	case IPRO:
		            		accessMode = AccessModeEnum.SLG.name(); // Set mode as social
		            		playMode = PlayModeEnum.FAN.name();
		            		break;
		        		default:
			            	accessMode = AccessModeEnum.RMG.name(); // Set mode as RMG
			            	playMode = PlayModeEnum.REAL.name();
			            	break;
		            	}
		        		modeData = new ModeData(new UserAccessMode(accessMode, messageSource.getMessage("MESSAGE_" + accessMode, null, null)), 
	            				new UserPlayMode(playMode, messageSource.getMessage("MESSAGE_" + playMode, null, null)));
	        			
		        	} else {
		        	/*
		        	 * If the accessmode is RMG then we are allowing the user to have access to all the three play mode.
		        	 * The system will default to REAL as playmode and the menu in the client will allow the user to switch to
		        	 * Social or Fantasy play mode.
		        	 */
		        		modeData = new ModeData(new UserAccessMode(AccessModeEnum.RMG.name()), new UserPlayMode(PlayModeEnum.REAL.name())); // Set mode as real money
		        	}
		        } else {
		        	queryString.delete(0,  queryString.length());
		        	queryString.append("select lcs from IproCountry c, IproCountryState cs, IproLegalCountry lc,")
			    				.append(" IproLegalCountryState lcs")
						    	.append(" where c.countryCode =:countryCode and c.countryCode = lc.iproCountry.countryCode")
						    	.append(" and cs.shortName = :stateCode and cs.countryStateId = lcs.iproCountryState.id")
						    	.append(" and lcs.enabledInd = 'Y' and lc.enabledInd = 'Y' and ((lc.fantasy='Y' and lcs.fantasy='Y')")
						    	.append(" or (lc.rmg='Y' and lcs.fantasy='Y') or (lc.fantasy='Y' and lcs.rmg='Y'))");
		        	query = session.createQuery(queryString.toString())
					        .setParameter("countryCode", location.getCountrycode())
					        .setParameter("stateCode", location.getStatecode());
		        	
		        	IproLegalCountryState fantasycountrystate = (IproLegalCountryState)query.uniqueResult();
		        	
		        	if (fantasycountrystate != null){
		        		/* We have to add one more level of check for the country and state are allowed for FANTASY or not. If the 
			        	 * state is allowed for FANTASY then the play mode will be set to FAN and the user can switch to Social in the 
			        	 * App menu. This option is allowed only when the user is not in the RMG access mode.
			        	 *  
			        	 */
		        		String accessMode = AccessModeEnum.SLG.name();
		        		String playMode = PlayModeEnum.FAN.name();
		        		if (provider != null) {
			        		modeData = new ModeData(new UserAccessMode(accessMode, messageSource.getMessage("MESSAGE_" + accessMode, null, null)), 
		            				new UserPlayMode(playMode, messageSource.getMessage("MESSAGE_" + playMode, null, null)));
		        		} else {
		        			modeData = new ModeData(new UserAccessMode(accessMode), new UserPlayMode(playMode));
		        		}
		        	} else {			        	 
			        	logger.info("====================The access mode is set to "+ AccessModeEnum.SLG.toString());
			        	modeData = new ModeData(new UserAccessMode(AccessModeEnum.SLG.name()), new UserPlayMode(PlayModeEnum.SOC.name())); // Set mode as social
		        	}
		        }
			} else {
				modeData = new ModeData(new UserAccessMode(AccessModeEnum.SLG.name()), new UserPlayMode(PlayModeEnum.SOC.name())); // Set mode as social
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		logger.info("returned getAccessModeFromLocation 77908709089");
		return modeData;
	}
	
	/* Force to terminate the user's serssion based on uuid */
	public void forceterminateSession(String uuid, String statusStr) {
		logger.debug("The iProToken is: "+uuid);
		Session session = getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.append("select t from IproUserSession t where ");
			queryString.append("hex(t.iproUser.id)=:useruuid ");
			queryString.append("order by t.sessionId desc");
			Query query = session.createQuery(queryString.toString());
			query.setMaxResults(1);
	        query.setParameter("useruuid", uuid);
			IproUserSession userSession = (IproUserSession) query.uniqueResult();
	
			if (userSession != null) {
				if (userSession.getIproUserSessionStatus()
						.getSessionStatusCode().equalsIgnoreCase("ACT")) {
					IproUserSessionStatus status = new IproUserSessionStatus();
					status.setSessionStatusCode(statusStr);
					userSession.setIproUserSessionStatus(status);
					userSession.setEndDateTime(new Timestamp(new Date().getTime()));
					session.saveOrUpdate(userSession);

				}
			}
			tx.commit();
		} catch (InvalidSessionException ex) {
			if (tx != null) tx.rollback();
			throw new InvalidSessionException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null)	tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	/* To terminate the user's session on session timeout/logout/force logout.
	 * Timeout time is configured in Web.xml*/
	public void sessionTimeout(String iProToken, String statusStr) {
		logger.debug("The iProToken is: "+iProToken);
		Session session = getCurrentSession();
		Transaction tx = null;
		try {
			if (authapi == null) {
				authapi = new IProTokenHandler();
			}
			SecureToken secureToken = authapi.getDecryptediProToken(iProToken);
			
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.append("select t from IproUserSession t where ");
			queryString.append("t.providerAuthToken=:iProToken ");
			queryString.append("and t.iproUser.iproSkin.iproOperator.id=:operatorCode ");
			queryString.append("and t.iproUser.iproSkin.id=:skincode ");
			queryString.append("and hex(t.iproUser.id)=:useruuid ");
			queryString.append("and t.iproUser.userName=:userid order by t.sessionId desc");
			Query query = session.createQuery(queryString.toString());
			query.setMaxResults(1);
	        query.setParameter("operatorCode", secureToken.getPid());
	        query.setParameter("skincode", secureToken.getSid());
	        query.setParameter("useruuid", secureToken.getUuid());
	        query.setParameter("userid", secureToken.getUid());
	        query.setParameter("iProToken", iProToken);
			IproUserSession userSession = (IproUserSession) query.uniqueResult();
	
			if (userSession != null) {
				if (userSession.getIproUserSessionStatus()
						.getSessionStatusCode().equalsIgnoreCase("ACT")) {
					IproUserSessionStatus status = new IproUserSessionStatus();
					status.setSessionStatusCode(statusStr);
					userSession.setIproUserSessionStatus(status);
					userSession.setEndDateTime(new Timestamp(new Date().getTime()));
					session.saveOrUpdate(userSession);

				} else {
					if (messageSource != null)
						throw new InvalidSessionException(messageSource.getMessage("SESSION_"+ userSession.getIproUserSessionStatus().getSessionStatusCode(), 
	        				null, null));
				}

			} else {
				if (messageSource != null)
					throw new InvalidSessionException(messageSource.getMessage("INVALID_SESSION", null, null));
			}
			tx.commit();
		} catch (InvalidSessionException ex) {
			if (tx != null) tx.rollback();
			throw new InvalidSessionException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null)	tx.rollback();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public boolean checkIfUserExists(String userId, String skinCode) {		
		Session session = getCurrentSession();
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select hex(u.userUuid) from IproUser u, IproSkin s where u.userName=:userName");
			queryString.append(" and s.skinCode=:skinCode and s.skinCode=u.iproSkin.id");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userName", userId);
			query.setParameter("skinCode", skinCode);
			
			String uuid = (String)query.uniqueResult();				
			if (uuid != null) {
				return true;
			}
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}		
		return false;
	}
	
	public boolean checkIfUserExists(String userName, String userEmail, String skinCode) {		
		Session session = getCurrentSession();
		boolean isUserExist = false;
		try {
			if (userEmail != null) {
				logger.debug("Checking user email");
				isUserExist = checkIfUserEmailExist(userEmail, null, skinCode, session);
				if (isUserExist) {
					logger.debug("user email exist");
					throw new UserAlreadyRegisteredException(messageSource.getMessage("EMAIL_EXIST", null, null));
				}
				logger.debug("user email not exist");
			}
			if (userName != null && !isUserExist) {
				logger.debug("Checking user name");
				isUserExist = checkIfUserNameExist(userName, null, skinCode, session);
				if (isUserExist) {
					logger.debug("user name exist");
					throw new UserAlreadyRegisteredException(messageSource.getMessage("USERNAME_EXIST", null, null));
				}
				logger.debug("user name not exist");
			}
		} finally {
			session.close();
		}
		return isUserExist;
	}
	
	public boolean checkIfUserExists(String userName, String userEmail, String password, String skinCode) {		
		Session session = getCurrentSession();
		try {
			if (userEmail != null) {
				return checkIfUserEmailExist(userEmail, password, skinCode, session);
			} else {
				return checkIfUserNameExist(userName, password, skinCode, session);
			}
		} finally {
			session.close();
		}
	}
	
	private boolean checkIfUserEmailExist(String userEmail, String password, String skinCode, Session session) {
		boolean isUserExists = false;
		try {			
			StringBuilder queryString = new StringBuilder();
			queryString.append("select ud.userPassword from ");
			queryString.append("IproUserDetail ud where ud.userEmail=:userEmail");			
			queryString.append(" and ud.iproUser.iproSkin.id=:skinCode");
			
			logger.debug("QueryString for the user check : " + queryString.toString());
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userEmail", userEmail);
			query.setParameter("skinCode", skinCode);
			String orgpassword = (String)query.uniqueResult();
			if (orgpassword != null) {
				if(password != null) {
					if (!orgpassword.equals(password)) {
						throw new InvalidPasswordException(messageSource.getMessage("INVALID_CREDENTIALS_EXCEPTION",null,null));
					} else {
						isUserExists = true;
					}
				} else {
					isUserExists = true;
				}
			}
		} catch (InvalidPasswordException ex) {
			throw new InvalidPasswordException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return isUserExists;
	}
	
	private boolean checkIfUserNameExist(String userName, String password, String skinCode, Session session) {
		boolean isUserExists = false;
		try {			
			StringBuilder queryString = new StringBuilder();
			
			queryString.append("select ud.userPassword from ");
			queryString.append("IproUserDetail ud where ud.iproUser.userName=:userName");			
			queryString.append(" and ud.iproUser.iproSkin.id=:skinCode");
			
			
			logger.debug("QueryString for the user check : " + queryString.toString());
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userName", userName);
			query.setParameter("skinCode", skinCode);
			String orgpassword = (String)query.uniqueResult();
			if (orgpassword != null) {
				if(password != null) {
					if (!orgpassword.equals(password)) {
						throw new InvalidPasswordException(messageSource.getMessage("INVALID_CREDENTIALS_EXCEPTION",null,null));
					} else {
						isUserExists = true;
					}
				} else {
					isUserExists = true;
				}
			}
		} catch (InvalidPasswordException ex) {
			throw new InvalidPasswordException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return isUserExists;
	}
	
	public String getUserNameFromEmailId(String emailId) {
		Session session = getCurrentSession();
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("select ud.iproUser.userName from IproUserDetail ud where ud.userEmail=:userEmail");			
			queryString.append(" and ud.iproUser.iproSkin.id=:skinCode");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userEmail", emailId);			
			query.setParameter("skinCode", "IPRO");
			
			return (String)query.uniqueResult();
		} catch (InvalidPasswordException ex) {
			throw new InvalidPasswordException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public BigDecimal getChipsForProduct(String providerName) {
		Session session = getCurrentSession();
		try {
			StringBuilder queryString = new StringBuilder();			
			queryString.append("select p.chipValue from IproPurchaseProduct p  where p.iproProductProvider.providerName=:providerName ");
        	queryString.append("and p.enabledInd='Y'");
        	
        	Query query = session.createQuery(queryString.toString());
        	query.setParameter("providerName", providerName);
        	query.setMaxResults(1);
        	
        	BigDecimal chipValue = (BigDecimal)query.uniqueResult();
			if (chipValue != null)
				return chipValue;
			else
				throw new InvalidProviderException(messageSource.getMessage("INVALID_PRODUCT_ID", null, null));
		} catch (InvalidProviderException ex) {
			throw new InvalidProviderException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	private IproUser constructUser(UserSocialProfile userProfile, UUID userUuid, String skin, String userStatus, String acctype) {
		IproUser user = new IproUser();
		
		IproSkin iProSkin = new IproSkin();
		iProSkin.setSkinCode(skin);
		user.setIproSkin(iProSkin);
		
		IproUserAccountType accType = new IproUserAccountType();
		accType.setAccountTypeCode(acctype);
		user.setIproUserAccountType(accType);
		
		IproUserStatus status = new IproUserStatus();
		status.setUserStatusCode(userStatus);
		user.setIproUserStatus(status);
		
		user.setUserName(userProfile.getUserId());
		user.setDisplayName(userProfile.getDisplayName());
		user.setUserUuid(userUuid);		
		user.setCreatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
		user.setUpdatedDate(TimeStampUtil.getCurrentSQLTimeStamp());
		
		return user;
	}
	
	private IproUserDetail constructUserDetail(IproUser user, String userEmail, String password) {
		IproUserDetail userDetail = new IproUserDetail();
		userDetail.setIproUser(user);
		userDetail.setUserEmail(userEmail);
		userDetail.setUserPassword(password);
		
		return userDetail;
	}
	
	private void verifySkin(String skinCode, Session session) {
		Query query = session.createQuery("select s from IproSkin s where s.skinCode=:skinCode");
		query.setParameter("skinCode", skinCode);
		
		IproSkin skin = (IproSkin)query.uniqueResult();
		if (skin == null) {
			throw new InvalidSkinException(messageSource.getMessage("INVALID_SKIN", null, null));
		}
	}
}