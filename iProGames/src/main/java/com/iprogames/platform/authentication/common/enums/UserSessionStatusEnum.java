package com.iprogames.platform.authentication.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum UserSessionStatusEnum {
	ACT, // Active
	EXP, // Expired
	OVR, // Overwritten	
	TMT, // Timed out
	TER, // Terminate
	LOG, // Logged out
	FOR, // Forced	
	FAL  // Failed
}
