package com.iprogames.platform.authentication.common;

import static com.iprogames.platform.util.Constants.GAME_SELECT;
import static com.iprogames.platform.util.Constants.SIDE_BETS_MARKET;
import static com.iprogames.platform.util.Constants.ACTIVE_GAMES;
import static com.iprogames.platform.util.Constants.LEAGUE_PLAY;
import static com.iprogames.platform.util.Constants.BUY_CHIPS;
import static com.iprogames.platform.util.Constants.ACTIVE_PICKS;
import static com.iprogames.platform.util.Constants.PROFILE;
import static com.iprogames.platform.util.Constants.LEADERS;
import static com.iprogames.platform.util.Constants.FRIENDS;
import static com.iprogames.platform.util.Constants.HOW_TO_PLAY;
import static com.iprogames.platform.util.Constants.SETTINGS;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class LobbyData {
	
	private String gs		= GAME_SELECT;
	private String sbm		= SIDE_BETS_MARKET;
	private String actvgms	= ACTIVE_GAMES;
	private String lgeply	= LEAGUE_PLAY;
	private String bycps	= BUY_CHIPS;
	private String apks		= ACTIVE_PICKS;
	private String prfle	= PROFILE;
	private String lead		= LEADERS;
	private String frds		= FRIENDS;
	private String htp		= HOW_TO_PLAY;
	private String stngs	= SETTINGS;
	private String mode		= null;
	
	public LobbyData() {
	}
	
	public LobbyData(String mode) {
		this.mode = mode;
	}

	public String getGs() {
		return gs;
	}

	public void setGs(String gs) {
		this.gs = gs;
	}

	public String getSbm() {
		return sbm;
	}

	public void setSbm(String sbm) {
		this.sbm = sbm;
	}

	public String getActvgms() {
		return actvgms;
	}

	public void setActvgms(String actvgms) {
		this.actvgms = actvgms;
	}

	public String getLgeply() {
		return lgeply;
	}

	public void setLgeply(String lgeply) {
		this.lgeply = lgeply;
	}

	public String getBycps() {
		return bycps;
	}

	public void setBycps(String bycps) {
		this.bycps = bycps;
	}

	public String getApks() {
		return apks;
	}

	public void setApks(String apks) {
		this.apks = apks;
	}

	public String getPrfle() {
		return prfle;
	}

	public void setPrfle(String prfle) {
		this.prfle = prfle;
	}

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public String getFrds() {
		return frds;
	}

	public void setFrds(String frds) {
		this.frds = frds;
	}

	public String getHtp() {
		return htp;
	}

	public void setHtp(String htp) {
		this.htp = htp;
	}

	public String getStngs() {
		return stngs;
	}

	public void setStngs(String stngs) {
		this.stngs = stngs;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return "LobbyData [gs=" + gs + ", sbm=" + sbm + ", actvgms=" + actvgms
				+ ", lgeply=" + lgeply + ", bycps=" + bycps + ", apks=" + apks
				+ ", prfle=" + prfle + ", lead=" + lead + ", frds=" + frds
				+ ", htp=" + htp + ", stngs=" + stngs + ", mode=" + mode + "]";
	}	
}
