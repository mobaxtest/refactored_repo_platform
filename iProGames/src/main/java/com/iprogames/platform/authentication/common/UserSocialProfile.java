package com.iprogames.platform.authentication.common;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public class UserSocialProfile {

	private String userId		= null;
	private String displayName	= null;
	
	public UserSocialProfile() {
	}
	
	public UserSocialProfile(String userId) {
		this.userId = userId;
		this.displayName = userId;
	}
	
	public UserSocialProfile(String userId, String displayName) {
		this.userId = userId;
		this.displayName = displayName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toString() {
		return "UserSocialProfile [userId=" + userId + ", displayName="
				+ displayName + "]";
	}
}
