package com.iprogames.platform.authentication.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.iprogames.platform.error.ErrorMessage;
import com.iprogames.platform.error.ErrorType;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class AuthResponse {
	
    private String username				= null;
    private String iProToken			= null;
    private String acctype				= null;
    private LobbyConnect lobbyConnect	= null;
    private UserAccessMode userAccessMode	= null;
    private UserPlayMode userPlayMode = null;
    private ErrorType type				= null;
    private ErrorMessage message		= null;
    
    public AuthResponse() {
    }
    
    public AuthResponse(String username, String iProToken, boolean isLobby) {
    	this.username = username;
    	this.iProToken = iProToken;
    	if (isLobby)
    		this.lobbyConnect = new LobbyConnect();
    }
    
    public AuthResponse(String username, String iProToken, String acctype, boolean isLobby) {
    	this.username = username;
    	this.iProToken = iProToken;
    	this.acctype = acctype;
    	if (isLobby)
    		this.lobbyConnect = new LobbyConnect();
    }
    
    public AuthResponse(String accessMode, String playMode, String accessMessage, String playMessage) {
    	this.userAccessMode = new UserAccessMode(accessMode, accessMessage);
    	this.userPlayMode = new UserPlayMode(playMode, playMessage);
    }
    
    public AuthResponse(UserAccessMode userAccessMode) {
    	this.userAccessMode = userAccessMode;
    }
    
 /*   public AuthResponse(String mode, String message, String optionalMode, String optionalMessage) {
    	this.userAccessMode = new UserAccessMode(mode, message, optionalMode, optionalMessage);
    }
*/
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getiProToken() {
        return iProToken;
    }

    public void setiProToken(String iProToken) {
        this.iProToken = iProToken;
    }

    public String getAcctype() {
		return acctype;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public LobbyConnect getLobbyConnect() {
        return lobbyConnect;
    }

    public void setLobbyConnect(LobbyConnect lobbyConnect) {
        this.lobbyConnect = lobbyConnect;
    }

	public UserAccessMode getUserAccessMode() {
		return userAccessMode;
	}

	public void setUserAccessMode(UserAccessMode userAccessMode) {
		this.userAccessMode = userAccessMode;
	}

	public UserPlayMode getUserPlayMode() {
		return userPlayMode;
	}

	public void setUserPlayMode(UserPlayMode userPlayMode) {
		this.userPlayMode = userPlayMode;
	}

	public ErrorType getType() {
		return type;
	}

	public void setType(ErrorType type) {
		this.type = type;
	}

	public ErrorMessage getMessage() {
		return message;
	}

	public void setMessage(ErrorMessage message) {
		this.message = message;
	}
}
