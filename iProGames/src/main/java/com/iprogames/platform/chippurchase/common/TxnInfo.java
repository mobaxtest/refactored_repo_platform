package com.iprogames.platform.chippurchase.common;

public class TxnInfo {

	private String txnrefid			= null;
	private String txnstatus		= null;
	private String txnproductid		= null;
	private String txnproviderid	= null;
	private boolean isNextState;
	
	public TxnInfo() {
		
	}
	
	public TxnInfo(boolean isNextState) {
		this.isNextState = isNextState;
	}
	
	public TxnInfo(String txnrefid) {
		this.txnrefid = txnrefid;
	}
	
	public TxnInfo(String txnrefid, String txnstatus, 
			String txnproductid, String txnproviderid) {
		this.txnrefid = txnrefid;
		this.txnstatus = txnstatus;
		this.txnproductid = txnproductid;
		this.txnproviderid = txnproviderid;
	}

	public String getTxnrefid() {
		return txnrefid;
	}

	public void setTxnrefid(String txnrefid) {
		this.txnrefid = txnrefid;
	}

	public String getTxnstatus() {
		return txnstatus;
	}

	public void setTxnstatus(String txnstatus) {
		this.txnstatus = txnstatus;
	}

	public String getTxnproductid() {
		return txnproductid;
	}

	public void setTxnproductid(String txnproductid) {
		this.txnproductid = txnproductid;
	}

	public String getTxnproviderid() {
		return txnproviderid;
	}

	public void setTxnproviderid(String txnproviderid) {
		this.txnproviderid = txnproviderid;
	}

	public boolean isNextState() {
		return isNextState;
	}

	public void setNextState(boolean isNextState) {
		this.isNextState = isNextState;
	}

	@Override
	public String toString() {
		return "TxnInfo [txnrefid=" + txnrefid + ", txnstatus=" + txnstatus
				+ ", txnproductid=" + txnproductid + ", txnproviderid="
				+ txnproviderid + ", isNextState=" + isNextState + "]";
	}
	
}
