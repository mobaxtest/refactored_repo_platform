package com.iprogames.platform.chippurchase.dao;

import static com.iprogames.util.uuid.UUIDUtil.getUUIDFromString;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDString;
import static com.iprogames.util.uuid.UUIDUtil.generateUUID;
import static com.iprogames.util.timestamp.TimeStampUtil.getCurrentSQLTimeStamp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.common.enums.UserAccountTypeEnum;
import com.iprogames.platform.authentication.common.enums.UserSessionStatusEnum;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.ChipPurchaseResponse;
import com.iprogames.platform.chippurchase.common.ProductPackage;
import com.iprogames.platform.chippurchase.common.TxnInfo;
import com.iprogames.platform.chippurchase.common.enums.FinTxnStatusEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.platform.dao.SessionFactoryHandler;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.InvalidProductException;
import com.iprogames.platform.exception.InvalidProviderException;
import com.iprogames.platform.exception.InvalidPurchaseIdException;
import com.iprogames.util.model.IproCurrency;
import com.iprogames.util.model.IproFinProviderTxnDetail;
import com.iprogames.util.model.IproFinTransaction;
import com.iprogames.util.model.IproFinTxnState;
import com.iprogames.util.model.IproProductProvider;
import com.iprogames.util.model.IproPurchaseProduct;
import com.iprogames.util.model.IproFinTxnStatus;
import com.iprogames.util.model.IproTxnType;
import com.iprogames.util.model.IproUser;
import com.iprogames.util.model.IproUserSession;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Repository("chippurchasedao")
public class ChipPurchaseDaoHandler<T extends Serializable> extends SessionFactoryHandler {

	Logger logger = LogManager.getLogger(ChipPurchaseDaoHandler.class);
	
	@Resource
	MessageSource messageSource;
	
	public ChipPurchaseDaoHandler() {
		super();
	}
	public ChipPurchaseDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	/* Add new record to FinTxnState and FinTxnStateDetail table with the Buy Chips Initiation request */
	@SuppressWarnings("unchecked")
	public ChipPurchaseResponse initiateBuyChipsTransaction(ChipPurchase request, SecureToken secureToken) {
		Session session = getCurrentSession();
		Transaction tx = null;
		ChipPurchaseResponse response = null;
		try {
			int providerId = 0;
			tx = session.beginTransaction();
			Query query = session.createQuery("select t from IproPurchaseProduct t where t.iproProductProvider.providerName=:provider and t.enabledInd='Y'");
	        query.setParameter("provider", request.getProvider());
	        List<IproPurchaseProduct> purchaseProduct = (List<IproPurchaseProduct>)query.list();
	        List<ProductPackage> packages = null;
	        if (purchaseProduct != null && purchaseProduct.size() > 0) {
	        	packages = new ArrayList<ProductPackage>();
	        	providerId = purchaseProduct.get(0).getIproProductProvider().getProviderId();
	        	for(IproPurchaseProduct product : purchaseProduct) {
	        		packages.add(new ProductPackage(product.getProProductIdentifier(),product.getChipValue().toString(),
	        				product.getChipValueStr(), product.getAssetUrl()));
	        	}
	        } else {
	        	throw new InvalidProviderException(messageSource.getMessage("INVALID_PROVIDER",null,null));
	        }
	        
			query = session.createQuery("select u.sessionId from IproUserSession u where hex(u.iproUser.userUuid)=:uuid and u.iproUserSessionStatus.id=:status order by u.sessionId desc")
					        .setParameter("uuid", secureToken.getUuid())
					        .setParameter("status", UserSessionStatusEnum.ACT.name())
					        .setMaxResults(1);	        
	        
	        int sessionId = (int)query.uniqueResult();
	        
	        IproUser user = new IproUser();
	        user.setUserUuid(getUUIDFromString(secureToken.getUuid()));
	   
			
			UUID referenceId = generateUUID();
			logger.info("==============Provider Id : " + providerId);
			logger.info("==============User Id : "+user.getDisplayName() +" "+user.getUserUuid());
			IproFinTransaction finTxn = constructFinTransaction(referenceId, providerId, 0, FinTxnStatusEnum.BCHP.name(), TxnTypeEnum.PUR.name(), user, sessionId);
			session.save(finTxn);
			
			/* Update FinTxnState Table */
			String txnData = null;
			if (request.getTxn() != null && request.getTxn().getTxndata() != null)
				txnData = request.getTxn().getTxndata();
			IproFinTxnState txnState = constructFinTxnState(finTxn.getFinTxnId(), FinTxnStatusEnum.BCHP.name(), txnData);
			session.saveOrUpdate(txnState);
			tx.commit();
			response = new ChipPurchaseResponse(request.getiProToken(), getUUIDString(referenceId), packages);
			
		} catch (InvalidProviderException ex){ 
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new InvalidProviderException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return response;
	}
	
	/* Update FinTxnState table with the new state for the transaction */
	public void updatePurchaseTransaction(ChipPurchase request, SecureToken secureToken, String finTxnState, String txnrefId, byte[] txndata) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			int productId = 0;
			Query query = null;
			if (request.getTxn() != null && request.getTxn().getTxnproductid() != null && 
					request.getTxn().getTxnproductid().length() > 0) {
				query = session.createQuery("select t.productId from IproPurchaseProduct t where t.proProductIdentifier=:productid and t.enabledInd='Y'");
		        query.setParameter("productid", request.getTxn().getTxnproductid());
		        productId = (Integer)query.uniqueResult();
		        if (productId == 0) {
		        	throw new InvalidProductException(messageSource.getMessage("INVALID_PRODUCT_ID",null,null));
		        }
			}
			logger.debug("txnrefId : " + txnrefId);
			
			txnrefId = (txnrefId != null? txnrefId : request.getTxn().getTxnrefid());
			
			StringBuilder queryString = new StringBuilder();
			queryString.append("select t from IproFinTransaction t where hex(t.iproUser.id)=:uuid ");
			queryString.append("and hex(t.finTxnId)=:refid");
			query = session.createQuery(queryString.toString());
	        query.setParameter("uuid", secureToken.getUuid());
	        query.setParameter("refid", txnrefId);
	        query.setMaxResults(1);
	        
	        logger.debug("User UUID : " + secureToken.getUuid());
	        logger.debug("Transaction reference Id : " + txnrefId);
	        IproFinTransaction reftxn = (IproFinTransaction)query.uniqueResult();
			if (reftxn != null) {
				double txnAmt = 0;
				String txnCurCode = null;
				if (request.getTxn() != null) {
					txnAmt = request.getTxn().getTxnamount();
					txnCurCode = request.getTxn().getTxncurrencycode();
				}
				
				reftxn = updateFinTransaction(reftxn, finTxnState, productId, txnAmt, txnCurCode);
				session.saveOrUpdate(reftxn);
				
				String txnData = null;
				if (request.getTxn() != null && request.getTxn().getTxndata() != null)
					txnData = request.getTxn().getTxndata();
				IproFinTxnState newtxnState = constructFinTxnState(reftxn.getFinTxnId(), finTxnState, txnData);
				session.saveOrUpdate(newtxnState);
				
				if (txndata != null && txndata.length > 0) {
					IproFinProviderTxnDetail detail = constructFinProviderTxnDetail(request, reftxn, txndata);
					session.saveOrUpdate(detail);
				}
			} else {
				throw new InvalidPurchaseIdException(messageSource.getMessage("INVALID_PURCHASE_ID",null,null));
			}
			
			tx.commit();
		} catch (InvalidProductException ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new InvalidProductException(ex.getMessage());
		} catch (InvalidPurchaseIdException ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new InvalidPurchaseIdException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}		
	
	/* Get the current Financial Transaction state from user uuid */
	public TxnInfo getCurrentFinTxnState(String userUuid) {
		Session session = getCurrentSession();
		TxnInfo txn = null;
		try {			
			StringBuilder queryString = new StringBuilder();
			queryString.append("select tx from IproFinTxnState tx, IproFinTransaction ft, IproUser u where ");
			queryString.append("hex(ft.iproUser.id)=hex(u.userUuid) and hex(u.userUuid)=:userUuid and ");
			queryString.append("hex(tx.iproFinTransaction.id)=hex(ft.finTxnId) and ");
			queryString.append("u.iproUserAccountType.id=:accType and ");
			queryString.append("ft.iproTxnType.id=:txnType ");
			queryString.append("order by tx.finTxnStateId desc");
			
			logger.debug("User UUID  : " + userUuid);
			Query query = session.createQuery(queryString.toString());
			query.setParameter("accType", UserAccountTypeEnum.REG.name());
	        query.setParameter("userUuid", userUuid);
	        query.setParameter("txnType",TxnTypeEnum.PUR.name());
	        query.setMaxResults(1);
	        			
	        logger.debug("User UUID : " + userUuid);
	        IproFinTxnState finTxnState = (IproFinTxnState)query.uniqueResult();
			if(finTxnState != null && finTxnState.getIproFinTransaction() != null) {
				String productName = null;
				IproFinTransaction finTxn = finTxnState.getIproFinTransaction();
				if (finTxn.getIproPurchaseProduct() != null)
					productName = finTxn.getIproPurchaseProduct().getProProductIdentifier();
				logger.debug("---------------------Transaction exists-----------------------");
				logger.debug("finTxn.getIproFinTxnStatus().getFinTxnStatusCode() : " + finTxn.getIproFinTxnStatus().getFinTxnStatusCode());
				txn = new TxnInfo(getUUIDString(finTxn.getFinTxnId()), finTxn.getIproFinTxnStatus().getFinTxnStatusCode(), 
						productName, finTxn.getIproProductProvider().getProviderName());
			} else {
				logger.debug("---------------------Transaction does not exists-----------------------");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return txn;
	}
	
	public BigDecimal getChipsForProduct(String productName) {
		Session session = getCurrentSession();
		try {
			StringBuilder queryString = new StringBuilder();			
			queryString.append("select p.chipValue from IproPurchaseProduct p where ");
        	queryString.append("p.proProductIdentifier=:productName and p.enabledInd='Y'");
        	
        	Query query = session.createQuery(queryString.toString());
        	query.setParameter("productName", productName);
        	query.setMaxResults(1);
        	
        	BigDecimal chipValue = (BigDecimal)query.uniqueResult();
			if (chipValue != null)
				return chipValue;
			else
				throw new InvalidProviderException(messageSource.getMessage("INVALID_PRODUCT_ID", null, null));
		} catch (InvalidProviderException ex) {
			throw new InvalidProviderException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public String getTxnReceipt(String txnrefid) {
		Session session = getCurrentSession();
		String receipt = null;
		try {			
			StringBuilder queryString = new StringBuilder();
			queryString.append("select ft.request from IproFinProviderTxnDetail ft where ");			
			queryString.append("hex(ft.iproFinTransaction.id)=:txnrefid ");	
			queryString.append("order by ft.finTxnDetailId desc");
			logger.debug("QueryString to get the receipt : " + queryString.toString());
			Query query = session.createQuery(queryString.toString());
			query.setParameter("status", FinTxnStatusEnum.ACCP.name());		
	        query.setParameter("txnrefid", txnrefid);
	        query.setMaxResults(1);
	        
	        byte[] receiptdata = (byte[])query.uniqueResult();
	        if (receiptdata.length > 0)
	        	receipt = new String(receiptdata);
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return receipt;
	}
	
	private IproFinTransaction constructFinTransaction(UUID txnRefId, int providerId, int productId, String txnStatus, String txnType, 
			IproUser user, int sessionId) {		
		IproFinTransaction finTxn = new IproFinTransaction();
		
		finTxn.setFinTxnId(txnRefId);
		
		IproProductProvider iproProductProvider = new IproProductProvider();
		iproProductProvider.setProviderId(providerId);
		finTxn.setIproProductProvider(iproProductProvider);
		
		IproFinTxnStatus status = new IproFinTxnStatus();
		status.setFinTxnStatusCode(txnStatus);
		finTxn.setIproFinTxnStatus(status);
		
		IproTxnType type = new IproTxnType();
		logger.debug("Transaction type code : " + txnType);
		type.setTxnTypeCode(txnType);
		finTxn.setIproTxnType(type);
		finTxn.setTxnBalanceUpdated("0");
		
		finTxn.setIproUser(user);
		
		if (sessionId > 0) {
			IproUserSession userSession = new IproUserSession();
			userSession.setSessionId(sessionId);
			finTxn.setIproUserSession(userSession);
		}
		
		if (productId > 0) {
			IproPurchaseProduct purchaseProduct = new IproPurchaseProduct();
			purchaseProduct.setProductId(productId);
			finTxn.setIproPurchaseProduct(purchaseProduct);
		}
		
		finTxn.setCreatedDate(getCurrentSQLTimeStamp());
		finTxn.setUpdatedDate(getCurrentSQLTimeStamp());
		return finTxn;
	}
	
	private IproFinTransaction updateFinTransaction(IproFinTransaction finTxn, String status, int productid, double txnAmt, String currencyCode) {
		
		if (txnAmt > 0)
			finTxn.setTxnAmount(BigDecimal.valueOf(txnAmt));
		
		if (currencyCode != null && currencyCode.length() > 0) {
			IproCurrency iproCurrency = new IproCurrency();
			iproCurrency.setCurrencyCode(currencyCode);
			finTxn.setIproCurrency(iproCurrency);
		}
		
		if (productid > 0) {
			IproPurchaseProduct iproPurchaseProduct = new IproPurchaseProduct();
			iproPurchaseProduct.setProductId(productid);
			finTxn.setIproPurchaseProduct(iproPurchaseProduct);
		}
		
		IproFinTxnStatus txnStatus = new IproFinTxnStatus();		
		logger.debug("Status Code : " + FinTxnStatusEnum.valueOf(status).name());
		txnStatus.setFinTxnStatusCode(FinTxnStatusEnum.valueOf(status).name());
		finTxn.setIproFinTxnStatus(txnStatus);

		finTxn.setUpdatedDate(getCurrentSQLTimeStamp());
		return finTxn;
	}
	
	private IproFinProviderTxnDetail constructFinProviderTxnDetail(ChipPurchase request, IproFinTransaction finTxn, byte[] response) {		
		IproFinProviderTxnDetail finTxnDetail = new IproFinProviderTxnDetail();
		finTxnDetail.setIproFinTransaction(finTxn);
		finTxnDetail.setResponse(response);
				
		if (request != null && request.getTxn() != null) {
			if (request.getTxn().getTxnreceipt() != null && request.getTxn().getTxnreceipt().length() > 0)
				finTxnDetail.setRequest(request.getTxn().getTxnreceipt().getBytes());
			
			finTxnDetail.setProAuthId(request.getTxn().getTxnid());
		}
		
		return finTxnDetail;
	}
	
	private IproFinTxnState constructFinTxnState(UUID txnrefId, String status, String txnData) {
		IproFinTxnState txnState = new IproFinTxnState();
		
		IproFinTransaction finTxn = new IproFinTransaction();
		finTxn.setFinTxnId(txnrefId);
		txnState.setIproFinTransaction(finTxn);
		
		txnState.setUpdatedDate(getCurrentSQLTimeStamp());
		
		if (txnData != null && txnData.length() > 0)
			txnState.setTxnData(txnData.getBytes());
		
		IproFinTxnStatus txnStatus = new IproFinTxnStatus();		
		logger.debug("Status Code : " + FinTxnStatusEnum.valueOf(status).name());
		txnStatus.setFinTxnStatusCode(FinTxnStatusEnum.valueOf(status).name());
		txnState.setIproFinTxnStatus(txnStatus);
				
		return txnState;
	}	
}
