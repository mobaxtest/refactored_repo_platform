package com.iprogames.platform.chippurchase.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.chippurchase.common.ChipInformation;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.ChipPurchaseResponse;
import com.iprogames.platform.chippurchase.common.Transaction;
import com.iprogames.platform.chippurchase.common.TxnInfo;
import com.iprogames.platform.chippurchase.common.enums.FinTxnStatusEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.platform.chippurchase.dao.ChipPurchaseDaoHandler;
import com.iprogames.platform.chippurchase.operator.VerifyPurchaseToken;
import com.iprogames.platform.chippurchase.operator.impl.TxnValidationResponse;
import com.iprogames.platform.chippurchase.sm.ChipPurchaseStateMachine;
import com.iprogames.platform.error.Actions;
import com.iprogames.platform.error.ButtonType;
import com.iprogames.platform.error.CodeType;
import com.iprogames.platform.error.CommandType;
import com.iprogames.platform.error.ErrorMessage;
import com.iprogames.platform.error.ErrorType;
import com.iprogames.platform.exception.InvalidProductException;
import com.iprogames.platform.exception.InvalidPurchaseIdException;
import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.platform.wallettransaction.service.WalletTransactionService;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Service
public class ChipPurchaseService {

	Logger logger = LogManager.getLogger(ChipPurchaseService.class);
	
	@SuppressWarnings("rawtypes")
	@Autowired
	ChipPurchaseDaoHandler chippurchasedao;
	
	@Autowired
	WalletTransactionService wallet;
	
	@Autowired
	VerifyPurchaseToken itunes;
	
	@Resource
	MessageSource messageSource;
	
	public ChipPurchaseResponse initiateBuyChipsTransaction(ChipPurchase request, SecureToken secureToken) {
		verifyCurrentFinTransaction(request, secureToken, FinTxnStatusEnum.BCHP);		
		ChipPurchaseResponse response = chippurchasedao.initiateBuyChipsTransaction(request, secureToken);
		if (response != null) {
			UserWallet userwallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), request.getiProToken());
			ChipInformation chipInfo = new ChipInformation("0", userwallet.getBalanceAmount().toString());
			response.setChips(chipInfo);
		}
		return response;
	}
	
	public ChipPurchaseResponse updatePurchaseTransaction(ChipPurchase request, SecureToken secureToken, String status) {
		TxnInfo txn = verifyCurrentFinTransaction(request, secureToken, FinTxnStatusEnum.valueOf(status));		
		chippurchasedao.updatePurchaseTransaction(request, secureToken, status, (txn != null ? txn.getTxnrefid() : null), null);
		if (txn != null && txn.getTxnrefid() != null) {
			request.getTxn().setTxnrefid(txn.getTxnrefid());
		}
		return new ChipPurchaseResponse(request.getiProToken(), request.getTxn());
	}
	
	public ChipPurchaseResponse updateCancelPurchaseTransaction(ChipPurchase request, SecureToken secureToken) {
		ChipPurchaseResponse response = null;
		TxnInfo txn = verifyCurrentFinTransaction(secureToken, FinTxnStatusEnum.CNCL, request.getTxn().getTxnrefid());		
		chippurchasedao.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.CNCL.name(), null, null);
		if (txn.isNextState() && request.getTxn().isIsretry()) {
			request.setProvider(txn.getTxnproviderid());
			logger.info("Provider Id : " + txn.getTxnproviderid());
			response = chippurchasedao.initiateBuyChipsTransaction(request, secureToken);
			if (response != null) {
				UserWallet userwallet = wallet.getBalanceAmount(FundTypeEnum.VCHP.name(), request.getiProToken());
				ChipInformation chipInfo = new ChipInformation("0", userwallet.getBalanceAmount().toString());
				response.setChips(chipInfo);
			}
		}
		return response;
	}
	
	public void updateFailPurchaseTransaction(ChipPurchase request, SecureToken secureToken) {		
		verifyCurrentFinTransaction(secureToken, FinTxnStatusEnum.FAIL, request.getTxn().getTxnrefid());
		chippurchasedao.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.FAIL.name(), null, null);		
	}
	
	/* Update the fin transaction state to Progress;
	 * Verify the receipt with the iTunes server;
	 * update the fin transaction state to Chips issue pending; 
	 * update the wallet with the chips earned and 
	 * update the fin transaction state with the completed state
	 */
	public ChipPurchaseResponse completePurchaseTransaction(ChipPurchase request, SecureToken secureToken) {
		logger.debug("Complete the transaction is called");
		FinTxnStatusEnum finTxnStatus = FinTxnStatusEnum.PROG;
		verifyCurrentFinTransaction(request, secureToken, finTxnStatus);
		chippurchasedao.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.PROG.name(), null,null);
		
		ChipPurchaseResponse response = verifyReceiptAndUpdateWallet(request, secureToken);
		response.setType(ErrorType.notification);
		ErrorMessage message = new ErrorMessage(CodeType.BALANCE_ALERT, null,
				null);
		Actions action = new Actions(ButtonType.CONTINUE, CommandType.ACK);
		message.getActions().add(action);
		response.setMessage(message);
		return response;
	}
	
	/* Verify the receipt with the iTunes server and update the wallet with the chips earned and 
	 * update the fin transaction state with the completed state
	 * If the verification fails, update the transaction state to PEND or FAIL;
	 */
	private ChipPurchaseResponse verifyReceiptAndUpdateWallet (ChipPurchase request, SecureToken secureToken) {
		ChipPurchaseResponse purchaseResponse = null;
		TxnValidationResponse response = itunes.verifyPurchaseToken(request.getTxn().getTxnreceipt());
		chippurchasedao.updatePurchaseTransaction(request, secureToken, response.getStatus(), null, 
				response.getTxnData() != null ? response.getTxnData().getBytes() : null);
		switch(FinTxnStatusEnum.valueOf(response.getStatus())) {
			case CISS:
				/* Issue Chips to user Wallet and Update the transaction as complete*/
				// Update wallet here
				//ChipInformation chipInfo = chippurchasedao.issueChipsToUserWalletAndTxnComplete(request, secureToken.getSid(), FundTypeEnum.VCHP.name());
				ChipInformation chipInfo = updateWallet(request.getiProToken(), request.getTxn().getTxnproductid(), FundTypeEnum.VCHP, 
						request.getTxn().getTxnrefid(), TxnTypeEnum.PUR);
				logger.info("**************Chips Purchased :" + chipInfo.getChipspurchased());
				logger.info("**************Total Chips :" + chipInfo.getTotalchips());
				chippurchasedao.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.COMP.name(), null,null);
				purchaseResponse = new ChipPurchaseResponse(request.getiProToken(), request.getTxn().getTxnrefid(), chipInfo);
				break;
			case PEND:
			case FAIL:
				chippurchasedao.updatePurchaseTransaction(request, secureToken, response.getStatus(), null, null);
				/* throw exception on this status to this user */
				break;
			default:
				break;
		}
		return purchaseResponse;
	}
	
	/* Update the wallet for after verifying the receipt from the iTunes or on reconcile */
	public ChipInformation updateWallet(String iProToken, String productName, FundTypeEnum fundType, String txnId, TxnTypeEnum txnType) {
		BigDecimal chips = chippurchasedao.getChipsForProduct(productName);
		CreditDebitChipsRequest request = new CreditDebitChipsRequest(fundType, chips, txnId, txnType);
		wallet.creditChipsToUserAccount(request, iProToken);
		UserWallet userWallet = wallet.getBalanceAmount(fundType.name(), iProToken);
		return new ChipInformation(chips.toString(), (userWallet != null ? userWallet.getBalanceAmount().toString() : null));
	}
	
	public void reconcileTransaction(ChipPurchase request, SecureToken secureToken) {
		TxnInfo txn = chippurchasedao.getCurrentFinTxnState(secureToken.getUuid());
		if (txn != null) {
			logger.info("********************Next status : " + txn.getTxnstatus());
			List<FinTxnStatusEnum> statusList = ChipPurchaseStateMachine.getNextState(Arrays.asList(FinTxnStatusEnum.valueOf(txn.getTxnstatus())));
			/* Based on the current state do the reconciliation process */
			switch (FinTxnStatusEnum.valueOf(txn.getTxnstatus())) {
				case BCHP:
					chippurchasedao.updatePurchaseTransaction(request, secureToken, statusList.get(0).name(), txn.getTxnrefid(), null);
					break;
				case PCHP:
					if(request.getTxn() != null && request.getTxn().getTxnrefid() != null &&
							request.getTxn().getTxnreceipt() != null &&
							request.getTxn().getTxnrefid().equals(txn.getTxnrefid())) {
						logger.debug("Complete the purchase transaction");
						completePurchaseTransaction(request, secureToken);
					} else {
						logger.debug("Update the transaction to CNCL state");
						chippurchasedao.updatePurchaseTransaction(request, secureToken, statusList.get(0).name(), txn.getTxnrefid(), null);
					}
					break;
				case CISS:
					/* Issue chips to user wallet and complete the transaction */
					if (request.getTxn() == null) {
						request.setTxn(new Transaction(txn.getTxnrefid(), txn.getTxnproductid()));
					} else {
						request.getTxn().setTxnrefid(txn.getTxnrefid());
						request.getTxn().setTxnproductid(txn.getTxnproductid());
					}
					updateWallet(request.getiProToken(), request.getTxn().getTxnproductid(), FundTypeEnum.VCHP, 
							request.getTxn().getTxnrefid(), TxnTypeEnum.PUR);
					/* Update the wallet here */
					chippurchasedao.updatePurchaseTransaction(request, secureToken, statusList.get(0).name(), txn.getTxnrefid(), null);
					break;
				case PROG:
				case PEND:
				case ACCP:
					/* Verify the receipt with iTunes again and update the server */
					request.getTxn().setTxnrefid(txn.getTxnrefid());
					request.getTxn().setTxnproductid(txn.getTxnproductid());
					request.getTxn().setTxnreceipt(chippurchasedao.getTxnReceipt(txn.getTxnrefid()));
					verifyReceiptAndUpdateWallet(request, secureToken);					
					break;					
				default:
					break;
			}
		}
	}
	
	
	/* check for the current transaction state in the table and cancel the transaction based on the next state */
	private TxnInfo verifyCurrentFinTransaction(ChipPurchase request, SecureToken secureToken, FinTxnStatusEnum desiredState) {
		TxnInfo txn = chippurchasedao.getCurrentFinTxnState(secureToken.getUuid());
		if (txn != null) {
			FinTxnStatusEnum txnState = FinTxnStatusEnum.valueOf(txn.getTxnstatus());
			if (ChipPurchaseStateMachine.isAllowedState(EnumSet.of(txnState), EnumSet.of(desiredState))) {
				if (request.getTxn() != null && request.getTxn().getTxnrefid() != null && !request.getTxn().getTxnrefid().equalsIgnoreCase(txn.getTxnrefid())) {
					throw new InvalidPurchaseIdException(messageSource.getMessage("INVALID_TRANSACTION_ID", null, null));
				}
				logger.debug("$$$$$$ Allowed Transaction : " + txnState.name() + "-" + desiredState.name());
			} else {
				logger.debug("State to be checked : " + new EnumSet[] {EnumSet.of(txnState), EnumSet.of(desiredState)});
				logger.debug("Current state: " + txnState.name());
				logger.debug("Desired State : " + desiredState.name());
				List<FinTxnStatusEnum> statusList = ChipPurchaseStateMachine.getNextState(Arrays.asList(txnState, desiredState));
				boolean isBuyChips = false;
				if (statusList != null && statusList.size() > 0) {					
					for(FinTxnStatusEnum status : statusList) {
						logger.debug("Transaction reference Id :" + txn.getTxnrefid());						
						if (status == FinTxnStatusEnum.BCHP) {
							request.setProvider("APPL");
							ChipPurchaseResponse response = chippurchasedao.initiateBuyChipsTransaction(request, secureToken);
							if (response != null && response.getTxn() != null && response.getTxn().getTxnrefid() != null)
								txn = new TxnInfo(response.getTxn().getTxnrefid());							
							isBuyChips = true;
						} else {
							chippurchasedao.updatePurchaseTransaction(request, secureToken, status.name(), txn.getTxnrefid(), null);
						}
					}
					if (!isBuyChips)
						txn = null;
				} else{
					throw new InvalidProductException(messageSource.getMessage("EXPTN_" + txnState.name() + "_" + desiredState.name(), null, null));
				}
			}
		}
		
		return txn;
	}	
	
	/* check for the current transaction state in the table and check for the next state */
	private TxnInfo verifyCurrentFinTransaction(SecureToken secureToken, FinTxnStatusEnum desiredState, String txnRefId) {
		TxnInfo txn = chippurchasedao.getCurrentFinTxnState(secureToken.getUuid());
		if (txn != null) {
			FinTxnStatusEnum txnState = FinTxnStatusEnum.valueOf(txn.getTxnstatus());
			/* Check for the next allowed state */
			logger.debug("$$$$$$ Transaction to check : " + txnState.name() + "-" + desiredState.name());
			List<FinTxnStatusEnum> statusList = ChipPurchaseStateMachine.getNextState(Arrays.asList(txnState, desiredState));
			if (ChipPurchaseStateMachine.isAllowedState(EnumSet.of(txnState), EnumSet.of(desiredState))) {
				logger.debug("$$$$$$ Allowed Transaction : " + txnState.name() + "-" + desiredState.name());
				if (txnRefId != null && txnRefId != null && !txnRefId.equalsIgnoreCase(txn.getTxnrefid())) {
					throw new InvalidPurchaseIdException(messageSource.getMessage("INVALID_TRANSACTION_ID", null, null));
				}
				if (statusList != null && statusList.size() > 0) {					
					txn.setNextState(true);
				}
			} else {
				logger.debug("$$$$$$ Not an Allowed Transaction : " + txnState.name() + "-" + desiredState.name());
				if (statusList != null && statusList.size() > 0) {					
					txn.setNextState(true);
				} else{
					throw new InvalidProductException(messageSource.getMessage("EXPTN_" + txnState.name() + "_" + desiredState.name(), null, null));
				}
			}
		}
		if (txn == null) {
			txn = new TxnInfo(false);
		}
		
		return txn;
	}
}
