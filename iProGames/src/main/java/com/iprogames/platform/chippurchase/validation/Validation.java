package com.iprogames.platform.chippurchase.validation;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.enums.FinTxnStatusEnum;
import com.iprogames.util.constraints.Condition;
import com.iprogames.util.constraints.exception.InvalidArgumentException;
import com.iprogames.util.constraints.exception.NullArgumentException;

@Service("purchaseValidation")
public class Validation {

	@Resource
	MessageSource messageSource;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notNullCondition;
	
	@SuppressWarnings("unchecked")
	public void checkPurchaseTransactionRequest(ChipPurchase request, FinTxnStatusEnum status) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
			
			switch(status) {
				case BCHP:
					notNullCondition.check(request.getProvider());
					break;
				case PCHP:
					notNullCondition.check(request.getTxn());
					notNullCondition.check(request.getTxn().getTxnproductid());
					notNullCondition.check(request.getTxn().getTxnrefid());
					notNullCondition.check(request.getTxn().getTxnamount());
					notNullCondition.check(request.getTxn().getTxncurrencycode());
					break;
				case ACCP:
					notNullCondition.check(request);
					notNullCondition.check(request.getiProToken());
					notNullCondition.check(request.getTxn());
					notNullCondition.check(request.getTxn().getTxnrefid());
					notNullCondition.check(request.getTxn().getTxnreceipt());
					break;
				case ACK:
				case CNCL:
				case FAIL:
					notNullCondition.check(request.getTxn());
					notNullCondition.check(request.getTxn().getTxnrefid());
					break;
				default:
					break;
			}
		} catch (InvalidArgumentException e) {
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
}
