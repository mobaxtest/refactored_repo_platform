package com.iprogames.platform.chippurchase.sm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.iprogames.platform.chippurchase.common.enums.FinTxnStatusEnum;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 */
public class ChipPurchaseStateMachine {

	static Logger logger = LogManager.getLogger(ChipPurchaseStateMachine.class);
	
	private static volatile EnumSet<FinTxnStatusEnum> currentState;

	private static Map<EnumSet<FinTxnStatusEnum>, EnumSet<?>[]> stateMap;
	
	private static Map<List<FinTxnStatusEnum>, EnumSet<?>[]> nextstateMap;
	
	static {
		stateMap = new HashMap<EnumSet<FinTxnStatusEnum>, EnumSet<?>[]>();

		stateMap.put(EnumSet.of(FinTxnStatusEnum.NONE), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.BCHP)});
		stateMap.put(EnumSet.of(FinTxnStatusEnum.BCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.PCHP), EnumSet.of(FinTxnStatusEnum.CNCL) });
		stateMap.put(EnumSet.of(FinTxnStatusEnum.PCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.PROG), EnumSet.of(FinTxnStatusEnum.CNCL) });
		stateMap.put(EnumSet.of(FinTxnStatusEnum.PROG), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CISS), EnumSet.of(FinTxnStatusEnum.FAIL), EnumSet.of(FinTxnStatusEnum.PEND) });
		stateMap.put(EnumSet.of(FinTxnStatusEnum.CISS), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.COMP) });
		stateMap.put(EnumSet.of(FinTxnStatusEnum.COMP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.ACK),EnumSet.of(FinTxnStatusEnum.BCHP) });
		stateMap.put(EnumSet.of(FinTxnStatusEnum.FAIL), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.BCHP)});
		stateMap.put(EnumSet.of(FinTxnStatusEnum.CNCL), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.BCHP)});
		stateMap.put(EnumSet.of(FinTxnStatusEnum.ACK), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.BCHP)});
		stateMap.put(EnumSet.of(FinTxnStatusEnum.PEND), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.FAIL), EnumSet.of(FinTxnStatusEnum.CISS)});
		
		nextstateMap = new HashMap<List<FinTxnStatusEnum>, EnumSet<?>[]>();
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.BCHP, FinTxnStatusEnum.BCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CNCL)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.PCHP, FinTxnStatusEnum.PCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CNCL), EnumSet.of(FinTxnStatusEnum.BCHP)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.PCHP, FinTxnStatusEnum.BCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CNCL)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.PCHP, FinTxnStatusEnum.CNCL), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.BCHP)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.BCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CNCL)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.PCHP), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.CNCL)});
		nextstateMap.put(Arrays.asList(FinTxnStatusEnum.CISS), new EnumSet[] { EnumSet.of(FinTxnStatusEnum.COMP)});
		
		// set initial state
		currentState = EnumSet.of(FinTxnStatusEnum.BCHP);
	}
	
	/**
	 * Determine if the given state is allowed to be next.
	 * 
	 * @param desiredState
	 *            the desired state
	 * 
	 * @return boolean True if the state is reachable from the current state
	 */
	private static boolean isReachable(EnumSet<FinTxnStatusEnum> desiredState) {
		System.out.println("Desired State : " + desiredState);
		for (EnumSet<?> s : stateMap.get(currentState)) {

			if (s.equals(desiredState)) {
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Sets the current application state.
	 * 
	 * @param currentTxnState
	 *            the current txn state 
	 */
	private static void setCurrent(EnumSet<FinTxnStatusEnum> currentTxnState) {
		currentState = currentTxnState;
		logger.debug("at state: " + currentState);
	}
	
	/**
	 * Check if the next state is valid for the current state
	 * 
	 * @param currentState - Current txn state
	 * @param desiredState - next txn state
	 * @return
	 */
	public static boolean isAllowedState(EnumSet<FinTxnStatusEnum> currentState, EnumSet<FinTxnStatusEnum> desiredState) {
		setCurrent(currentState);
		return isReachable(desiredState);
	}
	
	private static List<FinTxnStatusEnum> next(List<FinTxnStatusEnum> stateList) {
		List<FinTxnStatusEnum> states = new ArrayList<FinTxnStatusEnum>();
		if (nextstateMap.get(stateList) != null) {
			for (EnumSet<?> s : nextstateMap.get(stateList)) {
				if (s.iterator().hasNext()) {
					logger.debug("Status code : " + s.iterator().next().name());
					states.add(FinTxnStatusEnum.valueOf(s.iterator().next().name()));
				}
			}
		}
		
		return states;
	}
	
	public static List<FinTxnStatusEnum> getNextState(List<FinTxnStatusEnum> currentState) {		
		return next(currentState);
	}
	
	public static void main(String[] args) {
		List<FinTxnStatusEnum> set = getNextState(Arrays.asList(FinTxnStatusEnum.CISS));
		for(FinTxnStatusEnum status : set) {
			logger.debug(status.name());
		}
				
	}
}