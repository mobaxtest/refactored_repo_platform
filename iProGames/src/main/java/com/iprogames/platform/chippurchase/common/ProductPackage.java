package com.iprogames.platform.chippurchase.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ProductPackage {

    private String productid		= null;
    private String chipvalue		= null;
    private String chipvalueStr		= null;
    private String asserturl		= null;

    public ProductPackage() {
    	
    }
    
    public ProductPackage(String productid, String chipvalue) {
    	this.productid = productid;
    	this.chipvalue = chipvalue;
    }
    
    public ProductPackage(String productid, String chipvalue, String chipvalueStr, String asserturl) {
    	this.productid = productid;
    	this.chipvalue = chipvalue;
    	this.chipvalueStr = chipvalueStr;
    	this.asserturl = asserturl;
    }
    
    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getChipvalue() {
		return chipvalue;
	}

	public void setChipvalue(String chipvalue) {
		this.chipvalue = chipvalue;
	}

	public String getChipvalueStr() {
		return chipvalueStr;
	}

	public void setChipvalueStr(String chipvalueStr) {
		this.chipvalueStr = chipvalueStr;
	}

	public String getAsserturl() {
		return asserturl;
	}

	public void setAsserturl(String asserturl) {
		this.asserturl = asserturl;
	}

	@Override
	public String toString() {
		return "ProductPackage [productid=" + productid + ", chipvalue="
				+ chipvalue + ", chipvalueStr=" + chipvalueStr + ", asserturl="
				+ asserturl + "]";
	}
}
