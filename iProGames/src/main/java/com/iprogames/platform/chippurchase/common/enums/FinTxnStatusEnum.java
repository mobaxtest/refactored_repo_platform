package com.iprogames.platform.chippurchase.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum FinTxnStatusEnum {

	NONE,
	ACCP,
	ACK,
	BCHP,
	CISS,
	COMP,
	FAIL,
	HOLD,
	PCHP,
	PEND,
	PROG,
	REFD,
	VOID,
	CNCL
	
}
