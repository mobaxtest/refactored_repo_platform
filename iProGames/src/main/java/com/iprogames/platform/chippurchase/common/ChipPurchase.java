package com.iprogames.platform.chippurchase.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ChipPurchase {

	private String username		= null;
	private String useremail	= null;
	private String iProToken	= null;
	private String provider		= null;
	private Transaction txn		= null;
	
	public ChipPurchase() {
		
	}
	
	public ChipPurchase(Transaction txn) {
		this.txn = txn;
	}
	
	public ChipPurchase(String iProToken, Transaction txn) {
		this.iProToken = iProToken;
		this.txn = txn;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Transaction getTxn() {
		return txn;
	}

	public void setTxn(Transaction txn) {
		this.txn = txn;
	}

	@Override
	public String toString() {
		return "ChipPurchase [username=" + username + ", useremail="
				+ useremail + ", iProToken=" + iProToken + ", provider="
				+ provider + ", txn=" + txn.toString() + "]";
	}
}
