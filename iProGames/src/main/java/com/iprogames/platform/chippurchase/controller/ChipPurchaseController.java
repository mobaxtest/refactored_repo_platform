package com.iprogames.platform.chippurchase.controller;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.ChipPurchaseResponse;
import com.iprogames.platform.chippurchase.common.enums.FinTxnStatusEnum;
import com.iprogames.platform.chippurchase.service.ChipPurchaseService;
import com.iprogames.platform.chippurchase.validation.Validation;
import com.iprogames.platform.profiler.Monitorable;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Controller
@RequestMapping("/purchase")
public class ChipPurchaseController {
	
	Logger logger = LogManager.getLogger(ChipPurchaseController.class);
	
	private AuthenticationService service;
	
	private ChipPurchaseService chipPurchaseService;
	
	private Validation purchaseValidation;
	
	@Resource
	MessageSource messageSource;
	
	@Autowired
    public ChipPurchaseController(AuthenticationService service, ChipPurchaseService chipPurchaseService, 
    		Validation purchaseValidation) {
        this.service = service;
        this.chipPurchaseService = chipPurchaseService;
        this.purchaseValidation = purchaseValidation;
    }
	
	/** Buy Chips Initiation request 
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - ChipPurchaseResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				FundNotEnabledException - Fund not enabled
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidProviderException - Not a valid provider
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/initiate/buychips", method = RequestMethod.POST)
	@ResponseBody
	public ChipPurchaseResponse initiateBuyChips(@RequestBody ChipPurchase request) {
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.BCHP);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());
		return chipPurchaseService.initiateBuyChipsTransaction(request, secureToken);
	}
	
	/** Package Initiation request 
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxnrefid() - Transaction reference Id
	 * 			request.getTxn().getTxnproductid() - Valid product to be purchased
	 * 			request.getTxn().getTxnamount() - Transacted amount
	 * 			request.getTxn().getTxncurrencycode() - Curreny Code in which the transaction is done
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - ChipPurchaseResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				InvalidSessionException - Not a valid session status
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidPurchaseIdException - Not a valid transaction reference id
	 * 				InvalidProductException - Not a valid product to be purchased
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/initiate", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public ChipPurchaseResponse initiatePurchaseTransaction(@RequestBody ChipPurchase request) {
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.PCHP);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());
		return chipPurchaseService.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.PCHP.name());
	}
	
	/** Purchase completion request 
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxnrefid() - Transaction reference Id
	 * 			request.getTxn().getTxnreceipt() - Valid product to be purchased
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - ChipPurchaseResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				InvalidSessionException - Not a valid session status
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidPurchaseIdException - Not a valid transaction reference id
	 * 				InvalidProductException - Not a valid product to be purchased
	 * 				FundNotEnabledException - Fund not enabled
	 * 				TransactionTypeNotEnabledException - Given txn type is not enabled
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/complete", method = RequestMethod.POST)
	@ResponseBody
	public ChipPurchaseResponse completePurchaseTrasaction(@RequestBody ChipPurchase request) {
		logger.debug("***********************Complete the transaction");
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.ACCP);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());
		return chipPurchaseService.completePurchaseTransaction(request, secureToken);		
	}
	
	/** Purchase acknowledge request
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxnrefid() - Transaction reference Id
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - void
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				InvalidSessionException - Not a valid session status
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidPurchaseIdException - Not a valid transaction reference id
	 * 				InvalidProductException - Not a valid product to be purchased
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/ack", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public void acknowledgePurchaseTransaction(@RequestBody ChipPurchase request) {
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.ACK);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());
		chipPurchaseService.updatePurchaseTransaction(request, secureToken, FinTxnStatusEnum.ACK.name());
	}
	
	/** Purchase cancel request 
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxnrefid() - Transaction reference Id
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - ChipPurchaseResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				InvalidSessionException - Not a valid session status
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidPurchaseIdException - Not a valid transaction reference id
	 * 				InvalidProductException - Not a valid product to be purchased
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public ChipPurchaseResponse cancelPurchaseTrasaction(@RequestBody ChipPurchase request) {
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.CNCL);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());
		return chipPurchaseService.updateCancelPurchaseTransaction(request, secureToken);
	}
	
	/** Purchase failed request
	 * 
	 * @params - request - ChipPurchase
	 * 			request.getiProToken() - IproToken to validate the user
	 * 			request.getProvider() - Provider - now it is APPL
	 * 			request.getTxn().getTxnrefid() - Transaction reference Id
	 * 			request.getTxn().getTxndata() (Optional) - Txn data if to be 
	 * 									recorded in the server for the client 
	 * @return - void
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments 				
	 * 				InvalidSessionException - Not a valid session status
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidPurchaseIdException - Not a valid transaction reference id
	 * 				InvalidProductException - Not a valid product to be purchased
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/fail", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public void failPurchaseTrasaction(@RequestBody ChipPurchase request) {
		purchaseValidation.checkPurchaseTransactionRequest(request, FinTxnStatusEnum.FAIL);
		SecureToken secureToken = service.validateiProTokenInternal(request.getiProToken());		
		chipPurchaseService.updateFailPurchaseTransaction(request, secureToken);
	}
}