package com.iprogames.platform.chippurchase.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ChipInformation {

	private String totalchips		= null;
	private String chipspurchased	= null;
	
	public ChipInformation() {
		
	}
	
	public ChipInformation(String chipspurchased) {
		this.chipspurchased = chipspurchased;
	}
	
	public ChipInformation(String chipspurchased, String totalchips) {
		this.chipspurchased = chipspurchased;
		this.totalchips = totalchips;
	}

	public String getTotalchips() {
		return totalchips;
	}

	public void setTotalchips(String totalchips) {
		this.totalchips = totalchips;
	}

	public String getChipspurchased() {
		return chipspurchased;
	}

	public void setChipspurchased(String chipspurchased) {
		this.chipspurchased = chipspurchased;
	}

	@Override
	public String toString() {
		return "ChipInformation [totalchips=" + totalchips
				+ ", chipspurchased=" + chipspurchased + "]";
	}
}
