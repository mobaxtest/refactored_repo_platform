package com.iprogames.platform.chippurchase.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum ProductProviderEnum {

	APPL, // Apple Provider 
	IPROGST, //Provider for the guest user
	IPROREG, // Provider for the registered user
	IPROGREG // Provider for the user who is registering in the 
			 //	iproserver playing as guest
	
}
