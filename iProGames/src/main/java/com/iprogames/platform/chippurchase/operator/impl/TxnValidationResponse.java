package com.iprogames.platform.chippurchase.operator.impl;

public class TxnValidationResponse {

	private String status;
	private String txnData;
	
	public TxnValidationResponse() {
		
	}
	
	public TxnValidationResponse(String status, String txnData) {
		this.status = status;
		this.txnData = txnData;		
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxnData() {
		return txnData;
	}

	public void setTxnData(String txnData) {
		this.txnData = txnData;
	}
}
