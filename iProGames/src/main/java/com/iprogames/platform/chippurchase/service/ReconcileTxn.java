package com.iprogames.platform.chippurchase.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.chippurchase.common.ChipPurchase;
import com.iprogames.platform.chippurchase.common.Transaction;

@Service
public class ReconcileTxn implements Runnable {

	Logger logger = LogManager.getLogger(ReconcileTxn.class);
	
	@Autowired
	ChipPurchaseService service;
	
	@Autowired
	AuthenticationService authService;
	
	private String iProToken;
	private Transaction txn;
	
	public ReconcileTxn() {
		
	}
	
	public ReconcileTxn (String iProToken, Transaction txn) {
		this.iProToken = iProToken;
		this.txn = txn;
	}
	
	public void setTxn(String iProToken, Transaction txn) {
		this.iProToken = iProToken;
		this.txn = txn;
	}
	
	public void startProcess() {
	    Thread t = new Thread(this);
	    t.start();
	}
	
	@Override
	public void run() {
		logger.debug("Thread Started for reconcile flow");
		ChipPurchase request = new ChipPurchase (iProToken, txn);
		logger.debug("IPRO TOKEN to be validated : " + iProToken);
		SecureToken token = authService.validateiProTokenInternal(iProToken);
		service.reconcileTransaction(request, token);
		logger.debug("Thread stopped after reconcile flow");
	}

}
