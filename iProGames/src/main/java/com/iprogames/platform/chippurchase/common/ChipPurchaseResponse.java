package com.iprogames.platform.chippurchase.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.iprogames.platform.error.ErrorMessage;
import com.iprogames.platform.error.ErrorType;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class ChipPurchaseResponse {

	private String iProToken				= null;
	private String provider					= null;
	private Transaction txn					= null;
	private ChipInformation chips			= null;
	private List<ProductPackage> packages 	= new ArrayList<ProductPackage>();
	private ErrorType type;
    private ErrorMessage message;
    
	public ChipPurchaseResponse() {
		
	}
	
	public ChipPurchaseResponse(String iProToken, Transaction txn) {
		this.iProToken = iProToken;
		this.txn = txn;
		this.txn.setTxndata(null);
	}
	
	public ChipPurchaseResponse(String iProToken, String txnrefid, List<ProductPackage> packages) {
		this.iProToken = iProToken;
		this.txn = new Transaction(txnrefid);
		this.packages = packages;
	}
	
	public ChipPurchaseResponse(String iProToken, String txnrefid, ChipInformation chips) {
		this.iProToken = iProToken;
		this.txn = new Transaction(txnrefid);
		this.chips = chips;
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Transaction getTxn() {
		return txn;
	}

	public void setTxn(Transaction txn) {
		this.txn = txn;
	}

	public ChipInformation getChips() {
		return chips;
	}

	public void setChips(ChipInformation chips) {
		this.chips = chips;
	}
	
	public List<ProductPackage> getPackages() {
		if (packages == null) {
			packages = new ArrayList<ProductPackage>();
		}
		
        return packages;
    }

    public void setPackages(List<ProductPackage> packages) {
        this.packages = packages;
    }

	public ErrorType getType() {
		return type;
	}

	public void setType(ErrorType type) {
		this.type = type;
	}

	public ErrorMessage getMessage() {
		return message;
	}

	public void setMessage(ErrorMessage message) {
		this.message = message;
	}
	
}
