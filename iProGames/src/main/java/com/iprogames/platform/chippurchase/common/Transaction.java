package com.iprogames.platform.chippurchase.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class Transaction {

	private String txnrefid			= null;
	private String txnid			= null;
	private String txnreceipt		= null;
	private String txndata			= null;
	private String txnproductid		= null;
	private double txnamount;
	private String txncurrencycode	= null;
	private boolean isretry;
	
	public Transaction() {
		
	}
	
	public Transaction(String txnrefid) {
		this.txnrefid = txnrefid;		
	}
	
	public Transaction(String txnrefid, String txnproductid) {
		this.txnrefid = txnrefid;
		this.txnproductid = txnproductid;
	}

	public Transaction(String txnrefid, String txnreceipt, String txndata) {
		this.txnrefid = txnrefid;	
		this.txnreceipt = txnreceipt;
		this.txndata = txndata;
	}
	
	public String getTxnrefid() {
		return txnrefid;
	}

	public void setTxnrefid(String txnrefid) {
		this.txnrefid = txnrefid;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getTxnreceipt() {
		return txnreceipt;
	}

	public void setTxnreceipt(String txnreceipt) {
		this.txnreceipt = txnreceipt;
	}

	public String getTxndata() {
		return txndata;
	}

	public void setTxndata(String txndata) {
		this.txndata = txndata;
	}

	public String getTxnproductid() {
		return txnproductid;
	}

	public void setTxnproductid(String txnproductid) {
		this.txnproductid = txnproductid;
	}

	public double getTxnamount() {
		return txnamount;
	}

	public void setTxnamount(double txnamount) {
		this.txnamount = txnamount;
	}

	public String getTxncurrencycode() {
		return txncurrencycode;
	}

	public void setTxncurrencycode(String txncurrencycode) {
		this.txncurrencycode = txncurrencycode;
	}

	public boolean isIsretry() {
		return isretry;
	}

	public void setIsretry(boolean isretry) {
		this.isretry = isretry;
	}

	@Override
	public String toString() {
		return "Transaction [txnrefid=" + txnrefid + ", txnid=" + txnid
				+ ", txnreceipt=" + txnreceipt + ", txndata=" + txndata
				+ ", txnproductid=" + txnproductid + ", txnamount=" + txnamount
				+ ", txncurrencycode=" + txncurrencycode + ", isretry="
				+ isretry + "]";
	}
}
