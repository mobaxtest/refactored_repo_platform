package com.iprogames.platform.chippurchase.operator;

import com.iprogames.platform.chippurchase.operator.impl.TxnValidationResponse;

public interface VerifyPurchaseToken {

	public TxnValidationResponse verifyPurchaseToken(String txnReceipt);
	
}
