package com.iprogames.platform.gameserver.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class GameSelectResponse {

	private List<GameEvent> gameEvents				= new ArrayList<GameEvent>();
	private UserGameSearchOptions searchOptions		= null;
	
	public GameSelectResponse() {
		
	}
	
	public GameSelectResponse(List<GameEvent> gameEvents) {
		this.gameEvents = gameEvents;
	}
	
	public GameSelectResponse(UserGameSearchOptions searchOptions) {
		this.searchOptions = searchOptions;
	}
	
	public GameSelectResponse(List<GameEvent> gameEvents, UserGameSearchOptions searchOptions) {
		this.gameEvents = gameEvents;
		this.searchOptions = searchOptions;
	}

	public List<GameEvent> getGameEvents() {
		if (gameEvents == null) {
			gameEvents = new ArrayList<GameEvent>();
		}
		return gameEvents;
	}

	public void setEvents(List<GameEvent> gameEvents) {
		this.gameEvents = gameEvents;
	}

	public UserGameSearchOptions getSearchOptions() {
		return searchOptions;
	}

	public void setSearchOptions(UserGameSearchOptions searchOptions) {
		this.searchOptions = searchOptions;
	}
}
