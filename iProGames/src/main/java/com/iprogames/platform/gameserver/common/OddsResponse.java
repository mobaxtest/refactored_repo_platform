package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class OddsResponse extends PicksResponse {

	private Odds odds;
	
	public OddsResponse() {
		
	}

	/**
	 * @return the odds
	 */
	public Odds getOdds() {
		return odds;
	}

	/**
	 * @param odds the odds to set
	 */
	public void setOdds(Odds odds) {
		this.odds = odds;
	}

	@Override
	public String toString() {
		return "OddsResponse [odds=" + odds + super.toString() + "]";
	}
}
