package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class PicksEvent {

	private String type; // Various event type
	private int down;
    private String pos; // Possesion
    private String yl; // YardLine from the offensive team perspective
    private String ytg; // Yards to go
    private String dir; // Direction
    private String desc;
    private int per; // period or quarter
    private int gtm; // Game Time left mins for the quarter
    private int gts; // Game Time left sec for the quarter
    private Team homeTeam;
    private Team awayTeam;
    
    public PicksEvent() {
    	
    }

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the down
	 */
	public int getDown() {
		return down;
	}

	/**
	 * @param down the down to set
	 */
	public void setDown(int down) {
		this.down = down;
	}

	/**
	 * @return the pos
	 */
	public String getPos() {
		return pos;
	}

	/**
	 * @param pos the pos to set
	 */
	public void setPos(String pos) {
		this.pos = pos;
	}

	/**
	 * @return the yl
	 */
	public String getYl() {
		return yl;
	}

	/**
	 * @param yl the yl to set
	 */
	public void setYl(String yl) {
		this.yl = yl;
	}

	/**
	 * @return the ytg
	 */
	public String getYtg() {
		return ytg;
	}

	/**
	 * @param ytg the ytg to set
	 */
	public void setYtg(String ytg) {
		this.ytg = ytg;
	}

	/**
	 * @return the dir
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the per
	 */
	public int getPer() {
		return per;
	}

	/**
	 * @param per the per to set
	 */
	public void setPer(int per) {
		this.per = per;
	}

	/**
	 * @return the gtm
	 */
	public int getGtm() {
		return gtm;
	}

	/**
	 * @param gtm the gtm to set
	 */
	public void setGtm(int gtm) {
		this.gtm = gtm;
	}

	/**
	 * @return the gts
	 */
	public int getGts() {
		return gts;
	}

	/**
	 * @param gts the gts to set
	 */
	public void setGts(int gts) {
		this.gts = gts;
	}

	/**
	 * @return the homeTeam
	 */
	public Team getHomeTeam() {
		return homeTeam;
	}

	/**
	 * @param homeTeam the homeTeam to set
	 */
	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	/**
	 * @return the awayTeam
	 */
	public Team getAwayTeam() {
		return awayTeam;
	}

	/**
	 * @param awayTeam the awayTeam to set
	 */
	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	@Override
	public String toString() {
		return "PicksEvent [type=" + type + ", down=" + down + ", pos=" + pos
				+ ", yl=" + yl + ", ytg=" + ytg + ", dir=" + dir + ", desc="
				+ desc + ", per=" + per + ", gtm=" + gtm + ", gts=" + gts
				+ ", homeTeam=" + homeTeam.toString() + ", awayTeam=" + awayTeam.toString() + "]";
	}
    
}
