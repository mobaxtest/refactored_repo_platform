package com.iprogames.platform.gameserver.validation;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.gameserver.common.GameSelectRequest;
import com.iprogames.platform.gameserver.common.GetActiveGameRequest;
import com.iprogames.platform.gameserver.common.UserGameCreateRequest;
import com.iprogames.platform.gameserver.common.UserGameJoinRequest;
import com.iprogames.platform.gameserver.common.UserGameSearchRequest;
import com.iprogames.util.constraints.Condition;
import com.iprogames.util.constraints.exception.InvalidArgumentException;
import com.iprogames.util.constraints.exception.NullArgumentException;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * Validation for all game server request objects
 */
@Service("gameValidation")
public class Validation {

	Logger logger = LogManager.getLogger(Validation.class);
	@Resource
	MessageSource messageSource;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notNullCondition;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private Condition notValidIntegerCondition;
	
	@SuppressWarnings("unchecked")
	public void checkGameSelectRequest(GameSelectRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
			
		} catch (InvalidArgumentException e) {
			logger.error("GameSelectRequest is Invalid : "+ request.toString());
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			logger.error("GameSelectRequest is contain null values for required parameter :"+ request.toString());
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkUserGameSearchRequest(UserGameSearchRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
			notNullCondition.check(request.getEventId());
		} catch (InvalidArgumentException e) {
			logger.error("UserGameSearchRequest is Invalid : "+ request.toString());
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));			
		} catch (NullArgumentException e) {
			logger.error("UserGameSearchRequest is contain null values for required parameter :"+ request.toString());
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkUserGameCreateRequest(UserGameCreateRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
			notNullCondition.check(request.getEventId());
			notValidIntegerCondition.check(request.getNoOfSeat());
			notValidIntegerCondition.check(request.getPicksCount());
			notValidIntegerCondition.check(request.getRebuyCount());
		} catch (InvalidArgumentException e) {
			logger.error("UserGameCreateRequest is Invalid : "+ request.toString());
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			logger.error("UserGameCreateRequest is contain null values for required parameter :"+ request.toString());
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkUserGameJoinRequest(UserGameJoinRequest request) {
		try {
			notNullCondition.check(request);
			notValidIntegerCondition.check(request.getUserGameId());
		} catch (InvalidArgumentException e) {
			logger.error("UserGameJoinRequest is Invalid : "+ request.toString());
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			logger.error("UserGameJoinRequest is contain null values for required parameter :"+ request.toString());
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkgetActiveGamesRequest(GetActiveGameRequest request) {
		try {
			notNullCondition.check(request);
			notNullCondition.check(request.getiProToken());
		} catch (InvalidArgumentException e) {
			logger.error("GetActiveGameRequest is Invalid : "+ request.toString());
			throw new InvalidArgumentException(messageSource.getMessage("INVALID_PARAM", null, null));
		} catch (NullArgumentException e) {
			logger.error("GetActiveGameRequest is contain null values for required parameter :"+ request.toString());
			throw new NullArgumentException(messageSource.getMessage("MISSING_PARAM", null, null));
		}
	}
	
	
}
