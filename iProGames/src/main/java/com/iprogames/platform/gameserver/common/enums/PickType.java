package com.iprogames.platform.gameserver.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum PickType {

	CAT("Catch"),
	RUN("Run"),
	TD("Touch Down"),
	FUM("Fumble"),
	PAS("Pass"),
	INT("Interception"),
	FD("First Down"),
	NFD("No First Down"),
	FG("Field Goal"),
	NFG("No Field Goal"),
	SAC("Sack"),
	TAC("Tackle"),
	TO("Turn Over"),
	TEY("1-3 Yards"),
	ETY("4-8 Yards"),
	TFY("9-35 Yards"),
	HY("Hundred Yards"),
	LH("Left Hash"),
	RH("Right Hash"),
	MH("Middle Hash");
	
	
	private String pickType;
	
	private static Map<String, PickType> pickMapping;
	private PickType(String pickType) {
		   this.pickType = pickType;
	}

	public String getPickType(){
		return pickType;
	}
	
	public static PickType getPickType(String pickType){
		if(pickMapping == null){
			initMapping();
		}
		return pickMapping.get(pickType);
	}
	
	private static void initMapping(){
		pickMapping = new HashMap<String, PickType>();
		for(PickType c : values()){
			pickMapping.put(c.pickType, c);
		}
	}
}
