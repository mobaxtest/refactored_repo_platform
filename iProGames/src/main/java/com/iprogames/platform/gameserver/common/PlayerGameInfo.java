package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class PlayerGameInfo {
	private int userGameId		= 0;
	private int addChipsLeft	= 0;
	private int picksRemain		= 0;	
	private double chipsBalance	= 0;
	
	public PlayerGameInfo() {
		
	}
	
	public PlayerGameInfo(int userGameId, int addChipsLeft, int picksRemain, double chipsBalance) {
		this.userGameId = userGameId;
		this.addChipsLeft = addChipsLeft;
		this.picksRemain = picksRemain;
		this.chipsBalance = chipsBalance;
	}
	
	public PlayerGameInfo(int userGameId) {
		this.userGameId = userGameId;
	}

	public int getUserGameId() {
		return userGameId;
	}

	public void setUserGameId(int userGameId) {
		this.userGameId = userGameId;
	}
	
	public int getAddChipsLeft() {
		return addChipsLeft;
	}

	public void setAddChipsLeft(int addChipsLeft) {
		this.addChipsLeft = addChipsLeft;
	}

	public int getPicksRemain() {
		return picksRemain;
	}

	public void setPicksRemain(int picksRemain) {
		this.picksRemain = picksRemain;
	}

	public double getChipsBalance() {
		return chipsBalance;
	}

	public void setChipsBalance(double chipsBalance) {
		this.chipsBalance = chipsBalance;
	}

	@Override
	public String toString() {
		return "PlayerGameInfo [userGameId=" + userGameId + ", addChipsLeft="
				+ addChipsLeft + ", picksRemain=" + picksRemain
				+ ", chipsBalance=" + chipsBalance + "]";
	}
	
}
