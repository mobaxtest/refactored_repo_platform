package com.iprogames.platform.gameserver.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class UserGameSearchOptions {

	List<Integer> opponents			= new ArrayList<Integer>();
	List<Integer> rebuys			= new ArrayList<Integer>();
	List<Integer> picksPerGame		= new ArrayList<Integer>();
	List<Double> chips				= new ArrayList<Double>();
	
	public UserGameSearchOptions() {
		
	}	

	public List<Integer> getOpponents() {
		return opponents;
	}

	public void setOpponents(List<Integer> opponents) {
		this.opponents = opponents;
	}

	public List<Integer> getRebuys() {
		return rebuys;
	}

	public void setRebuys(List<Integer> rebuys) {
		this.rebuys = rebuys;
	}

	public List<Integer> getPicksPerGame() {
		return picksPerGame;
	}

	public void setPicksPerGame(List<Integer> picksPerGame) {
		this.picksPerGame = picksPerGame;
	}
	
	public List<Double> getChips() {
		return chips;
	}

	public void setChips(List<Double> chips) {
		this.chips = chips;
	}
}
