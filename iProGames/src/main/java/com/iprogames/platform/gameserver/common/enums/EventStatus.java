package com.iprogames.platform.gameserver.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum EventStatus {

	PRE_GAME ("Pre-Game"),
	IN_PROGRESS ("In-Progress"), 
	FINAL ("Final"), 
	POSTPONED ("Postponed"),
	CANCELLED ("Cancelled"), 
	DELAYED ("Delayed");
	
	private final String value;
	
	EventStatus(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
	public static EventStatus fromValue(String v) {
        for (EventStatus c: EventStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
