package com.iprogames.platform.gameserver.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.authentication.service.AuthenticationService;
import com.iprogames.platform.gameserver.common.GameSelectRequest;
import com.iprogames.platform.gameserver.common.GameSelectResponse;
import com.iprogames.platform.gameserver.common.GetActiveGameRequest;
import com.iprogames.platform.gameserver.common.GetActiveGameResponse;
import com.iprogames.platform.gameserver.common.OddsRequest;
import com.iprogames.platform.gameserver.common.PicksRequest;
import com.iprogames.platform.gameserver.common.PicksResponse;
import com.iprogames.platform.gameserver.common.UserGameCreateRequest;
import com.iprogames.platform.gameserver.common.UserGameCreateResponse;
import com.iprogames.platform.gameserver.common.UserGameJoinRequest;
import com.iprogames.platform.gameserver.common.UserGameJoinResponse;
import com.iprogames.platform.gameserver.common.UserGameSearchRequest;
import com.iprogames.platform.gameserver.common.UserGameSearchResponse;
import com.iprogames.platform.gameserver.service.GameService;
import com.iprogames.platform.gameserver.validation.Validation;
import com.iprogames.platform.profiler.Monitorable;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Controller
@RequestMapping("/game")
public class GameController {

	Logger logger = LogManager.getLogger(GameController.class);
	
	private GameService gameService;
	
	private AuthenticationService authService;
	
	private Validation gameValidation;
	
	@Autowired
    public GameController(GameService gameService, AuthenticationService authService, Validation gameValidation) {
		this.gameService = gameService;
        this.authService = authService;
        this.gameValidation = gameValidation;
    }
	
	/** Select the games for the current week 
	 * 
	 * @params - gameRequest - GameSelectRequest
	 * 			gameRequest.getiProToken() - IproToken to validate the user
	 * 			gameRequest.isSearch() - true - Search options will be sent as a part of response
	 * 								   - false - No search options will be sent in the response
	 * 
	 * @return - GameSelectResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidSessionException - Not an active session status
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public GameSelectResponse gameSelect(@RequestBody GameSelectRequest gameRequest) {
		gameValidation.checkGameSelectRequest(gameRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(gameRequest.getiProToken());
		return gameService.getGameEventList(secureToken, "NFL", gameRequest.isSearch());
	}
	
	/** Select the user games for the event
	 * 
	 * @params - searchRequest - UserGameSearchRequest
	 * 			searchRequest.getiProToken() - IproToken to validate the user
	 * 			searchRequest.getEventId() - eventid from the game select response
	 * 			searchRequest.getOpponents() - Players in the user game
	 * 			searchRequest.getRebuys() - No. of rebuys for the user game
	 * 			searchRequest.getPicksPerGame() - No. of picks for the user game
	 * 
	 * @return - UserGameSearchResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidSessionException - Not an active session status
	 * 				NoEventsFoundException - No active event found for the given event id
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/usergame/search", method = RequestMethod.POST)
	@ResponseBody
	public UserGameSearchResponse userGameSearch(@RequestBody UserGameSearchRequest searchRequest) {
		gameValidation.checkUserGameSearchRequest(searchRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(searchRequest.getiProToken());
		return gameService.getUserGameList(searchRequest, secureToken);
	}
	
	/** Create the user game for the real game
	 * 
	 * @params - createRequest - UserGameCreateRequest
	 * 			createRequest.getiProToken() - IproToken to validate the user
	 * 			createRequest.getEventId() - eventid from the game select response
	 * 			createRequest.getNoOfSeat() - Players in the user game
	 * 			createRequest.getRebuyCount() - No. of rebuys for the user game
	 * 			createRequest.getPicksCount() - No. of picks for the user game
	 * 			createRequest.getGameType()(Optional) - Game Type - If it is not given, default will be REG
	 * 			createRequest.getGameMode()(Optional) - Game Type - If it is not given, default will be PUB
	 * 
	 * @return - UserGameCreateResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidSessionException - Not an active session status
	 * 				NoEventsFoundException - No active event found for the given event id
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/usergame/create", method = RequestMethod.POST)
	@ResponseBody
	public UserGameCreateResponse userGameCreate(@RequestBody UserGameCreateRequest createRequest) {
		gameValidation.checkUserGameCreateRequest(createRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(createRequest.getiProToken());
		return gameService.createUserGame(createRequest, secureToken);
	}
	
	/** Join the user game
	 * 
	 * @params - joinRequest - UserGameJoinRequest
	 * 			createRequest.getiProToken() - IproToken to validate the user
	 * 			createRequest.getUserGameId() - user game idfrom the game search response
	 * 
	 * @return - UserGameJoinResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidSessionException - Not an active session status
	 * 				NoEventsFoundException - No active event found for the given event id
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */
	@Monitorable
	@RequestMapping(value = "/usergame/join", method = RequestMethod.POST)
	@ResponseBody
	public UserGameJoinResponse userGameJoin(@RequestBody UserGameJoinRequest joinRequest) {
		gameValidation.checkUserGameJoinRequest(joinRequest);
		logger.info("Get the token");
		SecureToken secureToken = authService.validateiProTokenInternal(joinRequest.getiProToken());
		logger.info("Got the token");
		return gameService.joinUserGame(joinRequest, secureToken);
	}
	
	/** Get the active games the user is involved
	 * 
	 * @params - gameRequest - GetActiveGameRequest
	 * 			gameRequest.getiProToken() - IproToken to validate the user
	 * 
	 * @return - GetActiveGameResponse
	 * 
	 * @Exception - InvalidArgumentException - Not a valid argument in the request
	 * 				NullArgumentException - Missing mandatory arguments
	 * 				InvalidiProTokenException - Not a valid iprotoken
	 * 				InvalidSessionException - Not an active session status
	 * 				NoEventsFoundException - No active event found for the given event id
	 * 				IProGamesGeneralException - SQL error
	 * 			
	 * 			- Exception messages are handled via controller advice to send custom error
	 * 			  messages in json format
	 * */	
	@Monitorable
	@RequestMapping(value = "/player/activegames", method = RequestMethod.POST)
	@ResponseBody
	public GetActiveGameResponse getActiveGames(@RequestBody GetActiveGameRequest gameRequest) {
		gameValidation.checkgetActiveGamesRequest(gameRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(gameRequest.getiProToken());
		return gameService.getActivePlayerGames(secureToken);
	}
	
	/** Add picks for the user game
	 * 
	 * @params - picksRequest - PicksRequest
	 * 			 picksRequest.getiProToken() - IproToken to validate the user
	 * 
	 * @return - PicksResponse
	 * */
	@Monitorable
	@RequestMapping(value = "/picks/add", method = RequestMethod.POST)
	@ResponseBody
	public PicksResponse addPicks(@RequestBody PicksRequest picksRequest) {
		//gameValidation.checkPicksRequest(picksRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(picksRequest.getiProToken());
		return null;
	}
	
	/**
	 * Get Odds for the various bet fields
	 * @param oddRequest
	 * @return
	 */
	@Monitorable
	@RequestMapping(value = "/picks/fields", method = RequestMethod.POST)
	@ResponseBody
	public PicksResponse getOddsForFields(@RequestBody OddsRequest oddRequest) {
		//gameValidation.checkGetOddsForFieldsRequest(picksRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(oddRequest.getiProToken());
		return null;
	}
	
	/**
	 * Get Property Picks for pick catagory Team, Player, PropPicks
	 * @param oddRequest
	 * @return
	 */
	@Monitorable
	@RequestMapping(value = "/picks/prop/get", method = RequestMethod.POST)
	@ResponseBody
	public PicksResponse getPropPicks(@RequestBody PicksRequest picksRequest) {
		//gameValidation.checkPropPicksRequest(picksRequest);
		SecureToken secureToken = authService.validateiProTokenInternal(picksRequest.getiProToken());
		return null;
	}
}