package com.iprogames.platform.gameserver.service;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.gameserver.common.GameSelectResponse;
import com.iprogames.platform.gameserver.common.GetActiveGameResponse;
import com.iprogames.platform.gameserver.common.UserGameCreateRequest;
import com.iprogames.platform.gameserver.common.UserGameCreateResponse;
import com.iprogames.platform.gameserver.common.UserGameJoinRequest;
import com.iprogames.platform.gameserver.common.UserGameJoinResponse;
import com.iprogames.platform.gameserver.common.UserGameSearchRequest;
import com.iprogames.platform.gameserver.common.UserGameSearchResponse;
import com.iprogames.platform.gameserver.common.enums.GameType;
import com.iprogames.platform.gameserver.common.internal.RegularUserGameJoin;
import com.iprogames.platform.gameserver.dao.GameServerDaoHandler;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Service
public class GameService {

	@SuppressWarnings("rawtypes")
	@Autowired
	GameServerDaoHandler gameDao;
	
	@Autowired
	GameTxnService gameTxnService;
	
	@Autowired
	GameTxnService gameTxn;
	Logger logger = LogManager.getLogger(GameService.class);
	/* Get the real game event list */
	public GameSelectResponse getGameEventList(SecureToken secureToken, String leagueName, boolean isSearch) {
		return gameDao.getGameList(secureToken, leagueName, isSearch);
	}
	
	/* Get all the user game event list */
	public UserGameSearchResponse getUserGameList(UserGameSearchRequest request, SecureToken token) {
		return gameDao.getUserGameList(request, token);
	}
	
	/* Create the user game */
	public UserGameCreateResponse createUserGame (UserGameCreateRequest createRequest, SecureToken token) {
		BigDecimal initialChips = gameDao.getIproSportsChipsConfig((createRequest.getChips() == 0) ? 0.00 : createRequest.getChips(), 
				GameType.REG, token.getPlaymode(), null);		
		logger.debug("The create request initial chips :"+createRequest.getChips());
		logger.debug("The iprotoken is "+ createRequest.getiProToken());
		logger.debug("The initial chips "+initialChips);
		gameTxnService.checkUserWalletForBalance(createRequest.getiProToken(), initialChips.doubleValue(), FundTypeEnum.VCHP);
		return gameDao.createUserGame(createRequest, token);
	}
	
	/* Join user game */
	public UserGameJoinResponse joinUserGame (UserGameJoinRequest joinRequest, SecureToken token) {
		/* Check if the user joined already */
		UserGameJoinResponse response = gameDao.checkIfUserAlreadyJoined(joinRequest.getUserGameId(), token.getUuid());
		if (response == null) {
			BigDecimal initialChips = gameDao.getIproSportsChipsConfig(0.00, GameType.REG, token.getPlaymode(), null);	
			logger.info("Got the chips");
			gameTxn.checkUserWalletForBalance(joinRequest.getiProToken(), initialChips.doubleValue(), FundTypeEnum.VCHP);
			RegularUserGameJoin regUserGame = gameDao.joinUserGame(joinRequest, token);
			gameTxnService.updateDebitToWallet(joinRequest.getiProToken(), token.getUuid(), token.getSesid(), 
					FundTypeEnum.VCHP, regUserGame, TxnTypeEnum.WAGR);		
			response = gameDao.updateUserGameJoin(regUserGame);
		}
		return response;
	}
	
	/* Get Active player games */
	public GetActiveGameResponse getActivePlayerGames(SecureToken token) {
		return gameDao.getActivePlayerGames(token);
	}	
}
