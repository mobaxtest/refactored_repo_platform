package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class GetActiveGameRequest {

	private String iProToken;
	
	public GetActiveGameRequest() {
		
	}
	
	public GetActiveGameRequest(String iProToken) {
		this.iProToken = iProToken;
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	@Override
	public String toString() {
		return "GetActiveGameRequest [iProToken=" + iProToken + "]";
	}
}
