package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameCreateResponse {

	private UserGame userGame = null;
	
	public UserGameCreateResponse() {
		
	}
	
	public UserGameCreateResponse(UserGame userGame) {
		this.userGame = userGame;
	}

	public UserGame getUserGame() {
		return userGame;
	}

	public void setUserGame(UserGame userGame) {
		this.userGame = userGame;
	}
}
