package com.iprogames.platform.gameserver.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameJoinResponse {

	private PlayerGameInfo playerGameInfo	= null;
	private List<UserScore> userScore 	= new ArrayList<UserScore>();
	
	public UserGameJoinResponse() {
		
	}	
	
	public UserGameJoinResponse(PlayerGameInfo userGame) {
		this.playerGameInfo = userGame;
	}
	
	public UserGameJoinResponse(PlayerGameInfo userGame, List<UserScore> userScore) {
		this.playerGameInfo = userGame;
		if (userScore != null && userScore.size() > 0)
			this.userScore.addAll(userScore);
	}

	public PlayerGameInfo getPlayerGameInfo() {
		return playerGameInfo;
	}

	public void setPlayerGameInfo(PlayerGameInfo playerGameInfo) {
		this.playerGameInfo = playerGameInfo;
	}

	public List<UserScore> getUserScore() {
		return userScore;
	}

	public void setUserScore(List<UserScore> userScore) {
		this.userScore = userScore;
	}	
}
