package com.iprogames.platform.gameserver.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class Picks {

	private int pid;	// Pick id
	private double tpa; // total picks amount
    private int tpc; // total picks count
    private String pref; // Pick reference sent by server
    private List<PickItem> pi; // Pick Items
    
    public Picks() {
    	
    }

	/**
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * @param pid the pid to set
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * @return the tpa
	 */
	public double getTpa() {
		return tpa;
	}

	/**
	 * @param tpa the tpa to set
	 */
	public void setTpa(double tpa) {
		this.tpa = tpa;
	}

	/**
	 * @return the tpc
	 */
	public int getTpc() {
		return tpc;
	}

	/**
	 * @param tpc the tpc to set
	 */
	public void setTpc(int tpc) {
		this.tpc = tpc;
	}

	/**
	 * @return the pref
	 */
	public String getPref() {
		return pref;
	}

	/**
	 * @param pref the pref to set
	 */
	public void setPref(String pref) {
		this.pref = pref;
	}

	/**
	 * @return the pi
	 */
	public List<PickItem> getPi() {
		return pi;
	}

	/**
	 * @param pi the pi to set
	 */
	public void setPi(List<PickItem> pi) {
		this.pi = pi;
	}

	@Override
	public String toString() {
		return "Picks [pid=" + pid + ", tpa=" + tpa + ", tpc=" + tpc
				+ ", pref=" + pref + ", pi=" + pi.toString() + "]";
	}
}
