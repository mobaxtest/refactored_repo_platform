package com.iprogames.platform.gameserver.common.enums;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
public enum GameTxnStatusEnum {

	COMP,
	PROG,
	VOID	
}
