package com.iprogames.platform.gameserver.dao;


import static com.iprogames.util.timestamp.TimeStampUtil.getCurrentSQLTimeStamp;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDFromString;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.iprogames.platform.authentication.common.SecureToken;
import com.iprogames.platform.dao.SessionFactoryHandler;
import com.iprogames.platform.exception.GameNotActiveException;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.exception.InvalidSearchOptionsException;
import com.iprogames.platform.exception.NoEventsFoundException;
import com.iprogames.platform.exception.PlayersExceedingException;
import com.iprogames.platform.gameserver.common.GameEvent;
import com.iprogames.platform.gameserver.common.GameSelectResponse;
import com.iprogames.platform.gameserver.common.GetActiveGameResponse;
import com.iprogames.platform.gameserver.common.PlayerGameInfo;
import com.iprogames.platform.gameserver.common.Team;
import com.iprogames.platform.gameserver.common.UserGame;
import com.iprogames.platform.gameserver.common.UserGameCreateRequest;
import com.iprogames.platform.gameserver.common.UserGameCreateResponse;
import com.iprogames.platform.gameserver.common.UserGameJoinRequest;
import com.iprogames.platform.gameserver.common.UserGameJoinResponse;
import com.iprogames.platform.gameserver.common.UserGameSearchOptions;
import com.iprogames.platform.gameserver.common.UserGameSearchRequest;
import com.iprogames.platform.gameserver.common.UserGameSearchResponse;
import com.iprogames.platform.gameserver.common.UserScore;
import com.iprogames.platform.gameserver.common.enums.EventStatus;
import com.iprogames.platform.gameserver.common.enums.GameMode;
import com.iprogames.platform.gameserver.common.enums.GameType;
import com.iprogames.platform.gameserver.common.internal.RegularUserGameJoin;
import com.iprogames.platform.hazelcast.common.HazelChipsConfig;
import com.iprogames.util.model.sports.IproSports;
import com.iprogames.util.model.sports.IproSportsAddChipsConfig;
import com.iprogames.util.model.sports.IproSportsGameMode;
import com.iprogames.util.model.sports.IproSportsGameType;
import com.iprogames.util.model.sports.IproSportsPicksCntConfig;
import com.iprogames.util.model.sports.IproSportsPlayMode;
import com.iprogames.util.model.sports.IproSportsSeatConfig;
import com.iprogames.util.model.sports.IproSportsTeam;
import com.iprogames.util.model.sports.IproSportsUserGame;
import com.iprogames.util.model.sports.IproSportsUserGameReg;
import com.iprogames.util.timestamp.TimeStampUtil;
import com.iprogames.util.uuid.UUIDUtil;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Repository("gameDao")
public class GameServerDaoHandler <T extends Serializable> extends SessionFactoryHandler {
	Logger logger = LogManager.getLogger(GameServerDaoHandler.class);

	@Resource
	MessageSource messageSource;	
	
	public GameServerDaoHandler() {
		super();
	}
	public GameServerDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	public GameSelectResponse getGameList(SecureToken secureToken, String leagueName, boolean isSearch) {
		Session session = getCurrentSession();
		GameSelectResponse gameresponse = null;
		try {

			List<String> eventstatusList = new ArrayList<String>();
			eventstatusList.add(EventStatus.PRE_GAME.value());
			eventstatusList.add(EventStatus.IN_PROGRESS.value());
			int sportsId = 0;
			StringBuilder queryString = new StringBuilder();
			queryString.append("select e.eventId, t, t1, ")
					.append("e.startDateLocal, e.startDateUtc, es.iproSport.id from ")
					.append("IproSportsEventSchedule e, IproSportsTeam t, IproSportsTeam t1, IproSportsEventStatus es ");
			if (leagueName != null && leagueName.length() > 0) {
				queryString.append(", IproSportsLeague l")
							.append(" where l.abbreviation=:leagueName and l.leagueId=e.leagueId and es.iproSport.id=l.iproSport.id and");
			} else {
				queryString.append(" where");
			}
			queryString.append(" e.homeTeamId=t.teamId and e.awayTeamId=t1.teamId and ")
						.append("es.eventStatusId=e.eventStatusId and (");
			for(int i=0; i < eventstatusList.size(); i++) {
				if(i != 0) {
					queryString.append(" or ");
				}
				queryString.append("es.eventStatusName=:eventstatus").append(i);	
			}
			queryString.append(") and ").append("e.startDateUtc between :weekstart and :weekend order by e.startDateLocal");
			Query query = session.createQuery(queryString.toString());

			DateTimeZone UtcTz = DateTimeZone.UTC;
			
			//DateTime now = new DateTime(UtcTz);
			//Date weekstart = now.withDayOfWeek(DateTimeConstants.WEDNESDAY).withTimeAtStartOfDay().toDateTime(UtcTz).toDate();
			//Date weekend = now.withDayOfWeek(DateTimeConstants.SUNDAY).plusDays( 3 ).withTimeAtStartOfDay().toDateTime(UtcTz).toDate();
			/*LocalDate now = new LocalDate(UtcTz);
			Date weekstart = now.toDateTimeAtStartOfDay(UtcTz).toDate();
			Date weekend = new LocalDate().withDayOfWeek(DateTimeConstants.SUNDAY).dayOfWeek().withMinimumValue().plusDays(3).toDateTimeAtStartOfDay(UtcTz).toDate();
*/			
			DateTime dateTimeUtc = (new DateTime(UtcTz)).withTimeAtStartOfDay();
			
			Date weekstart = dateTimeUtc.toLocalDate().toDate();
			int dayOfWeek = dateTimeUtc.getDayOfWeek();
			Date weekend = null;
			if(dayOfWeek <3){
				weekend = dateTimeUtc.withDayOfWeek(DateTimeConstants.SUNDAY).dayOfWeek().withMinimumValue().plusDays(2).toLocalDate().toDate();
			}else{
				weekend = dateTimeUtc.withDayOfWeek(DateTimeConstants.SUNDAY).dayOfWeek().withMaximumValue().plusDays(3).toLocalDate().toDate();
			}
			logger.debug("The Datetime in UTC is"+dateTimeUtc.toLocalDate().toDate());
			
			query.setParameter("weekstart", weekstart);
			query.setParameter("weekend", weekend);

			logger.debug("The weekstart is "+weekstart.toString());
			logger.debug("The weekend is "+weekend.toString());

			if (leagueName != null && leagueName.length() > 0) {
				query.setParameter("leagueName", leagueName);				
			}
			for(int i=0; i < eventstatusList.size(); i++) {
				query.setParameter("eventstatus" + i, eventstatusList.get(i));
			}

			List<Object> objList = (List<Object>)query.list();
			if (!objList.isEmpty()) {
				List<GameEvent> events = new ArrayList<GameEvent>();
				for(int i = 0; i < objList.size(); i++) {
					Object[] adObjArray = (Object[])objList.get(i);
					IproSportsTeam sth = (IproSportsTeam)adObjArray[1];
					IproSportsTeam sta = (IproSportsTeam)adObjArray[2];
					Team homeTeam = new Team(sth.getTeamName(), sth.getTeamLocation(), sth.getTeamAbbreviation().toUpperCase(), sth.getTintColor(), sth.getOutlineColor());
					Team awayTeam = new Team(sta.getTeamName(), sta.getTeamLocation(), sta.getTeamAbbreviation().toUpperCase(), sta.getTintColor(), sta.getOutlineColor());
					GameEvent event = new GameEvent((Integer)adObjArray[0], homeTeam, awayTeam,
							(Date)adObjArray[3], (Date)adObjArray[4]);

					logger.debug("Local Start Date : " + (Date)adObjArray[3]);

					events.add(event);
					if (sportsId == 0)
						sportsId = (Integer)adObjArray[5];
				}
				logger.debug("Size of the game list : " + events.size());
				gameresponse = new GameSelectResponse();
				gameresponse.getGameEvents().addAll(events);
				logger.debug("List found");
			} else {
				logger.debug("List not found");
			}

			String playMode = secureToken.getPlaymode();
			String gameType = GameType.REG.name();
			if(gameresponse != null && isSearch && sportsId > 0) {
				UserGameSearchOptions searchOptions = new UserGameSearchOptions();
				searchOptions.getOpponents().addAll(getOpponents(sportsId, playMode, gameType, session));
				searchOptions.getRebuys().addAll(getAddChipsCounts(sportsId, playMode, gameType, session));
				searchOptions.getPicksPerGame().addAll(getPicksPerGame(sportsId, playMode, gameType, session));
				searchOptions.getChips().addAll(getChips(sportsId, playMode, gameType, session));
				gameresponse.setSearchOptions(searchOptions);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return gameresponse;
	}

	@SuppressWarnings("unchecked")
	public UserGameSearchResponse getUserGameList(UserGameSearchRequest request, SecureToken token) {
		Session session = getCurrentSession();
		UserGameSearchResponse response = null;
		try {
			logger.debug("1");
			List<String> eventstatusList = new ArrayList<String>();
			eventstatusList.add(EventStatus.PRE_GAME.value());
			eventstatusList.add(EventStatus.IN_PROGRESS.value());
			StringBuilder queryString = new StringBuilder();
			queryString.append("select e.eventId, t, t1 from ");
			queryString.append("IproSportsEventSchedule e, IproSportsTeam t, IproSportsTeam t1, IproSportsEventStatus es ");			
			queryString.append(" where e.homeTeamId=t.teamId and e.awayTeamId=t1.teamId and ");
			queryString.append("es.eventStatusId=e.eventStatusId and (");
			for(int i=0; i < eventstatusList.size(); i++) {
				if(i != 0) {
					queryString.append(" or ");
				}
				queryString.append("es.eventStatusName=:eventstatus").append(i);	
			}
			queryString.append(") and ");
			queryString.append("e.eventId=:eventId");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("eventId", request.getEventId());
			for(int i=0; i < eventstatusList.size(); i++) {
				query.setParameter("eventstatus" + i, eventstatusList.get(i));
			}
			logger.debug("2");
			Object objList = (Object)query.uniqueResult();
			GameEvent event = null;
			if (objList != null) {
				Object[] adObjArray = (Object[])objList;
				IproSportsTeam sth = (IproSportsTeam)adObjArray[1];
				IproSportsTeam sta = (IproSportsTeam)adObjArray[2];
				Team homeTeam = new Team(sth.getTeamName(), sth.getTeamLocation(), sth.getTeamAbbreviation().toUpperCase(), sth.getTintColor(), sth.getOutlineColor());
				Team awayTeam = new Team(sta.getTeamName(), sta.getTeamLocation(), sta.getTeamAbbreviation().toUpperCase(), sta.getTintColor(), sta.getOutlineColor());
				event = new GameEvent((Integer)adObjArray[0], homeTeam, awayTeam);
			} else {
				throw new NoEventsFoundException(messageSource.getMessage("NO_EVENTS", null, null));
			}
			logger.debug("3");

			String playMode = token.getPlaymode();
			
			queryString.delete(0, queryString.length());			
			queryString.append("select ug from IproSportsUserGame ug where ");
			queryString.append("ug.eventId=:eventId and ug.iproSportsPlayMode.id=:playMode and ");
			queryString.append("ug.curUserCount < ug.noOfSeat");
			if (request.getRebuys() != 0)
				queryString.append(" and ug.addChipsCount=:addChipsCount");
			if (request.getOpponents() > 0)
				queryString.append(" and ug.noOfSeat=:noOfSeat");
			if (request.getPicksPerGame() != 0)
				queryString.append(" and ug.picksCount=:picks");

			logger.debug(queryString.toString());
			query = session.createQuery(queryString.toString());
			query.setParameter("eventId", request.getEventId());
			if (request.getRebuys() != 0)
				query.setParameter("addChipsCount", request.getRebuys());
			if (request.getOpponents() > 0)
				query.setParameter("noOfSeat", request.getOpponents() + 1);
			if (request.getPicksPerGame() != 0)
				query.setParameter("picks", request.getPicksPerGame());
			query.setParameter("playMode", playMode);
			
			
			/*queryString.append("select ug from IproSportsUserGame ug ");
			queryString.append("join ug.iproSportsUserGameRegs ugr ");*/
			//queryString.append("left outer join ugr.iproUser u ");
			//queryString.append("left outer join ugr.iproUser u ");//with hex(ugr.iproUser.id)=hex(u.id) ");
			//queryString.append("with hex(u.id)=:userUuid ");
			/*queryString.append("where ");
			queryString.append("ug.eventId=:eventId and ug.iproSportsPlayMode.id=:playMode and ug.curUserCount < ug.noOfSeat ");*/
			//queryString.append("and ug.userGameId=ugr.iproSportsUserGame.userGameId and hex(ugr.iproUser.id)=hex(u.id) and hex(u.id)=:userUuid");
			/*if (request.getRebuys() != 0)
				queryString.append(" and ug.addChipsCount=:addChipsCount");
			if (request.getOpponents() > 0)
				queryString.append(" and ug.noOfSeat=:noOfSeat");
			if (request.getPicksPerGame() != 0)
				queryString.append(" and ug.picksCount=:picks");
			*/

			/*select ug.USER_GAME_ID,ug.EVENT_ID, hex(u.USER_UUID) from IPRO_SPORTS_USER_GAME ug 
			join IPRO_SPORTS_USER_GAME_REG ugr using (USER_GAME_ID) 
			left outer join IPRO_USER u on hex(ugr.USER_UUID)=hex(u.USER_UUID) 
			and hex(ugr.USER_UUID)='64034FE71FF2409DB037E28C6398A9FC' where 
			ug.EVENT_ID=1421825 group by ug.USER_GAME_ID;*/
			
			/*logger.info("Token for the user : " + token.getUuid());
			logger.info("Event Id for the game : " + request.getEventId());
			logger.debug(queryString.toString());
			query = session.createQuery(queryString.toString());
			query.setParameter("eventId", request.getEventId());*/
			//query.setParameter("userUuid", token.getUuid());
			/*if (request.getRebuys() != 0)
				query.setParameter("addChipsCount", request.getRebuys());
			if (request.getOpponents() > 0)
				query.setParameter("noOfSeat", request.getOpponents());
			if (request.getPicksPerGame() != 0)
				query.setParameter("picks", request.getPicksPerGame());
			query.setParameter("playMode", playMode);*/

			List<IproSportsUserGame> userGames = (List<IproSportsUserGame>)query.list();

			queryString.delete(0, queryString.length());			
			queryString.append("select ug from IproSportsUserGame ug, IproSportsUserGameReg ugr where ");
			queryString.append("ug.eventId=:eventId and ug.iproSportsPlayMode.id=:playMode and ");
			queryString.append("ug.curUserCount = ug.noOfSeat and ug.userGameId=ugr.iproSportsUserGame.id and ");
			queryString.append("hex(ugr.userUuid)=:userUuid");
			if (request.getRebuys() != 0)
				queryString.append(" and ug.addChipsCount=:addChipsCount");
			if (request.getOpponents() > 0)
				queryString.append(" and ug.noOfSeat=:noOfSeat");
			if (request.getPicksPerGame() != 0)
				queryString.append(" and ug.picksCount=:picks");

			logger.debug(queryString.toString());
			query = session.createQuery(queryString.toString())
							.setParameter("eventId", request.getEventId())
							.setParameter("userUuid", token.getUuid());
			if (request.getRebuys() != 0)
				query.setParameter("addChipsCount", request.getRebuys());
			if (request.getOpponents() > 0)
				query.setParameter("noOfSeat", request.getOpponents() + 1);
			if (request.getPicksPerGame() != 0)
				query.setParameter("picks", request.getPicksPerGame());
			query.setParameter("playMode", playMode);
			
			List<IproSportsUserGame> joineduserGames = (List<IproSportsUserGame>)query.list();
			if (!joineduserGames.isEmpty()) {
				userGames.addAll(joineduserGames);
			}
			
			if (userGames != null && userGames.size() > 0) {
				logger.debug("User game search results : " + userGames.size());
				event.getUserGames().addAll(constructUserGames(userGames, getUUIDFromString(token.getUuid())));
			} else {
				UserGame bestmatch = getbestMatchUserGames(request, playMode, getUUIDFromString(token.getUuid()), session);
				if (bestmatch != null)
					event.setBestMatch(bestmatch);
				if (request.getRebuys() != 0 || request.getOpponents() == 0 || request.getPicksPerGame() != 0) {
					UserGame createOptions = getCreateOptionsForUserGame(request, GameType.REG, playMode, session);
					if (createOptions != null)
						event.setCreateOptions(createOptions);
				}
			}

			response = new UserGameSearchResponse(event);
		} catch (NoEventsFoundException ex) {
			throw new NoEventsFoundException(ex.getMessage());
		} catch (InvalidSearchOptionsException ex) {
			throw new InvalidSearchOptionsException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}

		return response;
	}	
	
	public UserGameCreateResponse createUserGame (UserGameCreateRequest createRequest, SecureToken token) {
		Session session = getCurrentSession();
		UserGameCreateResponse response = null;
		Transaction tx = null;
		logger.info("&&&&&&&& createRequest.getChips()" + createRequest.getChips());
		try {
			tx = session.beginTransaction();
			List<String> eventstatusList = new ArrayList<String>();
			eventstatusList.add(EventStatus.PRE_GAME.value());
			eventstatusList.add(EventStatus.IN_PROGRESS.value());
			StringBuilder queryString = new StringBuilder();
			queryString.append("select es.iproSport from IproSportsEventSchedule e, IproSportsEventStatus es where ");
			queryString.append("e.eventId=:eventId and ");
			queryString.append("es.eventStatusId=e.eventStatusId and (");
			for(int i=0; i < eventstatusList.size(); i++) {
				if(i != 0) {
					queryString.append(" or ");
				}
				queryString.append("es.eventStatusName=:eventstatus").append(i);	
			}
			queryString.append(")");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("eventId", createRequest.getEventId());
			for(int i=0; i < eventstatusList.size(); i++) {
				query.setParameter("eventstatus" + i, eventstatusList.get(i));
			}
			IproSports sportId = (IproSports) query.uniqueResult();

			GameType gameType = createRequest.getGameType() != null ? GameType.valueOf(createRequest.getGameType()) : GameType.REG;
			
						
			String playMode = token.getPlaymode();
			
			int addChipsCount = getAddChipsCount(createRequest.getRebuyCount(), gameType, 
					playMode, session);

			int noOfSeat = getNoOfSeat(createRequest.getNoOfSeat(), gameType, 
					playMode, session);

			int picksCount = getPicksCount(createRequest.getPicksCount(), gameType, 
					playMode, session);

			BigDecimal initialChips = getIproSportsChipsConfig(createRequest.getChips(), gameType, 
					playMode, session);
			
			IproSportsUserGame userGame = constructUserGameCreate(createRequest, token.getUuid(), playMode, sportId, 
					addChipsCount, noOfSeat, picksCount, initialChips);
			session.saveOrUpdate(userGame);
			logger.debug("The game id is "+ userGame.getUserGameId());
			tx.commit();
			UUID userUuid = getUUIDFromString(token.getUuid());
			boolean isRejoin = false;
			if (userGame.getIproSportsUserGameRegs() != null) {
				for (IproSportsUserGameReg usergameReg : userGame.getIproSportsUserGameRegs()) {
					if (usergameReg.getUserUuid().equals(userUuid)) {
						isRejoin = true;
						break;
					}
				}
			}
			UserGame game = new UserGame(userGame.getUserGameId(), userGame.getEventId(), userGame.getAddChipsCount(),
					userGame.getNoOfSeat()-1, userGame.getPicksCount(),
					userGame.getInitialChips().doubleValue(), 
					GameMode.fromValue(userGame.getIproSportsGameMode().getGameModeCode()).name(),
					GameType.valueOf(userGame.getIproSportsGameType().getGameTypeCode()).value(), isRejoin ? "true" : "false");
			response = new UserGameCreateResponse(game);
		} catch (NoEventsFoundException ex) {
			if (tx != null) tx.rollback();
			throw new NoEventsFoundException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		return response;
	}	
	
	public UserGameJoinResponse checkIfUserAlreadyJoined (int userGameId, String userUUID) {
		Session session = getCurrentSession();
		UserGameJoinResponse response = null;
		try {
			Query query = session.createQuery("select ug from IproSportsUserGameReg ug where ug.iproSportsUserGame.id=:usergameId and hex(ug.userUuid)=:userUUID")
						.setParameter("usergameId", userGameId)
						.setParameter("userUUID", userUUID);
						
			IproSportsUserGameReg usergameJoin = (IproSportsUserGameReg) query.uniqueResult();
			if (usergameJoin != null) {
				int gameId = usergameJoin.getIproSportsUserGame().getUserGameId();
				PlayerGameInfo game = new PlayerGameInfo(usergameJoin.getIproSportsUserGame().getUserGameId(), usergameJoin.getAddChipsRemain(), 
						usergameJoin.getPicksRemain(), usergameJoin.getChipsBalance().doubleValue());
				List<UserScore> scores = getScoresForAllUserinGame(gameId, session);
				response = new UserGameJoinResponse(game, scores);
			} else {
				logger.info("User not joined");
			}
			
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	public RegularUserGameJoin joinUserGame (UserGameJoinRequest joinRequest, SecureToken token) {
		Session session = getCurrentSession();
		RegularUserGameJoin response = null;
		Transaction tx = null;
		logger.info("User game join - start");
		try {
			tx = session.beginTransaction();
			List<String> eventstatusList = new ArrayList<String>();
			eventstatusList.add(EventStatus.PRE_GAME.value());
			eventstatusList.add(EventStatus.IN_PROGRESS.value());
			StringBuilder queryString = new StringBuilder();
			queryString.append("select ug from IproSportsUserGame ug, IproSportsEventSchedule e, IproSportsEventStatus es where ");
			queryString.append("ug.userGameId=:userGameId and e.eventId=ug.eventId and ");
			queryString.append("es.eventStatusId=e.eventStatusId and (");
			for(int i=0; i < eventstatusList.size(); i++) {
				if(i != 0) {
					queryString.append(" or ");
				}
				queryString.append("es.eventStatusName=:eventstatus").append(i);	
			}
			queryString.append(")");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userGameId", joinRequest.getUserGameId());
			for(int i=0; i < eventstatusList.size(); i++) {
				query.setParameter("eventstatus" + i, eventstatusList.get(i));
			}
			IproSportsUserGame userGame = (IproSportsUserGame) query.uniqueResult();
			
			if (userGame == null) {
				throw new GameNotActiveException(messageSource.getMessage("GAME_NOT_ACTIVE", null, null));
			}
			
			query = session.createQuery("select ug from IproSportsUserGameReg ug where ug.iproSportsUserGame.id=:usergameId and hex(ug.userUuid)=:userUUID");
			query.setParameter("usergameId", userGame.getUserGameId());
			query.setParameter("userUUID", token.getUuid());
						
			IproSportsUserGameReg usergameJoin = (IproSportsUserGameReg) query.uniqueResult();
							
			if(userGame.getNoOfSeat() == userGame.getCurUserCount()) {
				throw new PlayersExceedingException(messageSource.getMessage("PLAYERS_EXCEED", null, null));
			}
			
			usergameJoin = constructUserGameJoin(userGame, token.getUuid());
			session.saveOrUpdate(usergameJoin);
			logger.info("The user game reg id is : "+usergameJoin.getUserGameRegId() );
			userGame.setCurUserCount(userGame.getCurUserCount() + 1);
			session.saveOrUpdate(userGame);
			response = new RegularUserGameJoin(userGame.getEventId(), usergameJoin.getUserGameRegId(), userGame.getInitialChips());
			
			logger.info("2");
			tx.commit();
			logger.info("userGame.getEventId() : " + userGame.getEventId());
			
			logger.info("4");
		} catch (GameNotActiveException ex) { 
			if (tx != null) tx.rollback();			
			throw new GameNotActiveException(ex.getMessage());
		} catch (PlayersExceedingException ex) { 
			if (tx != null) tx.rollback();			
			throw new PlayersExceedingException(ex.getMessage());
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		
		return response;
	}
	
	public UserGameJoinResponse updateUserGameJoin(RegularUserGameJoin userGamejoin) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("select ug from IproSportsUserGameReg ug where ug.userGameRegId=:gameId");
			query.setParameter("gameId", userGamejoin.getUserGameRegId());
			
			IproSportsUserGameReg usergameJoin = (IproSportsUserGameReg) query.uniqueResult();
			usergameJoin = updateUserGameJoin(usergameJoin, userGamejoin.getInitialChips());
			session.saveOrUpdate(usergameJoin);
			
			tx.commit();
			int gameId = usergameJoin.getIproSportsUserGame().getUserGameId();
			PlayerGameInfo game = new PlayerGameInfo(gameId, usergameJoin.getAddChipsRemain(), 
					usergameJoin.getPicksRemain(), usergameJoin.getChipsBalance().doubleValue());
			List<UserScore> scores = getScoresForAllUserinGame(gameId, session);
			return new UserGameJoinResponse(game, scores);
		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public GetActiveGameResponse getActivePlayerGames(SecureToken token) {
		Session session = getCurrentSession();
		GetActiveGameResponse response = null;
		try {
			DateTimeZone UtcTz = DateTimeZone.UTC;
			DateTime dateTimeUtc = (new DateTime(UtcTz)).withTimeAtStartOfDay();
			
			Date weekstart = dateTimeUtc.toLocalDate().toDate();
			int dayOfWeek = dateTimeUtc.getDayOfWeek();
			Date weekend = null;
			if(dayOfWeek <3){
				weekend = dateTimeUtc.withDayOfWeek(DateTimeConstants.SUNDAY).dayOfWeek().withMinimumValue().plusDays(2).toLocalDate().toDate();
			}else{
				weekend = dateTimeUtc.withDayOfWeek(DateTimeConstants.SUNDAY).dayOfWeek().withMaximumValue().plusDays(3).toLocalDate().toDate();
			}
			
			List<String> eventstatusList = new ArrayList<String>();
			eventstatusList.add(EventStatus.PRE_GAME.value());
			eventstatusList.add(EventStatus.IN_PROGRESS.value());
			StringBuilder queryString = new StringBuilder();
			queryString.append("select ug, e.eventId, t, t1 from IproSportsUserGameReg ugr, IproSportsUserGame ug, IproSportsTeam t, IproSportsTeam t1, ");
			queryString.append("IproSportsEventSchedule e, IproSportsEventStatus es where ");
			queryString.append("e.eventId=ug.eventId and ");
			queryString.append("e.homeTeamId=t.teamId and e.awayTeamId=t1.teamId and ");
			queryString.append("hex(ugr.userUuid)=:userid and ugr.iproSportsUserGame.id=ug.id and ");
			queryString.append("es.eventStatusId=e.eventStatusId and (");
			for(int i=0; i < eventstatusList.size(); i++) {
				if(i != 0) {
					queryString.append(" or ");
				}
				queryString.append("es.eventStatusName=:eventstatus").append(i);	
			}
			queryString.append(") and ");
			queryString.append("e.startDateUtc between :weekstart and :weekend group by ug.eventId");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("userid", token.getUuid());
			
			logger.debug("The Datetime in UTC is"+dateTimeUtc.toLocalDate().toDate());
			
			query.setParameter("weekstart", weekstart);
			query.setParameter("weekend", weekend);
			for(int i=0; i < eventstatusList.size(); i++) {
				query.setParameter("eventstatus" + i, eventstatusList.get(i));
			}
			
			
			List<Object> objList = (List<Object>) query.list();
			if (!objList.isEmpty()) {
				logger.info("Size of the object : " + objList.size());
				List<GameEvent> events = new ArrayList<GameEvent>();
				List<UserGame> userGames = new ArrayList<UserGame>();
				int previousEventId = 0;
				GameEvent event = null;
				UUID userUuid = getUUIDFromString(token.getUuid());
				for(int i = 0; i < objList.size(); i++) {
					Object[] adObjArray = (Object[])objList.get(i);
					IproSportsUserGame userGame = (IproSportsUserGame)adObjArray[0];
					int eventId = (int)adObjArray[1];
					if (previousEventId == 0) {
						IproSportsTeam sth = (IproSportsTeam)adObjArray[2];
						IproSportsTeam sta = (IproSportsTeam)adObjArray[3];
						Team homeTeam = new Team(sth.getTeamName(), sth.getTeamLocation(), sth.getTeamAbbreviation().toUpperCase(), sth.getTintColor(), sth.getOutlineColor());
						Team awayTeam = new Team(sta.getTeamName(), sta.getTeamLocation(), sta.getTeamAbbreviation().toUpperCase(), sta.getTintColor(), sta.getOutlineColor());
						event = new GameEvent(eventId, homeTeam, awayTeam);
						
					} else if(previousEventId != 0 && eventId != previousEventId) {					
						event.setUserGames(userGames);
						events.add(event);
						IproSportsTeam sth = (IproSportsTeam)adObjArray[2];
						IproSportsTeam sta = (IproSportsTeam)adObjArray[3];
						Team homeTeam = new Team(sth.getTeamName(), sth.getTeamLocation(), sth.getTeamAbbreviation().toUpperCase(), sth.getTintColor(), sth.getOutlineColor());
						Team awayTeam = new Team(sta.getTeamName(), sta.getTeamLocation(), sta.getTeamAbbreviation().toUpperCase(), sta.getTintColor(), sta.getOutlineColor());
						event = new GameEvent(eventId, homeTeam, awayTeam);
						userGames = new ArrayList<UserGame>();
					}
					
					previousEventId = eventId;
					boolean isRejoin = false;
					if (userGame.getIproSportsUserGameRegs() != null) {
						for (IproSportsUserGameReg usergameReg : userGame.getIproSportsUserGameRegs()) {
							if (usergameReg.getUserUuid().equals(userUuid)) {
								isRejoin = true;
								break;
							}
						}
					}
					UserGame game = new UserGame(userGame.getUserGameId(), userGame.getEventId(), userGame.getAddChipsCount(),
							userGame.getNoOfSeat()-1, userGame.getPicksCount(),
							userGame.getInitialChips().doubleValue(), 
							GameMode.fromValue(userGame.getIproSportsGameMode().getGameModeCode()).name(),
							GameType.valueOf(userGame.getIproSportsGameType().getGameTypeCode()).value(), isRejoin ? "true" : "false");
					userGames.add(game);					
				}
				event.setUserGames(userGames);
				events.add(event);
				response = new GetActiveGameResponse(events);				
			}			
			logger.debug("Active games response for the uuid : " + token.getUuid() + " : " + convertObjectToString(response));
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
		
		return response;
	}
	
	private static String convertObjectToString(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
        	return mapper.writeValueAsString(object);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return null;
    }
	
	@SuppressWarnings("unchecked")
	public List<UserScore> getScoresForAllUserinGame(int gameId, Session session) {
		List<UserScore> scores = new ArrayList<UserScore>();
		try {
			Query query = session.createQuery("select ug from IproSportsUserGameReg ug where ug.iproSportsUserGame.id=:gameId");
			query.setParameter("gameId", gameId);
			
			List<IproSportsUserGameReg> usergames = (List<IproSportsUserGameReg>) query.list();
			
			if (usergames != null && usergames.size() > 0) {
				for (IproSportsUserGameReg usergame : usergames) {
					scores.add(new UserScore(UUIDUtil.getUUIDString(usergame.getUserUuid()), usergame.getScore(), 0));
				}
			}			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		
		return scores;
	}
	
	private UserGame getCreateOptionsForUserGame (UserGameSearchRequest request, GameType gameType, String playMode, Session session) {
		
		int noOfSeat = getNoOfSeat(request.getOpponents(), gameType, 
				playMode, session);		
			
		int addChipsCount = getAddChipsCount(request.getRebuys(), gameType, 
				playMode, session);		

		int picksCount = getPicksCount(request.getPicksPerGame(), gameType, 
				playMode, session);
		
		return new UserGame(request.getEventId(), addChipsCount, noOfSeat,
				picksCount, GameMode.PUBLIC.name(),
				gameType.value());		
	}

	private UserGame getbestMatchUserGames(UserGameSearchRequest request, String playMode, UUID userUuid, Session session) {
		try {
			StringBuilder queryString = new StringBuilder();
			
			queryString.append("select ug from IproSportsUserGame ug, IproSportsUserGameReg ugr where ");
			queryString.append("ug.iproSportsPlayMode.id=:playMode and ug.curUserCount = ug.noOfSeat and ");
			queryString.append("ug.userGameId=ugr.iproSportsUserGame.id and ");
			queryString.append("hex(ugr.userUuid)=:userUuid and ");
			queryString.append("((ug.eventId=:eventId and ug.noOfSeat=:noOfSeat and ug.addChipsCount=:addChipsCount) or ");
			queryString.append("(ug.eventId=:eventId and ug.noOfSeat=:noOfSeat and ug.picksCount=:picksCnt) or ");
			queryString.append("(ug.eventId=:eventId and ug.addChipsCount=:addChipsCount and ug.picksCount=:picksCnt) or ");
			queryString.append("(ug.eventId=:eventId and ug.noOfSeat=:noOfSeat) or ");
			queryString.append("(ug.eventId=:eventId and ug.addChipsCount=:addChipsCount) or ");
			queryString.append("(ug.eventId=:eventId and ug.picksCount=:picksCnt) or ");
			queryString.append("(ug.eventId=:eventId)) order by ug.id desc ");
			
			Query query = session.createQuery(queryString.toString());
			query.setParameter("playMode", playMode);
			query.setParameter("eventId", request.getEventId());
			query.setParameter("noOfSeat", request.getOpponents());
			query.setParameter("addChipsCount", request.getRebuys());
			query.setParameter("picksCnt", request.getPicksPerGame());
			query.setParameter("userUuid", getUUIDString(userUuid));
			
			query.setMaxResults(1);
			
			IproSportsUserGame userGame = (IproSportsUserGame)query.uniqueResult();
			
			if (userGame == null) {
				queryString.delete(0, queryString.length());
				queryString.append("select ug from IproSportsUserGame ug where ");
				queryString.append("ug.iproSportsPlayMode.id=:playMode and ug.curUserCount < ug.noOfSeat and ");
				queryString.append("((ug.eventId=:eventId and ug.noOfSeat=:noOfSeat and ug.addChipsCount=:addChipsCount) or ");
				queryString.append("(ug.eventId=:eventId and ug.noOfSeat=:noOfSeat and ug.picksCount=:picksCnt) or ");
				queryString.append("(ug.eventId=:eventId and ug.addChipsCount=:addChipsCount and ug.picksCount=:picksCnt) or ");
				queryString.append("(ug.eventId=:eventId and ug.addChipsCount=:noOfSeat) or ");
				queryString.append("(ug.eventId=:eventId and ug.addChipsCount=:addChipsCount) or ");
				queryString.append("(ug.eventId=:eventId and ug.picksCount=:picksCnt) or ");
				queryString.append("(ug.eventId=:eventId)) order by ug.id desc ");
				
				query = session.createQuery(queryString.toString());
				query.setParameter("playMode", playMode);
				query.setParameter("eventId", request.getEventId());
				query.setParameter("noOfSeat", request.getOpponents());
				query.setParameter("addChipsCount", request.getRebuys());
				query.setParameter("picksCnt", request.getPicksPerGame());
				
				query.setMaxResults(1);
				
				userGame = (IproSportsUserGame)query.uniqueResult();
			}
			
			if (userGame != null) {
				boolean isRejoin = false;
				if (userGame.getIproSportsUserGameRegs() != null) {
					for (IproSportsUserGameReg usergameReg : userGame.getIproSportsUserGameRegs()) {
						if (usergameReg.getUserUuid().equals(userUuid)) {
							isRejoin = true;
							break;
						}
					}
				}
				return new UserGame(userGame.getUserGameId(), userGame.getEventId(), userGame.getAddChipsCount(),
						userGame.getNoOfSeat()-1, userGame.getPicksCount(),
						userGame.getInitialChips().doubleValue(), 
						GameMode.fromValue(userGame.getIproSportsGameMode().getGameModeCode()).name(),
						GameType.valueOf(userGame.getIproSportsGameType().getGameTypeCode()).value(), isRejoin ? "true" : "false");
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return null;
	}
	
	private List<UserGame> constructUserGames(List<IproSportsUserGame> userGames, UUID userUuid) {
		List<UserGame> games = new ArrayList<UserGame>();

		for(IproSportsUserGame userGame : userGames) {
			boolean isRejoin = false;
			if (userGame.getIproSportsUserGameRegs() != null) {
				logger.debug("User check for register is : " + userUuid.toString());
				for (IproSportsUserGameReg usergameReg : userGame.getIproSportsUserGameRegs()) {
					logger.debug("User registered is : " + usergameReg.getUserUuid().toString());
					if (usergameReg.getUserUuid().equals(userUuid)) {
						isRejoin = true;
						break;
					}
				}
			}			
				
			UserGame game = new UserGame(userGame.getUserGameId(), userGame.getEventId(), userGame.getAddChipsCount(),
					userGame.getNoOfSeat()-1, userGame.getPicksCount(),
					userGame.getInitialChips().doubleValue(), 
					GameMode.fromValue(userGame.getIproSportsGameMode().getGameModeCode()).name(),
					GameType.valueOf(userGame.getIproSportsGameType().getGameTypeCode()).value(), isRejoin ? "true" : "false");
			games.add(game);
		}
		return games;
	}
	
	@SuppressWarnings("unchecked")
	private List<Integer> getOpponents(int sportsId, String mode, String gameType, Session session) {
		List<Integer> opponents = new ArrayList<Integer>();
		logger.info("getOpponents  called");
		try {
	/*		StringBuilder queryString = new StringBuilder();
			queryString.append("select o.noOfSeat from IproSportsSeatConfig o where ");
			queryString.append("o.iproSport.id=:sportsId and o.iproSportsPlayMode.id=:mode and o.iproSportsGameType.id=:gameType");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("sportsId", sportsId);
			query.setParameter("mode", mode);
			query.setParameter("gameType", gameType);

			opponents = (List<Integer>)query.list();*/
			IMap<Integer,IproSportsSeatConfig> re = getIproSeatConfigMap();
			logger.info("Map size  " + re.size());
			for (Map.Entry<Integer, IproSportsSeatConfig> entry : re.entrySet())
			{
				logger.info(entry.getKey() + "/" + entry.getValue());
			}
			logger.info("gameType " + gameType);
			logger.info("sportsId " + sportsId);
			logger.info("mode " + mode);
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			@SuppressWarnings("rawtypes")
			Predicate predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType ).and(eo.get( "iproSport.sportsId" ).equal( sportsId )).and( eo.get( "iproSportsPlayMode.playMode" ).equal( mode ) );			
			Set<IproSportsSeatConfig> seatConfigMap = (Set<IproSportsSeatConfig>)(getIproSeatConfigMap()).values(predicate);
			if(seatConfigMap != null)
        	{
				for(IproSportsSeatConfig seatConfig : seatConfigMap)
				{
					logger.info("Adding opponents count");
					opponents.add(seatConfig.getNoOfSeat());
				}
        	}			
			
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return opponents;
	}

	@SuppressWarnings("unchecked")
	private List<Integer> getAddChipsCounts(int sportsId, String mode, String gameType, Session session) {
		List<Integer> addChipsCounts = new ArrayList<Integer>();
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select o.addChipsCount from IproSportsAddChipsConfig o where ");
			queryString.append("o.iproSport.id=:sportsId and o.iproSportsPlayMode.id=:mode and ");
			queryString.append("o.iproSportsGameType.id=:gameType order by o.addChipsConfigId asc");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("sportsId", sportsId);
			query.setParameter("mode", mode);
			query.setParameter("gameType", gameType);

			addChipsCounts = (List<Integer>)query.list();*/
			
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			@SuppressWarnings("rawtypes")
			Predicate predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType ).and(eo.get( "iproSport.sportsId" ).equal( sportsId )).and( eo.get( "iproSportsPlayMode.playMode" ).equal( mode ) );			
			Set<IproSportsAddChipsConfig> chipsConfigMap = (Set<IproSportsAddChipsConfig>)(getIproAddChipsConfigMap()).values(predicate);
			if(chipsConfigMap != null)
        	{
				for(IproSportsAddChipsConfig chipsConfig : chipsConfigMap)
				{
					logger.info("Adding add chips count");
					addChipsCounts.add(chipsConfig.getAddChipsCount());
				}
        	}			
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return addChipsCounts;
	}

	@SuppressWarnings("unchecked")
	private List<Integer> getPicksPerGame(int sportsId, String mode, String gameType, Session session) {
		List<Integer> picks = new ArrayList<Integer>();
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select o.picksCount from IproSportsPicksCntConfig o where ");
			queryString.append("o.iproSport.id=:sportsId and o.iproSportsPlayMode.id=:mode and ");
			queryString.append("o.iproSportsGameType.id=:gameType order by o.picksCntConfigId desc");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("sportsId", sportsId);
			query.setParameter("mode", mode);
			query.setParameter("gameType", gameType);

			picks = (List<Integer>)query.list();*/
			
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			@SuppressWarnings("rawtypes")
			Predicate predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType ).and(eo.get( "iproSport.sportsId" ).equal( sportsId )).and( eo.get( "iproSportsPlayMode.playMode" ).equal( mode ) );			
			Set<IproSportsPicksCntConfig> picksCntConfigMap = (Set<IproSportsPicksCntConfig>)(getIproPicksCntConfigMap()).values(predicate);			
			if(picksCntConfigMap != null)
        	{
				for(IproSportsPicksCntConfig picksCntConfig : picksCntConfigMap)
				{
					logger.info("picksCntConfig.getPicksCount()" + picksCntConfig.getPicksCount());	
					picks.add(picksCntConfig.getPicksCount());
				}
        	}			
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return picks;
	}

	@SuppressWarnings("unchecked")
	private List<Double> getChips(int sportsId, String mode, String gameType, Session session) {
		List<Double> chips = new ArrayList<Double>();
		try {
			logger.debug("SportId : " + sportsId);
			logger.debug("Play Mode : " + mode);
			logger.debug("Game Type : " + gameType);
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select o.initialChips from IproSportsChipsConfig o where ");
			queryString.append("o.iproSport.id=:sportsId and o.iproSportsPlayMode.id=:mode and ");
			queryString.append("o.iproSportsGameType.id=:gameType order by o.chipsConfigId desc");
			Query query = session.createQuery(queryString.toString());
			query.setParameter("sportsId", sportsId);
			query.setParameter("mode", mode);
			query.setParameter("gameType", gameType);

			List<BigDecimal> decimalchips = (List<BigDecimal>)query.list();
			if (decimalchips == null) {
				logger.debug("Not found");
				return chips;
			}
			logger.debug("Chips found");
			for(BigDecimal chip : decimalchips) {
				logger.debug("Chips to be added : " + chip);
				chips.add(chip.doubleValue());
			}*/
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			/*Predicate testp = eo.get("iproSportsGameType.gameTypeCode").equal(gameType);
			Set<IproSportsChipsConfig> test = (Set<IproSportsChipsConfig>)(getIproChipsConfigMap()).values(testp);
			if(test != null)
				logger.info("got test map");*/
			
			@SuppressWarnings("rawtypes")
			//Predicate predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType ).and(eo.get( "iproSport.sportsId" ).equal( sportsId )).and( eo.get( "iproSportsPlayMode.playMode" ).equal( mode ) );			
			Predicate predicate = eo.get( "gameTypeCode" ).equal( gameType ).and(eo.get( "sportsId" ).equal( sportsId )).and( eo.get( "playMode" ).equal( mode ) );
			//Set<IproSportsChipsConfig> chipsConfigMap = (Set<IproSportsChipsConfig>)(getIproChipsConfigMap()).values(predicate);
			Set<HazelChipsConfig> chipsConfigMap = (Set<HazelChipsConfig>)(getIproChipsConfigMap()).values(predicate);
			if(chipsConfigMap != null)
        	{
				for(HazelChipsConfig chipsConfig : chipsConfigMap)
				{
					logger.info("chipsConfig.getInitialChips()" + chipsConfig.getInitialChips());	
					chips.add(new Double(chipsConfig.getInitialChips()));
				}
        	}
			
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return chips;
	}

	private int getAddChipsCount (int addChipsCount, GameType gameType, String playMode, Session session) {
		IproSportsAddChipsConfig addChipsConfig = null;
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select rb from IproSportsAddChipsConfig rb where ");
			queryString.append("rb.iproSportsGameType.id=:gameType and ");
			queryString.append("rb.iproSportsPlayMode.id=:playMode");
			if (addChipsCount != 0)
				queryString.append(" and rb.addChipsCount=:addChipsCount");
			queryString.append(" order by rb.addChipsConfigId desc");
			Query query = session.createQuery(queryString.toString());			
			query.setParameter("gameType", gameType.name());
			query.setParameter("playMode", playMode);
			if (addChipsCount != 0)
				query.setParameter("addChipsCount", addChipsCount);
			query.setMaxResults(1);

			rebuyConfig = (IproSportsAddChipsConfig) query.uniqueResult();
			if (rebuyConfig == null)
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_REBUY_COUNT", null, null));*/
			
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			@SuppressWarnings("rawtypes")
			Predicate predicate = null;
			if (addChipsCount > 0)
				predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) ).and( eo.get( "addChipsCount" ).equal( addChipsCount ) );
			else
				predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) );
			Set<IproSportsAddChipsConfig> chipsConfigMap = (Set<IproSportsAddChipsConfig>)(getIproAddChipsConfigMap()).values(predicate);
			if(chipsConfigMap != null && chipsConfigMap.size() > 0)
        	{
				addChipsConfig = chipsConfigMap.iterator().next();
        	}
			else
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_REBUY_COUNT", null, null));
		} catch (InvalidSearchOptionsException ex) {
			throw new InvalidSearchOptionsException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		logger.info("Returning add chips frm config " +  addChipsConfig.getAddChipsCount());
		return addChipsConfig.getAddChipsCount();
	}

	private int getNoOfSeat (int noOfSeat, GameType gameType, String playMode, Session session) {
		logger.info("getNoOfSeat  called" );
		IproSportsSeatConfig seatConfig = null;
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select sc from IproSportsSeatConfig sc where ");
			queryString.append("sc.iproSportsGameType.id=:gameType and ");
			queryString.append("sc.iproSportsPlayMode.id=:playMode");
			if (noOfSeat > 0)
				queryString.append(" and sc.noOfSeat=:noOfSeat");
			queryString.append(" order by sc.seatConfigId desc");
			Query query = session.createQuery(queryString.toString());			
			query.setParameter("gameType", gameType.name());
			query.setParameter("playMode", playMode);
			if (noOfSeat > 0)
				query.setParameter("noOfSeat", noOfSeat);
			query.setMaxResults(1);

			seatConfig = (IproSportsSeatConfig) query.uniqueResult();
			if (seatConfig == null)
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_SEAT", null, null));*/
			logger.info("gameType.name() " + gameType.name());
			logger.info("iproSportsPlayMode.playMode " + playMode);
			logger.info("noOfSeat " + noOfSeat);
			IMap<Integer,IproSportsSeatConfig> re = getIproSeatConfigMap();
			logger.info("Map size  " + re.size());
			for (Map.Entry<Integer, IproSportsSeatConfig> entry : re.entrySet())
			{
				logger.info(entry.getKey() + "/" + entry.getValue());
			}


			EntryObject eo = new PredicateBuilder().getEntryObject();			
			@SuppressWarnings("rawtypes")
			Predicate predicate = null;
			if (noOfSeat > 0)
				predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) ).and( eo.get( "noOfSeat" ).equal( noOfSeat ) );
			else
				predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) );
			Set<IproSportsSeatConfig> seatConfigMap = (Set<IproSportsSeatConfig>)(getIproSeatConfigMap()).values(predicate);
			if(seatConfigMap != null && seatConfigMap.size() > 0)
        	{
				seatConfig = seatConfigMap.iterator().next();
				logger.info("getNoOfSeat  called seatConfig" + seatConfig.getNoOfSeat() );
        	}
			else
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_SEAT", null, null));
		} catch (InvalidSearchOptionsException ex) {
			throw new InvalidSearchOptionsException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return seatConfig.getNoOfSeat();
	}

	private int getPicksCount (int picksCount, GameType gameType, String playMode, Session session) {
		IproSportsPicksCntConfig picksConfig = null;
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select pc from IproSportsPicksCntConfig pc where ");
			queryString.append("pc.iproSportsGameType.id=:gameType and ");
			queryString.append("pc.iproSportsPlayMode.id=:playMode");
			if (picksCount != 0)
				queryString.append(" and pc.picksCount=:picksCount");
			queryString.append(" order by pc.picksCntConfigId desc");
			
			Query query = session.createQuery(queryString.toString());			
			query.setParameter("gameType", gameType.name());
			query.setParameter("playMode", playMode);
			if (picksCount != 0)
				query.setParameter("picksCount", picksCount);
			query.setMaxResults(1);

			picksConfig = (IproSportsPicksCntConfig) query.uniqueResult();
			if (picksConfig == null)
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_PICKS_COUNT", null, null));*/
			
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			Set<IproSportsPicksCntConfig> picksCntMap = null;			
				@SuppressWarnings("rawtypes")
				Predicate predicate = null;
				if (picksCount > 0)
				{
					logger.debug("with picksCount ");
					predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) ).and(eo.get( "picksCount" ).equal( picksCount ));			
					picksCntMap = (Set<IproSportsPicksCntConfig>)(getIproPicksCntConfigMap()).values(predicate);
				}
				else
				{
					logger.debug("without picksCount");
					predicate = eo.get( "iproSportsGameType.gameTypeCode" ).equal( gameType.name() ).and( eo.get( "iproSportsPlayMode.playMode" ).equal( playMode ) );					
					picksCntMap = (Set<IproSportsPicksCntConfig>)(getIproPicksCntConfigMap()).values(predicate);
				}
				 
				if(picksCntMap != null && picksCntMap.size() > 0)
	        	{
					logger.debug("Chips config map is not null " + picksCntMap.size());
					picksConfig = picksCntMap.iterator().next();
	        	}
	        	else
	        	{
	        		logger.debug("Thrwing exception");
	        		throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_PICKS_COUNT", null, null));
	        	}
				
		} catch (InvalidSearchOptionsException ex) {
			throw new InvalidSearchOptionsException(ex.getMessage());
		} catch (Exception ex) {
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		}
		return picksConfig.getPicksCount();
	}

	public BigDecimal getIproSportsChipsConfig (Double initialChips, GameType gameType, String playMode, Session session) {
		HazelChipsConfig chipsConfig = null;
		boolean isNewSessionExists = false;
		/*if (session == null) {
			session = getCurrentSession();
			isNewSessionExists = true;
		}*/
		try {
			/*StringBuilder queryString = new StringBuilder();
			queryString.append("select c from IproSportsChipsConfig c where ");			
			queryString.append("c.iproSportsGameType.id=:gameType and ");
			queryString.append("c.iproSportsPlayMode.id=:playMode");
			if (initialChips != null && initialChips.intValue() > 0)
				queryString.append(" and c.initialChips=:initialChips");
			
			Query query = session.createQuery(queryString.toString());
			if (initialChips != null && initialChips.intValue() > 0)
				query.setParameter("initialChips", BigDecimal.valueOf(initialChips));
			query.setParameter("gameType", gameType.name());
			query.setParameter("playMode", playMode);
			query.setMaxResults(1);

			chipsConfig = (IproSportsChipsConfig) query.uniqueResult();
			if (chipsConfig == null)
				throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_INITIAL_CHIPS", null, null));*/
			BigDecimal chipsValue = new BigDecimal(12000.00);
			logger.info("Getting from chips map " + chipsValue);
			logger.info("gameType.name() " + gameType.name());
			logger.info("playMode) " + playMode);		
		
			EntryObject eo = new PredicateBuilder().getEntryObject();			
			Set<HazelChipsConfig> chipsConfigMap = null;			
				@SuppressWarnings("rawtypes")
				Predicate predicate = null;
				if (initialChips != null && initialChips.intValue() > 0)
				{
					logger.info("with initial cjips");								
					predicate = eo.get( "gameTypeCode" ).equal( gameType.name() ).and( eo.get( "playMode" ).equal( playMode ) ).and(eo.get( "initialChips" ).equal( BigDecimal.valueOf(initialChips) ));
					chipsConfigMap = (Set<HazelChipsConfig>)(getIproChipsConfigMap()).values(predicate);
				}
				else
				{
					logger.info("without initial cjips");					
					predicate = eo.get( "gameTypeCode" ).equal( gameType.name() ).and( eo.get( "playMode" ).equal( playMode ) );
					chipsConfigMap = (Set<HazelChipsConfig>)(getIproChipsConfigMap()).values(predicate);
				}
				 
				if(chipsConfigMap != null && chipsConfigMap.size() > 0)
	        	{
					logger.info("Chips config map is not null " + chipsConfigMap.size());
					chipsConfig = chipsConfigMap.iterator().next();
	        	}
	        	else
	        	{
	        		logger.info("Thrwing exception");
	        		throw new InvalidSearchOptionsException(messageSource.getMessage("NOT_A_VALID_INITIAL_CHIPS", null, null));
	        	}
			
			return new BigDecimal(chipsConfig.getInitialChips());
		} catch (InvalidSearchOptionsException ex) {
			throw new InvalidSearchOptionsException(ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			if (isNewSessionExists) {
				session.close();
			}
		}
	}

	private IproSportsUserGame constructUserGameCreate(UserGameCreateRequest createRequest, String userUuid, String playMode, 
			IproSports sportsId, int addChipsCount,  
			int noOfSeat, int picksCount, BigDecimal initialChips) {
		IproSportsUserGame userGame = new IproSportsUserGame();
		userGame.setEventId(createRequest.getEventId());

		IproSportsPlayMode iproSportsPlayMode = new IproSportsPlayMode();
		iproSportsPlayMode.setPlayMode(playMode);
		userGame.setIproSportsPlayMode(iproSportsPlayMode);

		IproSportsGameMode gameMode = new IproSportsGameMode();
		if (createRequest.getGameMode() != null)
			gameMode.setGameModeCode(createRequest.getGameMode());
		else
			gameMode.setGameModeCode(GameMode.PUBLIC.value());
		userGame.setIproSportsGameMode(gameMode);

		IproSportsGameType gameType = new IproSportsGameType();
		if (createRequest.getGameType() != null)
			gameType.setGameTypeCode(createRequest.getGameType());
		else
			gameType.setGameTypeCode(GameType.REG.name());
		userGame.setIproSportsGameType(gameType);

		userGame.setUserUuid(UUIDUtil.getUUIDFromString(userUuid));

		userGame.setNoOfSeat(noOfSeat + 1);;
		userGame.setInitialChips(initialChips);
		userGame.setPicksCount(picksCount);
		userGame.setAddChipsCount(addChipsCount);
		userGame.setIproSport(sportsId);
		userGame.setCurUserCount(0);
		userGame.setCreatedDate(TimeStampUtil.getCurrentSQLTimeStamp());

		return userGame;
	}
	
	private IproSportsUserGameReg constructUserGameJoin (IproSportsUserGame userGame, String UUID) {
		IproSportsUserGameReg userGameJoin = new IproSportsUserGameReg();

		userGameJoin.setIproSportsUserGame(userGame);
		userGameJoin.setUserUuid(getUUIDFromString(UUID));
		userGameJoin.setCreatedDate(getCurrentSQLTimeStamp());
		userGameJoin.setAddChipsRemain(userGame.getAddChipsCount());
		userGameJoin.setScore(0);
		userGameJoin.setPicksRemain(userGame.getPicksCount());
		
		return userGameJoin;
	}
	
	private IproSportsUserGameReg updateUserGameJoin (IproSportsUserGameReg userGamejoin, BigDecimal chipsAdded) {
		userGamejoin.setUpdatedDate(getCurrentSQLTimeStamp());
		userGamejoin.setChipsBalance(userGamejoin.getChipsBalance() != null ? userGamejoin.getChipsBalance().add(chipsAdded) : chipsAdded);		
		userGamejoin.setAddChipsRemain((userGamejoin.getTotalChipsAdded() == null ||  userGamejoin.getAddChipsRemain() == -1) ? userGamejoin.getAddChipsRemain() : userGamejoin.getAddChipsRemain() - 1);
		userGamejoin.setTotalChipsAdded(userGamejoin.getTotalChipsAdded() != null ? userGamejoin.getTotalChipsAdded().add(chipsAdded) : chipsAdded);
		return userGamejoin;
	}
	
	private IMap<Integer,HazelChipsConfig> getIproChipsConfigMap()
	{
		return getHazelcastInstance().getMap("iproChipsConfigMap");		
	}
	
	private IMap<Integer,IproSportsAddChipsConfig> getIproAddChipsConfigMap()
	{
		return getHazelcastInstance().getMap("iproAddChipsConfigMap");		
	}
	
	private IMap<Integer,IproSportsPicksCntConfig> getIproPicksCntConfigMap()
	{
		return getHazelcastInstance().getMap("iproPicksCntConfigMap");		
	}
	
	private IMap<Integer,IproSportsSeatConfig> getIproSeatConfigMap()
	{
		return getHazelcastInstance().getMap("iproSeatConfigMap");		
	}
	
}
