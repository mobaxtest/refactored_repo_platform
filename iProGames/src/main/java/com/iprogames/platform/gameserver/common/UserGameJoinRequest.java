package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameJoinRequest {

	private String iProToken	= null;
	private int userGameId		= 0;
	
	public UserGameJoinRequest() {
		
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public int getUserGameId() {
		return userGameId;
	}

	public void setUserGameId(int userGameId) {
		this.userGameId = userGameId;
	}
	
	@Override
	public String toString() {
		return "UserGameJoinRequest [iProToken=" + iProToken + ", userGameId="
				+ userGameId + "]";
	}		
}
