package com.iprogames.platform.gameserver.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class GameEvent {

	private int eventId					= 0;
	private Team homeTeam				= null;
	private Team awayTeam				= null;
	private Date localStartDate			= null;
	private Date utcStartDate			= null;
	private List<UserGame> userGames	= new ArrayList<UserGame>();
	private UserGame bestMatch			= null;
	private UserGame createOptions		= null;
	
	public GameEvent() {
	}
		
	public GameEvent(int eventId, Team homeTeam, Team awayTeam) {
		this.eventId = eventId;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
	}
	
	public GameEvent(int eventId, Team homeTeam, Team awayTeam,
			Date localStartDate, Date utcStartDate) {
		this.eventId = eventId;
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.localStartDate = localStartDate;
		this.utcStartDate = utcStartDate;
	}
	
	public int getEventId() {
		return eventId;
	}
	
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Date getLocalStartDate() {
		return localStartDate;
	}
	
	public void setLocalStartDate(Date localStartDate) {
		this.localStartDate = localStartDate;
	}
	
	public Date getUtcStartDate() {
		return utcStartDate;
	}
	
	public void setUtcStartDate(Date utcStartDate) {
		this.utcStartDate = utcStartDate;
	}
	
	public List<UserGame> getUserGames() {		
		return userGames;
	}

	public void setUserGames(List<UserGame> userGames) {
		this.userGames = userGames;
	}

	public UserGame getBestMatch() {
		return bestMatch;
	}

	public void setBestMatch(UserGame bestMatch) {
		this.bestMatch = bestMatch;
	}

	public UserGame getCreateOptions() {
		return createOptions;
	}

	public void setCreateOptions(UserGame createOptions) {
		this.createOptions = createOptions;
	}

	@Override
	public String toString() {
		return "GameEvent [eventId=" + eventId + ", homeTeam="
				+ homeTeam + ", awayTeam=" + awayTeam + ", localStartDate=" + localStartDate
				+ ", utcStartDate=" + utcStartDate + "]";
	}
	
}
