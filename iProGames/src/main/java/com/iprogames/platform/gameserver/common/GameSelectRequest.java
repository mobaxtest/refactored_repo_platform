package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class GameSelectRequest {
	
	private String username		= null;
	private String iProToken	= null;
	private boolean isSearch;
	
	public GameSelectRequest() {		
	}
	
	public GameSelectRequest(String username, String iProToken) {
		this.username = username;
		this.iProToken = iProToken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public boolean isSearch() {
		return isSearch;
	}

	public void setSearch(boolean isSearch) {
		this.isSearch = isSearch;
	}

	@Override
	public String toString() {
		return "GameSelectRequest [username=" + username + ", iProToken="
				+ iProToken + "]";
	}
}
