package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameSearchRequest {

	private String iProToken		= null;
	private int eventId				= 0;
	private int opponents			= 0;
	private int rebuys				= 0;
	private int picksPerGame		= 0;
	private double chips			= 0;
	
	public UserGameSearchRequest() {
		
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getOpponents() {
		return opponents;
	}

	public void setOpponents(int opponents) {
		this.opponents = opponents;
	}

	public int getRebuys() {
		return rebuys;
	}

	public void setRebuys(int rebuys) {
		this.rebuys = rebuys;
	}

	public int getPicksPerGame() {
		return picksPerGame;
	}

	public void setPicksPerGame(int picksPerGame) {
		this.picksPerGame = picksPerGame;
	}

	public double getChips() {
		return chips;
	}

	public void setChips(double chips) {
		this.chips = chips;
	}

	@Override
	public String toString() {
		return "UserGameSearchRequest [iProToken=" + iProToken + ", eventId="
				+ eventId + ", opponents=" + opponents + ", rebuys=" + rebuys
				+ ", picksPerGame=" + picksPerGame + ", chips=" + chips + "]";
	}
	
}
