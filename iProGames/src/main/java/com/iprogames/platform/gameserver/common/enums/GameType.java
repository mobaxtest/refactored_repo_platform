package com.iprogames.platform.gameserver.common.enums;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
public enum GameType {

	LEG ("League"), // LEAGUE
	REG ("Head To Head"), // REGULAR
	TMT ("Tourney"), // TOURNEY 
	SIN ("Single game tournament");
	private final String value;
	
	GameType(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
	public static GameType fromValue(String v) {
        for (GameType c: GameType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
