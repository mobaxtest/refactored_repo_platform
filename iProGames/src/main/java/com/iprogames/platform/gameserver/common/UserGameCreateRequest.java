package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameCreateRequest {

	private String iProToken	= null;
	private int eventId			= 0;
	private String gameType		= null;
	private String gameMode		= null;
	private int rebuyCount		= 0;
	private int noOfSeat		= 0;
	private int picksCount		= 0;
	private double chips		= 0;
	
	public UserGameCreateRequest() {
		
	}

	public String getiProToken() {
		return iProToken;
	}

	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getGameType() {
		return gameType;
	}

	public void setGameType(String gameType) {
		this.gameType = gameType;
	}

	public String getGameMode() {
		return gameMode;
	}

	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}

	public int getRebuyCount() {
		return rebuyCount;
	}

	public void setRebuyCount(int rebuyCount) {
		this.rebuyCount = rebuyCount;
	}

	public int getNoOfSeat() {
		return noOfSeat;
	}

	public void setNoOfSeat(int noOfSeat) {
		this.noOfSeat = noOfSeat;
	}

	public int getPicksCount() {
		return picksCount;
	}

	public void setPicksCount(int picksCount) {
		this.picksCount = picksCount;
	}

	public double getChips() {
		return chips;
	}

	public void setChips(double chips) {
		this.chips = chips;
	}

	@Override
	public String toString() {
		return "UserGameCreateRequest [iProToken=" + iProToken + ", eventId="
				+ eventId + ", gameType=" + gameType + ", gameMode=" + gameMode
				+ ", rebuyCount=" + rebuyCount + ", noOfSeat=" + noOfSeat
				+ ", picksCount=" + picksCount + ", chips=" + chips + "]";
	}
	
}
