package com.iprogames.platform.gameserver.service;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.iprogames.platform.gameserver.common.enums.GameTxnStatusEnum;
import com.iprogames.platform.exception.InsufficientChipsException;
import com.iprogames.platform.gameserver.common.internal.RegularUserGameJoin;
import com.iprogames.platform.gameserver.dao.GameTxnDaoHandler;
import com.iprogames.platform.util.enums.FundTypeEnum;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.platform.wallettransaction.common.CreditDebitChipsRequest;
import com.iprogames.platform.wallettransaction.common.UserWallet;
import com.iprogames.platform.wallettransaction.service.WalletTransactionService;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * Service that credit/debit chips to/from user wallet and update the game transaction table
 */
@Service
public class GameTxnService {

	@SuppressWarnings("rawtypes")
	@Autowired
	GameTxnDaoHandler gameTxnDao;
	
	@Autowired
	WalletTransactionService wallet;
	
	@Resource
	MessageSource messageSource;
	
	/* Update the Game trasaction and wallet for debiting chips from user wallet */
	public void updateDebitToWallet(String iProToken, String UUID, int sessionId, FundTypeEnum fundType, RegularUserGameJoin regUserGame, TxnTypeEnum txnType) {
		String gameTxnId = gameTxnDao.createGameTrasaction(regUserGame.getEventId(), regUserGame.getUserGameRegId(), UUID, sessionId, 
				regUserGame.getInitialChips(), GameTxnStatusEnum.PROG, txnType);
		CreditDebitChipsRequest request = new CreditDebitChipsRequest(fundType, regUserGame.getInitialChips(), gameTxnId, txnType);
		wallet.debitChipsFromUserAccount(request, iProToken);
		gameTxnDao.updateGameTrasaction(gameTxnId, GameTxnStatusEnum.COMP);
	}
	
	/* Update the Game trasaction and wallet for crediting the chips to user wallet */
	public void updateCreditToWallet(String iProToken, String UUID, int sessionId, FundTypeEnum fundType, RegularUserGameJoin regUserGame, TxnTypeEnum txnType) {
		String gameTxnId = gameTxnDao.createGameTrasaction(regUserGame.getEventId(), regUserGame.getUserGameRegId(), UUID, sessionId, 
				regUserGame.getInitialChips(), GameTxnStatusEnum.PROG, txnType);
		CreditDebitChipsRequest request = new CreditDebitChipsRequest(fundType, regUserGame.getInitialChips(), gameTxnId, txnType);
		wallet.creditChipsToUserAccount(request, iProToken);
		gameTxnDao.updateGameTrasaction(gameTxnId, GameTxnStatusEnum.COMP);
	}
	
	public void checkUserWalletForBalance(String iProToken, Double minBalance, FundTypeEnum fundType) {
		UserWallet userWallet = wallet.getBalanceAmount(fundType.name(), iProToken);
		if (userWallet != null && userWallet.getBalanceAmount() != null) {
			if (userWallet.getBalanceAmount().doubleValue() < minBalance) {
				throw new InsufficientChipsException(messageSource.getMessage("INSUFFICIENT_CHIPS", null, null));
			}
		} else {
			throw new InsufficientChipsException(messageSource.getMessage("INSUFFICIENT_CHIPS", null, null));
		}
	}
}
