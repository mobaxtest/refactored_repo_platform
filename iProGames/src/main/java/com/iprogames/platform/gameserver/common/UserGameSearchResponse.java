package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserGameSearchResponse {

	private GameEvent gameEvent = null;
	
	public UserGameSearchResponse() {
		
	}
	
	public UserGameSearchResponse(GameEvent gameEvent) {
		this.gameEvent = gameEvent;
	}

	public GameEvent getGameEvent() {
		return gameEvent;
	}

	public void setGameEvents(GameEvent gameEvent) {
		this.gameEvent = gameEvent;
	}
}
