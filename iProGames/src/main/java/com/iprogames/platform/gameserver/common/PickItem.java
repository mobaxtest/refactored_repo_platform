package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class PickItem {

	private String pt; // pick type
	private String pp; // pick position
	private String pa; // pick amount
	
	public PickItem() {
		
	}

	/**
	 * @return the pt
	 */
	public String getPt() {
		return pt;
	}

	/**
	 * @param pt the pt to set
	 */
	public void setPt(String pt) {
		this.pt = pt;
	}

	/**
	 * @return the pp
	 */
	public String getPp() {
		return pp;
	}

	/**
	 * @param pp the pp to set
	 */
	public void setPp(String pp) {
		this.pp = pp;
	}

	/**
	 * @return the pa
	 */
	public String getPa() {
		return pa;
	}

	/**
	 * @param pa the pa to set
	 */
	public void setPa(String pa) {
		this.pa = pa;
	}

	@Override
	public String toString() {
		return "PickItem [pt=" + pt + ", pp=" + pp + ", pa=" + pa + "]";
	}	
}
