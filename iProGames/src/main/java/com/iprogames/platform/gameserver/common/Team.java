package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 */
@JsonInclude(Include.NON_EMPTY)
public class Team {

	private String teamName = null;
	private String teamLocation	 = null;
	private String teamAbbr	 = null;
	private String tintColor	 = null;
	private String outlineColor	 = null;
	
	public Team() {
		
	}
	
	public Team(String teamName, String teamLocation, String teamAbbr, String tintColor, String outlineColor) {
		this.teamName = teamName;
		this.teamLocation = teamLocation;
		this.teamAbbr = teamAbbr;
		this.tintColor = tintColor;
		this.outlineColor = outlineColor;		
	}

	/**
	 * @return the teamName
	 */
	public String getTeamName() {
		return teamName;
	}

	/**
	 * @param teamName the teamName to set
	 */
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/**
	 * @return the teamLocation
	 */
	public String getTeamLocation() {
		return teamLocation;
	}

	/**
	 * @param teamLocation the teamLocation to set
	 */
	public void setTeamLocation(String teamLocation) {
		this.teamLocation = teamLocation;
	}

	/**
	 * @return the teamAbbr
	 */
	public String getTeamAbbr() {
		return teamAbbr;
	}

	/**
	 * @param teamAbbr the teamAbbr to set
	 */
	public void setTeamAbbr(String teamAbbr) {
		this.teamAbbr = teamAbbr;
	}

	/**
	 * @return the tintColor
	 */
	public String getTintColor() {
		return tintColor;
	}

	/**
	 * @param tintColor the tintColor to set
	 */
	public void setTintColor(String tintColor) {
		this.tintColor = tintColor;
	}

	/**
	 * @return the outlineColor
	 */
	public String getOutlineColor() {
		return outlineColor;
	}

	/**
	 * @param outlineColor the outlineColor to set
	 */
	public void setOutlineColor(String outlineColor) {
		this.outlineColor = outlineColor;
	}

	/**
	 *  Team as String
	 */
	@Override
	public String toString() {
		return "Team [teamName=" + teamName + ", teamLocation=" + teamLocation
				+ ", teamAbbr=" + teamAbbr + ", tintColor=" + tintColor
				+ ", outlineColor=" + outlineColor + "]";
	}


	
}
