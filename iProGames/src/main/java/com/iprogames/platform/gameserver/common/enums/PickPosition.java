package com.iprogames.platform.gameserver.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum PickPosition {

	TMR("Team Run"),
	TMP("Team Pass"),
	TOTD("Offensive Touch Down"),
	TDTD("Offensive Touch Down"),
	TFD("Team First Down"),
	TFG("Team Field Goal"),
	TSAC("Team Sack"),
	TSAF("Team Saftey"),
	TTO("Team Turn Over"),
	T3Y("Team 1-3 Yards"),
	T8Y("Team 4-8 Yards"),
	T35Y("Team 9-35 Yards"),
	THY("Team Hundred Yards"),
	TLH("Team Left Hash"),
	TRH("Team Right Hash"),
	TMH("Team Middle Hash"),
	OP1("Offensive position 1"),
	OP2("Offensive position 2"),
	OP3("Offensive position 3"),
	OP4("Offensive position 4"),
	OP5("Offensive position 5"),
	OP6("Offensive position 6"),
	OP7("Offensive position 7"),
	OP8("Offensive position 8"),
	OP9("Offensive position 9"),
	OP10("Offensive position 10"),
	OP11("Offensive position 11"),
	OP12("Offensive position 12"),
	OC1("Offensive corner 1-2-5-6"),
	OC2("Offensive corner 2-3-6-7"),
	OC3("Offensive corner 3-4-7-8"),
	OC5("Offensive corner 5-6-9-10"),
	OC6("Offensive corner 6-7-10-11"),
	OC7("Offensive corner 7-8-11-12"),
	DP1("Defensive position 1"),
	DP2("Defensive position 2"),
	DP3("Defensive position 3"),
	DP4("Defensive position 4"),
	DP5("Defensive position 5"),
	DP6("Defensive position 6"),
	DP7("Defensive position 7"),
	DP8("Defensive position 8"),
	DP9("Defensive position 9"),
	DP10("Defensive position 10"),
	DP11("Defensive position 11"),
	DP12("Defensive position 12"),
	DC1("Defensive corner 1-2-5-6"),
	DC2("Defensive corner 2-3-6-7"),
	DC3("Defensive corner 3-4-7-8"),
	DC5("Defensive corner 5-6-9-10"),
	DC6("Defensive corner 6-7-10-11"),
	DC7("Defensive corner 7-8-11-12");
	
	private String pickPosition;
	
	private static Map<String, PickPosition> pickMapping;
	private PickPosition(String pickPosition) {
		   this.pickPosition = pickPosition;
	}

	public String getPickPosition(){
		return pickPosition;
	}
	
	public static PickPosition getPickPosition(String pickPosition){
		if(pickMapping == null){
			initMapping();
		}
		return pickMapping.get(pickPosition);
	}
	
	private static void initMapping(){
		pickMapping = new HashMap<String, PickPosition>();
		for(PickPosition c : values()){
			pickMapping.put(c.pickPosition, c);
		}
	}
}
