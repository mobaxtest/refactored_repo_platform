package com.iprogames.platform.gameserver.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class GetActiveGameResponse {

	private List<GameEvent> playerActiveGames 	= null;
	
	public GetActiveGameResponse() {
		
	}
	
	public GetActiveGameResponse(List<GameEvent> playerActiveGames) {
		this.playerActiveGames = playerActiveGames;
	}

	public List<GameEvent> getPlayerActiveGames() {
		if (playerActiveGames == null)
			playerActiveGames = new ArrayList<GameEvent>();
		
		return playerActiveGames;
	}

	public void setPlayerActiveGames(List<GameEvent> playerActiveGames) {
		this.playerActiveGames = playerActiveGames;
	}	
}
