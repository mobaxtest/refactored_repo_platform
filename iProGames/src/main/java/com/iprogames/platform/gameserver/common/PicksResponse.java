package com.iprogames.platform.gameserver.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class PicksResponse {

	private String iProToken;
	private String username;
	private String disname;
	private String imgurl; // image url for the user
	private int userGameId;
	private double chipsbal;
	private double score;
	private int rank;
	private int acc; //add chips count left
	private int pl;  // picks left
	private String pref; // Pick reference sent by server
	private int timer;  // the time on the timer until the next event. 
						//It is in seconds. The timer should be split with the total 
						//seconds so the full circle will be used for the timer seconds.
	private List<UserScore> ranks;
	private PicksEvent event;
	private Picks picks;
	
	public PicksResponse() {
		
	}

	/**
	 * @return the iProToken
	 */
	public String getiProToken() {
		return iProToken;
	}

	/**
	 * @param iProToken the iProToken to set
	 */
	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the disname
	 */
	public String getDisname() {
		return disname;
	}

	/**
	 * @param disname the disname to set
	 */
	public void setDisname(String disname) {
		this.disname = disname;
	}

	/**
	 * @return the imgurl
	 */
	public String getImgurl() {
		return imgurl;
	}

	/**
	 * @param imgurl the imgurl to set
	 */
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	/**
	 * @return the userGameId
	 */
	public int getUserGameId() {
		return userGameId;
	}

	/**
	 * @param userGameId the userGameId to set
	 */
	public void setUserGameId(int userGameId) {
		this.userGameId = userGameId;
	}

	/**
	 * @return the chipsbal
	 */
	public double getChipsbal() {
		return chipsbal;
	}

	/**
	 * @param chipsbal the chipsbal to set
	 */
	public void setChipsbal(double chipsbal) {
		this.chipsbal = chipsbal;
	}

	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * @return the acc
	 */
	public int getAcc() {
		return acc;
	}

	/**
	 * @param acc the acc to set
	 */
	public void setAcc(int acc) {
		this.acc = acc;
	}

	/**
	 * @return the pl
	 */
	public int getPl() {
		return pl;
	}

	/**
	 * @param pl the pl to set
	 */
	public void setPl(int pl) {
		this.pl = pl;
	}

	/**
	 * @return the pref
	 */
	public String getPref() {
		return pref;
	}

	/**
	 * @param pref the pref to set
	 */
	public void setPref(String pref) {
		this.pref = pref;
	}

	/**
	 * @return the timer
	 */
	public int getTimer() {
		return timer;
	}

	/**
	 * @param timer the timer to set
	 */
	public void setTimer(int timer) {
		this.timer = timer;
	}

	/**
	 * @return the ranks
	 */
	public List<UserScore> getRanks() {
		return ranks;
	}

	/**
	 * @param ranks the ranks to set
	 */
	public void setRanks(List<UserScore> ranks) {
		this.ranks = ranks;
	}

	/**
	 * @return the event
	 */
	public PicksEvent getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(PicksEvent event) {
		this.event = event;
	}

	/**
	 * @return the picks
	 */
	public Picks getPicks() {
		return picks;
	}

	/**
	 * @param picks the picks to set
	 */
	public void setPicks(Picks picks) {
		this.picks = picks;
	}

	@Override
	public String toString() {
		return "PicksResponse [iProToken=" + iProToken + ", username="
				+ username + ", disname=" + disname + ", imgurl=" + imgurl
				+ ", userGameId=" + userGameId + ", chipsbal=" + chipsbal
				+ ", score=" + score + ", rank=" + rank + ", acc=" + acc
				+ ", pl=" + pl + ", pref=" + pref + ", timer=" + timer
				+ ", ranks=" + ranks.toString() + ", event=" + event.toString() 
				+ ", picks=" + picks.toString() + "]";
	}
}
