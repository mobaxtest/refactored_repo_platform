package com.iprogames.platform.gameserver.common.enums;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
public enum GameMode {

	PRIVATE ("PRI"), // PRIVATE
	PUBLIC ("PUB"); // PUBLIC
	
	private final String value;
	
	GameMode(String v) {
        value = v;
    }
	
	public String value() {
        return value;
    }
	
	public static GameMode fromValue(String v) {
        for (GameMode c: GameMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
