package com.iprogames.platform.gameserver.common.internal;

import java.math.BigDecimal;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * Used for Internal purpose
 */
public class RegularUserGameJoin {

	private int eventId;
	private int userGameRegId;
	private BigDecimal initialChips;
	
	public RegularUserGameJoin() {
		
	}
	
	public RegularUserGameJoin(int eventId, int userGameRegId, BigDecimal initialChips) {
		this.eventId = eventId;
		this.userGameRegId = userGameRegId;
		this.initialChips = initialChips;
	}
	
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getUserGameRegId() {
		return userGameRegId;
	}

	public void setUserGameRegId(int userGameRegId) {
		this.userGameRegId = userGameRegId;
	}

	public BigDecimal getInitialChips() {
		return initialChips;
	}

	public void setInitialChips(BigDecimal initialChips) {
		this.initialChips = initialChips;
	}

}
