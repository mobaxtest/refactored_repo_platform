package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * PicksRequest - Shared for both PropertyPicks request and PickRequest
 */
@JsonInclude(Include.NON_EMPTY)
public class PicksRequest {

	private String iProToken;
	private String username;
	private String userGameId;
	private String pc;//"T|P|PP" // Pick category - Team, Player, PropPicks
	private Picks picks;
	
	public PicksRequest() {
		
	}

	/**
	 * @return the iProToken
	 */
	public String getiProToken() {
		return iProToken;
	}

	/**
	 * @param iProToken the iProToken to set
	 */
	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the userGameId
	 */
	public String getUserGameId() {
		return userGameId;
	}

	/**
	 * @param userGameId the userGameId to set
	 */
	public void setUserGameId(String userGameId) {
		this.userGameId = userGameId;
	}

	/**
	 * @return the picks
	 */
	public Picks getPicks() {
		return picks;
	}

	/**
	 * @param picks the picks to set
	 */
	public void setPicks(Picks picks) {
		this.picks = picks;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	@Override
	public String toString() {
		return "PicksRequest [iProToken=" + iProToken + ", username="
				+ username + ", userGameId=" + userGameId + ", pc=" + pc
				+ ", picks=" + picks + "]";
	}
}
