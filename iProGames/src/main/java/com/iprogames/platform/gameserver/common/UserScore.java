package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
* Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
*/
@JsonInclude(Include.NON_EMPTY)
public class UserScore {

	private String userid		= null;
	private String disname		= null;
	private int score			= 0;	
	private int rank			= 0;
	private String imgurl		= null; // image url for the user
	
	public UserScore () {
		
	}
	
	public UserScore (String userid, int score, int rank) {
		this.userid = userid;
		this.score = score;
		this.rank = rank;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getDisname() {
		return disname;
	}

	public void setDisname(String disname) {
		this.disname = disname;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getImgurl() {
		return imgurl;
	}

	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	@Override
	public String toString() {
		return "UserScore [userid=" + userid + ", disname=" + disname
				+ ", score=" + score + ", rank=" + rank + ", imgurl=" + imgurl
				+ "]";
	}
}
