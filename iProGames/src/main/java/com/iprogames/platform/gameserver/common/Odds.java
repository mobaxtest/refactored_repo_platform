package com.iprogames.platform.gameserver.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class Odds {

	 private String type; // odds type - Odds or multiplier
	 private List<PickItem> odd;
	 
	 public Odds() {
		 
	 }

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the odd
	 */
	public List<PickItem> getOdd() {
		return odd;
	}

	/**
	 * @param odd the odd to set
	 */
	public void setOdd(List<PickItem> odd) {
		this.odd = odd;
	}

	@Override
	public String toString() {
		return "Odds [type=" + type + ", odd=" + odd.toString() + "]";
	}
}
