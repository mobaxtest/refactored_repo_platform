package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 * 
 */
@JsonInclude(Include.NON_EMPTY)
public class OddsRequest {

	private String iProToken;
	private String username;
	private String userGameId;
	private String pc; // Pick category - Team, Player, PropPicks
	private String pp; // Pick Position
	private String pt; // Pick Type
	
	public OddsRequest() {
		
	}

	/**
	 * @return the iProToken
	 */
	public String getiProToken() {
		return iProToken;
	}

	/**
	 * @param iProToken the iProToken to set
	 */
	public void setiProToken(String iProToken) {
		this.iProToken = iProToken;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the userGameId
	 */
	public String getUserGameId() {
		return userGameId;
	}

	/**
	 * @param userGameId the userGameId to set
	 */
	public void setUserGameId(String userGameId) {
		this.userGameId = userGameId;
	}

	/**
	 * @return the pc
	 */
	public String getPc() {
		return pc;
	}

	/**
	 * @param pc the pc to set
	 */
	public void setPc(String pc) {
		this.pc = pc;
	}

	/**
	 * @return the pp
	 */
	public String getPp() {
		return pp;
	}

	/**
	 * @param pp the pp to set
	 */
	public void setPp(String pp) {
		this.pp = pp;
	}

	/**
	 * @return the pt
	 */
	public String getPt() {
		return pt;
	}

	/**
	 * @param pt the pt to set
	 */
	public void setPt(String pt) {
		this.pt = pt;
	}

	@Override
	public String toString() {
		return "OddsRequest [iProToken=" + iProToken + ", username=" + username
				+ ", userGameId=" + userGameId + ", pc=" + pc + ", pp=" + pp
				+ ", pt=" + pt + "]";
	}
}
