package com.iprogames.platform.gameserver.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum GameEventType {

	TWOPP("2PP"), 		// Two point pass attempt
	TWOPR("2PR"), 		// Two point rush attempt
	TWOMIN("2MIN"), 	// Two minute warning
	ADV("ADV"), 		// Advisory Text
	BFG("BFG"),   		// Blocked Field Goal
	BP("BP"), 			// Blocked punt
	BPAT("BPAT"),  		// Blocked Extra Point
	CP("CP"), 			// Complete pass
	DELAY("DELAY"),  	// Game Delayed
	ENDGAME("ENDGAME"), // Game Over
	FEED("FEED"),		// Feed Number
	FGA("FGA"),			// Field Goal Attempt
	FUM("FUM"),			// Fumble
	INT("INT"),			// Interception
	IP("IP"),			// Incomplete pass
	KKR("KKR"),			// Kickoff with kicking team recovering
	KR("KR"), 			// Kick off with receiving team returning 
	LAT("LAT"),			// Lateral pass / back ward pass
	MFG("MFG"),			// Missed field goal return
	NUP("NUP"),			// New player addition
	PAT("PAT"),			// Point after attempt
	PE("PE"),			// Period over
	PEN("PEN"),			// Penalty
	PR("PR"),			// Punt Return
	PS("PS"),			// Period Starting
	RUSH("RUSH"),		// Rushing play
	SACK("SACK"),		// Quarterback sack
	SAF("SAF"),			// Safety
	TIME("TIME"),		// Time of game
	TOUT("TOUT"),		// Time out
	CT("CT"); 			// Coin Toss
	
	private String eventType;
	
	private static Map<String, GameEventType> typeMapping;
	private GameEventType(String eventType) {
		   this.eventType = eventType;
	}

	public String getGameEventType(){
		return eventType;
	}
	
	public static GameEventType getGameEventType(String eventType){
		if(typeMapping == null){
			initMapping();
		}
		return typeMapping.get(eventType);
	}
	
	private static void initMapping(){
		typeMapping = new HashMap<String, GameEventType>();
		for(GameEventType c : values()){
			typeMapping.put(c.eventType, c);
		}
	}
}
