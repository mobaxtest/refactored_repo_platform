package com.iprogames.platform.gameserver.dao;

import java.io.Serializable;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import com.iprogames.platform.dao.SessionFactoryHandler;


/**
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 * @author priyadharshini
 */
@Repository("picksDao")
public class PicksDaoHandler <T extends Serializable> extends SessionFactoryHandler {
	Logger logger = LogManager.getLogger(PicksDaoHandler.class);

	@Resource
	MessageSource messageSource;
	
	public PicksDaoHandler() {
		super();
	}
	public PicksDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

}
