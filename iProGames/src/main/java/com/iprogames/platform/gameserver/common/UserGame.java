package com.iprogames.platform.gameserver.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@JsonInclude(Include.NON_EMPTY)
public class UserGame {

	private int userGameId		= 0;
	private int eventId			= 0;
	private int rebuyCount		= 0;
	private int noOfSeat		= 0;
	private int picksCount		= 0;
	private double chips		= 0;
	private String gameMode		= null;
	private String gameType		= null;
	private String isRejoin		= null;
	
	public UserGame() {
		
	}
	
	public UserGame(int eventId, int rebuyCount, int noOfSeat, 
			int picksCount, String gameMode, String gameType) {
		this.eventId = eventId;
		this.rebuyCount = rebuyCount;
		this.noOfSeat = noOfSeat;
		this.picksCount = picksCount;
		this.gameMode = gameMode;
		this.gameType = gameType;
	}

	public UserGame(int userGameId, int eventId, int rebuyCount, int noOfSeat, 
			int picksCount, double chips, String gameMode, String gameType, String isRejoin) {
		this.userGameId = userGameId;
		this.eventId = eventId;
		this.rebuyCount = rebuyCount;
		this.noOfSeat = noOfSeat;
		this.picksCount = picksCount;
		this.chips = chips;
		this.gameMode = gameMode;
		this.gameType = gameType;
		this.isRejoin = isRejoin;
	}
	
	public int getUserGameId() {
		return userGameId;
	}
	public void setUserGameId(int userGameId) {
		this.userGameId = userGameId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}	
	public int getRebuyCount() {
		return rebuyCount;
	}
	public void setRebuyCount(int rebuyCount) {
		this.rebuyCount = rebuyCount;
	}
	public int getNoOfSeat() {
		return noOfSeat;
	}
	public void setNoOfSeat(int noOfSeat) {
		this.noOfSeat = noOfSeat;
	}
	public int getPicksCount() {
		return picksCount;
	}
	public void setPicksCount(int picksCount) {
		this.picksCount = picksCount;
	}
	public double getChips() {
		return chips;
	}
	public void setChips(double chips) {
		this.chips = chips;
	}

	public String getGameMode() {
		return gameMode;
	}

	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}

	public String getGameType() {
		return gameType;
	}

	public void setGameType(String gameType) {
		this.gameType = gameType;
	}

	public String getIsRejoin() {
		return isRejoin;
	}

	public void setIsRejoin(String isRejoin) {
		this.isRejoin = isRejoin;
	}

	@Override
	public String toString() {
		return "UserGame [userGameId=" + userGameId + ", eventId=" + eventId
				+ ", rebuyCount=" + rebuyCount + ", noOfSeat=" + noOfSeat
				+ ", picksCount=" + picksCount + ", chips=" + chips
				+ ", gameMode=" + gameMode + ", gameType=" + gameType
				+ ", isRejoin=" + isRejoin + "]";
	}
	
}
