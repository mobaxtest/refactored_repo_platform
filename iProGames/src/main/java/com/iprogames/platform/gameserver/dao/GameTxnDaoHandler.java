package com.iprogames.platform.gameserver.dao;


import static com.iprogames.util.uuid.UUIDUtil.getUUIDFromString;
import static com.iprogames.util.uuid.UUIDUtil.generateUUID;
import static com.iprogames.util.uuid.UUIDUtil.getUUIDString;
import static com.iprogames.util.timestamp.TimeStampUtil.getCurrentSQLTimeStamp;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Repository;

import com.iprogames.platform.gameserver.common.enums.GameTxnStatusEnum;
import com.iprogames.platform.dao.SessionFactoryHandler;
import com.iprogames.platform.exception.IProGamesGeneralException;
import com.iprogames.platform.util.enums.TxnTypeEnum;
import com.iprogames.util.model.sports.IproSportsGameTxnStatus;
import com.iprogames.util.model.sports.IproSportsUserGameTransaction;

/*
 * Copyright (c) 2014 iPro,Inc. and its affiliates. All rights reserved.
 */
@Repository("gameTxnDao")
public class GameTxnDaoHandler <T extends Serializable> extends SessionFactoryHandler {
	Logger logger = LogManager.getLogger(GameTxnDaoHandler.class);

	@Resource
	MessageSource messageSource;
	
	public GameTxnDaoHandler() {
		super();
	}
	public GameTxnDaoHandler(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public String createGameTrasaction(int eventid, int gameRegid, String UUID, int sessionId, BigDecimal txnAmt, GameTxnStatusEnum status, TxnTypeEnum txnType) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			IproSportsUserGameTransaction gameTxn = constructUserGameTransaction(eventid, gameRegid, UUID, txnAmt, status, txnType, sessionId);
			session.save(gameTxn);
			tx.commit();			
			return getUUIDString(gameTxn.getGameTxnId());

		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}
		
	public String updateGameTrasaction(String txnId, GameTxnStatusEnum status) {
		Session session = getCurrentSession();
		Transaction tx = null;
		
		try {
			logger.info("txnid: " + txnId);
			tx = session.beginTransaction();
			StringBuilder queryString = new StringBuilder();
			queryString.append("select tx from IproSportsUserGameTransaction tx where ");
			queryString.append("hex(tx.gameTxnId)=:gameTxnId");
			
			Query query = session.createQuery(queryString.toString());
			query.setParameter("gameTxnId", txnId);
			
			IproSportsUserGameTransaction gameTxn = (IproSportsUserGameTransaction)query.uniqueResult();
	        
			if (gameTxn != null) {
				gameTxn = updateGameTransaction(gameTxn, status);
				session.saveOrUpdate(gameTxn);
			}
			tx.commit();			
			return gameTxn.getGameTxnId().toString();

		} catch (Exception ex) {
			if (tx != null) tx.rollback();
			ex.printStackTrace();
			throw new IProGamesGeneralException(messageSource.getMessage("SERVER_EXCEPTION",null,null));
		} finally {
			session.close();
		}
	}

	private IproSportsUserGameTransaction constructUserGameTransaction(int eventId, int txnRefId, String UUID, BigDecimal txnAmt, GameTxnStatusEnum status, TxnTypeEnum txnType, int sessionId) {
		IproSportsUserGameTransaction txn = new IproSportsUserGameTransaction();

		txn.setTxnRefId(txnRefId);		
		txn.setGameTxnId(generateUUID());
		txn.setTxnAmt(txnAmt);
		if (sessionId > 0) {
			txn.setSessionId(sessionId);
		}
		
		logger.info("Txn id to be created : " + txn.getGameTxnId());
		logger.info("The transaction type about to be created is "+txnType.name());
		txn.setUserUuid(getUUIDFromString(UUID));
		IproSportsGameTxnStatus txnStatus = new IproSportsGameTxnStatus();
		txnStatus.setGameTxnStatusCode(status.name());
		txn.setIproSportsGameTxnStatus(txnStatus);
		
		txn.setTxnTypeCode(txnType.name());
		return txn;
	}
	
	private IproSportsUserGameTransaction updateGameTransaction(IproSportsUserGameTransaction gameTxn, GameTxnStatusEnum status) {

		IproSportsGameTxnStatus txnStatus = new IproSportsGameTxnStatus();
		txnStatus.setGameTxnStatusCode(status.name());
		gameTxn.setIproSportsGameTxnStatus(txnStatus);
		
		gameTxn.setUpdatedDate(getCurrentSQLTimeStamp());
		
		return gameTxn;
	}
	
}
