package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MapLoader;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.IproTxnType;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproTxnTypesMapLoader implements MapLoader<String, IproTxnType> {	
	
	Logger logger = LogManager.getLogger(IproFundTypesMapLoader.class);
	
	@Override
	public IproTxnType load(String txnType) {
		IproTxnType iproTxnType = getHazelCastDAO().getIproTxnTypeById(txnType);
        logger.debug("load method is being processed. iproTxnType : " + iproTxnType);
		return iproTxnType;
	}

	@Override
	public Map<String, IproTxnType> loadAll(Collection<String> txnTypeColl) {
		Map<String, IproTxnType> iproTxnTypeMap;
		
		if(txnTypeColl != null && txnTypeColl.size() > 1){
			iproTxnTypeMap = getHazelCastDAO().getIproTxnTypeMap(txnTypeColl);
		}else{
			iproTxnTypeMap = getHazelCastDAO().getIproTxnTypeMap();
		}
        logger.debug("loadAll method is being processed. iproTxnTypeMap : " + iproTxnTypeMap);
		return iproTxnTypeMap;
	}

	@Override
	public Set<String> loadAllKeys() {		
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproTxnTypeKeys();
		else
			logger.debug("dao in chips config is null");
		return null;
	}

	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
	}
}
