package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MapLoader;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.sports.IproSportsAddChipsConfig;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproSportsAddChipsConfigMapLoader implements MapLoader<Integer, IproSportsAddChipsConfig> {
	Logger logger = LogManager.getLogger(IproSportsAddChipsConfigMapLoader.class);
	@Override
	public IproSportsAddChipsConfig load(Integer addchipsConfigId) {
		IproSportsAddChipsConfig iproAddChipsConfig = getHazelCastDAO().getIproSportsAddChipsConfig(addchipsConfigId);
        logger.debug("load method is being processed. iproAddChipsConfig : " + iproAddChipsConfig);
		return iproAddChipsConfig;
	}

	@Override
	public Map<Integer, IproSportsAddChipsConfig> loadAll(
			Collection<Integer> addchipsConfigColl) {
		Map<Integer, IproSportsAddChipsConfig> iproAddChipsConfigMap;// = getHazelCastDAO().getIproSportsChipsConfigMap(chipsConfigId);        
        if(addchipsConfigColl != null && addchipsConfigColl.size() > 1){
        	iproAddChipsConfigMap = getHazelCastDAO().getIproSportsAddChipsConfigMap(addchipsConfigColl);
		}else{
			iproAddChipsConfigMap = getHazelCastDAO().getIproSportsAddChipsConfigMap();
		}
        logger.debug("loadAll method is being processed. iproSportsUserGameMap : " + iproAddChipsConfigMap);
		return iproAddChipsConfigMap;
	}

	@Override
	public Set<Integer> loadAllKeys() {
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproSportsAddChipsConfigKeys();
		else
			logger.debug("dao in add chips config is null");
		return null;
	}
	
	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		if(ApplicationContextUtils.getApplicationContext() != null)
			return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
		else
			return null;
		//return hazelCastDAO;
	}

}
