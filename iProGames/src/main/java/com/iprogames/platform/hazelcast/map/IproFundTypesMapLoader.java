package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hazelcast.core.MapLoader;
import com.hazelcast.spring.context.SpringAware;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.IproFundType;
import com.iprogames.util.session.context.ApplicationContextUtils;


public class IproFundTypesMapLoader implements MapLoader<String, IproFundType> {	
	
	Logger logger = LogManager.getLogger(IproFundTypesMapLoader.class);
	
	@Override
	public IproFundType load(String fundType) {
		IproFundType iproFundType = getHazelCastDAO().getIproFundTypeById(fundType);
        logger.debug("load method is being processed. IproFundType : " + iproFundType);
		return iproFundType;
	}

	@Override
	public Map<String, IproFundType> loadAll(Collection<String> fundTypeColl) {
		Map<String, IproFundType> iproFundTypeMap;
		
		if(fundTypeColl != null && fundTypeColl.size() > 1){
			iproFundTypeMap = getHazelCastDAO().getIproFundTypeMap(fundTypeColl);
		}else{
			iproFundTypeMap = getHazelCastDAO().getIproFundTypeMap();
		}
        logger.debug("loadAll method is being processed. iproFundTypeMap : " + iproFundTypeMap);
		return iproFundTypeMap;
	}

	@Override
	public Set<String> loadAllKeys() {		
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproFundTypeKeys();
		else
			logger.debug("dao in chips config is null");
		return null;
	}

	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
	}
}
