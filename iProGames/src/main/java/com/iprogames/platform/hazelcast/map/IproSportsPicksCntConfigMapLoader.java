package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MapLoader;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.sports.IproSportsPicksCntConfig;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproSportsPicksCntConfigMapLoader implements MapLoader<Integer, IproSportsPicksCntConfig> {
	Logger logger = LogManager.getLogger(IproSportsPicksCntConfigMapLoader.class);
	@Override
	public IproSportsPicksCntConfig load(Integer addchipsConfigId) {
		IproSportsPicksCntConfig iproPicksCntConfig = getHazelCastDAO().getIproSportsPicksCntConfig(addchipsConfigId);
        logger.debug("load method is being processed. iproAddChipsConfig : " + iproPicksCntConfig);
		return iproPicksCntConfig;
	}

	@Override
	public Map<Integer, IproSportsPicksCntConfig> loadAll(
			Collection<Integer> picksCntConfigColl) {
		Map<Integer, IproSportsPicksCntConfig> iproSportsPicksCntConfigMap;        
        if(picksCntConfigColl != null && picksCntConfigColl.size() > 1){
        	iproSportsPicksCntConfigMap = getHazelCastDAO().getIproSportsPicksCntConfigMap(picksCntConfigColl);
		}else{
			iproSportsPicksCntConfigMap = getHazelCastDAO().getIproSportsPicksCntConfigMap();
		}
        logger.debug("loadAll method is being processed. iproSportsPicksCntConfigMap : " + iproSportsPicksCntConfigMap);
		return iproSportsPicksCntConfigMap;
	}

	@Override
	public Set<Integer> loadAllKeys() {
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproSportsPicksCntConfigKeys();
		else
			logger.debug("dao in picks cnt config is null");
		return null;
	}
	
	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		if(ApplicationContextUtils.getApplicationContext() != null)
			return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
		else
			return null;
		//return hazelCastDAO;
	}


}
