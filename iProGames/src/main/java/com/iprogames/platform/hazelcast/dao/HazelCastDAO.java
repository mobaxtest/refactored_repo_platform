package com.iprogames.platform.hazelcast.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import com.iprogames.platform.hazelcast.common.HazelChipsConfig;
import com.iprogames.util.model.IproFundType;
import com.iprogames.util.model.IproTxnType;
import com.iprogames.util.model.IproUserSession;
import com.iprogames.util.model.sports.IproSportsAddChipsConfig;
import com.iprogames.util.model.sports.IproSportsChipsConfig;
import com.iprogames.util.model.sports.IproSportsPicksCntConfig;
import com.iprogames.util.model.sports.IproSportsSeatConfig;
import com.iprogames.util.model.sports.IproSportsUserGame;

@Transactional(readOnly = true)
public class HazelCastDAO{

	private SessionFactory sessionFactory;
	Logger logger = LogManager.getLogger(HazelCastDAO.class);
	
    public void setSessionFactory(SessionFactory sessionFactory) {
    	logger.debug("The session factory is set ################# "+sessionFactory);
        this.sessionFactory = sessionFactory;
    }
    
    public List<IproFundType> getIproFundTypes() {
    	logger.debug("getIproFundTypes is called");
        @SuppressWarnings("unchecked")
		List<IproFundType> list = sessionFactory.getCurrentSession().createQuery("from IproFundType").list();
        return list;
    }

	public IproFundType getIproFundTypeById(String fundType) {
		IproFundType iproFundType = (IproFundType) sessionFactory.getCurrentSession()
							                .createQuery("from IproFundType where id=?")
							                .setParameter(0, fundType).uniqueResult();
		return iproFundType;
	}

	public Map<String, IproFundType> getIproFundTypeMap() {
	    	Map<String, IproFundType> iproFundTypes = new HashMap<String, IproFundType>();
	    	List<IproFundType> list = getIproFundTypes();
			for(IproFundType iproFundType : list) {
				
				iproFundTypes.put(iproFundType.getFundType(), iproFundType);
			}
			
			return iproFundTypes;
	}

    public Map<String, IproFundType> getIproFundTypeMap(Collection<String> fundTypes) {
    	Map<String, IproFundType> iproFundTypes = new HashMap<String, IproFundType>();
		for(String fundType : fundTypes) {
			IproFundType iproFundType = (IproFundType) sessionFactory.getCurrentSession()
						                .createQuery("from IproFundType where id=?")
						                .setParameter(0, fundType)
						                .uniqueResult();
			iproFundTypes.put(iproFundType.getFundType(), iproFundType);
		}
		
		return iproFundTypes;
	}

	public Set<String> getIproFundTypeKeys() {
		logger.debug("The getIproFundTypeKeys " );
		@SuppressWarnings("unchecked")
		List<String> fundTypeList = sessionFactory.getCurrentSession()
							        .createQuery("select ft.id from IproFundType ft")
							        .list();
		return new HashSet<String>(fundTypeList);
	}

	public IproUserSession getIproUserSessionById(String iproToken) {
		IproUserSession iproUserSession = (IproUserSession) sessionFactory.getCurrentSession()
                .createQuery("from IproUserSession where providerAuthToken=?")
                .setParameter(0, iproToken).uniqueResult();
		return iproUserSession;		
	}
	
	public Map<String, IproUserSession> getIproUserSessionMap() {
    	Map<String, IproUserSession> iproUserSessions = new HashMap<String, IproUserSession>();
    	List<IproUserSession> list = getIproUserSessions();
		for(IproUserSession iproUserSession : list) {			
			iproUserSessions.put(iproUserSession.getProviderAuthToken(), iproUserSession);
		}		
		return iproUserSessions;
	}

	public List<IproUserSession> getIproUserSessions() {
    	logger.debug("getIproUserSessions is called");
        @SuppressWarnings("unchecked")
		List<IproUserSession> list = sessionFactory.getCurrentSession().createQuery("from IproUserSession").list();
        return list;
    }

	public Map<String, IproUserSession> getIproUserSessionMap(
			Collection<String> userSessionColl) {
		Map<String, IproUserSession> iproUserSessions = new HashMap<String, IproUserSession>();
		for(String iproToken : userSessionColl) {
			IproUserSession iproUserSession = (IproUserSession) sessionFactory.getCurrentSession()
						                .createQuery("from IproUserSession where providerAuthToken=?")
						                .setParameter(0, iproToken)
						                .uniqueResult();
			iproUserSessions.put(iproUserSession.getProviderAuthToken(), iproUserSession);
		}		
		return iproUserSessions;
	}

	public IproSportsUserGame getIproUserGameById(Integer gameId) {
		IproSportsUserGame iproSportsUserGame = (IproSportsUserGame) sessionFactory.getCurrentSession()
                .createQuery("from IproSportsUserGame where id=?")
                .setParameter(0, gameId).uniqueResult();
		return iproSportsUserGame;
	}

	public Map<Integer, IproSportsUserGame> getIproSportsUserGameMap(
			Collection<Integer> userGameColl) {
		Map<Integer, IproSportsUserGame> iproUserGames = new HashMap<Integer, IproSportsUserGame>();
		for(Integer gameId : userGameColl) {
			IproSportsUserGame iproUserGame = (IproSportsUserGame) sessionFactory.getCurrentSession()
						                .createQuery("from IproSportsUserGame where id=?")
						                .setParameter(0, gameId)
						                .uniqueResult();
			iproUserGames.put(iproUserGame.getUserGameId(), iproUserGame);
		}		
		return iproUserGames;
	}

	public Map<Integer, IproSportsUserGame> getIproSportsUserGameMap() {
		Map<Integer, IproSportsUserGame> iproUserGames = new HashMap<Integer, IproSportsUserGame>();
    	List<IproSportsUserGame> list = getIproUserGames();
		for(IproSportsUserGame iproUserGame : list) {			
			iproUserGames.put(iproUserGame.getUserGameId(), iproUserGame);
		}		
		return iproUserGames;		
	}
	
	public List<IproSportsUserGame> getIproUserGames() {
    	logger.debug("getIproUserGames is called");
        @SuppressWarnings("unchecked")
		List<IproSportsUserGame> list = sessionFactory.getCurrentSession().createQuery("from IproSportsUserGame").list();
        return list;
    }

	public void storeUserGame(IproSportsUserGame userGame) {
		// TODO Auto-generated method stub
		
	}

	public HazelChipsConfig getIproSportsChipsConfig(Integer chipsConfigId) {
		IproSportsChipsConfig iproChipsConfig = (IproSportsChipsConfig) sessionFactory.getCurrentSession()
                .createQuery("from IproSportsChipsConfig where id=?")
                .setParameter(0, chipsConfigId).uniqueResult();
		HazelChipsConfig hc = new HazelChipsConfig();
		hc.setChipsConfigId(iproChipsConfig.getChipsConfigId());
		hc.setConfigDesc(iproChipsConfig.getConfigDesc());
		hc.setGameTypeCode(iproChipsConfig.getIproSportsGameType().getGameTypeCode());
		hc.setInitialChips(iproChipsConfig.getInitialChips().intValue());
		hc.setPlayMode(iproChipsConfig.getIproSportsPlayMode().getPlayMode());
		hc.setSportsId(iproChipsConfig.getIproSport().getSportsId());
		return hc;
	}

	public Map<Integer, HazelChipsConfig> getIproSportsChipsConfigMap(Collection<Integer> chipsConfigColl) {
		Map<Integer, HazelChipsConfig> iproChipsConfigMap = new HashMap<Integer, HazelChipsConfig>();
    	List<IproSportsChipsConfig> list = getIproChipsConfigs();
		for(IproSportsChipsConfig iproChipsConfig : list) {		
			HazelChipsConfig hc = new HazelChipsConfig();
			hc.setChipsConfigId(iproChipsConfig.getChipsConfigId());
			hc.setConfigDesc(iproChipsConfig.getConfigDesc());
			hc.setGameTypeCode(iproChipsConfig.getIproSportsGameType().getGameTypeCode());
			hc.setInitialChips(iproChipsConfig.getInitialChips().intValue());
			hc.setPlayMode(iproChipsConfig.getIproSportsPlayMode().getPlayMode());
			hc.setSportsId(iproChipsConfig.getIproSport().getSportsId());
			iproChipsConfigMap.put(hc.getChipsConfigId(), hc);
		}		
		return iproChipsConfigMap;
	}

	private List<IproSportsChipsConfig> getIproChipsConfigs() {
		logger.debug("getIproChipsConfigs is called");
        @SuppressWarnings("unchecked")
		List<IproSportsChipsConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsChipsConfig order by chipsConfigId desc").list();
        return list;		
	}

	public Map<Integer, HazelChipsConfig> getIproSportsChipsConfigMap() {
		Map<Integer, HazelChipsConfig> iproChipsConfigs = new HashMap<Integer, HazelChipsConfig>();
    	List<IproSportsChipsConfig> list = getIproChipsConfigs();
		for(IproSportsChipsConfig iproChipsConfig : list) {		
			HazelChipsConfig hc = new HazelChipsConfig();
			hc.setChipsConfigId(iproChipsConfig.getChipsConfigId());
			hc.setConfigDesc(iproChipsConfig.getConfigDesc());
			hc.setGameTypeCode(iproChipsConfig.getIproSportsGameType().getGameTypeCode());
			hc.setInitialChips(iproChipsConfig.getInitialChips().intValue());
			hc.setPlayMode(iproChipsConfig.getIproSportsPlayMode().getPlayMode());
			hc.setSportsId(iproChipsConfig.getIproSport().getSportsId());
			iproChipsConfigs.put(hc.getChipsConfigId(), hc);
			logger.info("iproChipsConfig ************************************************************ " + hc.getInitialChips());			
		}		
		return iproChipsConfigs;	
	}

	public Set<Integer> getIproSportsChipsConfigKeys() {
		logger.debug("getIproSportsChipsConfigKeys is called");
		@SuppressWarnings("unchecked")
		List<IproSportsChipsConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsChipsConfig order by chipsConfigId desc").list();
		Set<Integer> keyList = new HashSet<Integer>();
		for(IproSportsChipsConfig cp : list)
		{
			keyList.add(cp.getChipsConfigId());
		}
		return keyList;
	}

	public IproSportsAddChipsConfig getIproSportsAddChipsConfig(
			Integer addchipsConfigId) {
		IproSportsAddChipsConfig iproSportsAddChips = (IproSportsAddChipsConfig) sessionFactory.getCurrentSession()
                .createQuery("from IproSportsAddChipsConfig where id=?")
                .setParameter(0, addchipsConfigId).uniqueResult();
		return iproSportsAddChips;
	}

	public Map<Integer, IproSportsAddChipsConfig> getIproSportsAddChipsConfigMap(
			Collection<Integer> addchipsConfigColl) {
		Map<Integer, IproSportsAddChipsConfig> iproAddChipsConfigMap = new HashMap<Integer, IproSportsAddChipsConfig>();
    	List<IproSportsAddChipsConfig> list = getIproAddChipsConfigs();
		for(IproSportsAddChipsConfig iproAddChipsConfig : list) {			
			iproAddChipsConfigMap.put(iproAddChipsConfig.getAddChipsConfigId(), iproAddChipsConfig);
		}		
		return iproAddChipsConfigMap;
	}
	
	private List<IproSportsAddChipsConfig> getIproAddChipsConfigs() {
		logger.debug("IproSportsAddChipsConfig is called");
        @SuppressWarnings("unchecked")
		List<IproSportsAddChipsConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsAddChipsConfig order by addChipsConfigId desc").list();
        return list;		
	}

	public Map<Integer, IproSportsAddChipsConfig> getIproSportsAddChipsConfigMap() {
		Map<Integer, IproSportsAddChipsConfig> iproAddChipsConfigMap = new HashMap<Integer, IproSportsAddChipsConfig>();
    	List<IproSportsAddChipsConfig> list = getIproAddChipsConfigs();
		for(IproSportsAddChipsConfig iproAddChipsConfig : list) {			
			iproAddChipsConfigMap.put(iproAddChipsConfig.getAddChipsConfigId(), iproAddChipsConfig);
		}		
		return iproAddChipsConfigMap;
	}
	
	public Set<Integer> getIproSportsAddChipsConfigKeys() {
		logger.debug("getIproSportsAddChipsConfigKeys is called");
		@SuppressWarnings("unchecked")
		List<IproSportsAddChipsConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsAddChipsConfig order by addChipsConfigId desc").list();
		Set<Integer> keyList = new HashSet<Integer>();
		for(IproSportsAddChipsConfig cp : list)
		{
			keyList.add(cp.getAddChipsConfigId());
		}
		return keyList;
	}

	public Set<String> getIproTxnTypeKeys() {
		logger.debug("The getIproTxnTypeKeys " );
		@SuppressWarnings("unchecked")
		List<String> txnTypeList = sessionFactory.getCurrentSession()
							        .createQuery("select ft.id from IproTxnType ft")
							        .list();
		return new HashSet<String>(txnTypeList);
	}

	public Map<String, IproTxnType> getIproTxnTypeMap() {
		Map<String, IproTxnType> iproTxnTypeMap = new HashMap<String, IproTxnType>();
    	List<IproTxnType> list = getIproTxnTypes();
		for(IproTxnType iproTxnType : list) {			
			iproTxnTypeMap.put(iproTxnType.getTxnTypeCode(), iproTxnType);
		}		
		return iproTxnTypeMap;		
	}

	private List<IproTxnType> getIproTxnTypes() {
		logger.debug("IproTxnType is called");
        @SuppressWarnings("unchecked")
		List<IproTxnType> list = sessionFactory.getCurrentSession().createQuery("from IproTxnType").list();
        return list;
	}

	public Map<String, IproTxnType> getIproTxnTypeMap(Collection<String> txnTypeColl) {
		Map<String, IproTxnType> iproTxnTypeMap = new HashMap<String, IproTxnType>();
    	List<IproTxnType> list = getIproTxnTypes();
		for(IproTxnType iproTxnType : list) {			
			iproTxnTypeMap.put(iproTxnType.getTxnTypeCode(), iproTxnType);
		}		
		return iproTxnTypeMap;
	}

	public IproTxnType getIproTxnTypeById(String txnType) {
		IproTxnType iproTxnType = (IproTxnType) sessionFactory.getCurrentSession()
                .createQuery("from IproTxnType where id=?")
                .setParameter(0, txnType).uniqueResult();
		return iproTxnType;
	}

	public IproSportsPicksCntConfig getIproSportsPicksCntConfig(
			Integer picksCntConfigId) {
		IproSportsPicksCntConfig iproPicksCnt = (IproSportsPicksCntConfig) sessionFactory.getCurrentSession()
                .createQuery("from IproSportsPicksCntConfig where id=?")
                .setParameter(0, picksCntConfigId).uniqueResult();
		return iproPicksCnt;
	}

	public Map<Integer, IproSportsPicksCntConfig> getIproSportsPicksCntConfigMap(
			Collection<Integer> picksCntConfigColl) {
		Map<Integer, IproSportsPicksCntConfig> iproPicksCntMap = new HashMap<Integer, IproSportsPicksCntConfig>();
    	List<IproSportsPicksCntConfig> list = getIproSportsPicksCnts();
		for(IproSportsPicksCntConfig iproPicksCnt : list) {			
			iproPicksCntMap.put(iproPicksCnt.getPicksCntConfigId(), iproPicksCnt);
		}		
		return iproPicksCntMap;
	}

	private List<IproSportsPicksCntConfig> getIproSportsPicksCnts() {
		logger.debug("getIproSportsPicksCntConfigMap is called");
        @SuppressWarnings("unchecked")
		List<IproSportsPicksCntConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsPicksCntConfig order by picksCntConfigId desc").list();
        return list;
	}

	public Map<Integer, IproSportsPicksCntConfig> getIproSportsPicksCntConfigMap() {
		Map<Integer, IproSportsPicksCntConfig> iproPicksCntMap = new HashMap<Integer, IproSportsPicksCntConfig>();
    	List<IproSportsPicksCntConfig> list = getIproSportsPicksCnts();
		for(IproSportsPicksCntConfig iproPicksCnt : list) {			
			iproPicksCntMap.put(iproPicksCnt.getPicksCntConfigId(), iproPicksCnt);
		}		
		return iproPicksCntMap;	
	}

	public Set<Integer> getIproSportsPicksCntConfigKeys() {
		logger.debug("The getIproSportsPicksCntConfigKeys " );
		@SuppressWarnings("unchecked")
		List<Integer> picksCntList = sessionFactory.getCurrentSession()
							        .createQuery("select ft.id from IproSportsPicksCntConfig ft")
							        .list();
		return new HashSet<Integer>(picksCntList);
	}

	public IproSportsSeatConfig getIproSportsSeatConfig(Integer seatConfigId) {
		IproSportsSeatConfig iproSeatConfig = (IproSportsSeatConfig) sessionFactory.getCurrentSession()
                .createQuery("from IproSportsSeatConfig where id=?")
                .setParameter(0, seatConfigId).uniqueResult();
		return iproSeatConfig;
	}

	public Map<Integer, IproSportsSeatConfig> getIproSportsSeatConfigMap(
			Collection<Integer> seatConfigColl) {
		Map<Integer, IproSportsSeatConfig> iproSeatConfigMap = new HashMap<Integer, IproSportsSeatConfig>();
    	List<IproSportsSeatConfig> list = getIproSportsSeatConfigs();
		for(IproSportsSeatConfig iproSeat : list) {			
			iproSeatConfigMap.put(iproSeat.getSeatConfigId(), iproSeat);
		}		
		return iproSeatConfigMap;
	}

	public Map<Integer, IproSportsSeatConfig> getIproSportsSeatConfigMap() {
		Map<Integer, IproSportsSeatConfig> iproSeatConfigMap = new HashMap<Integer, IproSportsSeatConfig>();
    	List<IproSportsSeatConfig> list = getIproSportsSeatConfigs();
		for(IproSportsSeatConfig iproSeat : list) {			
			iproSeatConfigMap.put(iproSeat.getSeatConfigId(), iproSeat);
		}		
		return iproSeatConfigMap;	
	}

	private List<IproSportsSeatConfig> getIproSportsSeatConfigs() {
		logger.debug("getIproSportsSeatConfigs is called");
        @SuppressWarnings("unchecked")
		List<IproSportsSeatConfig> list = sessionFactory.getCurrentSession().createQuery("from IproSportsSeatConfig order by seatConfigId desc").list();        
        return list;
	}

	public Set<Integer> getIproSportsSeatConfigKeys() {
		logger.debug("The getIproSportsSeatConfigKeys " );
		@SuppressWarnings("unchecked")
		List<Integer> seatList = sessionFactory.getCurrentSession()
							        .createQuery("select ft.id from IproSportsSeatConfig ft")
							        .list();		
		return new HashSet<Integer>(seatList);
	}
}