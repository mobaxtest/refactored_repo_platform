package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MapLoader;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.sports.IproSportsSeatConfig;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproSportsSeatConfigMapLoader implements MapLoader<Integer, IproSportsSeatConfig>{
	Logger logger = LogManager.getLogger(IproSportsSeatConfigMapLoader.class);
	@Override
	public IproSportsSeatConfig load(Integer seatConfigId) {
		IproSportsSeatConfig iproSeatConfig = getHazelCastDAO().getIproSportsSeatConfig(seatConfigId);
        logger.debug("load method is being processed. iproSeatConfig : " + iproSeatConfig);
		return iproSeatConfig;
	}

	@Override
	public Map<Integer, IproSportsSeatConfig> loadAll(Collection<Integer> seatConfigColl) {
		Map<Integer, IproSportsSeatConfig> iproSeatConfigMap;// = getHazelCastDAO().getIproSportsChipsConfigMap(chipsConfigId);        
        if(seatConfigColl != null && seatConfigColl.size() > 1){
        	iproSeatConfigMap = getHazelCastDAO().getIproSportsSeatConfigMap(seatConfigColl);
		}else{
			iproSeatConfigMap = getHazelCastDAO().getIproSportsSeatConfigMap();
		}
        logger.debug("loadAll method is being processed. iproSeatConfigMap : " + iproSeatConfigMap.size());
		return iproSeatConfigMap;
	}

	@Override
	public Set<Integer> loadAllKeys() {
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproSportsSeatConfigKeys();
		else
			logger.debug("dao in add chips config is null");
		return null;
	}
	
	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		if(ApplicationContextUtils.getApplicationContext() != null)
			return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
		else
			return null;		
	}

}
