package com.iprogames.platform.hazelcast.common;

import java.io.Serializable;
import java.math.BigDecimal;

public class HazelChipsConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int chipsConfigId;	
	private String configDesc;	
	private int initialChips;
	private String gameTypeCode;
	private int sportsId;	
	private String playMode;
	
	public int getChipsConfigId() {
		return this.chipsConfigId;
	}

	public void setChipsConfigId(int chipsConfigId) {
		this.chipsConfigId = chipsConfigId;
	}

	public String getConfigDesc() {
		return this.configDesc;
	}

	public void setConfigDesc(String configDesc) {
		this.configDesc = configDesc;
	}

	public int getInitialChips() {
		return this.initialChips;
	}

	public void setInitialChips(int initialChips) {
		this.initialChips = initialChips;
	}	

	public String getGameTypeCode() {
		return gameTypeCode;
	}

	public void setGameTypeCode(String gameTypeCode) {
		this.gameTypeCode = gameTypeCode;
	}

	public int getSportsId() {
		return sportsId;
	}

	public void setSportsId(int sportsId) {
		this.sportsId = sportsId;
	}

	public String getPlayMode() {
		return playMode;
	}

	public void setPlayMode(String playMode) {
		this.playMode = playMode;
	}
}