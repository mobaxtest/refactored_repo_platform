package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hazelcast.core.MapLoader;
import com.hazelcast.core.MapStore;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.model.sports.IproSportsUserGame;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproSportsUserGameMapLoader implements MapLoader<Integer, IproSportsUserGame>,MapStore<Integer,IproSportsUserGame>{
	Logger logger = LogManager.getLogger(IproSportsUserGameMapLoader.class);
	
	@Override
	public void delete(Integer arg0) {		
		
	}

	@Override
	public void deleteAll(Collection<Integer> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void store(Integer arg0, IproSportsUserGame userGame) {		
		getHazelCastDAO().storeUserGame(userGame);
	}

	@Override
	public void storeAll(Map<Integer, IproSportsUserGame> userGame) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IproSportsUserGame load(Integer gameId) {
		IproSportsUserGame iproUserGame = getHazelCastDAO().getIproUserGameById(gameId);
        logger.debug("load method is being processed. IproSportsUserGame : " + iproUserGame);
		return iproUserGame;		
	}

	@Override
	public Map<Integer, IproSportsUserGame> loadAll(Collection<Integer> userGameColl) {
		Map<Integer, IproSportsUserGame> iproSportsUserGameMap;
		
		if(userGameColl != null && userGameColl.size() > 1){
			iproSportsUserGameMap = getHazelCastDAO().getIproSportsUserGameMap(userGameColl);
		}else{
			iproSportsUserGameMap = getHazelCastDAO().getIproSportsUserGameMap();
		}
        logger.debug("loadAll method is being processed. iproSportsUserGameMap : " + iproSportsUserGameMap);
		return iproSportsUserGameMap;
	}

	@Override
	public Set<Integer> loadAllKeys() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
	}

}
