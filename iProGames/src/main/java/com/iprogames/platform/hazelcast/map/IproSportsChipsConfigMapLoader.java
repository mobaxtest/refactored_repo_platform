package com.iprogames.platform.hazelcast.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.hazelcast.core.MapLoader;
import com.iprogames.platform.hazelcast.common.HazelChipsConfig;
import com.iprogames.platform.hazelcast.dao.HazelCastDAO;
import com.iprogames.util.session.context.ApplicationContextUtils;

public class IproSportsChipsConfigMapLoader implements MapLoader<Integer, HazelChipsConfig> {
	/*@Autowired
	HazelCastDAO hazelCastDAO;*/
	Logger logger = LogManager.getLogger(IproSportsChipsConfigMapLoader.class);
	@Override
	public HazelChipsConfig load(Integer chipsConfigId) {
		HazelChipsConfig iproChipsConfig = getHazelCastDAO().getIproSportsChipsConfig(chipsConfigId);
        logger.debug("load method is being processed. IproSportsChipsConfig : " + iproChipsConfig);
		return iproChipsConfig;
	}

	@Override
	public Map<Integer, HazelChipsConfig> loadAll(Collection<Integer> chipsConfigColl) {
		Map<Integer, HazelChipsConfig> iproChipsConfigMap;// = getHazelCastDAO().getIproSportsChipsConfigMap(chipsConfigId);        
        if(chipsConfigColl != null && chipsConfigColl.size() > 1){
        	iproChipsConfigMap = getHazelCastDAO().getIproSportsChipsConfigMap(chipsConfigColl);
		}else{
			iproChipsConfigMap = getHazelCastDAO().getIproSportsChipsConfigMap();
		}
        logger.debug("loadAll method is being processed. iproSportsUserGameMap : " + iproChipsConfigMap);
		return iproChipsConfigMap;
	}

	@Override
	public Set<Integer> loadAllKeys() {
		if(getHazelCastDAO() != null)
			return getHazelCastDAO().getIproSportsChipsConfigKeys();
		else
			logger.debug("dao in chips config is null");
		return null;
	}
	
	public HazelCastDAO getHazelCastDAO() {
		logger.debug("The application context is "+ApplicationContextUtils.getApplicationContext());
		if(ApplicationContextUtils.getApplicationContext() != null)
			return ApplicationContextUtils.getApplicationContext().getBean(HazelCastDAO.class);
		else
			return null;
		//return hazelCastDAO;
	}

}
