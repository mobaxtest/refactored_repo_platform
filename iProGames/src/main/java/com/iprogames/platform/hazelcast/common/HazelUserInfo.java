package com.iprogames.platform.hazelcast.common;

import java.io.Serializable;

public class HazelUserInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8306045908160538771L;
	private String userAccssMode;

	public String getUserAccssMode() {
		return userAccssMode;
	}

	public void setUserAccssMode(String userAccssMode) {
		this.userAccssMode = userAccssMode;
	}

}
