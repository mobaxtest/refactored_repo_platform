package com.iprogames.platform.sfs.games;

import com.iprogames.platform.sfs.ext.EventHandlerExtension;
import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GameSelectReqtHandler extends BaseClientRequestHandler {

	public void handleClientRequest(User defaultUser, ISFSObject params) {
		
		//Fetch the Games Server URL for Games related transactions
		//Properties ConfigProperties = getParentExtension().getConfigProperties();
		//String GAMES_SERVER_URL = ConfigProperties.getProperty("games-url");
		//trace(GAMES_SERVER_URL); 
		
		trace("  -+++++++++++++-  Inside Game List Request Handler  -++++++++++++++-  ");
				
		//Set the game listing servlet URL path
		String servletUrl = "game/list";
		//trace("  --  GameListRequest: ", params.toString(), "  --  ");
		trace("  -+++++++++++++-  GameListRequest: ", params.getUtfString("GameListReq"), "  -++++++++++++++-  ");
		
		//Fetch the games listing
		
		//** Use static URL for Games server **//
			//String response = HttpClientHandler.HttpPostGameList(servletUrl, params.toString());
			String response = HttpClientHandler.HttpPostGameList(servletUrl, params.getUtfString("GameListReq"));
	
		//**Use URL from config file **//
			//String response = HttpClientHandler.HttpPostGameList(GAMES_SERVER_URL, servletUrl, params.toString());
	
		trace("  -++++++++++++++-  GameListResponse: ", response, "  -+++++++++++++++++-  ");
		
		//Formulate and send the response to client		
       	ISFSObject outData = new SFSObject();
		outData.putUtfString("response", response);
     
		EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();
		parentEx.send("GameSelect", outData, defaultUser);
	
	}
}
