package com.iprogames.platform.sfs.games;

import com.iprogames.platform.sfs.ext.EventHandlerExtension;
import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GameJoinReqtHandler extends BaseClientRequestHandler {

	public void handleClientRequest(User defaultUser, ISFSObject params) {
		// TODO Auto-generated method stub
		//Fetch the Games Server URL for Games related transactions
		//Properties ConfigProperties = getParentExtension().getConfigProperties();
		//String GAMES_SERVER_URL = ConfigProperties.getProperty("games-url");
		//trace(GAMES_SERVER_URL); 
		
		trace("  -^^^^^^^^^^^^-  Inside Game Join Request Handler  -^^^^^^^^^^^^^^-  ");
				
		//Set the game listing servlet URL path
		String servletUrl = "game/usergame/join";
		//trace("  --  GameJoinRequest: ", params.toString(), "  --  ");
		trace("  -^^^^^^^^^^^^-  GameJoinRequest: ", params.getUtfString("GameJoinReq"), "  -^^^^^^^^^^^-  ");
		
		//Join the User Game
		
		//** Use static URL for Games server **//
			//String response = HttpClientHandler.HttpPostGameJoin(servletUrl, params.toString());
			String response = HttpClientHandler.HttpPostGameJoin(servletUrl, params.getUtfString("GameJoinReq"));
	
		//**Use URL from config file **//
			//String response = HttpClientHandler.HttpPostGameJoin(GAMES_SERVER_URL, servletUrl, params.toString());
	
		trace("  -^^^^^^^^^^^^-  GameJoinResponse: ", response, "  -^^^^^^^^^^^-  ");
		
		//Formulate and send the response to client		
       	ISFSObject outData = new SFSObject();
		outData.putUtfString("response", response);
     
		EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();
		parentEx.send("GameJoin", outData, defaultUser);
		

	}

}
