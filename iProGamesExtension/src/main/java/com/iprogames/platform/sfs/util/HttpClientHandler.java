package com.iprogames.platform.sfs.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpClientHandler {

	//private static String HTTP_URL = "http://200.201.202.60:9090/iprogames/auth";
	//private static String GAMESERVER_URL = "http://200.201.202.60:9090/iprogames/";
	private static String HTTP_URL = "http://10.0.14.49:8080/iprogames/auth";
	private static String GAMESERVER_URL = "http://10.0.14.49:8080/iprogames/";
	
	public static String HttpPost(String servletUrl, String authToken) {
		try {
			System.out.println("  /////////////// Inside HttpPost - static ///////////////");
			
			//Formulate the URL for Auth verification
			String geturl = HTTP_URL + "/" + servletUrl;

			System.out.println(" +++++++ ServletURL: " + geturl + " +++++++ ");
			String postData = "{\"iProToken\":\"" + authToken + "\"}";	
			
			System.out.println(" +++++++ Auth Verification Request: " + postData + " +++++++ ");
			
			//Set connection
			URL obj = new URL(geturl);		
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST"); 
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());  			
			wr.writeBytes(postData);
			wr.flush();
			wr.close();

			// Receive the response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			//Formulate the response to Login Event Handler
			String inputLine;
			StringBuffer response = new StringBuffer();	  	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(" +++++++ Auth Verification Response: " + response.toString() + " +++++++ ");
			return response.toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  null;
	}
	
	
	public static String HttpPost(String HTTP_AUTH_URL, String servletURL, String authToken) {
try {
			System.out.println("  /////////////// Inside HttpPost - config ///////////////");
	
			//Formulate the URL for Auth verification
			String geturl = HTTP_AUTH_URL + "/" + servletURL;
			String postData = "{\"iProToken\":\"" + authToken + "\"}";				
			
			//Set connection
			URL obj = new URL(geturl);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST"); 
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());  			
			wr.writeBytes(postData);
			wr.flush();
			wr.close();

			// Receive the response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			//Formulate the response to Login Event Handler
			String inputLine;
			StringBuffer response = new StringBuffer();	  	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			
			//Send response to Login Event Handler
			return response.toString();
						
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
	
	public static String HttpPostGameList(String servletUrl, String reqGameList) {
        try {
        	System.out.println("  /////////////// Inside HttpPost GameList ///////////////");
        	
        	//Formulate the URL for Auth verification
            String geturl = GAMESERVER_URL + "/" + servletUrl;
            String postData = reqGameList;
           
            //Set connection
            URL obj = new URL(geturl);       
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
           
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());             
            wr.writeBytes(postData);
            wr.flush();
            wr.close();

            // Receive the response
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
           
            //Formulate the response to Game Select Req Handler
            String inputLine;
            StringBuffer response = new StringBuffer();           
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());
            return response.toString();
            
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return  null;
    }


	public static String HttpPostGameSearch(String servletUrl, String reqGameSearch) {
		// TODO Auto-generated method stub
		 try {
	        	System.out.println("  /////////////// Inside HttpPost GameSearch ///////////////");
	        	
	        	//Formulate the URL for Auth verification
	            String geturl = GAMESERVER_URL + "/" + servletUrl;
	            String postData = reqGameSearch;
	           
	            //Set connection
	            URL obj = new URL(geturl);       
	            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	            con.setRequestMethod("POST");
	            con.setRequestProperty("Content-Type", "application/json");
	            con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
	           
	            // Send post request
	            con.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(con.getOutputStream());             
	            wr.writeBytes(postData);
	            wr.flush();
	            wr.close();

	            // Receive the response
	            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	           
	            //Formulate the response to Game Search Req Handler
	            String inputLine;
	            StringBuffer response = new StringBuffer();           
	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            System.out.println(response.toString());
	            return response.toString();
	            
	        } catch (MalformedURLException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
		
		return null;
	}


	public static String HttpPostGameCreate(String servletUrl, String reqGameCreate) {
		// TODO Auto-generated method stub
		try {
		System.out.println("  /////////////// Inside HttpPost GameCreate ///////////////");
    	
    	//Formulate the URL for Game Create
        String geturl = GAMESERVER_URL + "/" + servletUrl;
        String postData = reqGameCreate;
       
        //Set connection
        URL obj = new URL(geturl);       
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
       
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());             
        wr.writeBytes(postData);
        wr.flush();
        wr.close();

        // Receive the response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
       
        //Formulate the response to Game Create Req Handler
        String inputLine;
        StringBuffer response = new StringBuffer();           
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());
        return response.toString();
        
    } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
		return null;
	}


	public static String HttpPostGameJoin(String servletUrl, String reqGameJoin) {
		// TODO Auto-generated method stub
		try {
		System.out.println("  /////////////// Inside HttpPost GameJoin ///////////////");
    	
    	//Formulate the URL for Game Join
        String geturl = GAMESERVER_URL + "/" + servletUrl;
        String postData = reqGameJoin;
       
        //Set connection
        URL obj = new URL(geturl);       
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
       
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());             
        wr.writeBytes(postData);
        wr.flush();
        wr.close();

        // Receive the response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
       
        //Formulate the response to Game Join Req Handler
        String inputLine;
        StringBuffer response = new StringBuffer();           
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());
        return response.toString();
        
    } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
		return null;
	}
	
	
	public static String HttpPostListActiveGames(String servletUrl, String reqListActiveGames) {
		// TODO Auto-generated method stub
		try {
		System.out.println("  /////////////// Inside HttpPost Active Games ///////////////");
    	
    	//Formulate the URL for Active Games Listing
        String geturl = GAMESERVER_URL + "/" + servletUrl;
        String postData = reqListActiveGames;
       
        //Set connection
        URL obj = new URL(geturl);       
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Content-Length", String.valueOf(postData.length()));
       
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());             
        wr.writeBytes(postData);
        wr.flush();
        wr.close();

        // Receive the response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
       
        //Formulate the response to Game Join Req Handler
        String inputLine;
        StringBuffer response = new StringBuffer();           
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());
        return response.toString();
        
    } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
		return null;
	}
	
}


