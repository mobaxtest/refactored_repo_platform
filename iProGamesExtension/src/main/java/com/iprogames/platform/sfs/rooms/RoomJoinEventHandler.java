/**
 * 
 */
package com.iprogames.platform.sfs.rooms;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

/**
 * @author Admin
 *
 */
public class RoomJoinEventHandler extends BaseServerEventHandler {

	/* (non-Javadoc)
	 * @see com.smartfoxserver.v2.extensions.IServerEventHandler#handleServerEvent(com.smartfoxserver.v2.core.ISFSEvent)
	 */
	public void handleServerEvent(ISFSEvent roomJoinEvent) throws SFSException {
		// TODO Auto-generated method stub
		
		String userJoined = (String) roomJoinEvent.getParameter(SFSEventParam.USER);
		trace(" @@ User: " + userJoined + " just joined the room: " + SFSEventParam.ROOM.toString() + " @@ ");
		
		

	}

}
