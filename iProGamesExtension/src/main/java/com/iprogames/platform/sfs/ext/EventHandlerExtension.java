package com.iprogames.platform.sfs.ext;

import com.iprogames.platform.sfs.games.GameCreateReqtHandler;
import com.iprogames.platform.sfs.games.GameJoinReqtHandler;
import com.iprogames.platform.sfs.games.GameSearchReqtHandler;
import com.iprogames.platform.sfs.games.GameSelectReqtHandler;
import com.iprogames.platform.sfs.games.ListActiveGamesReqtHandler;
import com.iprogames.platform.sfs.login.LoginEventHandler;
import com.iprogames.platform.sfs.login.LogoutEventHandler;
import com.iprogames.platform.sfs.rooms.RoomAddEventHandler;
import com.iprogames.platform.sfs.rooms.RoomJoinEventHandler;
import com.iprogames.platform.sfs.rooms.RoomLeaveEventHandler;
import com.iprogames.platform.sfs.rooms.RoomRemoveEventHandler;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class EventHandlerExtension extends SFSExtension {	
	
	/* This will be called when the extension is deployed */
	public void init() {
		trace("Extension Initialised.");
		//Create or Recreate Lobby here
		//Lobby/Room Data persistence handlers here
		
		/* This will be fired while user log in */
		addEventHandler(SFSEventType.USER_LOGIN, LoginEventHandler.class);
		
		/* This will be fired while user logs out */
		//addEventHandler(SFSEventType.USER_LOGOUT, LogoutEventHandler.class);
		
		/* This event will be fired when a user joins a room */
		//addEventHandler(SFSEventType.USER_JOIN_ROOM, RoomJoinEventHandler.class);
		
		/* This event will be fired when a user joins a room */
		//addEventHandler(SFSEventType.USER_LEAVE_ROOM, RoomLeaveEventHandler.class);
		
		/* This event will be fired when a new room is added to the Zone */
		//addEventHandler(SFSEventType.ROOM_ADDED, RoomAddEventHandler.class);
		
		/* This event will be fired when a room is removed from the Zone */
		//addEventHandler(SFSEventType.ROOM_REMOVED, RoomRemoveEventHandler.class);
		
			
		
		//Game Handlers
		addRequestHandler("GameSelect", GameSelectReqtHandler.class);
		addRequestHandler("GameSearch", GameSearchReqtHandler.class);
		addRequestHandler("GameCreate", GameCreateReqtHandler.class);
		addRequestHandler("GameJoin", GameJoinReqtHandler.class);
		
		addRequestHandler("ListActiveGames", ListActiveGamesReqtHandler.class);
		
		
	}
	
	@Override
	public void destroy()
	{
	    super.destroy();
	     
	    /*
	    * More code here...
	    */
	}   

}
