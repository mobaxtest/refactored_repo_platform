package com.iprogames.platform.sfs.games;

import com.iprogames.platform.sfs.ext.EventHandlerExtension;
import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GameSearchReqtHandler extends BaseClientRequestHandler {

	public void handleClientRequest(User defaultUser, ISFSObject params) {
		//Fetch the Games Server URL for Games related transactions
		//Properties ConfigProperties = getParentExtension().getConfigProperties();
		//String GAMES_SERVER_URL = ConfigProperties.getProperty("games-url");
		//trace(GAMES_SERVER_URL); 
		
		trace("  -<<<<<<<<<<<<<<-  Inside Game Search Request Handler  ->>>>>>>>>>>>>-  ");
				
		//Set the game listing servlet URL path
		String servletUrl = "game/usergame/search";
		//trace("  --  GameSearchRequest: ", params.toString(), "  --  ");
		trace("  -<<<<<<<<<<<<<<-  GameSearchRequest: ", params.getUtfString("GameSearchReq"), "  ->>>>>>>>>>>>>-  ");
		
		//Fetch the games search list
		
		//** Use static URL for Games server **//
			//String response = HttpClientHandler.HttpPostGameSearch(servletUrl, params.toString());
			String response = HttpClientHandler.HttpPostGameSearch(servletUrl, params.getUtfString("GameSearchReq"));
	
		//**Use URL from config file **//
			//String response = HttpClientHandler.HttpPostGameSearch(GAMES_SERVER_URL, servletUrl, params.toString());
	
		trace("  -<<<<<<<<<<<<<<<<-  GameSearchResponse: ", response, "  ->>>>>>>>>>>>>>>>-  ");
		
		//Formulate and send the response to client		
       	ISFSObject outData = new SFSObject();
		outData.putUtfString("response", response);
     
		EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();
		parentEx.send("GameSearch", outData, defaultUser);

	}

}
