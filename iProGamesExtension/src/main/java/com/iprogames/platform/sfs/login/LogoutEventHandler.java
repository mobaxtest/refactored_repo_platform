/**
 * 
 */
package com.iprogames.platform.sfs.login;

import java.util.Properties;

import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

/**
 * @author Admin
 *
 */
public class LogoutEventHandler extends BaseServerEventHandler {

	/* (non-Javadoc)
	 * @see com.smartfoxserver.v2.extensions.IServerEventHandler#handleServerEvent(com.smartfoxserver.v2.core.ISFSEvent)
	 */
	public void handleServerEvent(ISFSEvent event) throws SFSException {
		// TODO Auto-generated method stub
		
		//Fetch the User Management Server URL for Authentication verification
				Properties ConfigProperties = getParentExtension().getConfigProperties();
				String HTTP_AUTH_URL = ConfigProperties.getProperty("url");
				trace(HTTP_AUTH_URL);
				
				//Get the authentication token from the Login request 
				String authToken = (String) event.getParameter(SFSEventParam.LOGIN_NAME);
				trace("  **  Logout Token: ", authToken, "  **  ");
				
				//Set the authentication verification and lobby details servlet URL path
				String servletUrl = "logout";
				trace("  **  Logout servletUrl: ", servletUrl, "  **  ");
				
				//Do the verification 
					//** Use static URL for User management server **//
				String response = HttpClientHandler.HttpPost(servletUrl, authToken);
				
					//**Use URL from config file **//
				//String response = HttpClientHandler.HttpPost(HTTP_AUTH_URL, servletUrl, authToken);
				
				//trace(response);
				trace("  **  AuthVerificationResponse: ", response, "  * ");
				
				//Formulate and send the response to client
				ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
				outData.putUtfString("logout-response", response);
		

	}

}
