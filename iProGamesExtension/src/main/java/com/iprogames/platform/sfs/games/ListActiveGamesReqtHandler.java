/**
 * 
 */
package com.iprogames.platform.sfs.games;

import com.iprogames.platform.sfs.ext.EventHandlerExtension;
import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * @author Admin
 *
 */
public class ListActiveGamesReqtHandler extends BaseClientRequestHandler {

	/* (non-Javadoc)
	 * @see com.smartfoxserver.v2.extensions.IClientRequestHandler#handleClientRequest(com.smartfoxserver.v2.entities.User, com.smartfoxserver.v2.entities.data.ISFSObject)
	 */
	public void handleClientRequest(User defaultUser, ISFSObject params) {
		// TODO Auto-generated method stub
		
		
		//Fetch the Games Server URL for Games related transactions
		
		//Properties ConfigProperties = getParentExtension().getConfigProperties();
		//String GAMES_SERVER_URL = ConfigProperties.getProperty("games-url");
		//trace(GAMES_SERVER_URL); 
		
		trace("  -$$$$$$$$$$$$$$$$-  Inside  List Active Games Request Handler  -$$$$$$$$$$$$$$$$-  ");
				
		//Set the active game listing servlet URL path
		String servletUrl = "game/player/activegames";
		//trace("  --  ListActiveGamesReq: ", params.toString(), "  --  ");
		trace("  -$$$$$$$$$$$$$$$$-  ListActiveGamesReq: ", params.getUtfString("ListActiveGamesReq"), "  -$$$$$$$$$$$$$$$$-  ");
		
		//Fetch the Active games list
		
		//** Use static URL for Games server **//
			//String response = HttpClientHandler.HttpPostListActiveGames(servletUrl, params.toString());
			String response = HttpClientHandler.HttpPostListActiveGames(servletUrl, params.getUtfString("ListActiveGamesReq"));
	
		//**Use URL from config file **//
			//String response = HttpClientHandler.HttpPostListActiveGames(GAMES_SERVER_URL, servletUrl, params.toString());
	
		trace("  -$$$$$$$$$$$$$$$$-  ListActiveGamesResponse: ", response, "  -$$$$$$$$$$$$$$$$-  ");
		
		//Formulate and send the response to client		
       	ISFSObject outData = new SFSObject();
		outData.putUtfString("response", response);
     
		EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();
		parentEx.send("ListActiveGames", outData, defaultUser);
	}

}
