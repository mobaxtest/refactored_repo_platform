package com.iprogames.platform.sfs.games;

import java.util.ArrayList;

import com.iprogames.platform.sfs.ext.EventHandlerExtension;
import com.iprogames.platform.sfs.util.HttpClientHandler;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.ISFSApi;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GameCreateReqtHandler extends BaseClientRequestHandler {

	public void handleClientRequest(User defaultUser, ISFSObject params) {
		// TODO Auto-generated method stub
		//Fetch the Games Server URL for Games related transactions
				//Properties ConfigProperties = getParentExtension().getConfigProperties();
				//String GAMES_SERVER_URL = ConfigProperties.getProperty("games-url");
				//trace(GAMES_SERVER_URL); 
				
				trace("  -############-  Inside Game Create Request Handler  -################-  ");
						
				//Set the game listing servlet URL path
				String servletUrl = "game/usergame/create";
				//trace("  --  GameCreateRequest: ", params.toString(), "  --  ");
				trace("  -############-  GameCreateRequest: ", params.getUtfString("GameCreateReq"), "  -#############-  ");
				
				//Create the User Game
				
				
				trace("Params => :" +params.getDump());
				
				
				//** Use static URL for Games server **//
					//String response = HttpClientHandler.HttpPostGameCreate(servletUrl, params.toString());
					String response = HttpClientHandler.HttpPostGameCreate(servletUrl, params.getUtfString("GameCreateReq"));
			
				//**Use URL from config file **//
					//String response = HttpClientHandler.HttpPostGameCreate(GAMES_SERVER_URL, servletUrl, params.toString());
			
				trace("  -############-  GameCreateResponse: ", response, "  -#############-  ");
				
//				//Create Game Room
//				try {
//					createUserGameRoom(response);
//				} catch (SFSCreateRoomException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				
				//Formulate and send the response to client		
		       	ISFSObject outData = new SFSObject();
				outData.putUtfString("response", response);
		     
				EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();
				parentEx.send("GameCreate", outData, defaultUser);
				
	}

	private void createUserGameRoom(String response) throws SFSCreateRoomException 
	{
		// TODO Auto-generated method stub
		
		trace("  -?????????????????-  Room Create -?????????????????-  ");
		CreateRoomSettings settings = new CreateRoomSettings();
		
		//{"userGame":{"userGameId":1,"eventId":1421764,"rebuyCount":3,"noOfSeat":1,"picksCount":10,"chips":12000.0,"gameMode":"PUBLIC","gameType":"Head To Head"}}
		ISFSObject createUserGameRes = new SFSObject();
		//JSONObject resJSON = new JSONObject().fromObject(response);
		
	
	
		ArrayList<RoomVariable> roomVariables = new ArrayList<RoomVariable>();
		
		
		roomVariables.add(new SFSRoomVariable("eventId", 1421764));
		//roomVariables.add(new SFSRoomVariable("eventId", resJSON.get("userGameId")));
		roomVariables.add(new SFSRoomVariable("rebuyCount", 3));
		roomVariables.add(new SFSRoomVariable("noOfSeat", 5));
		roomVariables.add(new SFSRoomVariable("picksCount", 10));
		roomVariables.add(new SFSRoomVariable("chips", 12000.0));
		roomVariables.add(new SFSRoomVariable("gameMode", "PUBLIC"));
		roomVariables.add(new SFSRoomVariable("gameType", "Head To Head"));
		
		
	    settings.setName("UserGameNamehere");
	    settings.setMaxUsers(20);
	    settings.setMaxUsers(10);
	    settings.setRoomVariables(roomVariables);
    
	    EventHandlerExtension parentEx = (EventHandlerExtension) getParentExtension();	    
	    Room newRoom = getApi().createRoom(parentEx.getParentZone(), settings, null);
	    
	    trace("  -?????????????????-  Room Created: " +  newRoom.toString() + "  -?????????????????-  ");
	}

}
